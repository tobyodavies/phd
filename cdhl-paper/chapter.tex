\chapter{Conflict-Directed Heuristic Learning}
\label{chp:cdhl}

\begin{ch-abstract}
In this chapter we introduce Conflict-Directed Heuristic Learning,
which incrementally learns a near-perfect heuristic using an IDA*-like
depth-first state-space search.
\end{ch-abstract}

\section{Introduction}

There have been a number of recent algorithms developed which learn
from conflicts encountered during search, notably PDR \cite{suda2014} and
DFS-CL\cite{steinmetz2016}.  Neither of these algorithms find
cost-optimal plans, although PDR can be configured to produce minimum length plans.

These two algorithms differ slightly in the nature of conflicts they
detect and can learn to avoid.
Each search node in PDR (also called a ``proof obligation'') consists of a
state in the state space and a count of actions.

Conflicts in PDR are detected when the goal is not achievable from the
state in at most the specified number of actions.
From such a conflict, PDR learns a clause and action count,
blocking a set of states which cannot reach the goal in the specified number of actions.
In contrast, search nodes in DFS-CL do not include any count of future actions
and thus recognise conflict only when a state cannot reach the goal at all.

It appears at first glance that PDR could be trivially generalised
by replacing the limit on the number of actions with a limit on the total action cost.
However, PDR terminates when the clauses representing the set of states which
cannot reach the goal are the same in two adjacent layers.
This recognises a fixed point which occurs when adding actions is not
increasing the reachable set of states.
It is not obvious how to generalise this termination condition to the cost-optimal case.

The idea of limiting the total cost of actions used to reach the goal
is however the same fundamental idea behind the IDA* algorithm
\cite{korf1985}.  In this case conflicts are inaccuracies in the
heuristic function identified when the minimal $f$ value of a state's
successors is greater than the $f$-value of that state.  IDA* can be
augmented with a so-called ``transposition table'', which stores improved
heuristic estimates for previously expanded states \cite{reinefeld1994}.

IDA* with a transposition table eventually learns the perfect heuristic
for reachable states on the optimal path, and despite not generalising
this knowledge, the transposition table can still be used to avoid an
exponential amount of work because the same state can occur on an
exponential number of paths.

This chapter introduces ``Conflict-Directed Heuristic Learning'' (CDHL), which 
generalises IDA* by using regression to
analyse why a state's heuristic value is too low and learning a
clause that generalises to many states which may be seen in the future.


\begin{figure}
  \centering
  \pcodeN{
    \textsf{IDAStarTT}($h, s, h_{max}$) \\ 
    \> \textbf{if}(Goal($s$)) \\
    \> \> \textbf{return} $h$ \\
    \> \textbf{while}($h(s) \le h_{max}$) \\
    \> \> $o$ := $\argmin_o(h(s[o]) + c(o) | o \in O)$ \\
    \> \> \textbf{if}($h(s[o]) + c(o) > h(s)$) \\
    \> \> \> $h$ := UpdateH($h, s, h(s[o]) + c(o)$) \\
    \> \> \textbf{else} \\
    \> \> \> $h$ := IDAStar($h, s[o], h(s) - c(o)$) \\
    \> \> \> \textbf{if}($h(s[o]) + c(o) \le h_{max}$) \\
    \> \> \> \> \textbf{return} $h$ \\
    \> \textbf{return} $h$ \\
  }
  \caption{\label{cdhl:idastar-code}
    Pseudo-code for learning IDA* (including IDA* with a transposition table and CDHL)
  }
\end{figure}


\section{Preliminaries}

\paragraph{\SAS{} planning}
A \SAS{} planning task is a tuple $\tup{V, O, s_0, s_*, c}$ where $V$ is a
set of finite domain state variables, $O$ is a set of operators, $s_0$ is a full assignment of each variable to one of its values representing the initial state,
and $s_*$ is a partial assignment of some subset of $V$ representing the goal states.
Finally $c$ is a function $O \rightarrow \mathbb{N^+_0}$ that assigns a non-negative cost to each operator.

Each variable $X \in V$ has a domain $D(X)$,
we sometimes abuse notation and write $\Fact{X}{x} \in V$ which should be read
$X \in V \land x \in D(X)$.
Each operator $o$ has a set of preconditions $pre(o)$ which is a partial assignment representing the preconditions of that operator,
and a set of postconditions $post(o)$ which is a partial assignment representing the effects of the operator.
Producers, $prod(\Fact{X}{x}) = \{o \mid \Fact{X}{x'} \in pre(o) \land \Fact{X}{x} \in post(o) \land x' \neq x \}$
are the operators which cause $\Fact{X}{x}$ to become true.  
Note that for simplicity, we do not distinguish between preconditions and prevail conditions in this chapter.

A state $s$ in the search space is a full assignment of every variable to a value.
State $s$ is said to satisfy a partial assignment $F$ if all
assignments in $F$ are also in $s$, i.e.
$\Fact{X}{x} \in F \Rightarrow \Fact{X}{x} \in s$.
A state is said to be a goal state if it satisfies the partial assignment $s_*$.

An operator $o \in O$ is applicable in $s$ if
$s$ satisfies the partial assignment $pre(o)$.
If $o$ is applicable in state $s$, applying $o$ yields a new state $s[o]$ which is the same as $s$ except
that all assignments $\Fact{X}{x} \in post(o)$ replace any assignment to $X$.

A plan $\pi$ is a sequence of operators $o_1, \cdots o_n$ such that
$o_1$ is applicable in $s_0$, each subsequent operator is applicable
in the state resulting from applying the previous operators in
sequence, and the final state, $s_0[o_1, \cdots, o_n]$, satisfies $s_*$.
An optimal plan has the minimum sum of operator costs of all plans, a
\SAS{} planning task may have many optimal plans.


\paragraph{STRIPS planning} is an equivalent formalism to \SAS{} which represents a planning problem as a tuple
$\tup{F,O,I,G}$, where $F$ is the set of facts that can become true in
some state in the state space, $I$ and $G$ are subsets of $F$
representing the initial and goal states respectively.  While the
initial state is fully specified, any state $s$ such that $s \supseteq
G$ is a goal state.

The set of operators, $O$, is defined by four functions, $pre(o)$,
$add(o)$ and $del(o)$, each of which return a subset of $F$
representing, respectively: the minimum set of facts that must be true
to apply $o$; the facts that are added to the state after $o$ was
applied; and the facts deleted from the state after $o$ was applied.

From this definition it follows that any state having strictly more
facts than another cannot be further from the goal.  Indeed, in STRIPS
planning especially, it is what is \textbf{not} true in a state that
defines its heuristic value.
The ``Clausal Heuristics'' we will define in later sections of this chapter exploit this observation.

\SAS{} problems can be transformed fairly straightforwardly into STRIPS problems
by defining $F=\{[X=x] | X \in V, x \in D(V)\}$.
We also exploit the isomorphism between subsets of finite sets and assignments to sets of boolean variables
to model STRIPS states as partial models of boolean formulae which are then extended or refuted by a satisfiability problem solver.

\begin{figure}[t]
\begin{center}
\begin{tikzpicture}%
  [<-,place/.style={circle,draw=black}]
  \node (VarR) at (0,3) {$R:$} ;
  \node (L) at (1,3) [place, double] {$l$} ;
  \node (R) at (5,3)   [place,double] {$r$} ;
  \path (VarR) edge[->] (L) ;
  \path (L) edge[-triangle 45, bend left] node[below] {\small \Move{r}} (R) ;
  \path (R) edge[-triangle 45, bend left] node[above] {\small \Move{l}} (L) ;
  \node (VarB) at (0,1.5) {$B_i:$} ;
  \node (XL) at (1,1.5)   [place] {$l$} ;
  \node (XG) at (3,1.5)   [place] {$g$} ;
  \node (XR) at (5,1.5)   [place,double] {$r$} ;
  \path (VarB) edge[->] (XL) ; 
  \path (XL) edge[-triangle 45, bend left] node[above] {\small \Grip{i}{l}} (XG) ;
  \path (XG) edge[-triangle 45, bend left] node[above] {\small \Drop{i}{r}} (XR) ;
  \path (XR) edge[-triangle 45, bend left] node[below] {\small \Grip{i}{r}} (XG) ;
  \path (XG) edge[-triangle 45, bend left] node[below] {\small \Drop{i}{l}} (XL) ;

  \node (VarG) at (0,0) {$G:$} ;
  \node (E) at (1, 0) [place,double] {$e$} ;
  \node (N) at (5, 0) [place,double] {$n$} ;
  \path (VarG) edge[->] (E) ; 
  \path (E) edge[-triangle 45, bend left] node[below] {\small \Grip{*}{*}} (N) ;
  \path (N) edge[-triangle 45, bend left] node[above] {\small \Drop{*}{*}} (E) ;
\end{tikzpicture}
\end{center}
    \caption{Domain Transition Graphs in gripper\label{orsc:gripper}.
      %% The variable $R$ represents the location of the robot (in the left or right room) ;
      %% $B_i$ represents the location of ball $i$, (in the left or right room, or in the gripper);
      %% $G$ represents the state of the gripper (empty or non-empty).
      %% For each automaton, the initial state is marked by an incoming arrow, and the states consistent with the goal $s_*$ are double circled.
}
\end{figure}

\paragraph{The satisfiability problem} is the NP-complete problem of trying to find an assignment to a set of boolean variables such that the assignment is consistent with an arbitrary boolean formula.
Throughout this chapter we will assume that any formula is in conjunctive normal form (CNF): meaning the formula is a conjunction of disjunctions (clauses).

Note that a formula of the form
\[
p_1 \land \cdots \land p_n \Rightarrow q_1 \lor \cdots \lor q_n
\]
is equivalent to the clause:
\[
\lnot p_1 \lor \cdots \lor \lnot p_n \lor q_1 \lor \cdots \lor q_n
\]
by transformation using De Morgan's laws.

%% Complete satisfiability solvers are typically based on the conflict-directed clause learning (CDCL) approach...
%% TODO

\paragraph{Bounds literals}
are boolean literals of the form $[i \ge k]$ (encoding the fact that
the variable $i$ takes a value no less than the constant $k$).  Bounds
literals can encode integers as a set of boolean variables linear in
the integer's range.  In this chapter all integer variables encoded
this way have the property that assigning a larger value allows a
strictly larger set of models of the formula. Consequently we only
ever care about the lower bound of integer variables, and the set of
bounds literals can be theoretically infinite in size, and we merely
instantiate the ones that participate in some constraint.  We can then
safely assign the variable the smallest $k$ such that the formula is
not provably unsatisfiable.


%\paragraph{Abstractions}

\paragraph{Potential heuristics}
 map each atomic fact in a domain to a (possibly
negative) cost which can be summed to find an admissible heuristic
estimate for the state:
\[ h(s) = \sum\limits_{f \in s} w(f) \]
In our gripper example where actions all have a cost of 1, an
admissible potential heuristic could have $w([B_i = l]) = 2$, and
$w([B_i = h]) = 1$, and all other weights are zero.
\label{cdhl:gripper-pot}


\section{Reduced-cost}

In order to explain the intuition behind the introduced algorithm and to
facilitate proofs we introduce the notion of reduced cost of operators.
Reduced-cost is closely related to consistency, and measures the
increase in $f$ value caused by applying an operator in a specific
state.

Using our example potential heuristic from section \ref{cdhl:gripper-pot},
the ``move-r'' action would have a reduced-cost of 1 in the initial
state as applying this action does not reduce the heuristic value.
However ``grip-1-l'' and ``grip-2-l'' would both have reduced costs of zero
because they decrease the heuristic value by the same amount as their cost.

\begin{definition}[Reduced cost]
The reduced cost of an operator $o$ in state $s$ with respect to a heuristic $h$ is defined to be:
\begin{align*}
\gamma(h, s, o) =
\begin{cases}
 h(s[o]) + c(o) - h(s) &\text{if } pre(o) \subseteq s \\
\infty                 &\text{otherwise}
\end{cases}
\end{align*}
\end{definition}
From this definition we can see that if $h$ is consistent, no operator
will have negative reduced cost in any state.

Iterations of IDA* with a consistent heuristic can be seen as a search
for a solution considering only operators having zero reduced
cost.\footnote{Any admissible heuristic can be made consistent by maximising over that state's $f$-value and that of its parent.}
Subsequent iterations then use a new
heuristic with an increased value for $h(s_0)$.
IDA* is often enhanced with a so-called transposition table, in which case it learns 
a new heuristic with an increased value for each state it backtracks over.

In the pseudo-code from figure \ref{cdhl:idastar-code}, this is equivalent to 
$\text{UpdateH}(h, s, v)$ returning $h'$ where:
\begin{align*}
h'(s') =
\begin{cases}
  v    & \text{if } s' = s \\
  h(s) & \text{otherwise}
\end{cases}
\end{align*}

%\[ \delta(h, s) = min(\gamma(h, s, o) | o \in \text{applicable-operators}) \]

If we have some insight into the algebraic structure of $h$, it may be
possible to explain why a state has no zero-reduced-cost operators and
encode this in a new heuristic in such a way that it will not only
increase the value for the state being pruned, but many future states.

To this end we introduce a new class of heuristics, ``Clausal
heuristics'', which are highly amenable to explanation, and
incrementally learning new features.
This clausal representation is not necessarily the optimal one for
learning succinct, accurate heuristics, but is possibly the simplest
for proving why the technique works and exploring the relationship
with related learning techniques in SAT and CP.
We briefly discuss alternatives in section \ref{cdhl:future-work}.


\section{Clausal heuristics}

In our gripper example, the following clauses are obviously true of all states:
\[
\lnot [B_1 = r] \Rightarrow [h \ge 1] \\
\land \\
\lnot [B_2 = r] \Rightarrow [h \ge 1]
\]
%
Finding the minimum value of $h$ consistent with this particular SAT
formula for a state assigns a cost-estimate of one to all non-goal
states, and zero to any goal.
However more complex formulae can encode more powerful heuristics.

For example, if a ball is neither in the right room, nor in the
robot's hand, we know we must be at least 3 moves from achieving that
goal: the robot must pick-up the ball; move into the right room; and
finally drop it. This could be added to a clausal heuristic as the clause:
\[
\lnot [B_i = r] \land \lnot [B_i = g] \Rightarrow [h \ge 3]
\]

The focus of this section is on how to automatically derive such clauses from the conflicts encountered in a IDAStarTT-like search.

\begin{definition}[Simple clausal heuristics]
A simple clausal heuristic consist of a conjunction of boolean clauses, $C$ each of the form:
 \begin{align*}
&\lnot f_x \land \cdots \land \lnot f_z \Rightarrow [h \ge k_i] \\
%&\equiv \\
%&f_x \lor \cdots \lor f_z \lor [h \ge w_i] 
 \end{align*}
 where each $f_i$ is a fluent in the state space, and $k_i$ is some constant.

$h$ can be evaluated in a state $s$ as 
\[
h(s) = \text{min}(k | C \land \lnot [h \ge k+1] \land \bigwedge\limits_{f \in F \setminus s} \lnot f)
\]
\end{definition}

An important property of a clausal heuristic is that only one clause
in $C$ is actually ever necessary to obtain the same heuristic
estimate for any particular state.  We refer to that clause as the
\textit{explanation} for the bound.
\[
e_h(s) = \text{oneof}({c | c \in C \land \text{UNSAT}(\lnot [h \ge h(s)-1] \land c \land \bigwedge\limits_{f \in F \setminus s} \lnot f)})
\]
any deterministic function can be used for ``oneof''.

Because of this, evaluating a clausal heuristic for a state is at
worst polynomial in the number of clauses, even though we are using a
satisfiability solver. This is because the solver does not need to
make any decisions (except for the value of $h$) as they are all fixed
by the state being evaluated.

A clausal heuristic can be used to construct an abstract state space by mapping each state to its explanation.
An operator $o$ is applicable in an abstract state
$\lnot f_1 \land \cdots \lnot f_n \Rightarrow [h \ge p]$ if $pre(o) \cap \{f_1 \cdots f_n\} = \emptyset$.
This is a useful way to understand and visualise the updates made to heuristics.


\subsection{Integrating successor-generation}

In our IDAStarTT pseudo-code in figure \ref{cdhl:idastar-code},
we choose to apply an operator with minimal reduced cost.
A naive implementation of this would need to evaluate the heuristic
many times, and a lot of these evaluations would be redundant.

%In our gripper example... TODO: some action we can prune without re-calculating the heuristic

To simplify the integration of the successor generator, we define some
arbitrary strict total ordering $\prec$ on operators, and enhance our
heuristic to also return the minimum operator with zero reduced-cost.%
\footnote{In our experiments we sort first by maximum cost, then disambiguate arbitrarily.
This causes the depth-first search to expand children with smaller $h$-values first.}

Clauses in our heuristics can now take the form:
\[
\lnot f_x \land \cdots \land \lnot f_z \Rightarrow [h \ge w_i] \lor [o \succeq x_i]
\]
remembering that $[o \succeq ||O||+1] \equiv \text{False}$, and $[o \succeq min(O)] \equiv \text{True}$.

From our combined heuristic and successor-generator, the next action to attempt in a state $s$, denoted $\text{succ}(s)$,
is defined as:
\[
\text{succ}(s) =  \text{min}(q | q \in O \land C \land \lnot [h \ge h(s)+1] \land [o \succeq q] \land \bigwedge\limits_{f \in F \setminus s} \lnot f)
\]

The principal advantage of defining a total order on actions is that
it enables a single additional clause per state on the stack to prune
all of the paths that have already been explored.  This makes
incrementally improving the heuristic-estimate of a state
significantly easier, and simplifies garbage collection of unhelpful
clauses without sacrificing completeness.

This single explanation is:
\begin{align*}
e_{succ}(s) =& \argmin_c(\{ q | \\
            & \qquad  q \in O \land c \in C \land \\
            & \qquad  \text{UNSAT}(\lnot [o \succeq q] \land
                                  \lnot [h \ge h(s)+1] \land 
                                  c \land 
                                  \bigwedge\limits_{f \in F \setminus s} \lnot f) \})
\end{align*}


\subsection{Incrementally refining clausal heuristics}
\label{cdhl:improve}

The key idea in refining these heuristics is to prune each possible
successor in a generalisable way by identifying a set of states where
applying that action will always lead to the same clause applying.


Consider our gripper example.
A simple total order would be lexicographical in the name of the operators.
If we start with the blind heuristic figure \ref{cdhl:improve-fig}
shows several iterations of improvements to that heuristic. Visualised
by transforming the heuristic into an abstraction.

\begin{figure}
\centering

\begin{tikzpicture}[scale=1.5,<-,place/.style={circle,draw=black,minimum size=2em}]
  \node (I) at (2.35,1.5) {};
  \node (T) at (4,1) [place,double] {$\top$};
  \node (A) at (3,1.5) [place] {$a$};
  \node (B) at (3,0.5) [place] {$b$};
  \path (I) edge[->] (A);
  \path (A) edge[->] (T);
  \path (B) edge[->] (T);
\end{tikzpicture}
\vspace{.5pt}
\begin{tikzpicture}[scale=1.5,<-,place/.style={circle,draw=black,minimum size=2em}]
  \node (I) at (1.35,2) {};
  \node (T) at (4,1) [place,double] {$\top$};
  \node (A) at (3,1.5) [place] {$a$};
  \node (B) at (3,0.5) [place] {$b$};
  \node (C) at (2,2) [place] {$c$};
  \path (I) edge[->] (C);
  \path (C) edge[->] (A);
  \path (A) edge[->] (T);
  \path (B) edge[->] (T);
\end{tikzpicture}
\vspace{.5pt}
\begin{tikzpicture}[scale=1.5,<-,place/.style={circle,draw=black,minimum size=2em}]
  \node (I) at (0.35,1) {};
  \node (T) at (4,1) [place,double] {$\top$};
  \node (A) at (3,1.5) [place] {$a$};
  \node (B) at (3,0.5) [place] {$b$};
  \node (C) at (2,2) [place] {$c$};
  \node (D) at (2,1) [place] {$d$};
  \node (E) at (1,1) [place] {$e$};
  \path (I) edge[->] (E);
  \path (E) edge[->] (D);
  \path (C) edge[->] (A);
  \path (C) edge[->] (D);
  \path (D) edge[->] (A);
  \path (A) edge[->] (T);
  \path (B) edge[->] (T);
\end{tikzpicture}

\begin{tabular}{ r | c | l }
Iteration & Node & Clause \\
\hline
0 & a & $\lnot [B_1 = r] \Rightarrow [h \ge 1]$ \\
0 & b & $\lnot [B_2 = r] \Rightarrow [h \ge 1]$ \\
1 & c & $\lnot [B_1 = r] \land \lnot [B_1 = g] \Rightarrow [h \ge 2]$ \\
2 & d & $\lnot [B_1 = r] \land \lnot [R = r] \Rightarrow [h \ge 2]$ \\
2 & e & $\lnot [B_1 = r] \land \lnot [B_1 = g] \land \lnot [R = r] \Rightarrow [h \ge 3]$ \\
\end{tabular}

\caption{Improving a clausal heuristic for gripper \label{cdhl:improve-fig}}
\end{figure}

Improving a clausal heuristic is possible exactly when either:
\begin{itemize}
\item $\text{succ}(s)$ operator is not applicable in $s$; or
\item $\text{succ}(s)$ has positive reduced cost in $s$.
\end{itemize}

%% \begin{figure}
%% \CDHLegA{}
%% \CDHLegB{}
%% \caption{Improving an abstraction\label{cdhl:improve-fig}}
%% \end{figure}

To achieve this update several steps occur:
Initially the  clausal heuristic consists of the two clauses
encoding the blind heuristic:
\[
\lnot [B_1 = r] \Rightarrow [h \ge 1] \\
\land \\
\lnot [B_2 = r] \Rightarrow [h \ge 1] \\
\]
This heuristic can be visualised as the first automaton if figure \ref{cdhl:improve-fig}, corresponding to iteration 0.
Unfortunately edges in these automata cannot necessarily be mapped
directly to operators, but should be read as ``there exists an
operator that connects these two abstract states''..

IDAStarTT evaluates the heuristic value for the initial state as $1$, and
arbitrarily picks the first of the above clauses as the explanation.
It then attempts to compute the minimum reduced cost operator by trying the operators in order.
The lexicographically minimal operator is ``drop-1-l'', which is both
inapplicable in the initial state, and has a positive reduced cost in any state because the same explanation clause
would apply in the state after applying the operator.

We can add either
\[
\lnot [B_1 = r] \land \lnot [B_1 = g] \Rightarrow [h \ge \infty] \lor [o \succeq \textit{drop-1-r}]
\]
or
\[
\lnot [B_1 = r] \land \lnot [R = r] \Rightarrow [h \ge \infty] \lor [o \succeq \textit{drop-1-r}]
\]
or
\[
\lnot [B_1 = r] \Rightarrow [h \ge 2] \lor [o \succeq \textit{drop-1-r}]
\]
to the clausal heuristic to progress. Our implementation would choose
the first of these, but this choice is arbitrary.

The first two clauses are correct because all actions lexicographically less
than ``drop-1-r'' cannot be applied in any state where the left hand
side of the implication holds. In general we can choose any
precondition of the action being blocked to add to the left hand side
of the implication.
The last clause holds because the heuristic estimate for any states
where ball 1 is not already in the right room is obviously not
decreased by any action less than ``drop-1-r''. Note that even though
no state's $h$-value would decrease after applying this operator, we
cannot simply say
\[
\top \Rightarrow [h \ge 2] \lor [o \succeq \textit{drop-1-r}]
\]
as the goal may already be achieved. We must keep at least one reason that the goal is not yet achieved in the explanation clause.

Having added one of these clauses, we recompute the lexicographically-minimal operator consistent with $h(s) = 1$.
Any of the above clauses makes $o = \textit{drop-1-l}$ inconsistent with $h = 1$,
however we will assume the first of these clauses is added.
The next smallest operator is ``drop-1-r'', which again is inapplicable.
\[
\lnot [B_1 = r] \land \lnot [B_1 = g] \Rightarrow [h \ge \infty] \lor [o \succeq \textit{drop-2-l}]
\]
This clause strictly dominates the previously learned clause, meaning
that the previous clause can be deleted with no loss of information.

The next seven actions: ``drop-2-l'', ``drop-2-r'', ``grip-1-l'', ``grip-1-r'', ``grip-2-l'', ``grip-2-r'', and ``move-l-r'' all have
positive reduced cost as $[B_1 = r]$ is still not achieved.
Consequently we can learn:
\[
\lnot [B_1 = r] \land \lnot [B_1 = g] \Rightarrow [h \ge 2] \lor [o \succeq \textit{move-r-l}]
\]
Each of the four clauses we learn from trying these actions strictly
dominates its preceding clause, allowing us to delete them.

The next action, ``move-r-l'', also has positive reduced cost for the
same reason, but is interesting as it is the lexicographically last
operator, and so there is no next action so the $[o \succeq ?]$ term can be omitted as it is equivalent to false, allowing us to learn
\[
\lnot [B_1 = r] \land \lnot [B_1 = g] \Rightarrow [h \ge 2]
\]
Which is the new explanation for the heuristic-estimate of the initial state $h(s_0) = 2$.
At this point, the heuristic can be visualised as the second automaton in figure \ref{cdhl:improve-fig}, corresponding to iteration 1.

As yet we have not done any search, merely having reasoned
about the heuristic and the initial state to find properties of that
state that prevent us from finding zero-reduced-cost successors.

Having increased the heuristic estimate at the root from 1 to 2, we enter the second major iteration of IDA*.
We now attempt to compute the minimum reduced-cost action again.
We can start with ``drop-2-l'' because of the previously learned clause:
\[
\lnot [B_1 = r] \land \lnot [B_1 = g] \Rightarrow [h \ge \infty] \lor [o \succeq \textit{drop-2-l}]
\]

Neither ``drop-2-l'' nor ``drop-2-r'' add $[B_1 = r]$ or $[B_1 = g]$,
and thus cannot reduce the heuristic value of the state, so we learn:
\[
\lnot [B_1 = r] \land \lnot [R = r] \Rightarrow [h \ge 3] \lor [o \succeq \textit{grip-1-l}]
\]

After applying ``grip-1-l'' however, the $h$ value of the resulting state has
decreased by 1, meaning the operator had zero reduced cost.  We can
now recurse after applying this operator with a maximum $h$-value of 1.

We now search for a zero-reduced cost operator in this new state
starting with ``drop-1-l'', and learn that it increases the heuristic
value to 2.
\[
\lnot [B_1 = r] \land \lnot [R = r] \Rightarrow [h \ge 3] \lor [o \succeq \textit{drop-1-r}]
\]

We then find that ``drop-1-r'' is inapplicable in this state because $[R = r]$ does not hold,
and thus learn:
\[
\lnot [B_1 = r] \land \lnot [R = r] \Rightarrow [h \ge 3] \lor [o \succeq \textit{drop-2-l}]
\]

None of the remaining operators add $[B_1 = r]$ and so cannot change the heuristic value, and must have positive reduced cost.
The cost after each of these operators is however only 1, so we reduce the ``next heuristic value'' to 2 and learn:
\[
\lnot [B_1 = r]  \land \lnot [R = r] \Rightarrow [h \ge 2]
\]
This corresponds to node $d$ in figure \ref{cdhl:improve-fig}.

The recursive call of IDAStarTT returns as we have proved the goal is
not reachable with an $h$-budget of 1 after applying ``grip-1-l''.
%% We now regress this new clause through the operator taken by removing
%% from the left-hand-side any negated literals deleted by the operator;
%% increasing the minimum $h$-value by the cost of the operator; then
%% OR-ing it with the previous $e_{succ}(s)$ and incrementing $o$:
\[
\lnot [B_1 = r] \land \lnot [B_1 = g] \land \lnot [R = r] \Rightarrow [h \ge 3] \lor [o \succeq \textit{grip-1-r}]
\]

We now return to the top level of the stack and continue searching for
zero-reduced-cost operators. None of the remaining operators add
either $[B_1 = r]$ or $[B_1 = g]$ and therefore must have positive
reduced cost at the initial state, so we can learn:
\[
\lnot [B_1 = r] \land \lnot [B_1 = g] \land \lnot [R = r] \Rightarrow [h \ge 3]
\]
corresponding to node $e$ in figure \ref{cdhl:improve-fig}.
At this point, the heuristic can be visualised as the third automaton in figure \ref{cdhl:improve-fig}, corresponding to iteration 2.

This process of identifying and correcting flaws in the heuristic
continues until a sequence of zero-reduced-cost operators is found
connecting the initial and goal states.



%% Initially the clausal heuristic consists of the single clause
%% $\lnot G \Rightarrow [\textit{cost} \ge 1]$.
%% The minimum operator $o_1$ was found not to be applicable in $s$
%% because one precondition $P_1$ was not satisfied.  From this we can
%% learn a new clause: $\lnot P_1 \land \lnot G \Rightarrow
%% [\textit{cost} \ge 2] \lor [op \ge o_2]$.  $\text{succ}(s)$ is now
%% equal to $o_2$. We apply $o_2$ in $s$ and observe it does not achieve
%% the goal $g$, but instead $e_h(s[o_2])$ is the originial clause in our
%% starting database; learning that the reduced-cost of $o_2$ is 1, implying the third clause:
%% $\lnot P_1 \land \lnot G \Rightarrow [\textit{cost} \ge 2] \lor [op \ge o_3]$. However there are only 2 actions in this simple domain, thus
%% $[op \ge o_3] \equiv False$ and this clause can be simplified to $\lnot P_1 \land \lnot G \Rightarrow [\textit{cost} \ge 2]$.

Formally, we improve the heuristic by applying two rules in any order repeatedly until fixed-point:
\begin{enumerate}
\item When $\text{pre}(\text{succ}(s)) \not\subseteq s$ we learn a new clause $\phi \land p \Rightarrow [h \ge w_i] \lor [o \succeq \text{succ}(s) + 1]$ for some $p \in \text{pre}(\text{succ}(s))$
\item Alternatively when $\gamma(h, s, succ(s)) > 0$, we learn a new clause $(\phi_0 \Rightarrow [h \ge w_i] \lor [o \succeq \text{succ}(s) + 1]) \lor \mathcal{R}(e_h(s[succ(s)]), succ(s))$.
\end{enumerate}
Here $\phi_0 \Rightarrow [h \ge w_i] [o \succeq succ(s)] = e_{succ}(s)$; and
$\mathcal{R}(\phi \Rightarrow [h \ge w_i], o)$ is the regression operator, which regresses a clause $\phi$ through a single action $o$, and returns a clause representing a property of the set of states $S$ s.t. for all $\forall s' \in S: h(s'[o]) \ge w_i$.

$\mathcal{R}$ is defined as:
\[
\mathcal{R}(\phi \Rightarrow [h \ge w_i], o) = \bigwedge(\lnot f_i \lor f_i \in add(o)) \Rightarrow [h \ge w_i + cost(o)]
\]

\subsection{Integrating duplicate-detection}

In order to handle zero-cost actions we must avoid expanding duplicate states on the current path.
To do this we add one more type of variable to our clauses: $[\tup{g,d} \le \tup{y_i,z_i}]$.
Immediately before applying any zero-cost action, a clause of the form
$ \lnot f_x \land \cdots \land \lnot f_z \Rightarrow [\tup{g,d} \le \tup{g(s),d(s) + 1}] $ is added
to the heuristic, where $g$ represents the ``g-value'' of the node, and $d$ the depth of the node being expanded.

Consider a variant of our gripper example where movement actions have zero cost.
In the initial situation it would be possible to ``move-r'' then ``move-l'' and return to the same state.
We could however add the clause:
\begin{align*}
  \lnot [R=r] \land \lnot [B_1 = r] \land \lnot [B_1 = g] \land \lnot [B_2 = r] \land \lnot [B_2 = g] \land \lnot [G = n]\Rightarrow\\
  [\tup{g,d} \le \tup{0,1}] \lor [h \ge \infty]
\end{align*}
Before expanding the initial state
then when evaluating the heuristic after the cycle, the $h$-value of the node would be too high because $g=0$ and $d=3$ and IDAStarTT would backtrack.

This clause could then be regressed through the ``move-l'' operator in
much the same way as usual. The only special handling is of the
$[\tup{g,d} \le \tup{0,1}]$ term, which is handled analogously to $[h \ge k]$ terms by decrementing $g$ by the cost of the operator, and $d$ by 1.
Causing us to learn:
\begin{align*}
  \phi \land \lnot [B_1 = r] \land \lnot [B_1 = g] \land \lnot [B_2 = r]  \land \lnot [B_2 = g] \land \lnot [G = n] \Rightarrow\\
       [\tup{g,d} \le \tup{0,0}] \lor [h \ge k] \lor [o \ge \textit{move-r}]
\end{align*}
Assuming that the previous $e_{succ}(s_0[\textit{move-r}])$ was:
\[
\phi \Rightarrow [h \ge k] \lor [o \ge \textit{move-l}]
\]

Note that $[g \le \tup{0,0}] \equiv \textit{False}$, as we count depth
starting at 1. This means we have effectively learned that, to
be allowed to move right, we must first have advanced the state of one
of the balls in some way.

\begin{definition}[Clausal heuristic]
A clausal heuristic consists of a set of clauses of the general form:
\[
\lnot f_x \land \cdots \land \lnot f_z \Rightarrow [h \ge w_i] \lor [o \succeq x_i] \lor [\tup{g,d} \le \tup{y_i,z_i}]
\]
\end{definition}
Note that $[h \ge \infty] \equiv [o \succeq max(O)] \equiv [\tup{g,d} \le \tup{0,0}]
\equiv \text{False}$, so all elements on the right of the implication
are optional.

%TODO(Graph where we can learn that an unreached node is closed)


%% \begin{theorem}
%% Given sufficient memory, CDHL expands the same state at most once per f-bound considered.
%% \end{theorem}
%% If the same state is encountered, we have either 



\subsection{Garbage collection}
As briefly noted earlier, only 3 clauses per state on the stack are
necessary to guarantee completeness.  As long as $e_h(s)$,
$e_{succ}(s)$, and $e_g(s)$ are retained for all $s$ on the current
path, all other clauses can be removed, and CDHL will not explore the
same path with the same $f$-bound again.


\section{Lazily explaining arbitrary heuristics}

 
Any heuristics' lower-bound in a particular state can be explained in
terms of some subset of the fluents that do not hold in a state.  In
the worst case, the explanation can be exactly the false fluents:
adding more facts can only increase the set of plans originating in
that state, and thus cannot increase the distance to the goal.
However more succinct explanations are sometimes possible than this
naive encoding.  All heuristics can then be translated to abstractions
using at most one such explanation per reachable state in the state
space.


\begin{theorem}
Any consistent heuristic $h$ encoded into some conjunction clauses of
the form
 \[ \lnot f_i \land \cdots \land \lnot f_j \Rightarrow [h \ge h(s_i)] \]
for any reachable states $s_i$
will define a consistent clausal heuristic.
\end{theorem}

Note that although we can encode any heuristic in this clausal
abstraction formalism, it may require as many clauses as states in the state-space.


Potential heuristics are a particularly interesting class of
explainable heuristic.  These heuristics are typically defined in
terms of SAS+ rather than STRIPS problems, however SAS+ problems are
easily transformed into STRIPS.  Given this transformation, the
problem's fluents can be partitioned into disjoint mutually exclusive
sets $F_0 \cdots F_n$.

In gripper, we may want to explain $h(s_0) = 4$.  Using our potentials
described in section \ref{cdhl:gripper-pot}, we can see that the
variables $R$ and $G$ are irrelevant to the heuristic value and can be
ignored entirely.
We could therefore generate a clause:
\[
\lnot [B_1 = r] \land \lnot [B_1 = h] \land
\lnot [B_2 = r] \land \lnot [B_2 = h]
\Rightarrow [h \ge 4]
\]

Perhaps more interestingly, in the state immediately after ``grip-1-l'',
we can generate a similar clause:
\[
\lnot [B_1 = r] \land
\lnot [B_2 = r] \land \lnot [B_2 = h]
\Rightarrow [h \ge 3]
\]
Note that we omit the $[B_1 = l]$ literal as this has a higher weight
than the true value of this variable: $[B_1 = h]$. If $[B_1 = l]$ were
true, we would necessarily be in a worse state, so the assertion that
$h(s) \ge 3$ would still hold.

In general, we need only include a literal in a potential heuristic
explanation if that fluent being true in a state is strictly better
than the value of the same variable that is actually true in the
current state.

Formally, given a state $s$, a potential heuristic $h$ with weights
$w([X_i = x_j])$ for each fluent $[X_i = x_j] \in F$,
$h(s)$ can be explained by
$\phi \Rightarrow [h \ge h(s)]$ where 
\[
\phi = \bigwedge_{[X_i = x_j] \in F} (\exists k. [X_i = x_k] \in s \land w([X_i = x_j]) < w([X_i = x_k]) \Rightarrow \lnot [X_i = x_j])
\]
The right-hand side of each implication can be evaluated completely at
explanation time, reducing $\phi$ to a simple conjunction of negated
fluents.


%\tod{TODO(This is harder to explain succinctly than I'd like... come back to it)}


\subsection{Emulating IDA*}

If when learning new clauses we replace $\phi$ with exactly the set of
false fluents we explicitly turn-off generalisation, making the
resulting algorithm equivalent to IDA* with a transposition table.  If,
further, we immediately discard all non-essential clauses, we also
forget the learned heuristic values once they are popped from the
stack, effectively making the algorithm a variant of IDA*.  Our
implementation uses a hash-table to store any clause where $\phi$
entails exactly one syntactic state, making comparison
to typical transposition-table implementations more fair.


\section{Experiments}

In table \ref{cdhl-cov} we compare our implementation in 3 modes:
IDA*, IDA* with a transposition-table, and CDHL.  All 3 started with a
potential heuristic optimised to maximise the heuristic estimate at
the initial state.  We can see from this table that the generalised
explanations do confer some benefit.  In particular, we can see from
figure \ref{cdhl-nodes-scatter} that the number of expanded nodes is
often greatly reduced, however this comes at the cost of increased
processing time per node, sometimes outweighing the benefit as seen in
figure \ref{cdhl-times-scatter}.  There exists one instance where
IDA*-TT uses fewer nodes, we presume this is due to garbage
collection.


\begin{table}[h]
  \centering
  \begin{tabular}{l | r r | r}
 Domain & IDA* & IDA*-TT & CDHL\\ 
 \hline
 barman & \textbf{0} & \textbf{0} & \textbf{0}\\ 
 elevators & 0 & \textbf{7} & \textbf{7}\\ 
 floortile & \textbf{0} & \textbf{0} & \textbf{0}\\ 
 nomystery & 4 & 6 & \textbf{7}\\ 
 openstacks & 0 & 5 & \textbf{7}\\ 
 parcprinter & 2 & \textbf{5} & 2\\ 
 parking & 0 & \textbf{1} & 0\\ 
 pegsol & 0 & \textbf{6} & 2\\ 
 scanalyzer & \textbf{3} & \textbf{3} & 2\\ 
 sokoban & 0 & \textbf{3} & 2\\ 
 tidybot & 1 & 3 & \textbf{11}\\ 
 transport & \textbf{0} & \textbf{0} & \textbf{0}\\ 
 visitall & 14 & \textbf{15} & 14\\ 
 woodworking & 0 & 1 & \textbf{4}\\ 
\hline
 Total & 24 & 55 & \textbf{58}
  \end{tabular}
\caption[CDHL Results]{Coverage of IDA* and CDHL on the 2011 IPC benchmarks. (Max time: 30 minutes, Max memory: 100 MB)\label{cdhl-cov}}
\end{table}


\begin{figure}\centering \includegraphics[width=0.7\columnwidth]{../cdhl-paper/nodes_scatter.pdf}
  \caption{\label{cdhl-nodes-scatter} Nodes expanded in search for CDHL and IDA* with a transposition table}
\end{figure}
\begin{figure}\centering \includegraphics[width=0.7\columnwidth]{../cdhl-paper/times_scatter.pdf}
  \caption{\label{cdhl-times-scatter} Time to optimal solution for CDHL and IDA* with a transposition table}
\end{figure}


\section{Conclusions and Future Work}
\label{cdhl:future-work}
Our results show that the number of expanded nodes can be reduced by
several orders of magnitude compared to IDA*,
some of this benefit can be achieved using a simpler (and much faster)
transposition table, however reductions in nodes explored as significant
as three orders of magnitude can be achieved by generalising the
knowledge learned during search, even when starting with a
state-of-the-art potential heuristic.

These reductions come at a significant increased processing time per
node.  We hypothesise that traditional watched-literal data-structures \cite{moskewicz2001}
used to store clausal information may be poorly suited to this use:
watched literals perform best when some literals are ``cold'' (never
true) so their watch lists can become long at little cost to the
search. However in planning, most fluents are both true and false on
the optimal path to the goal.  Other data-structures (perhaps based on
decision diagrams or match-trees) may be better suited to this
use-pattern.

Additionally recent work by
\citeauthor{pommerening2017} (\citeyear{pommerening2017}) opens up
the possibility of learning higher-dimensional potential heuristics
which appear to often be perfect with quite small numbers of features.

