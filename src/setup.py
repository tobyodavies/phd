from setuptools import setup, find_packages
setup(
    name = "pymas",
    version = "0.1",
    packages = ['pymas'],
    package_data={'pymas': ['test/*.sh', 'test/networks/*.dot']},
    install_requires=["pulp>=1.5.4", "decorator", "glpk"],
    tests_require=['pytest>=2.3.4', 'networkx']
)
