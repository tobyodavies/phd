

(define (problem BW-rand-5)
(:domain blocksworld)
(:objects b1 b2 b3 b4 b5  - block
	  h1 h2 h3 h4 h5  - hand)
(:init
(handempty h1)(handempty h2)(handempty h3)(handempty h4)(handempty h5)
(on b1 b2)
(on b2 b5)
(ontable b3)
(ontable b4)
(ontable b5)
(clear b1)
(clear b3)
(clear b4)
)
(:goal
(and
(on b2 b4)
(on b5 b3))
))
;(:metric minimize (total-time)))
