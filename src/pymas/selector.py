"""
Fragment Selection
==================

This module drives the optimisation process

"""


from contextlib import contextmanager
import collections
import logging
import itertools as it
import math
import sys
import time

import pulp as p

#from pymas.model import Resource, Fragment
import pymas.utils as utils
from pymas.utils import LinkedList, reduced_cost, get_solver, SearchQ


logger = logging.getLogger(__name__)
EPS = 1e-4


class SetPacker(object):
    """
    Encapsulates a (MI)LP Set Packing solver

    A :py:class:`SetPacker` is not intended to store any state other than a set of fragments.
    All column generation and branching must be passed in.

    Includes methods for:

    - solving the LP relaxation (:py:meth:`solve_lp`)
    - optimally selecting a subset of the known fragments: :py:meth:`mip_select`
    - heuristically selecting an integer feasible solution

        - using fix-and-dive :py:meth:`dive_select`
        - using greedy packing in order of lp relaxation, then of reduced-cost :py:meth:`greedy_select`

    uses `pulp` for MIP interface
    """

    def __init__(self, fragments, solver_type=None):
        self.model = p.LpProblem("FragSelection")
        self.model.objective = p.LpAffineExpression()
        self.fragments = {}
        self.resources = {}

        solvers = (solver_type,) if solver_type else utils.SUPPORTED_SOLVERS
        self.lp_solver = get_solver(solvers, msg=False, mip=False)
        self.update(fragments, max_cols=sys.maxint)

    @contextmanager
    def fixed_frags(self, fixed_frags):
        """
        Fix some fragments to specific values (0 or 1)
        """
        setup, teardown = it.tee(fixed_frags)
        old_coeffs = {}
        try:
            for frag, val in setup:
                fvar = self.fragments[frag]
                fvar.lowBound, fvar.upBound = val, val
                ## if fvar in self.model.objective:
                ##     old_coeffs[fvar] = self.model.objective[fvar]
                ##     del self.model.objective[fvar]
            yield
        finally:
            ## self.model.objective.update(old_coeffs)
            for frag, val in teardown:
                fvar = self.fragments[frag]
                fvar.lowBound, fvar.upBound = 0, 1+EPS/2

    def solve_lp(self, fixed_frags=()):
        """
        Solve the LP relaxation, optionally with some fragments fixed to 0/1

        `fixed_frags` is an iterable of (:py:class:`pymas.model.Fragment`, [0|1]) pairs.
        """
        with self.fixed_frags(fixed_frags):
            self.model.solve(self.lp_solver)
        # Dedent to allow "true" objective
        return (p.valueOrDefault(self.model.objective),
                {f: p.valueOrDefault(fv)
                 for f, fv in self.fragments.iteritems()},
                {r: (con.pi or 0.0)
                 for r, con in self.resources.iteritems()})

    def cost(self, selection):
        """
        Cost a selection of fragments
        """
        return sum(f.cost for f in selection)

    def can_use(self, fragment, selection):
        """
        Could `fragment` be added to `selection`
        """
        return all((sum(f.resources.get(r, 0)
                       for f in selection) +
                    fragment.resources[r] <= r.avail)
                   for r in fragment.resources)

    def greedy_select(self, fixed_frags=()):
        """
        Solve the LP, then greedily add in the closest fragments to 1.0
        """
        _, frags, _duals = self.solve_lp(fixed_frags)

        selection = set(f for (f, val) in fixed_frags if val)
        unselected = sorted((f for f in self.fragments
                             if f not in selection and frags[f] >= 1e-3),
                            key=frags.get)
        for frag in unselected:
            if self.can_use(frag, selection):
                selection.add(frag)

        return selection

    def mip_select(self, fixed_frags=()):
        """
        Solve the MIP to select the optimal subset of fragments
        """
        frags = {}
        resources = {}
        prob = p.LpProblem()
        prob += 0
        for frag in self.fragments:
            frags[frag] = p.LpVariable("BinF%x"%id(frag), cat=p.LpBinary)
            for res, usage in frag.resources.iteritems():
                if res not in resources:
                    resources[res] = usage * frags[frag] <= res.avail
                    prob += resources[res]
                else:
                    resources[res][frags[frag]] = usage
            prob += prob.objective + frag.cost * frags[frag]

        prob.solve(get_solver(msg=False))
        return set(f for f in frags
                   if frags[f].value() >= 1-EPS)


    def dive_select(self, fixed_frags):
        """
        Use the fix-and-dive mip heuristic to select a subset of fragments
        """
        fixed_frags = set(fixed_frags)
        selection = set(f for (f, val) in fixed_frags if val)
        for i in xrange(1, 9):
            threshold = 1. - i/20. - 1e-10
            count = -1
            while count < len(selection):
                count = len(selection)
                _obj, frags, _duals = self.solve_lp(fixed_frags)
                for frag, fvar in frags.iteritems():
                    if (p.value(fvar) >= threshold and
                            self.can_use(frag, selection)):
                        selection.add(frag)
                        fixed_frags.add((frag, 1))
        return self.greedy_select(fixed_frags)

    def add(self, fragment):
        """
        Add a new :py:class:`pymas.model.Fragment` to the candidate set
        """
        if fragment in self.fragments:
            return
        var = p.LpVariable("F%x"%(id(fragment),),
                           0, 1.0+EPS/2)
        var.varValue = 0
        self.fragments[fragment] = var
        self.model.objective += fragment.cost * var
        for res in fragment.resources:
            if res not in self.resources:
                self.resources[res] = p.LpAffineExpression(0) <= res.avail
                self.model += self.resources[res]
            self.resources[res][var] = fragment.usage(res)

    def remove(self, fragment):
        """
        remove a :py:class:`pymas.model.Fragment` from the candidate set
        """
        if fragment in self.fragments:
            var = self.fragments[fragment]
            for res in fragment.resources:
                if res in self.resources and var in self.resources[res]:
                    del self.resources[res][var]
                if res in self.resources and not self.resources[res]:
                    del self.resources[res]
            del self.fragments[fragment]
            del self.model.objective[var]

    def update(self, fragments, duals=None, max_cols=1, max_cands=sys.maxint):
        """
        Add up-to `max_cols` negative reduced-cost fragments to the candidate set
        """
        if duals is None and self.fragments:
            logger.warn("None passed for duals")
            _, __, duals = self.solve_lp()
        duals = duals or {}
        newfrags = set()
        for frag in it.islice(fragments, max_cands):
            rcost = reduced_cost(duals, frag)
            new_col = frag not in self.fragments
            if rcost < -EPS and new_col:
                newfrags.add(frag)
                self.add(frag)
            else:
                logger.warn("bad column generated, (%.2f rc, %sduplicate)",
                            rcost, 'not ' if new_col else '')
            if len(newfrags) >= max_cols:
                break
        return newfrags

    def prune(self, ignore=(), selection=(), primals=None, duals=None):
        """
        Remove fragments that don't appear to be useful
        (whose reduced cost is greater than the mean)
        """
        if len(self.fragments) < 100:
            return
        primals = primals or {}
        duals = duals or {}
        mean_rc = (sum(reduced_cost(duals, f) for f in self.fragments)
                   /
                   (len(self.fragments) - 1e-3))
        to_remove = [f for f in self.fragments
                     if (primals.get(f, 0) < .5 and
                         reduced_cost(duals, f) > mean_rc and
                         f not in selection and
                         f not in ignore)]
        mean_rc = (sum(reduced_cost(duals, f) for f in to_remove)
                   /
                   (len(to_remove) - 1e-3))
        for frag in to_remove:
            if reduced_cost(duals, frag) > mean_rc:
                self.remove(frag)


class FragSelector(object):
    """
    The `FragSelector` generates an optimal subset of fragments using column generation
    The (possibly unbounded) set of available columns and resources is specified in the `colgen`
    """

    optgap = .001

    def __init__(self, initial_fragments, colgen,
                 solver_type=None, lp_iters=5000):
        self.newcols = colgen
        self.packer = SetPacker(initial_fragments, solver_type=solver_type)
        self.q = SearchQ([],
                         expand=self.expand_branch,
                         heuristic=self.heuristic_select)
        self.lp_iters = xrange(lp_iters)
        self.sduals = {}

    def cost(self, soln):
        return self.packer.cost(soln)

    def biting_resources(self, primals):
        """
        Return a `resources: set(fragments)` mapping
        for all resources which are totally consumed in the primal solution
        """
        res_pairs = [(r, f)
                     for f in primals
                     for r in f.resources]
        resources = {}
        for res, frag in res_pairs:
            resources.setdefault(res, set())
            resources[res].add(frag)

        return {r: fs
                for r, fs in resources.iteritems()
                if -EPS < r.avail - sum(f.resources[r] * primals[f]
                                        for f in fs) < EPS}

    def int_duals(self, primals, degree=2, generate_cost=0):
        """
        Return an overestimate of the duals that
        more-or-less guarantees that generated columns will improve
        the integer optimum
        """
        iduals = {}
        gradient = {r: min(f.cost
                           for f in primals
                           if r in f.resources)
                    for r in set(r
                                 for f in primals
                                 for r in f.resources)}
        usage = {r: sum(primals[f] * f.resources[r]
                        for f in primals
                        if r in f.resources)
                 for r in set(r
                              for f in primals
                              for r in f.resources)}
        for res, use in usage.iteritems():
            if use >= res.avail - EPS:
                iduals.setdefault(res, 0.0)
                iduals[res] += gradient[res]

        if generate_cost:
            for frag in self.fragments:
                for res in frag.resources:
                    if res.avail == 0:
                        #logger.warn("Try generate %s", res)
                        iduals.setdefault(res, 0.0)
                        iduals[res] += generate_cost
        return iduals

    def smoothed_duals(self, duals, alpha=0.75):
        """
        Using a simplified form of stabilized column generation
        from http://www.gerad.ca/colloques/ColumnGeneration2008/slides/EduardoU.pdf
        """
        for res in set(duals) | set(self.sduals):
            self.sduals[res] = (alpha*duals.get(res, 0.) +
                                (1-alpha)*self.sduals.get(res, 0.))
        return self.sduals

    def smoothed_colgen(self, duals, max_cols=1, optthresh=-1):
        xduals = self.smoothed_duals(duals)
        return self.packer.update(self.newcols(xduals, optthresh, self.fragments),
                                  xduals, max_cols=max_cols)

    def colgen(self, duals, fixed_frags, max_cols=2, optthresh=-EPS, smooth=.95):
        cols = set()
        # iDuals
        sduals = collections.Counter(self.smoothed_duals(duals, smooth))

        # Real Duals
        print "="*5, "smoothDuals", "="*5
        cols.update(self.packer.update(self.newcols(sduals, -1, self.fragments),
                                       sduals,
                                       max_cols=max_cols,
                                       max_cands=2*max_cols+1))
        if fixed_frags:
            sduals = collections.Counter()
            sduals.update(self.int_duals(dict(fixed_frags)))
            print "="*5, "iDuals", "="*5
            cols.update(self.packer.update(self.newcols(sduals, -1, self.fragments),
                                           sduals,
                                           max_cols=max_cols,
                                           max_cands=2*max_cols+1))

        ## sduals.update(self.int_duals({}, generate_cost=100))
        ## # gduals
        ## print "="*5, "gDuals", "="*5
        ## cols.update(self.packer.update(self.newcols(sduals, -1, self.fragments),
        ##                                sduals,
        ##                                max_cols=max_cols,
        ##                                max_cands=2*max_cols+1))

        return cols

    def solve_lp(self, fixed_frags=(), max_cols=1):
        """
        Solve the linear relaxation of the set packing problem

        Parameters:

        - `fixed_frags` An iterable of (:py:class:`pymas.model.Fragment`, int) pairs

        Returns
        Objective, primals, duals triple
        """
        optgap = self.optgap
        starttime = time.time()
        obj, primals, duals = self.packer.solve_lp(fixed_frags)
        bestobj = obj
        noimprove = 0
        self.sduals = {}
        for _ in self.lp_iters:
            optthresh = optgap*int(obj+.99)/(sum(primals.itervalues())+EPS) + EPS
            obj, primals, duals = self.packer.solve_lp(fixed_frags)
            smoothed_cols = self.colgen(duals,
                                        fixed_frags,
                                        max_cols=max_cols,
                                        optthresh=optthresh)
            if smoothed_cols:
                min_rc = min(reduced_cost(self.sduals, f)
                             for f in smoothed_cols)
            if not smoothed_cols or min_rc >= optthresh:
                logger.warn("No cols?")
                break
            ## if min_rc >= optthresh:
            ##     well_priced = self.packer.update(self.newcols(duals, optthresh),
            ##                                      duals,
            ##                                      max_cols=1)
            ##     if well_priced:
            ##         min_rc = min(reduced_cost(duals, f)
            ##                      for f in well_priced)
            ## if not well_priced:
            ##     logger.info("Breaking because no -ive rc column found %f",
            ##                 min_rc)
            ##     break
            M = min(f.cost for f in self.fragments)
            realobj = (M*sum(primals.itervalues()) - obj)
            realbest = (M*sum(primals.itervalues()) - bestobj)
            if (obj-bestobj)/(bestobj-EPS) < optgap:
                logger.warn("noimprove = %d. %.2f/%.2f=%g",
                             noimprove, obj, bestobj, obj/(bestobj-EPS))
                noimprove += 1
            else:
                noimprove = 0
                bestobj = min([bestobj, obj])
            if min_rc > optthresh:
                logging.info("optgap %.2f/%.2f (%.2f s)",
                             min_rc, obj, time.time()-starttime)
                break
            if noimprove >= 200:
                logging.warn("No improvement, breaking lp")
                break

            logging.warn("lpobj: %s (%.2f, %d fractional)",
                         obj,
                         sum(primals.itervalues()),
                         sum(1 for v in primals.itervalues()
                             if EPS <= v <= 1 - EPS))
            logging.warn("lpbound: %s",
                         obj+min_rc*sum(primals.itervalues()))
        return obj, primals, duals

    def heuristic_select(self, fixed_frags=(), heuristic='mip', satisfices=None):
        """
        Return a subset of the existing fragments.
        using one of the :py:class:`SetPacker` heuristics
        """
        soln = getattr(self.packer, heuristic + '_select')(fixed_frags)
        cost = self.packer.cost(soln)
        newcost = None
        replan = len(soln)/4 + 1
        if satisfices and satisfices(soln):
            print "Satisficing"
            return cost, soln

        _, primals, duals = self.packer.solve_lp()
        self.packer.update(self.newcols(duals, -1, self.fragments),
                           duals,
                           max_cols=len(self.fragments),
                           max_cands=len(self.fragments))
        while newcost is None or newcost < cost:

            if newcost is not None:
                cost = newcost
            replan = len(soln)/10 + 1
            linreplan = replan + int(math.log(len(self.fragments)+1)) + 1
            if not self.colgen(duals, [(f, 1) for f in soln],
                               max_cols=min(len(soln), linreplan)):
                break

            soln = getattr(self.packer, heuristic + '_select')(fixed_frags)
            newcost = self.packer.cost(soln)
            logging.warn("heuristic iter: %.2g -> %.2g", cost, newcost)
        logging.debug("Heuristic cost: %f (%d frags)", cost, len(soln))
        return cost, soln

    def dive_heuristic_select(self, fixed_frags=(), satisfices=None):
        """
        Return a subset of the existing fragments.
        using one of the :py:class:`SetPacker` heuristics
        """
        orig_fixed_frags = fixed_frags
        orig_soln = self.packer.mip_select(fixed_frags)
        fixed_frags = list(fixed_frags)
        _, primals, duals = self.packer.solve_lp(fixed_frags)
        soln = set([f for f, v in primals.iteritems() if v > 1-EPS])

        while len(soln) == len(self.fragments):
            _, primals, duals = self.packer.solve_lp(fixed_frags)
            soln = set([f for f, v in primals.iteritems() if v > 1-EPS])
            self.colgen(duals, {f: 1 for f in soln}, max_cols=1)
        fix_this_iter = int(sum(primals.values())/5) + 1
        self.colgen(duals, fixed_frags)
        while len(fixed_frags) < len(self.fragments):
            self.colgen(duals, [], max_cols=1)
            obj, primals, duals = self.packer.solve_lp(fixed_frags)
            soln = self.packer.mip_select(fixed_frags)

            print "Diving. obj =", obj,
            print ", #selected =", len(soln), "/", sum(primals.values()),
            print ", #infeasible =", sum(int(EPS < primals[f] < 1-EPS)
                                         for f in primals),
            print ", #fixed =", len(fixed_frags),
            print ", #frags =", len(self.fragments)
            if satisfices and satisfices(soln):
                print "Satisficing"
                break

            fix_this_iter = max(fix_this_iter+1, len(soln))
            if not self.colgen(duals, fixed_frags, max_cols=1):
                print "No new frags"
                break

            frags = sorted([f for f,v in primals.items() if v], key=primals.get, reverse=True)

            ## legal_frags = list(it.islice(((f, 1)
            ##                               for f in frags
            ##                               if self.packer.can_use(f, [fr for fr,v in fixed_frags if v])),
            ##                              0, fix_this_iter))
            legal_frags = sorted(soln, key=primals.get)[:fix_this_iter]

            fixed_frags = [(f, 1) for f in legal_frags]
            if len(legal_frags) == 0 and sum(primals.values()) > len(soln)+EPS:
                print "No legal frags"
                ## self.colgen(duals, fixed_frags, max_cols=2*fix_this_iter)
                ## self.colgen(duals, [], max_cols=2*fix_this_iter)
                #break

        soln = self.packer.mip_select(orig_fixed_frags)
        cost = self.packer.cost(soln)
        return cost, soln

    def select(self, fixed_frags=()):
        """
        Return the optimal subset of existing columns
        """
        return self.heuristic_select(fixed_frags, 'mip')

    def newbranch(self, fixedvars):
        """
        Add a pair of branches on fragment
        """

        obj, primals, _duals = self.solve_lp(fixedvars)
        ratio_fixed = sum(1. for fvar in primals.values()
                          if EPS <= p.valueOrDefault(fvar) <= 1 - EPS) / len(primals)
        return ratio_fixed*obj, obj, fixedvars

    def branch(self, frag, fixed):
        """
        Yield the branches resulting from the fixing up and down of `frag`
        """
        yield self.newbranch(fixed.cons((frag, 1)))
        yield self.newbranch(fixed.cons((frag, 0)))

    def expand_branch(self, fixed):
        _, primals, _duals = self.solve_lp(fixed)
        nfixed = sum(1 for f in primals
                     if not (EPS <= primals[f] <= 1 - EPS))
        if nfixed < len(primals):
            _, branchfrag = min((f.cost*primals[f], f)
                                for f in primals
                                if EPS <= primals[f] <= 1 - EPS)
            ret = self.branch(branchfrag, fixed)
            print ret
            return ret
        else:
            # No need to branch.
            # greedy is faster, and will get the same result as mip
            # when all vars are fixed
            cost, sel = self.select()
            self.q.add_solution(sel, cost)
        return []

    def solve(self, iters=sys.maxint, max_cols=3, satisfices=None):
        optgap = self.optgap
        obj, _p, _d = self.solve_lp(max_cols=max_cols)
        cost, soln = self.select()  # use mip soln at root
        self.q.addbranch(cost, obj, LinkedList.nil())
        self.q.add_solution(soln, cost)
        lpobj = obj
        if 1.*cost/(lpobj+EPS) > 1-optgap:
            print "GAP"
            return lpobj, self.best_cost, self.best_set
        for soln in self.q.solutions(iters):
            print "heuristic:",
            cost, soln = self.heuristic_select(satisfices=satisfices)
            if satisfices and satisfices(soln):
                print "SAT"
                break
            print cost
            if 1.*cost/(lpobj+EPS) > 1-optgap:
                return lpobj, self.best_cost, self.best_set
            else:
                print "gap:", 1.*cost/(lpobj+EPS)
            lpobj = min(lb for (_, lb, __) in self.q.branches if lb is not None)

            obj, primals, duals = self.solve_lp([(f, 1) for f in self.best_set],
                                                max_cols=max_cols)
            self.packer.prune(ignore=self.best_set,
                              selection=self.best_set,
                              primals=primals,
                              duals=duals)
            logging.warn("selector best: %s", self.best)
            logging.warn("optgap best: %.2f/%.2f", self.best[0], obj)
        obj, _, _ = self.packer.solve_lp()
        cost, soln = self.heuristic_select(satisfices=satisfices)
        return lpobj, self.best_cost, self.best_set

    @property
    def fragments(self):
        return self.packer.fragments

    @property
    def resources(self):
        return self.packer.resources

    @property
    def best(self):
        return self.q.best

    @property
    def best_cost(self):
        return self.best[0]

    @property
    def best_set(self):
        return self.best[1]
