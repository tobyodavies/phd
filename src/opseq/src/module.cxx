#include <string>
#include "siw_planner.hxx"
using namespace boost::python;

void set_ops(OpSetPlanner* planner, dict ops){
	auto keys = ops.keys();
	size_t n = len(keys);
	planner->opset.clear();
	for (size_t i=0; i < n; i++){
		int op = extract<int>(keys[i]);
		int count = extract<int>(ops[op]);
		// std::cerr << "Budget for #" << op << " " << planner->instance()->actions()[op]->signature()
		// 		  << " = " << count << std::endl;
	
		planner->opset[op] = count;
	}
}

list get_plan(OpSetPlanner* planner){
	list ret;
	if(planner->plan_found){
		for(auto it = std::begin(planner->plan); it != std::end(planner->plan); it++){
			ret.append(*it);
		}
	}
	return ret;
}

list get_cut(OpSetPlanner* planner){
	list ret;
	if(!planner->plan_found){
		for(aptk::Action_Idx op = 0; unsigned(op) <= planner->n_actions(); op++){
			if (planner->cut.isset(op)){
				ret.append(op);
			}
		}
	}
	return ret;
}

std::string action_signature(OpSetPlanner* planner, aptk::Action_Idx op) {
	return planner->instance()->actions()[op]->signature();
}

template<typename CLS>
CLS standard_interface(CLS cls){
	return cls
		.def("add_atom", &STRIPS_Problem::add_atom)
		.def("add_action", &STRIPS_Problem::add_action)
		.def("add_mutex_group", &STRIPS_Problem::add_mutex_group)
		.def("num_atoms", &STRIPS_Problem::n_atoms)
		.def("num_actions", &STRIPS_Problem::n_actions)
		.def("get_atom_name", &STRIPS_Problem::get_atom_name)
		.def("get_domain_name", &STRIPS_Problem::get_domain_name)
		.def("get_problem_name", &STRIPS_Problem::get_problem_name)
		.def("add_precondition", &STRIPS_Problem::add_precondition)
		.def("add_effect", &STRIPS_Problem::add_effect)
		.def("add_cond_effect", &STRIPS_Problem::add_cond_effect)
		.def("set_cost", &STRIPS_Problem::set_cost)
		.def("cost", &STRIPS_Problem::cost)
		.def("notify_negated_conditions", &STRIPS_Problem::notify_negated_conditions)
		.def("create_negated_fluents", &STRIPS_Problem::create_negated_fluents)
		.def("set_init", &STRIPS_Problem::set_init)
		.def("set_goal", &STRIPS_Problem::set_goal)
		.def("init", &STRIPS_Problem::init)
		.def("goal", &STRIPS_Problem::goal)
		.def("set_domain_name", &STRIPS_Problem::set_domain_name)
		.def("set_problem_name", &STRIPS_Problem::set_problem_name)
		.def("write_ground_pddl", &STRIPS_Problem::write_ground_pddl)
		.def("print_action", &STRIPS_Problem::print_action)
		.def("precs", &STRIPS_Problem::precs)
		.def("adds", &STRIPS_Problem::adds)
		.def("dels", &STRIPS_Problem::dels)
		.def("edels", &STRIPS_Problem::edels)
		.def("actions_adding", &STRIPS_Problem::actions_adding)
		.def("actions_deleting", &STRIPS_Problem::actions_deleting)
		.def("actions_requiring", &STRIPS_Problem::actions_requiring)
		.def("num_mutexes", &STRIPS_Problem::num_mutexes)
		.def("get_mutex", &STRIPS_Problem::get_mutex)
		;
}

BOOST_PYTHON_MODULE( libopset )
{
	standard_interface(class_<OpSetPlanner>("OpSetPlanner"))
		.def( init< std::string, std::string >() )
		.def( "setup", &OpSetPlanner::setup )
		.def( "solve", &OpSetPlanner::solve )
		.def( "num_layers", &OpSetPlanner::num_layers )
		.def( "get_plan", get_plan)
		.def( "get_cut", get_cut)
		.def( "set_ops", set_ops)
		.def( "add_relaxation", &OpSetPlanner::add_relaxation)
		.def( "has_relaxation", &OpSetPlanner::has_relaxation)
		.def( "action_signature", action_signature)
		.def_readwrite( "parsing_time", &OpSetPlanner::m_parsing_time )
		.def_readwrite( "log_filename", &OpSetPlanner::m_log_filename )
	;
}

