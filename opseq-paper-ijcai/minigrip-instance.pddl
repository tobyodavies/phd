(define (problem minigripper-1)
		(:domain minigripper)
		(:objects
			 b1 b2 - ball
			 left right - room)
		(:init
			(hand-empty)
			(hand-at right)
			(ball-at b1 left)
			(ball-at b2 left))
		(:goal
			(and
				(ball-at b1 right)
				(ball-at b2 right)
				)))