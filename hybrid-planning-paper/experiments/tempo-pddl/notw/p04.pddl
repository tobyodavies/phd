(define (problem bfrs_32)
(:domain bfrs)
(:objects
    p2 - location
    p1 - location
    d - location
    m1 - location
    m2 - location
    y - location
    x - location
    o0 - order
    o1 - order
    o2 - order
    o3 - order
    o4 - order
    o5 - order
    o6 - order
    o7 - order
    o8 - order
    o9 - order
    o10 - order
    o11 - order
    o12 - order
    o13 - order
    o14 - order
    o15 - order
    o16 - order
    o17 - order
    o18 - order
    o19 - order
    o20 - order
    o21 - order
    o22 - order
    o23 - order
    o24 - order
    o25 - order
    o26 - order
    o27 - order
    o28 - order
    o29 - order
    o30 - order
    o31 - order
    c0 - consist
    c1 - consist
    c2 - consist
    c3 - consist
    c4 - consist
    c5 - consist
    c6 - consist
    c7 - consist
    c8 - consist
    c9 - consist
    c10 - consist
    c11 - consist
    c12 - consist
    c13 - consist
    c14 - consist
    c15 - consist
    c16 - consist
    c17 - consist
    c18 - consist
    c19 - consist
    c20 - consist
    c21 - consist
    c22 - consist
    c23 - consist
    c24 - consist
    c25 - consist
    c26 - consist
    c27 - consist
    c28 - consist
    c29 - consist
    c30 - consist
    c31 - consist
)
(:init
  (is_yard y)
    (clear p2)
    (clear p1)
    (clear d)
    (clear m1)
    (clear m2)
    (clear y)
    (clear x)

    (conn p2 y)
    (conn y p2)
    (conn p1 y)
    (conn y p1)
    (conn d y)
    (conn y d)
    (conn d x)
    (conn x d)
    (conn d m1)
    (conn m1 d)
    (conn d m2)
    (conn m2 d)
    (conn y x)
    (conn x y)

    ;; order o0 ('m1', 'p2', 0, 12)
    (order_mine o0 m1)
    (order_port o0 p2)
    (not-delivered o0)

    ;; consist c0
    (available c0)
    (empty c0)
    (at c0 y)

    (compat c0 o0)

    ;; order o1 ('m1', 'p2', 12, 24)
    (order_mine o1 m1)
    (order_port o1 p2)
    (not-delivered o1)

    ;; consist c1
    (available c1)
    (empty c1)
    (at c1 y)

    (compat c1 o1)

    ;; order o2 ('m1', 'p2', 24, 36)
    (order_mine o2 m1)
    (order_port o2 p2)
    (not-delivered o2)

    ;; consist c2
    (available c2)
    (empty c2)
    (at c2 y)

    (compat c2 o2)

    ;; order o3 ('m1', 'p2', 36, 48)
    (order_mine o3 m1)
    (order_port o3 p2)
    (not-delivered o3)

    ;; consist c3
    (available c3)
    (empty c3)
    (at c3 y)

    (compat c3 o3)

    ;; order o4 ('m1', 'p2', 48, 60)
    (order_mine o4 m1)
    (order_port o4 p2)
    (not-delivered o4)

    ;; consist c4
    (available c4)
    (empty c4)
    (at c4 y)

    (compat c4 o4)

    ;; order o5 ('m1', 'p2', 60, 72)
    (order_mine o5 m1)
    (order_port o5 p2)
    (not-delivered o5)

    ;; consist c5
    (available c5)
    (empty c5)
    (at c5 y)

    (compat c5 o5)

    ;; order o6 ('m1', 'p2', 72, 84)
    (order_mine o6 m1)
    (order_port o6 p2)
    (not-delivered o6)

    ;; consist c6
    (available c6)
    (empty c6)
    (at c6 y)

    (compat c6 o6)

    ;; order o7 ('m1', 'p2', 84, 96)
    (order_mine o7 m1)
    (order_port o7 p2)
    (not-delivered o7)

    ;; consist c7
    (available c7)
    (empty c7)
    (at c7 y)

    (compat c7 o7)

    ;; order o8 ('m1', 'p1', 0, 12)
    (order_mine o8 m1)
    (order_port o8 p1)
    (not-delivered o8)

    ;; consist c8
    (available c8)
    (empty c8)
    (at c8 y)

    (compat c8 o8)

    ;; order o9 ('m1', 'p1', 12, 24)
    (order_mine o9 m1)
    (order_port o9 p1)
    (not-delivered o9)

    ;; consist c9
    (available c9)
    (empty c9)
    (at c9 y)

    (compat c9 o9)

    ;; order o10 ('m1', 'p1', 24, 36)
    (order_mine o10 m1)
    (order_port o10 p1)
    (not-delivered o10)

    ;; consist c10
    (available c10)
    (empty c10)
    (at c10 y)

    (compat c10 o10)

    ;; order o11 ('m1', 'p1', 36, 48)
    (order_mine o11 m1)
    (order_port o11 p1)
    (not-delivered o11)

    ;; consist c11
    (available c11)
    (empty c11)
    (at c11 y)

    (compat c11 o11)

    ;; order o12 ('m1', 'p1', 48, 60)
    (order_mine o12 m1)
    (order_port o12 p1)
    (not-delivered o12)

    ;; consist c12
    (available c12)
    (empty c12)
    (at c12 y)

    (compat c12 o12)

    ;; order o13 ('m1', 'p1', 60, 72)
    (order_mine o13 m1)
    (order_port o13 p1)
    (not-delivered o13)

    ;; consist c13
    (available c13)
    (empty c13)
    (at c13 y)

    (compat c13 o13)

    ;; order o14 ('m1', 'p1', 72, 84)
    (order_mine o14 m1)
    (order_port o14 p1)
    (not-delivered o14)

    ;; consist c14
    (available c14)
    (empty c14)
    (at c14 y)

    (compat c14 o14)

    ;; order o15 ('m1', 'p1', 84, 96)
    (order_mine o15 m1)
    (order_port o15 p1)
    (not-delivered o15)

    ;; consist c15
    (available c15)
    (empty c15)
    (at c15 y)

    (compat c15 o15)

    ;; order o16 ('m2', 'p2', 0, 12)
    (order_mine o16 m2)
    (order_port o16 p2)
    (not-delivered o16)

    ;; consist c16
    (available c16)
    (empty c16)
    (at c16 y)

    (compat c16 o16)

    ;; order o17 ('m2', 'p2', 12, 24)
    (order_mine o17 m2)
    (order_port o17 p2)
    (not-delivered o17)

    ;; consist c17
    (available c17)
    (empty c17)
    (at c17 y)

    (compat c17 o17)

    ;; order o18 ('m2', 'p2', 24, 36)
    (order_mine o18 m2)
    (order_port o18 p2)
    (not-delivered o18)

    ;; consist c18
    (available c18)
    (empty c18)
    (at c18 y)

    (compat c18 o18)

    ;; order o19 ('m2', 'p2', 36, 48)
    (order_mine o19 m2)
    (order_port o19 p2)
    (not-delivered o19)

    ;; consist c19
    (available c19)
    (empty c19)
    (at c19 y)

    (compat c19 o19)

    ;; order o20 ('m2', 'p2', 48, 60)
    (order_mine o20 m2)
    (order_port o20 p2)
    (not-delivered o20)

    ;; consist c20
    (available c20)
    (empty c20)
    (at c20 y)

    (compat c20 o20)

    ;; order o21 ('m2', 'p2', 60, 72)
    (order_mine o21 m2)
    (order_port o21 p2)
    (not-delivered o21)

    ;; consist c21
    (available c21)
    (empty c21)
    (at c21 y)

    (compat c21 o21)

    ;; order o22 ('m2', 'p2', 72, 84)
    (order_mine o22 m2)
    (order_port o22 p2)
    (not-delivered o22)

    ;; consist c22
    (available c22)
    (empty c22)
    (at c22 y)

    (compat c22 o22)

    ;; order o23 ('m2', 'p2', 84, 96)
    (order_mine o23 m2)
    (order_port o23 p2)
    (not-delivered o23)

    ;; consist c23
    (available c23)
    (empty c23)
    (at c23 y)

    (compat c23 o23)

    ;; order o24 ('m2', 'p1', 0, 12)
    (order_mine o24 m2)
    (order_port o24 p1)
    (not-delivered o24)

    ;; consist c24
    (available c24)
    (empty c24)
    (at c24 y)

    (compat c24 o24)

    ;; order o25 ('m2', 'p1', 12, 24)
    (order_mine o25 m2)
    (order_port o25 p1)
    (not-delivered o25)

    ;; consist c25
    (available c25)
    (empty c25)
    (at c25 y)

    (compat c25 o25)

    ;; order o26 ('m2', 'p1', 24, 36)
    (order_mine o26 m2)
    (order_port o26 p1)
    (not-delivered o26)

    ;; consist c26
    (available c26)
    (empty c26)
    (at c26 y)

    (compat c26 o26)

    ;; order o27 ('m2', 'p1', 36, 48)
    (order_mine o27 m2)
    (order_port o27 p1)
    (not-delivered o27)

    ;; consist c27
    (available c27)
    (empty c27)
    (at c27 y)

    (compat c27 o27)

    ;; order o28 ('m2', 'p1', 48, 60)
    (order_mine o28 m2)
    (order_port o28 p1)
    (not-delivered o28)

    ;; consist c28
    (available c28)
    (empty c28)
    (at c28 y)

    (compat c28 o28)

    ;; order o29 ('m2', 'p1', 60, 72)
    (order_mine o29 m2)
    (order_port o29 p1)
    (not-delivered o29)

    ;; consist c29
    (available c29)
    (empty c29)
    (at c29 y)

    (compat c29 o29)

    ;; order o30 ('m2', 'p1', 72, 84)
    (order_mine o30 m2)
    (order_port o30 p1)
    (not-delivered o30)

    ;; consist c30
    (available c30)
    (empty c30)
    (at c30 y)

    (compat c30 o30)

    ;; order o31 ('m2', 'p1', 84, 96)
    (order_mine o31 m2)
    (order_port o31 p1)
    (not-delivered o31)

    ;; consist c31
    (available c31)
    (empty c31)
    (at c31 y)

    (compat c31 o31)

)
(:goal
(and
  (delivered o0)
  (at c0 y)
  (delivered o1)
  (at c1 y)
  (delivered o2)
  (at c2 y)
  (delivered o3)
  (at c3 y)
  (delivered o4)
  (at c4 y)
  (delivered o5)
  (at c5 y)
  (delivered o6)
  (at c6 y)
  (delivered o7)
  (at c7 y)
  (delivered o8)
  (at c8 y)
  (delivered o9)
  (at c9 y)
  (delivered o10)
  (at c10 y)
  (delivered o11)
  (at c11 y)
  (delivered o12)
  (at c12 y)
  (delivered o13)
  (at c13 y)
  (delivered o14)
  (at c14 y)
  (delivered o15)
  (at c15 y)
  (delivered o16)
  (at c16 y)
  (delivered o17)
  (at c17 y)
  (delivered o18)
  (at c18 y)
  (delivered o19)
  (at c19 y)
  (delivered o20)
  (at c20 y)
  (delivered o21)
  (at c21 y)
  (delivered o22)
  (at c22 y)
  (delivered o23)
  (at c23 y)
  (delivered o24)
  (at c24 y)
  (delivered o25)
  (at c25 y)
  (delivered o26)
  (at c26 y)
  (delivered o27)
  (at c27 y)
  (delivered o28)
  (at c28 y)
  (delivered o29)
  (at c29 y)
  (delivered o30)
  (at c30 y)
  (delivered o31)
  (at c31 y)
))
(:metric minimize (total-time)))

