%% Prob w/ ${len(orders)} orders
<%
_nidx = graph.nodes().index
nidx = lambda n: _nidx(n) + 1
edges = graph.edges() + [(d, s) for s, d in graph.edges()] + [(n, n) for n in graph.nodes()]
%>

NBLOCKS = ${len(graph.nodes())};
NEDGES  = ${len(edges)};
NORDERS = ${len(orders)};

max_time  = ${max(ulb+1 for (m, p, la, ulb) in orders) + 20};
max_dwell = 5;

yard = ${nidx('y')};
%%edges: ${edges}
edges = array2d(EDGES, 1..2, ${[nidx(n) for e in edges for n in e]});

order_mines   = ${[nidx(m) for (m, p, la, ulb) in orders]};
order_ports   = ${[nidx(p) for (m, p, la, ulb) in orders]};
load_after    = ${[la+1 for (m, p, la, ulb) in orders]};
unload_before = ${[ulb+1 for (m, p, la, ulb) in orders]};
