(define (problem bfrs_8)
(:domain bfrs)
(:objects
    p2 - location
    p1 - location
    d - location
    m1 - location
    m2 - location
    y - location
    x - location
    o0 - order
    o1 - order
    o2 - order
    o3 - order
    o4 - order
    o5 - order
    o6 - order
    o7 - order
    c0 - consist
    c1 - consist
    c2 - consist
    c3 - consist
    c4 - consist
    c5 - consist
    c6 - consist
    c7 - consist
)
(:init
  (is_yard y)
    (clear p2)
    (clear p1)
    (clear d)
    (clear m1)
    (clear m2)
    (clear y)
    (clear x)

    (conn p2 y)
    (conn y p2)
    (conn p1 y)
    (conn y p1)
    (conn d y)
    (conn y d)
    (conn d x)
    (conn x d)
    (conn d m1)
    (conn m1 d)
    (conn d m2)
    (conn m2 d)
    (conn y x)
    (conn x y)

    ;; order o0 ('m1', 'p2', 0, 12)
    (order_mine o0 m1)
    (order_port o0 p2)
    (= (load_after_time o0) 0)
    (= (unload_before_time o0) 12)
    (not-delivered o0)

    ;; consist c0
    (= (current_time c0) 0)
    (available c0)
    (empty c0)
    (at c0 y)

    (compat c0 o0)

    ;; order o1 ('m1', 'p2', 12, 24)
    (order_mine o1 m1)
    (order_port o1 p2)
    (= (load_after_time o1) 12)
    (= (unload_before_time o1) 24)
    (not-delivered o1)

    ;; consist c1
    (= (current_time c1) 0)
    (available c1)
    (empty c1)
    (at c1 y)

    (compat c1 o1)

    ;; order o2 ('m1', 'p1', 0, 12)
    (order_mine o2 m1)
    (order_port o2 p1)
    (= (load_after_time o2) 0)
    (= (unload_before_time o2) 12)
    (not-delivered o2)

    ;; consist c2
    (= (current_time c2) 0)
    (available c2)
    (empty c2)
    (at c2 y)

    (compat c2 o2)

    ;; order o3 ('m1', 'p1', 12, 24)
    (order_mine o3 m1)
    (order_port o3 p1)
    (= (load_after_time o3) 12)
    (= (unload_before_time o3) 24)
    (not-delivered o3)

    ;; consist c3
    (= (current_time c3) 0)
    (available c3)
    (empty c3)
    (at c3 y)

    (compat c3 o3)

    ;; order o4 ('m2', 'p2', 0, 12)
    (order_mine o4 m2)
    (order_port o4 p2)
    (= (load_after_time o4) 0)
    (= (unload_before_time o4) 12)
    (not-delivered o4)

    ;; consist c4
    (= (current_time c4) 0)
    (available c4)
    (empty c4)
    (at c4 y)

    (compat c4 o4)

    ;; order o5 ('m2', 'p2', 12, 24)
    (order_mine o5 m2)
    (order_port o5 p2)
    (= (load_after_time o5) 12)
    (= (unload_before_time o5) 24)
    (not-delivered o5)

    ;; consist c5
    (= (current_time c5) 0)
    (available c5)
    (empty c5)
    (at c5 y)

    (compat c5 o5)

    ;; order o6 ('m2', 'p1', 0, 12)
    (order_mine o6 m2)
    (order_port o6 p1)
    (= (load_after_time o6) 0)
    (= (unload_before_time o6) 12)
    (not-delivered o6)

    ;; consist c6
    (= (current_time c6) 0)
    (available c6)
    (empty c6)
    (at c6 y)

    (compat c6 o6)

    ;; order o7 ('m2', 'p1', 12, 24)
    (order_mine o7 m2)
    (order_port o7 p1)
    (= (load_after_time o7) 12)
    (= (unload_before_time o7) 24)
    (not-delivered o7)

    ;; consist c7
    (= (current_time c7) 0)
    (available c7)
    (empty c7)
    (at c7 y)

    (compat c7 o7)

)
(:goal
(and
  (delivered o0)
  (at c0 y)
  (delivered o1)
  (at c1 y)
  (delivered o2)
  (at c2 y)
  (delivered o3)
  (at c3 y)
  (delivered o4)
  (at c4 y)
  (delivered o5)
  (at c5 y)
  (delivered o6)
  (at c6 y)
  (delivered o7)
  (at c7 y)
))
(:metric minimize (total-time)))

