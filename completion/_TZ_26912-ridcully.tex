\documentclass[presentation]{beamer}
%\documentclass[handout,notes=show]{beamer}
\usetheme{Madrid}
%for default nicta style use:
\usepackage{etex}
\usepackage[labelformat=empty]{caption}

% Required Packages
\usepackage{times}
\usepackage{helvet}
\usepackage{courier}
\usepackage{verbatim}
\usepackage{natbib}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{bm} 

\usepackage{graphicx}
\usepackage{adjustbox}

\usepackage[noend]{algpseudocode}
\usepackage{algorithm}
\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}
\usepackage{xifthen}
\AtBeginSection[]
{
   \begin{frame}
       \frametitle{Outline}
       \tableofcontents[currentsection]
   \end{frame}
}


%\input{../macros-sebastian}

\title[Conflict in Planning]{Learning from Conflict \\ in Classical, Multi-Agent \& temporal Planning}

\author[Toby Davies]{Toby O. Davies}
\institute[Unimelb]{The University of Melbourne}
\date{9th December 2016}


% user defined
\newcommand{\dwd}{Dantzig-Wolfe Decomposition}
\newcommand{\Bfrsp}{Bulk Freight Rail Scheduling Problem}
\newcommand{\bfrsp}{BFRSP}
\newcommand{\Fbp}{fragment-based planning}
\newcommand{\FBP}{Fragment-Based Planning}
\newcommand{\fbp}{FBP}
\newcommand{\Math}[1]{\ensuremath{#1}}
\newcommand{\vect}[1]{\Math{\boldsymbol{ \tilde #1 }}} 	% vector
\newcommand{\lpdual}{\pi}
\newcommand{\tuple}[1]{\Math{\langle #1 \rangle}}		% tuple
\newcommand{\Tuple}[1]{\Math{\left\langle #1 \right\rangle}}		% tuple
\newcommand{\tup}[1]{\tuple{#1}}            			% tuple
\newcommand{\card}[1]{|{#1}|}                     % cardinality of a set

%\newcommand{\tablefont}{\small}
\newcommand{\tablefont}{\normalsize}

\newcommand{\quine}[1]{[\![#1]\!]}
\newcommand{\id}[1]{\mbox{\textit{#1}}}
\newcommand{\Frags}{\id{Frags}}
\newcommand{\Res}{\id{Res}}
\newcommand{\Goals}{\id{Goals}}
\newcommand{\LowBound}{\id{LowBound}}
\newcommand{\UpBound}{\id{UpBound}}
\newcommand{\SolveLP}{\id{SolveLP}}
\newcommand{\SolveIP}{\id{SolveIP}}
\newcommand{\LinFBP}{\id{LinFBP}}
\newcommand{\Queue}{\id{Queue}}
\newcommand{\initfrags}{\id{initfrags}}
\newcommand{\Branches}{\id{Branches}}
\newcommand{\BranchRs}{\id{BranchRs}}
\newcommand{\LRs}{\id{LRs}}
\newcommand{\Fs}{\id{Fs}}
\newcommand{\Gs}{\id{Gs}}
\newcommand{\Rs}{\id{Rs}}
\newcommand{\Clear}{\id{Clear}}
\newcommand{\Occupied}{\id{Occupied}}
\newcommand{\plusplus}{\mathbin{\mathtt{++}}}


\newcommand{\dcol}[1]{\multicolumn{2}{|c|}{#1}}

\newcommand{\maybehighlight}[2]{\ifthenelse{\isin{#2}{#1}}{\tikzset{fill=red!50}}{}}

\tikzset{
    Y/.code={\maybehighlight{#1}{Y}},
    D/.code={\maybehighlight{#1}{D}},
    M1/.code={\maybehighlight{#1}{M1}},
    M2/.code={\maybehighlight{#1}{M2}},
    P1/.code={\maybehighlight{#1}{P1}},
    P2/.code={\maybehighlight{#1}{P2}},
    state/.style={draw=black,text=black,shape=circle},
    branch/.style={draw=black,text=black},
}
\newcommand{\track}[1]{
  \begin{tikzpicture}[-,node distance=1.3cm,semithick]
    \node[state, Y=#1] (Y)                    {$y$};
    \node[state, D=#1] (D)  [left of=Y]       {$d$};
    \node[state,M1=#1] (M1) [above left of=D] {$m_1$};
    \node[state,M2=#1] (M2) [below left of=D] {$m_2$};
    \node[state,P1=#1] (P1) [above right of=Y]{$p_1$};
    \node[state,P2=#1] (P2) [below right of=Y]{$p_2$};
    
    \path (Y)  edge (D)
          (D)  edge (M1)
          (M2) edge (D)
          (Y)  edge (P1)
          (Y)  edge (P2)
          (Y)  edge (D)
          (D)  edge (M1)
          (Y)  edge (P1)
          (Y)  edge (D)
          (D)  edge (M2)
          (Y)  edge (P2);
  \end{tikzpicture}
}

%% OpSeq

\newtheorem{thm}{Theorem}

\newcommand{\tcol}[1]{\multicolumn{3}{|c|}{#1}}
\newcommand{\lb}[2]{[Y_{#1}\ge#2]}
\newcommand{\AC}{\mathcal{C}}
\newcommand{\atmost}[1]{\le_{#1}\!}
\newcommand{\Gl}[1]{[\Sigma \AC(o) \ge #1]}

\usepackage{bbding}
\newcommand*\tick{\item[\color{green!60!black}\Checkmark]}
\newcommand*\fail{\item[\color{red}\XSolidBrush]}

\newcommand{\Move}[1]{\textit{move-#1}}
\newcommand{\Grip}[2]{\textit{grip-#1-#2}}
\newcommand{\Drop}[2]{\textit{drop-#1-#2}}
\newcommand{\gripper}{
\begin{center}
 \begin{adjustbox}{width=\textwidth}
\begin{tikzpicture}%
  [<-,place/.style={circle,draw=black}, scale=1.3]
  \node (VarR) at (0,3) {$R:$} ;
  \node (L) at (1,3) [place, double] {$l$} ;
  \node (R) at (5,3)   [place,double] {$r$} ;
  \path (VarR) edge[->] (L) ; 
  \path (L) edge[-triangle 45, bend left] node[below] {\small \Move{r}} (R) ;
  \path (R) edge[-triangle 45, bend left] node[above] {\small \Move{l}} (L) ;
  \node (VarB) at (0,1.5) {$B_i:$} ;
  \node (XL) at (1,1.5)   [place] {$l$} ;
  \node (XG) at (3,1.5)   [place] {$g$} ;
  \node (XR) at (5,1.5)   [place,double] {$r$} ;
  \path (VarB) edge[->] (XL) ; 
  \path (XL) edge[-triangle 45, bend left] node[above] {\small \Grip{i}{l}} (XG) ;
  \path (XG) edge[-triangle 45, bend left] node[above] {\small \Drop{i}{r}} (XR) ;
  \path (XR) edge[-triangle 45, bend left] node[below] {\small \Grip{i}{r}} (XG) ;
  \path (XG) edge[-triangle 45, bend left] node[below] {\small \Drop{i}{l}} (XL) ;

  \node (VarG) at (0,0) {$G:$} ;
  \node (E) at (1, 0) [place,double] {$e$} ;
  \node (N) at (5, 0) [place,double] {$n$} ;
  \path (VarG) edge[->] (E) ; 
  \path (E) edge[-triangle 45, bend left] node[below] {\small \Grip{*}{*}} (N) ;
  \path (N) edge[-triangle 45, bend left] node[above] {\small \Drop{*}{*}} (E) ;
\end{tikzpicture}
 \end{adjustbox}
\end{center}
}

\newcommand{\CDHLEG}{
\begin{center}
 \begin{adjustbox}{width=\textwidth}
\begin{tikzpicture}%
  [<-,node/.style={circle,draw=black,minimum size=4em,align=center}, scale=1.3]
  \node (Goal) at (5,1) [node, double] {$\top$ \\\small $[h\ge 0]$} ;
  \node (NotGoal) at (3,1)[node] {$\lnot G$ \\\small $[h\ge 1]$} ;
  \path (NotGoal) edge[->] node[above] {\small $o_1$} (Goal);
  \only<1>{
    \path (NotGoal) edge[loop below, ->] node[above] {\small $o_2$} (NotGoal);
  }
  \only<2->{
    \node (NotPrec) at (1,1)[node] {$\lnot P_1$\\\small$[h\ge 2]$} ;
    \path (NotPrec) edge[->] node[above] {\small $o_2$} (NotGoal);
  }
\end{tikzpicture}
 \end{adjustbox}
\end{center}
}



\begin{document}
\begin{frame}
  \frametitle{What is the conflict?}

\end{document}
