#ifndef LANDMARK_HXX
#define LANDMARK_HXX

#include <unordered_map>
#include <unordered_set>
#include <memory>

#include "aptk/bit_set.hxx"

#include "strips_prob.hxx"
#include "strips_state.hxx"
#include "watched_lit_succ_gen.hxx"


namespace lml {
typedef int OpID;
typedef unsigned FluentID;

using aptk::State;
using aptk::STRIPS_Problem;
using aptk::Bit_Set;

// A Plain-Old-Data class representing an explained landmark
struct Landmark {

	unsigned id;

	// The (G)LM bounds literals
	std::unordered_map<OpID, unsigned> op_counts;

	// The set of facts which this lm enables us to reach. 
	aptk::Bit_Set maximal;

	// The set of parent states that must be proven unsolvalble
	// before the states matched by this lm can be proven
	// unsolvable.
	std::unordered_set<State*> loop_roots;

	double cost_assigned;
	unsigned size(){
		return op_counts.size();
	}
	bool applies(const State& s){
		return maximal.contains(s.fluent_set());
		for(auto f : s.fluent_vec())
			if(!maximal.isset(f))
				return false;
		return true;
	}
	bool dump(const STRIPS_Problem& prob, const State& s){
		std::cerr << "{";
		for(auto f : maximal)
			std::cerr << "" << prob.fluents()[f]->signature() << ", ";
		std :: cerr << "} superset of s -> ";
		for(auto bound_lit : op_counts)
			std::cerr << "[" 
				  << prob.actions()[bound_lit.first]->signature()
				  << ">= "
				  << bound_lit.second << "] \\/ ";
		std::cerr << "false" << std::endl;
		if(!applies(s)){
			aptk::Bit_Set fs = s.fluent_set();
			fs.remove(maximal);
			std::cerr << "Does not apply because ";
			std::cerr << prob.fluents()[fs.min_elem()]->signature() << std::endl;
			return false;
		} else {
			std::cerr << "Applies" << std::endl;
			return true;
		}
	}
};

// LMDB manages the set of landmarks applicable in a path from the root.
// It re-activates landmarks when they become reapplicable.
// it is optimised for depth-first search,
class LMDB {
public:
	LMDB(STRIPS_Problem& prob): prob(prob), wl(prob) {}
	void setup();

	// Compute the zero reduced cost actions applicable in s.
	void applicable_actions(const State& s, std::vector<OpID>& ops);
	// Compute the maximum cost that can be assigned to lm.
	double lm_cost(const State& s, Landmark* lm);

	// Update applicable and disabled LMs. Return a landmark violated by s if one exists.
	// NB: s is the state *after* applying op.
	Landmark* progress_lms(State s, OpID op);
	
	void backtrack(const State& s, OpID op);

	Landmark* regress(const State& s, const std::vector< std::pair<OpID, Landmark*> >&);

	// Add a (delete-relaxed) landmark to the database.
	Landmark* add_lm(State s, std::vector<OpID> lm);

	// State s has been seen before on the stack.
	Landmark* add_loop(const State& s);

	// Return the reduced cost of a bounds-literal
	double rc(OpID op, unsigned occurance) const;

	double eval(State s);
	double incr_eval(const State& s);
	double lmcut(State s);

	unsigned num_active() const;
	unsigned size() const { return all_lms.size(); }

	friend class LandmarksTest_Undiscount_Test;
	friend class LandmarksTest_Discount_Test;
	friend class LandmarksTest_Progress_Test;
	friend class LandmarksTest_ProgressEvalBacktrack_Test;
	friend class LandmarksTest_Regress_Test;

protected:
	void add_watcher(const State& s, Landmark* lm);
	void discount_lm(Landmark* lm, double);
	void discount_op(OpID, unsigned, double);
	void undiscount_lm(Landmark*, double);
	void undiscount_op(OpID, unsigned, double);

private:
	const STRIPS_Problem& prob;
	aptk::WatchedLitSuccGen wl;
	std::vector< std::vector<double> > reduced_cost;
	std::vector< Landmark* > applicable_lms;
	std::vector< std::vector<Landmark*> > disabled_lms;

	std::vector< std::unique_ptr<Landmark> > all_lms;

	// Temporaries for regression
	std::vector<OpID> ops_to_explain;
	std::vector<unsigned> block_count;
	Bit_Set relevant_facts;
};

}
#endif
