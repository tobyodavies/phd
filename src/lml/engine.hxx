#include "action.hxx"
#include "landmark.hxx"

namespace lml {

using aptk::Action;
using aptk::State;
using aptk::STRIPS_Problem;

struct Node {
	Node(const State& s);
	Node(const Node& n, Action* act);
	State s;
	unsigned g;
	unsigned f;

	std::vector< std::pair<OpID, Landmark*> > succ_lms;
};

class SearchEngine {
public:
	SearchEngine(STRIPS_Problem& prob);
	Landmark* search(std::vector<OpID>& plan);
	void setup();

	double lb();

	LMDB& get_lmdb(){ return lmdb; }

	bool in_stack(const State& s);

private:
	STRIPS_Problem& prob;
	LMDB lmdb;

	std::vector<OpID> applicable;
	std::vector<Node> stack;

	unsigned long expansions;

	// vector of indexes into the stack s.t.
	// forall idx in last_held[f] => 
	//     stack[idx].entails(f) /\ !stack[idx+1].entails(f)
	std::vector< std::vector<unsigned> > fluent_last_held;
};

}
