#include "engine.hxx"

namespace lml {

Node::Node(const State& s) :s(s), g(0), f(0){
}

Node::Node(const Node& n, Action* act) :s(n.s), g(n.g+act->cost()), f(n.f-act->cost()){
	s.unset(act->del_vec());
	s.set(act->add_vec());
}

SearchEngine::SearchEngine(STRIPS_Problem& prob):prob(prob), lmdb(prob){
}

void SearchEngine::setup(){
	prob.make_action_tables(false);
	lmdb.setup();
	State s0(prob);
	s0.set(prob.init());
	stack.emplace_back(s0);
	lmdb.lmcut(s0);
}

Landmark* SearchEngine::search(std::vector<OpID>& plan){
	auto node = stack.back();
	if(node.s.entails(prob.goal()))
		return NULL;
	// std::cerr << "begin depth " << stack.size() << std::endl;

	node.f = node.g + lmdb.incr_eval(node.s);

	applicable.clear();
	lmdb.applicable_actions(node.s, applicable);
	for(auto op : applicable){
		node.succ_lms.emplace_back(std::pair<OpID, Landmark*>(op, NULL));
	}

	expansions += 1;
	if(expansions % 1000 == 0)
		std::cerr << "." ;
	if(expansions % 80000 == 0)
		std::cerr << std::endl;

	for(auto& slm : node.succ_lms){
		auto op = slm.first;
		auto act = prob.actions()[slm.first];
		stack.emplace_back(Node(node, act));
		auto lm = lmdb.progress_lms(stack.back().s, op);
		if(lm == NULL || lm->cost_assigned <= 1e-6){
			plan.push_back(op);
			lm = search(plan);
			if(lm == NULL)
				return NULL;
			plan.pop_back();
		}
		slm.second = lm;
		lmdb.backtrack(node.s, op);
		stack.pop_back();
	}
	auto glm = lmdb.regress(node.s, node.succ_lms);

	std::cerr << "depth " << stack.size() << std::endl;
	std::cerr << "applic_lms " << lmdb.num_active() << std::endl;
	node.f = node.g + lmdb.incr_eval(node.s);
	std::cerr << "f " << node.f << std::endl;
	std::cerr << "lmc " << glm->cost_assigned << std::endl;
	std::cerr << std::endl;
	//glm->dump(prob, node.s);
	return glm;
}

double SearchEngine::lb(){
	return lmdb.incr_eval(stack.back().s);
}

bool SearchEngine::in_stack(const State& s){
	return false;
}


}
