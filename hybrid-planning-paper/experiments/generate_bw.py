
import pymas.test.blocksworld as bw
import mako.template

import pddlparse

def gen_model(pddlfile, makofile):
    tpl = mako.template.Template(filename=makofile)
    init, goal = pddlparse.parse_blocksworld(pddlfile)
    return tpl.render(init=init, goal=goal)


if __name__ == '__main__':
    import sys
    print gen_model(sys.argv[1], "problem.mako")
