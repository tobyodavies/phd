(define (problem BW-rand-7)
(:domain blocksworld)
(:objects b1 b2 b3 b4 b5 b6 b7  - block
	  h1 h2 h3 h4 h5 h6 h7  - hand)
(:init
(handempty h1)(handempty h2)(handempty h3)(handempty h4)(handempty h5)(handempty h6)(handempty h7)
(on b1 b2)
(ontable b2)
(ontable b3)
(on b4 b7)
(on b5 b6)
(ontable b6)
(on b7 b5)
(clear b1)
(clear b3)
(clear b4)
)
(:goal
(and
(on b1 b7)
(on b2 b4)
(on b3 b2)
(on b4 b6))
)
(:metric minimize (total-time)))



