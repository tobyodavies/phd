(define (problem bfrs_256)
(:domain bfrs)
(:objects
    p2 - location
    p1 - location
    d - location
    m1 - location
    m2 - location
    y - location
    x - location
    o0 - order
    o1 - order
    o2 - order
    o3 - order
    o4 - order
    o5 - order
    o6 - order
    o7 - order
    o8 - order
    o9 - order
    o10 - order
    o11 - order
    o12 - order
    o13 - order
    o14 - order
    o15 - order
    o16 - order
    o17 - order
    o18 - order
    o19 - order
    o20 - order
    o21 - order
    o22 - order
    o23 - order
    o24 - order
    o25 - order
    o26 - order
    o27 - order
    o28 - order
    o29 - order
    o30 - order
    o31 - order
    o32 - order
    o33 - order
    o34 - order
    o35 - order
    o36 - order
    o37 - order
    o38 - order
    o39 - order
    o40 - order
    o41 - order
    o42 - order
    o43 - order
    o44 - order
    o45 - order
    o46 - order
    o47 - order
    o48 - order
    o49 - order
    o50 - order
    o51 - order
    o52 - order
    o53 - order
    o54 - order
    o55 - order
    o56 - order
    o57 - order
    o58 - order
    o59 - order
    o60 - order
    o61 - order
    o62 - order
    o63 - order
    o64 - order
    o65 - order
    o66 - order
    o67 - order
    o68 - order
    o69 - order
    o70 - order
    o71 - order
    o72 - order
    o73 - order
    o74 - order
    o75 - order
    o76 - order
    o77 - order
    o78 - order
    o79 - order
    o80 - order
    o81 - order
    o82 - order
    o83 - order
    o84 - order
    o85 - order
    o86 - order
    o87 - order
    o88 - order
    o89 - order
    o90 - order
    o91 - order
    o92 - order
    o93 - order
    o94 - order
    o95 - order
    o96 - order
    o97 - order
    o98 - order
    o99 - order
    o100 - order
    o101 - order
    o102 - order
    o103 - order
    o104 - order
    o105 - order
    o106 - order
    o107 - order
    o108 - order
    o109 - order
    o110 - order
    o111 - order
    o112 - order
    o113 - order
    o114 - order
    o115 - order
    o116 - order
    o117 - order
    o118 - order
    o119 - order
    o120 - order
    o121 - order
    o122 - order
    o123 - order
    o124 - order
    o125 - order
    o126 - order
    o127 - order
    o128 - order
    o129 - order
    o130 - order
    o131 - order
    o132 - order
    o133 - order
    o134 - order
    o135 - order
    o136 - order
    o137 - order
    o138 - order
    o139 - order
    o140 - order
    o141 - order
    o142 - order
    o143 - order
    o144 - order
    o145 - order
    o146 - order
    o147 - order
    o148 - order
    o149 - order
    o150 - order
    o151 - order
    o152 - order
    o153 - order
    o154 - order
    o155 - order
    o156 - order
    o157 - order
    o158 - order
    o159 - order
    o160 - order
    o161 - order
    o162 - order
    o163 - order
    o164 - order
    o165 - order
    o166 - order
    o167 - order
    o168 - order
    o169 - order
    o170 - order
    o171 - order
    o172 - order
    o173 - order
    o174 - order
    o175 - order
    o176 - order
    o177 - order
    o178 - order
    o179 - order
    o180 - order
    o181 - order
    o182 - order
    o183 - order
    o184 - order
    o185 - order
    o186 - order
    o187 - order
    o188 - order
    o189 - order
    o190 - order
    o191 - order
    o192 - order
    o193 - order
    o194 - order
    o195 - order
    o196 - order
    o197 - order
    o198 - order
    o199 - order
    o200 - order
    o201 - order
    o202 - order
    o203 - order
    o204 - order
    o205 - order
    o206 - order
    o207 - order
    o208 - order
    o209 - order
    o210 - order
    o211 - order
    o212 - order
    o213 - order
    o214 - order
    o215 - order
    o216 - order
    o217 - order
    o218 - order
    o219 - order
    o220 - order
    o221 - order
    o222 - order
    o223 - order
    o224 - order
    o225 - order
    o226 - order
    o227 - order
    o228 - order
    o229 - order
    o230 - order
    o231 - order
    o232 - order
    o233 - order
    o234 - order
    o235 - order
    o236 - order
    o237 - order
    o238 - order
    o239 - order
    o240 - order
    o241 - order
    o242 - order
    o243 - order
    o244 - order
    o245 - order
    o246 - order
    o247 - order
    o248 - order
    o249 - order
    o250 - order
    o251 - order
    o252 - order
    o253 - order
    o254 - order
    o255 - order
    c0 - consist
    c1 - consist
    c2 - consist
    c3 - consist
    c4 - consist
    c5 - consist
    c6 - consist
    c7 - consist
    c8 - consist
    c9 - consist
    c10 - consist
    c11 - consist
    c12 - consist
    c13 - consist
    c14 - consist
    c15 - consist
    c16 - consist
    c17 - consist
    c18 - consist
    c19 - consist
    c20 - consist
    c21 - consist
    c22 - consist
    c23 - consist
    c24 - consist
    c25 - consist
    c26 - consist
    c27 - consist
    c28 - consist
    c29 - consist
    c30 - consist
    c31 - consist
    c32 - consist
    c33 - consist
    c34 - consist
    c35 - consist
    c36 - consist
    c37 - consist
    c38 - consist
    c39 - consist
    c40 - consist
    c41 - consist
    c42 - consist
    c43 - consist
    c44 - consist
    c45 - consist
    c46 - consist
    c47 - consist
    c48 - consist
    c49 - consist
    c50 - consist
    c51 - consist
    c52 - consist
    c53 - consist
    c54 - consist
    c55 - consist
    c56 - consist
    c57 - consist
    c58 - consist
    c59 - consist
    c60 - consist
    c61 - consist
    c62 - consist
    c63 - consist
    c64 - consist
    c65 - consist
    c66 - consist
    c67 - consist
    c68 - consist
    c69 - consist
    c70 - consist
    c71 - consist
    c72 - consist
    c73 - consist
    c74 - consist
    c75 - consist
    c76 - consist
    c77 - consist
    c78 - consist
    c79 - consist
    c80 - consist
    c81 - consist
    c82 - consist
    c83 - consist
    c84 - consist
    c85 - consist
    c86 - consist
    c87 - consist
    c88 - consist
    c89 - consist
    c90 - consist
    c91 - consist
    c92 - consist
    c93 - consist
    c94 - consist
    c95 - consist
    c96 - consist
    c97 - consist
    c98 - consist
    c99 - consist
    c100 - consist
    c101 - consist
    c102 - consist
    c103 - consist
    c104 - consist
    c105 - consist
    c106 - consist
    c107 - consist
    c108 - consist
    c109 - consist
    c110 - consist
    c111 - consist
    c112 - consist
    c113 - consist
    c114 - consist
    c115 - consist
    c116 - consist
    c117 - consist
    c118 - consist
    c119 - consist
    c120 - consist
    c121 - consist
    c122 - consist
    c123 - consist
    c124 - consist
    c125 - consist
    c126 - consist
    c127 - consist
    c128 - consist
    c129 - consist
    c130 - consist
    c131 - consist
    c132 - consist
    c133 - consist
    c134 - consist
    c135 - consist
    c136 - consist
    c137 - consist
    c138 - consist
    c139 - consist
    c140 - consist
    c141 - consist
    c142 - consist
    c143 - consist
    c144 - consist
    c145 - consist
    c146 - consist
    c147 - consist
    c148 - consist
    c149 - consist
    c150 - consist
    c151 - consist
    c152 - consist
    c153 - consist
    c154 - consist
    c155 - consist
    c156 - consist
    c157 - consist
    c158 - consist
    c159 - consist
    c160 - consist
    c161 - consist
    c162 - consist
    c163 - consist
    c164 - consist
    c165 - consist
    c166 - consist
    c167 - consist
    c168 - consist
    c169 - consist
    c170 - consist
    c171 - consist
    c172 - consist
    c173 - consist
    c174 - consist
    c175 - consist
    c176 - consist
    c177 - consist
    c178 - consist
    c179 - consist
    c180 - consist
    c181 - consist
    c182 - consist
    c183 - consist
    c184 - consist
    c185 - consist
    c186 - consist
    c187 - consist
    c188 - consist
    c189 - consist
    c190 - consist
    c191 - consist
    c192 - consist
    c193 - consist
    c194 - consist
    c195 - consist
    c196 - consist
    c197 - consist
    c198 - consist
    c199 - consist
    c200 - consist
    c201 - consist
    c202 - consist
    c203 - consist
    c204 - consist
    c205 - consist
    c206 - consist
    c207 - consist
    c208 - consist
    c209 - consist
    c210 - consist
    c211 - consist
    c212 - consist
    c213 - consist
    c214 - consist
    c215 - consist
    c216 - consist
    c217 - consist
    c218 - consist
    c219 - consist
    c220 - consist
    c221 - consist
    c222 - consist
    c223 - consist
    c224 - consist
    c225 - consist
    c226 - consist
    c227 - consist
    c228 - consist
    c229 - consist
    c230 - consist
    c231 - consist
    c232 - consist
    c233 - consist
    c234 - consist
    c235 - consist
    c236 - consist
    c237 - consist
    c238 - consist
    c239 - consist
    c240 - consist
    c241 - consist
    c242 - consist
    c243 - consist
    c244 - consist
    c245 - consist
    c246 - consist
    c247 - consist
    c248 - consist
    c249 - consist
    c250 - consist
    c251 - consist
    c252 - consist
    c253 - consist
    c254 - consist
    c255 - consist
)
(:init
  (is_yard y)
    (clear p2)
    (clear p1)
    (clear d)
    (clear m1)
    (clear m2)
    (clear y)
    (clear x)

    (conn p2 y)
    (conn y p2)
    (conn p1 y)
    (conn y p1)
    (conn d y)
    (conn y d)
    (conn d x)
    (conn x d)
    (conn d m1)
    (conn m1 d)
    (conn d m2)
    (conn m2 d)
    (conn y x)
    (conn x y)

    ;; order o0 ('m1', 'p2', 0, 12)
    (order_mine o0 m1)
    (order_port o0 p2)
    (not-delivered o0)

    ;; consist c0
    (available c0)
    (empty c0)
    (at c0 y)

    (compat c0 o0)

    ;; order o1 ('m1', 'p2', 12, 24)
    (order_mine o1 m1)
    (order_port o1 p2)
    (not-delivered o1)

    ;; consist c1
    (available c1)
    (empty c1)
    (at c1 y)

    (compat c1 o1)

    ;; order o2 ('m1', 'p2', 24, 36)
    (order_mine o2 m1)
    (order_port o2 p2)
    (not-delivered o2)

    ;; consist c2
    (available c2)
    (empty c2)
    (at c2 y)

    (compat c2 o2)

    ;; order o3 ('m1', 'p2', 36, 48)
    (order_mine o3 m1)
    (order_port o3 p2)
    (not-delivered o3)

    ;; consist c3
    (available c3)
    (empty c3)
    (at c3 y)

    (compat c3 o3)

    ;; order o4 ('m1', 'p2', 48, 60)
    (order_mine o4 m1)
    (order_port o4 p2)
    (not-delivered o4)

    ;; consist c4
    (available c4)
    (empty c4)
    (at c4 y)

    (compat c4 o4)

    ;; order o5 ('m1', 'p2', 60, 72)
    (order_mine o5 m1)
    (order_port o5 p2)
    (not-delivered o5)

    ;; consist c5
    (available c5)
    (empty c5)
    (at c5 y)

    (compat c5 o5)

    ;; order o6 ('m1', 'p2', 72, 84)
    (order_mine o6 m1)
    (order_port o6 p2)
    (not-delivered o6)

    ;; consist c6
    (available c6)
    (empty c6)
    (at c6 y)

    (compat c6 o6)

    ;; order o7 ('m1', 'p2', 84, 96)
    (order_mine o7 m1)
    (order_port o7 p2)
    (not-delivered o7)

    ;; consist c7
    (available c7)
    (empty c7)
    (at c7 y)

    (compat c7 o7)

    ;; order o8 ('m1', 'p2', 96, 108)
    (order_mine o8 m1)
    (order_port o8 p2)
    (not-delivered o8)

    ;; consist c8
    (available c8)
    (empty c8)
    (at c8 y)

    (compat c8 o8)

    ;; order o9 ('m1', 'p2', 108, 120)
    (order_mine o9 m1)
    (order_port o9 p2)
    (not-delivered o9)

    ;; consist c9
    (available c9)
    (empty c9)
    (at c9 y)

    (compat c9 o9)

    ;; order o10 ('m1', 'p2', 120, 132)
    (order_mine o10 m1)
    (order_port o10 p2)
    (not-delivered o10)

    ;; consist c10
    (available c10)
    (empty c10)
    (at c10 y)

    (compat c10 o10)

    ;; order o11 ('m1', 'p2', 132, 144)
    (order_mine o11 m1)
    (order_port o11 p2)
    (not-delivered o11)

    ;; consist c11
    (available c11)
    (empty c11)
    (at c11 y)

    (compat c11 o11)

    ;; order o12 ('m1', 'p2', 144, 156)
    (order_mine o12 m1)
    (order_port o12 p2)
    (not-delivered o12)

    ;; consist c12
    (available c12)
    (empty c12)
    (at c12 y)

    (compat c12 o12)

    ;; order o13 ('m1', 'p2', 156, 168)
    (order_mine o13 m1)
    (order_port o13 p2)
    (not-delivered o13)

    ;; consist c13
    (available c13)
    (empty c13)
    (at c13 y)

    (compat c13 o13)

    ;; order o14 ('m1', 'p2', 168, 180)
    (order_mine o14 m1)
    (order_port o14 p2)
    (not-delivered o14)

    ;; consist c14
    (available c14)
    (empty c14)
    (at c14 y)

    (compat c14 o14)

    ;; order o15 ('m1', 'p2', 180, 192)
    (order_mine o15 m1)
    (order_port o15 p2)
    (not-delivered o15)

    ;; consist c15
    (available c15)
    (empty c15)
    (at c15 y)

    (compat c15 o15)

    ;; order o16 ('m1', 'p2', 192, 204)
    (order_mine o16 m1)
    (order_port o16 p2)
    (not-delivered o16)

    ;; consist c16
    (available c16)
    (empty c16)
    (at c16 y)

    (compat c16 o16)

    ;; order o17 ('m1', 'p2', 204, 216)
    (order_mine o17 m1)
    (order_port o17 p2)
    (not-delivered o17)

    ;; consist c17
    (available c17)
    (empty c17)
    (at c17 y)

    (compat c17 o17)

    ;; order o18 ('m1', 'p2', 216, 228)
    (order_mine o18 m1)
    (order_port o18 p2)
    (not-delivered o18)

    ;; consist c18
    (available c18)
    (empty c18)
    (at c18 y)

    (compat c18 o18)

    ;; order o19 ('m1', 'p2', 228, 240)
    (order_mine o19 m1)
    (order_port o19 p2)
    (not-delivered o19)

    ;; consist c19
    (available c19)
    (empty c19)
    (at c19 y)

    (compat c19 o19)

    ;; order o20 ('m1', 'p2', 240, 252)
    (order_mine o20 m1)
    (order_port o20 p2)
    (not-delivered o20)

    ;; consist c20
    (available c20)
    (empty c20)
    (at c20 y)

    (compat c20 o20)

    ;; order o21 ('m1', 'p2', 252, 264)
    (order_mine o21 m1)
    (order_port o21 p2)
    (not-delivered o21)

    ;; consist c21
    (available c21)
    (empty c21)
    (at c21 y)

    (compat c21 o21)

    ;; order o22 ('m1', 'p2', 264, 276)
    (order_mine o22 m1)
    (order_port o22 p2)
    (not-delivered o22)

    ;; consist c22
    (available c22)
    (empty c22)
    (at c22 y)

    (compat c22 o22)

    ;; order o23 ('m1', 'p2', 276, 288)
    (order_mine o23 m1)
    (order_port o23 p2)
    (not-delivered o23)

    ;; consist c23
    (available c23)
    (empty c23)
    (at c23 y)

    (compat c23 o23)

    ;; order o24 ('m1', 'p2', 288, 300)
    (order_mine o24 m1)
    (order_port o24 p2)
    (not-delivered o24)

    ;; consist c24
    (available c24)
    (empty c24)
    (at c24 y)

    (compat c24 o24)

    ;; order o25 ('m1', 'p2', 300, 312)
    (order_mine o25 m1)
    (order_port o25 p2)
    (not-delivered o25)

    ;; consist c25
    (available c25)
    (empty c25)
    (at c25 y)

    (compat c25 o25)

    ;; order o26 ('m1', 'p2', 312, 324)
    (order_mine o26 m1)
    (order_port o26 p2)
    (not-delivered o26)

    ;; consist c26
    (available c26)
    (empty c26)
    (at c26 y)

    (compat c26 o26)

    ;; order o27 ('m1', 'p2', 324, 336)
    (order_mine o27 m1)
    (order_port o27 p2)
    (not-delivered o27)

    ;; consist c27
    (available c27)
    (empty c27)
    (at c27 y)

    (compat c27 o27)

    ;; order o28 ('m1', 'p2', 336, 348)
    (order_mine o28 m1)
    (order_port o28 p2)
    (not-delivered o28)

    ;; consist c28
    (available c28)
    (empty c28)
    (at c28 y)

    (compat c28 o28)

    ;; order o29 ('m1', 'p2', 348, 360)
    (order_mine o29 m1)
    (order_port o29 p2)
    (not-delivered o29)

    ;; consist c29
    (available c29)
    (empty c29)
    (at c29 y)

    (compat c29 o29)

    ;; order o30 ('m1', 'p2', 360, 372)
    (order_mine o30 m1)
    (order_port o30 p2)
    (not-delivered o30)

    ;; consist c30
    (available c30)
    (empty c30)
    (at c30 y)

    (compat c30 o30)

    ;; order o31 ('m1', 'p2', 372, 384)
    (order_mine o31 m1)
    (order_port o31 p2)
    (not-delivered o31)

    ;; consist c31
    (available c31)
    (empty c31)
    (at c31 y)

    (compat c31 o31)

    ;; order o32 ('m1', 'p2', 384, 396)
    (order_mine o32 m1)
    (order_port o32 p2)
    (not-delivered o32)

    ;; consist c32
    (available c32)
    (empty c32)
    (at c32 y)

    (compat c32 o32)

    ;; order o33 ('m1', 'p2', 396, 408)
    (order_mine o33 m1)
    (order_port o33 p2)
    (not-delivered o33)

    ;; consist c33
    (available c33)
    (empty c33)
    (at c33 y)

    (compat c33 o33)

    ;; order o34 ('m1', 'p2', 408, 420)
    (order_mine o34 m1)
    (order_port o34 p2)
    (not-delivered o34)

    ;; consist c34
    (available c34)
    (empty c34)
    (at c34 y)

    (compat c34 o34)

    ;; order o35 ('m1', 'p2', 420, 432)
    (order_mine o35 m1)
    (order_port o35 p2)
    (not-delivered o35)

    ;; consist c35
    (available c35)
    (empty c35)
    (at c35 y)

    (compat c35 o35)

    ;; order o36 ('m1', 'p2', 432, 444)
    (order_mine o36 m1)
    (order_port o36 p2)
    (not-delivered o36)

    ;; consist c36
    (available c36)
    (empty c36)
    (at c36 y)

    (compat c36 o36)

    ;; order o37 ('m1', 'p2', 444, 456)
    (order_mine o37 m1)
    (order_port o37 p2)
    (not-delivered o37)

    ;; consist c37
    (available c37)
    (empty c37)
    (at c37 y)

    (compat c37 o37)

    ;; order o38 ('m1', 'p2', 456, 468)
    (order_mine o38 m1)
    (order_port o38 p2)
    (not-delivered o38)

    ;; consist c38
    (available c38)
    (empty c38)
    (at c38 y)

    (compat c38 o38)

    ;; order o39 ('m1', 'p2', 468, 480)
    (order_mine o39 m1)
    (order_port o39 p2)
    (not-delivered o39)

    ;; consist c39
    (available c39)
    (empty c39)
    (at c39 y)

    (compat c39 o39)

    ;; order o40 ('m1', 'p2', 480, 492)
    (order_mine o40 m1)
    (order_port o40 p2)
    (not-delivered o40)

    ;; consist c40
    (available c40)
    (empty c40)
    (at c40 y)

    (compat c40 o40)

    ;; order o41 ('m1', 'p2', 492, 504)
    (order_mine o41 m1)
    (order_port o41 p2)
    (not-delivered o41)

    ;; consist c41
    (available c41)
    (empty c41)
    (at c41 y)

    (compat c41 o41)

    ;; order o42 ('m1', 'p2', 504, 516)
    (order_mine o42 m1)
    (order_port o42 p2)
    (not-delivered o42)

    ;; consist c42
    (available c42)
    (empty c42)
    (at c42 y)

    (compat c42 o42)

    ;; order o43 ('m1', 'p2', 516, 528)
    (order_mine o43 m1)
    (order_port o43 p2)
    (not-delivered o43)

    ;; consist c43
    (available c43)
    (empty c43)
    (at c43 y)

    (compat c43 o43)

    ;; order o44 ('m1', 'p2', 528, 540)
    (order_mine o44 m1)
    (order_port o44 p2)
    (not-delivered o44)

    ;; consist c44
    (available c44)
    (empty c44)
    (at c44 y)

    (compat c44 o44)

    ;; order o45 ('m1', 'p2', 540, 552)
    (order_mine o45 m1)
    (order_port o45 p2)
    (not-delivered o45)

    ;; consist c45
    (available c45)
    (empty c45)
    (at c45 y)

    (compat c45 o45)

    ;; order o46 ('m1', 'p2', 552, 564)
    (order_mine o46 m1)
    (order_port o46 p2)
    (not-delivered o46)

    ;; consist c46
    (available c46)
    (empty c46)
    (at c46 y)

    (compat c46 o46)

    ;; order o47 ('m1', 'p2', 564, 576)
    (order_mine o47 m1)
    (order_port o47 p2)
    (not-delivered o47)

    ;; consist c47
    (available c47)
    (empty c47)
    (at c47 y)

    (compat c47 o47)

    ;; order o48 ('m1', 'p2', 576, 588)
    (order_mine o48 m1)
    (order_port o48 p2)
    (not-delivered o48)

    ;; consist c48
    (available c48)
    (empty c48)
    (at c48 y)

    (compat c48 o48)

    ;; order o49 ('m1', 'p2', 588, 600)
    (order_mine o49 m1)
    (order_port o49 p2)
    (not-delivered o49)

    ;; consist c49
    (available c49)
    (empty c49)
    (at c49 y)

    (compat c49 o49)

    ;; order o50 ('m1', 'p2', 600, 612)
    (order_mine o50 m1)
    (order_port o50 p2)
    (not-delivered o50)

    ;; consist c50
    (available c50)
    (empty c50)
    (at c50 y)

    (compat c50 o50)

    ;; order o51 ('m1', 'p2', 612, 624)
    (order_mine o51 m1)
    (order_port o51 p2)
    (not-delivered o51)

    ;; consist c51
    (available c51)
    (empty c51)
    (at c51 y)

    (compat c51 o51)

    ;; order o52 ('m1', 'p2', 624, 636)
    (order_mine o52 m1)
    (order_port o52 p2)
    (not-delivered o52)

    ;; consist c52
    (available c52)
    (empty c52)
    (at c52 y)

    (compat c52 o52)

    ;; order o53 ('m1', 'p2', 636, 648)
    (order_mine o53 m1)
    (order_port o53 p2)
    (not-delivered o53)

    ;; consist c53
    (available c53)
    (empty c53)
    (at c53 y)

    (compat c53 o53)

    ;; order o54 ('m1', 'p2', 648, 660)
    (order_mine o54 m1)
    (order_port o54 p2)
    (not-delivered o54)

    ;; consist c54
    (available c54)
    (empty c54)
    (at c54 y)

    (compat c54 o54)

    ;; order o55 ('m1', 'p2', 660, 672)
    (order_mine o55 m1)
    (order_port o55 p2)
    (not-delivered o55)

    ;; consist c55
    (available c55)
    (empty c55)
    (at c55 y)

    (compat c55 o55)

    ;; order o56 ('m1', 'p2', 672, 684)
    (order_mine o56 m1)
    (order_port o56 p2)
    (not-delivered o56)

    ;; consist c56
    (available c56)
    (empty c56)
    (at c56 y)

    (compat c56 o56)

    ;; order o57 ('m1', 'p2', 684, 696)
    (order_mine o57 m1)
    (order_port o57 p2)
    (not-delivered o57)

    ;; consist c57
    (available c57)
    (empty c57)
    (at c57 y)

    (compat c57 o57)

    ;; order o58 ('m1', 'p2', 696, 708)
    (order_mine o58 m1)
    (order_port o58 p2)
    (not-delivered o58)

    ;; consist c58
    (available c58)
    (empty c58)
    (at c58 y)

    (compat c58 o58)

    ;; order o59 ('m1', 'p2', 708, 720)
    (order_mine o59 m1)
    (order_port o59 p2)
    (not-delivered o59)

    ;; consist c59
    (available c59)
    (empty c59)
    (at c59 y)

    (compat c59 o59)

    ;; order o60 ('m1', 'p2', 720, 732)
    (order_mine o60 m1)
    (order_port o60 p2)
    (not-delivered o60)

    ;; consist c60
    (available c60)
    (empty c60)
    (at c60 y)

    (compat c60 o60)

    ;; order o61 ('m1', 'p2', 732, 744)
    (order_mine o61 m1)
    (order_port o61 p2)
    (not-delivered o61)

    ;; consist c61
    (available c61)
    (empty c61)
    (at c61 y)

    (compat c61 o61)

    ;; order o62 ('m1', 'p2', 744, 756)
    (order_mine o62 m1)
    (order_port o62 p2)
    (not-delivered o62)

    ;; consist c62
    (available c62)
    (empty c62)
    (at c62 y)

    (compat c62 o62)

    ;; order o63 ('m1', 'p2', 756, 768)
    (order_mine o63 m1)
    (order_port o63 p2)
    (not-delivered o63)

    ;; consist c63
    (available c63)
    (empty c63)
    (at c63 y)

    (compat c63 o63)

    ;; order o64 ('m1', 'p1', 0, 12)
    (order_mine o64 m1)
    (order_port o64 p1)
    (not-delivered o64)

    ;; consist c64
    (available c64)
    (empty c64)
    (at c64 y)

    (compat c64 o64)

    ;; order o65 ('m1', 'p1', 12, 24)
    (order_mine o65 m1)
    (order_port o65 p1)
    (not-delivered o65)

    ;; consist c65
    (available c65)
    (empty c65)
    (at c65 y)

    (compat c65 o65)

    ;; order o66 ('m1', 'p1', 24, 36)
    (order_mine o66 m1)
    (order_port o66 p1)
    (not-delivered o66)

    ;; consist c66
    (available c66)
    (empty c66)
    (at c66 y)

    (compat c66 o66)

    ;; order o67 ('m1', 'p1', 36, 48)
    (order_mine o67 m1)
    (order_port o67 p1)
    (not-delivered o67)

    ;; consist c67
    (available c67)
    (empty c67)
    (at c67 y)

    (compat c67 o67)

    ;; order o68 ('m1', 'p1', 48, 60)
    (order_mine o68 m1)
    (order_port o68 p1)
    (not-delivered o68)

    ;; consist c68
    (available c68)
    (empty c68)
    (at c68 y)

    (compat c68 o68)

    ;; order o69 ('m1', 'p1', 60, 72)
    (order_mine o69 m1)
    (order_port o69 p1)
    (not-delivered o69)

    ;; consist c69
    (available c69)
    (empty c69)
    (at c69 y)

    (compat c69 o69)

    ;; order o70 ('m1', 'p1', 72, 84)
    (order_mine o70 m1)
    (order_port o70 p1)
    (not-delivered o70)

    ;; consist c70
    (available c70)
    (empty c70)
    (at c70 y)

    (compat c70 o70)

    ;; order o71 ('m1', 'p1', 84, 96)
    (order_mine o71 m1)
    (order_port o71 p1)
    (not-delivered o71)

    ;; consist c71
    (available c71)
    (empty c71)
    (at c71 y)

    (compat c71 o71)

    ;; order o72 ('m1', 'p1', 96, 108)
    (order_mine o72 m1)
    (order_port o72 p1)
    (not-delivered o72)

    ;; consist c72
    (available c72)
    (empty c72)
    (at c72 y)

    (compat c72 o72)

    ;; order o73 ('m1', 'p1', 108, 120)
    (order_mine o73 m1)
    (order_port o73 p1)
    (not-delivered o73)

    ;; consist c73
    (available c73)
    (empty c73)
    (at c73 y)

    (compat c73 o73)

    ;; order o74 ('m1', 'p1', 120, 132)
    (order_mine o74 m1)
    (order_port o74 p1)
    (not-delivered o74)

    ;; consist c74
    (available c74)
    (empty c74)
    (at c74 y)

    (compat c74 o74)

    ;; order o75 ('m1', 'p1', 132, 144)
    (order_mine o75 m1)
    (order_port o75 p1)
    (not-delivered o75)

    ;; consist c75
    (available c75)
    (empty c75)
    (at c75 y)

    (compat c75 o75)

    ;; order o76 ('m1', 'p1', 144, 156)
    (order_mine o76 m1)
    (order_port o76 p1)
    (not-delivered o76)

    ;; consist c76
    (available c76)
    (empty c76)
    (at c76 y)

    (compat c76 o76)

    ;; order o77 ('m1', 'p1', 156, 168)
    (order_mine o77 m1)
    (order_port o77 p1)
    (not-delivered o77)

    ;; consist c77
    (available c77)
    (empty c77)
    (at c77 y)

    (compat c77 o77)

    ;; order o78 ('m1', 'p1', 168, 180)
    (order_mine o78 m1)
    (order_port o78 p1)
    (not-delivered o78)

    ;; consist c78
    (available c78)
    (empty c78)
    (at c78 y)

    (compat c78 o78)

    ;; order o79 ('m1', 'p1', 180, 192)
    (order_mine o79 m1)
    (order_port o79 p1)
    (not-delivered o79)

    ;; consist c79
    (available c79)
    (empty c79)
    (at c79 y)

    (compat c79 o79)

    ;; order o80 ('m1', 'p1', 192, 204)
    (order_mine o80 m1)
    (order_port o80 p1)
    (not-delivered o80)

    ;; consist c80
    (available c80)
    (empty c80)
    (at c80 y)

    (compat c80 o80)

    ;; order o81 ('m1', 'p1', 204, 216)
    (order_mine o81 m1)
    (order_port o81 p1)
    (not-delivered o81)

    ;; consist c81
    (available c81)
    (empty c81)
    (at c81 y)

    (compat c81 o81)

    ;; order o82 ('m1', 'p1', 216, 228)
    (order_mine o82 m1)
    (order_port o82 p1)
    (not-delivered o82)

    ;; consist c82
    (available c82)
    (empty c82)
    (at c82 y)

    (compat c82 o82)

    ;; order o83 ('m1', 'p1', 228, 240)
    (order_mine o83 m1)
    (order_port o83 p1)
    (not-delivered o83)

    ;; consist c83
    (available c83)
    (empty c83)
    (at c83 y)

    (compat c83 o83)

    ;; order o84 ('m1', 'p1', 240, 252)
    (order_mine o84 m1)
    (order_port o84 p1)
    (not-delivered o84)

    ;; consist c84
    (available c84)
    (empty c84)
    (at c84 y)

    (compat c84 o84)

    ;; order o85 ('m1', 'p1', 252, 264)
    (order_mine o85 m1)
    (order_port o85 p1)
    (not-delivered o85)

    ;; consist c85
    (available c85)
    (empty c85)
    (at c85 y)

    (compat c85 o85)

    ;; order o86 ('m1', 'p1', 264, 276)
    (order_mine o86 m1)
    (order_port o86 p1)
    (not-delivered o86)

    ;; consist c86
    (available c86)
    (empty c86)
    (at c86 y)

    (compat c86 o86)

    ;; order o87 ('m1', 'p1', 276, 288)
    (order_mine o87 m1)
    (order_port o87 p1)
    (not-delivered o87)

    ;; consist c87
    (available c87)
    (empty c87)
    (at c87 y)

    (compat c87 o87)

    ;; order o88 ('m1', 'p1', 288, 300)
    (order_mine o88 m1)
    (order_port o88 p1)
    (not-delivered o88)

    ;; consist c88
    (available c88)
    (empty c88)
    (at c88 y)

    (compat c88 o88)

    ;; order o89 ('m1', 'p1', 300, 312)
    (order_mine o89 m1)
    (order_port o89 p1)
    (not-delivered o89)

    ;; consist c89
    (available c89)
    (empty c89)
    (at c89 y)

    (compat c89 o89)

    ;; order o90 ('m1', 'p1', 312, 324)
    (order_mine o90 m1)
    (order_port o90 p1)
    (not-delivered o90)

    ;; consist c90
    (available c90)
    (empty c90)
    (at c90 y)

    (compat c90 o90)

    ;; order o91 ('m1', 'p1', 324, 336)
    (order_mine o91 m1)
    (order_port o91 p1)
    (not-delivered o91)

    ;; consist c91
    (available c91)
    (empty c91)
    (at c91 y)

    (compat c91 o91)

    ;; order o92 ('m1', 'p1', 336, 348)
    (order_mine o92 m1)
    (order_port o92 p1)
    (not-delivered o92)

    ;; consist c92
    (available c92)
    (empty c92)
    (at c92 y)

    (compat c92 o92)

    ;; order o93 ('m1', 'p1', 348, 360)
    (order_mine o93 m1)
    (order_port o93 p1)
    (not-delivered o93)

    ;; consist c93
    (available c93)
    (empty c93)
    (at c93 y)

    (compat c93 o93)

    ;; order o94 ('m1', 'p1', 360, 372)
    (order_mine o94 m1)
    (order_port o94 p1)
    (not-delivered o94)

    ;; consist c94
    (available c94)
    (empty c94)
    (at c94 y)

    (compat c94 o94)

    ;; order o95 ('m1', 'p1', 372, 384)
    (order_mine o95 m1)
    (order_port o95 p1)
    (not-delivered o95)

    ;; consist c95
    (available c95)
    (empty c95)
    (at c95 y)

    (compat c95 o95)

    ;; order o96 ('m1', 'p1', 384, 396)
    (order_mine o96 m1)
    (order_port o96 p1)
    (not-delivered o96)

    ;; consist c96
    (available c96)
    (empty c96)
    (at c96 y)

    (compat c96 o96)

    ;; order o97 ('m1', 'p1', 396, 408)
    (order_mine o97 m1)
    (order_port o97 p1)
    (not-delivered o97)

    ;; consist c97
    (available c97)
    (empty c97)
    (at c97 y)

    (compat c97 o97)

    ;; order o98 ('m1', 'p1', 408, 420)
    (order_mine o98 m1)
    (order_port o98 p1)
    (not-delivered o98)

    ;; consist c98
    (available c98)
    (empty c98)
    (at c98 y)

    (compat c98 o98)

    ;; order o99 ('m1', 'p1', 420, 432)
    (order_mine o99 m1)
    (order_port o99 p1)
    (not-delivered o99)

    ;; consist c99
    (available c99)
    (empty c99)
    (at c99 y)

    (compat c99 o99)

    ;; order o100 ('m1', 'p1', 432, 444)
    (order_mine o100 m1)
    (order_port o100 p1)
    (not-delivered o100)

    ;; consist c100
    (available c100)
    (empty c100)
    (at c100 y)

    (compat c100 o100)

    ;; order o101 ('m1', 'p1', 444, 456)
    (order_mine o101 m1)
    (order_port o101 p1)
    (not-delivered o101)

    ;; consist c101
    (available c101)
    (empty c101)
    (at c101 y)

    (compat c101 o101)

    ;; order o102 ('m1', 'p1', 456, 468)
    (order_mine o102 m1)
    (order_port o102 p1)
    (not-delivered o102)

    ;; consist c102
    (available c102)
    (empty c102)
    (at c102 y)

    (compat c102 o102)

    ;; order o103 ('m1', 'p1', 468, 480)
    (order_mine o103 m1)
    (order_port o103 p1)
    (not-delivered o103)

    ;; consist c103
    (available c103)
    (empty c103)
    (at c103 y)

    (compat c103 o103)

    ;; order o104 ('m1', 'p1', 480, 492)
    (order_mine o104 m1)
    (order_port o104 p1)
    (not-delivered o104)

    ;; consist c104
    (available c104)
    (empty c104)
    (at c104 y)

    (compat c104 o104)

    ;; order o105 ('m1', 'p1', 492, 504)
    (order_mine o105 m1)
    (order_port o105 p1)
    (not-delivered o105)

    ;; consist c105
    (available c105)
    (empty c105)
    (at c105 y)

    (compat c105 o105)

    ;; order o106 ('m1', 'p1', 504, 516)
    (order_mine o106 m1)
    (order_port o106 p1)
    (not-delivered o106)

    ;; consist c106
    (available c106)
    (empty c106)
    (at c106 y)

    (compat c106 o106)

    ;; order o107 ('m1', 'p1', 516, 528)
    (order_mine o107 m1)
    (order_port o107 p1)
    (not-delivered o107)

    ;; consist c107
    (available c107)
    (empty c107)
    (at c107 y)

    (compat c107 o107)

    ;; order o108 ('m1', 'p1', 528, 540)
    (order_mine o108 m1)
    (order_port o108 p1)
    (not-delivered o108)

    ;; consist c108
    (available c108)
    (empty c108)
    (at c108 y)

    (compat c108 o108)

    ;; order o109 ('m1', 'p1', 540, 552)
    (order_mine o109 m1)
    (order_port o109 p1)
    (not-delivered o109)

    ;; consist c109
    (available c109)
    (empty c109)
    (at c109 y)

    (compat c109 o109)

    ;; order o110 ('m1', 'p1', 552, 564)
    (order_mine o110 m1)
    (order_port o110 p1)
    (not-delivered o110)

    ;; consist c110
    (available c110)
    (empty c110)
    (at c110 y)

    (compat c110 o110)

    ;; order o111 ('m1', 'p1', 564, 576)
    (order_mine o111 m1)
    (order_port o111 p1)
    (not-delivered o111)

    ;; consist c111
    (available c111)
    (empty c111)
    (at c111 y)

    (compat c111 o111)

    ;; order o112 ('m1', 'p1', 576, 588)
    (order_mine o112 m1)
    (order_port o112 p1)
    (not-delivered o112)

    ;; consist c112
    (available c112)
    (empty c112)
    (at c112 y)

    (compat c112 o112)

    ;; order o113 ('m1', 'p1', 588, 600)
    (order_mine o113 m1)
    (order_port o113 p1)
    (not-delivered o113)

    ;; consist c113
    (available c113)
    (empty c113)
    (at c113 y)

    (compat c113 o113)

    ;; order o114 ('m1', 'p1', 600, 612)
    (order_mine o114 m1)
    (order_port o114 p1)
    (not-delivered o114)

    ;; consist c114
    (available c114)
    (empty c114)
    (at c114 y)

    (compat c114 o114)

    ;; order o115 ('m1', 'p1', 612, 624)
    (order_mine o115 m1)
    (order_port o115 p1)
    (not-delivered o115)

    ;; consist c115
    (available c115)
    (empty c115)
    (at c115 y)

    (compat c115 o115)

    ;; order o116 ('m1', 'p1', 624, 636)
    (order_mine o116 m1)
    (order_port o116 p1)
    (not-delivered o116)

    ;; consist c116
    (available c116)
    (empty c116)
    (at c116 y)

    (compat c116 o116)

    ;; order o117 ('m1', 'p1', 636, 648)
    (order_mine o117 m1)
    (order_port o117 p1)
    (not-delivered o117)

    ;; consist c117
    (available c117)
    (empty c117)
    (at c117 y)

    (compat c117 o117)

    ;; order o118 ('m1', 'p1', 648, 660)
    (order_mine o118 m1)
    (order_port o118 p1)
    (not-delivered o118)

    ;; consist c118
    (available c118)
    (empty c118)
    (at c118 y)

    (compat c118 o118)

    ;; order o119 ('m1', 'p1', 660, 672)
    (order_mine o119 m1)
    (order_port o119 p1)
    (not-delivered o119)

    ;; consist c119
    (available c119)
    (empty c119)
    (at c119 y)

    (compat c119 o119)

    ;; order o120 ('m1', 'p1', 672, 684)
    (order_mine o120 m1)
    (order_port o120 p1)
    (not-delivered o120)

    ;; consist c120
    (available c120)
    (empty c120)
    (at c120 y)

    (compat c120 o120)

    ;; order o121 ('m1', 'p1', 684, 696)
    (order_mine o121 m1)
    (order_port o121 p1)
    (not-delivered o121)

    ;; consist c121
    (available c121)
    (empty c121)
    (at c121 y)

    (compat c121 o121)

    ;; order o122 ('m1', 'p1', 696, 708)
    (order_mine o122 m1)
    (order_port o122 p1)
    (not-delivered o122)

    ;; consist c122
    (available c122)
    (empty c122)
    (at c122 y)

    (compat c122 o122)

    ;; order o123 ('m1', 'p1', 708, 720)
    (order_mine o123 m1)
    (order_port o123 p1)
    (not-delivered o123)

    ;; consist c123
    (available c123)
    (empty c123)
    (at c123 y)

    (compat c123 o123)

    ;; order o124 ('m1', 'p1', 720, 732)
    (order_mine o124 m1)
    (order_port o124 p1)
    (not-delivered o124)

    ;; consist c124
    (available c124)
    (empty c124)
    (at c124 y)

    (compat c124 o124)

    ;; order o125 ('m1', 'p1', 732, 744)
    (order_mine o125 m1)
    (order_port o125 p1)
    (not-delivered o125)

    ;; consist c125
    (available c125)
    (empty c125)
    (at c125 y)

    (compat c125 o125)

    ;; order o126 ('m1', 'p1', 744, 756)
    (order_mine o126 m1)
    (order_port o126 p1)
    (not-delivered o126)

    ;; consist c126
    (available c126)
    (empty c126)
    (at c126 y)

    (compat c126 o126)

    ;; order o127 ('m1', 'p1', 756, 768)
    (order_mine o127 m1)
    (order_port o127 p1)
    (not-delivered o127)

    ;; consist c127
    (available c127)
    (empty c127)
    (at c127 y)

    (compat c127 o127)

    ;; order o128 ('m2', 'p2', 0, 12)
    (order_mine o128 m2)
    (order_port o128 p2)
    (not-delivered o128)

    ;; consist c128
    (available c128)
    (empty c128)
    (at c128 y)

    (compat c128 o128)

    ;; order o129 ('m2', 'p2', 12, 24)
    (order_mine o129 m2)
    (order_port o129 p2)
    (not-delivered o129)

    ;; consist c129
    (available c129)
    (empty c129)
    (at c129 y)

    (compat c129 o129)

    ;; order o130 ('m2', 'p2', 24, 36)
    (order_mine o130 m2)
    (order_port o130 p2)
    (not-delivered o130)

    ;; consist c130
    (available c130)
    (empty c130)
    (at c130 y)

    (compat c130 o130)

    ;; order o131 ('m2', 'p2', 36, 48)
    (order_mine o131 m2)
    (order_port o131 p2)
    (not-delivered o131)

    ;; consist c131
    (available c131)
    (empty c131)
    (at c131 y)

    (compat c131 o131)

    ;; order o132 ('m2', 'p2', 48, 60)
    (order_mine o132 m2)
    (order_port o132 p2)
    (not-delivered o132)

    ;; consist c132
    (available c132)
    (empty c132)
    (at c132 y)

    (compat c132 o132)

    ;; order o133 ('m2', 'p2', 60, 72)
    (order_mine o133 m2)
    (order_port o133 p2)
    (not-delivered o133)

    ;; consist c133
    (available c133)
    (empty c133)
    (at c133 y)

    (compat c133 o133)

    ;; order o134 ('m2', 'p2', 72, 84)
    (order_mine o134 m2)
    (order_port o134 p2)
    (not-delivered o134)

    ;; consist c134
    (available c134)
    (empty c134)
    (at c134 y)

    (compat c134 o134)

    ;; order o135 ('m2', 'p2', 84, 96)
    (order_mine o135 m2)
    (order_port o135 p2)
    (not-delivered o135)

    ;; consist c135
    (available c135)
    (empty c135)
    (at c135 y)

    (compat c135 o135)

    ;; order o136 ('m2', 'p2', 96, 108)
    (order_mine o136 m2)
    (order_port o136 p2)
    (not-delivered o136)

    ;; consist c136
    (available c136)
    (empty c136)
    (at c136 y)

    (compat c136 o136)

    ;; order o137 ('m2', 'p2', 108, 120)
    (order_mine o137 m2)
    (order_port o137 p2)
    (not-delivered o137)

    ;; consist c137
    (available c137)
    (empty c137)
    (at c137 y)

    (compat c137 o137)

    ;; order o138 ('m2', 'p2', 120, 132)
    (order_mine o138 m2)
    (order_port o138 p2)
    (not-delivered o138)

    ;; consist c138
    (available c138)
    (empty c138)
    (at c138 y)

    (compat c138 o138)

    ;; order o139 ('m2', 'p2', 132, 144)
    (order_mine o139 m2)
    (order_port o139 p2)
    (not-delivered o139)

    ;; consist c139
    (available c139)
    (empty c139)
    (at c139 y)

    (compat c139 o139)

    ;; order o140 ('m2', 'p2', 144, 156)
    (order_mine o140 m2)
    (order_port o140 p2)
    (not-delivered o140)

    ;; consist c140
    (available c140)
    (empty c140)
    (at c140 y)

    (compat c140 o140)

    ;; order o141 ('m2', 'p2', 156, 168)
    (order_mine o141 m2)
    (order_port o141 p2)
    (not-delivered o141)

    ;; consist c141
    (available c141)
    (empty c141)
    (at c141 y)

    (compat c141 o141)

    ;; order o142 ('m2', 'p2', 168, 180)
    (order_mine o142 m2)
    (order_port o142 p2)
    (not-delivered o142)

    ;; consist c142
    (available c142)
    (empty c142)
    (at c142 y)

    (compat c142 o142)

    ;; order o143 ('m2', 'p2', 180, 192)
    (order_mine o143 m2)
    (order_port o143 p2)
    (not-delivered o143)

    ;; consist c143
    (available c143)
    (empty c143)
    (at c143 y)

    (compat c143 o143)

    ;; order o144 ('m2', 'p2', 192, 204)
    (order_mine o144 m2)
    (order_port o144 p2)
    (not-delivered o144)

    ;; consist c144
    (available c144)
    (empty c144)
    (at c144 y)

    (compat c144 o144)

    ;; order o145 ('m2', 'p2', 204, 216)
    (order_mine o145 m2)
    (order_port o145 p2)
    (not-delivered o145)

    ;; consist c145
    (available c145)
    (empty c145)
    (at c145 y)

    (compat c145 o145)

    ;; order o146 ('m2', 'p2', 216, 228)
    (order_mine o146 m2)
    (order_port o146 p2)
    (not-delivered o146)

    ;; consist c146
    (available c146)
    (empty c146)
    (at c146 y)

    (compat c146 o146)

    ;; order o147 ('m2', 'p2', 228, 240)
    (order_mine o147 m2)
    (order_port o147 p2)
    (not-delivered o147)

    ;; consist c147
    (available c147)
    (empty c147)
    (at c147 y)

    (compat c147 o147)

    ;; order o148 ('m2', 'p2', 240, 252)
    (order_mine o148 m2)
    (order_port o148 p2)
    (not-delivered o148)

    ;; consist c148
    (available c148)
    (empty c148)
    (at c148 y)

    (compat c148 o148)

    ;; order o149 ('m2', 'p2', 252, 264)
    (order_mine o149 m2)
    (order_port o149 p2)
    (not-delivered o149)

    ;; consist c149
    (available c149)
    (empty c149)
    (at c149 y)

    (compat c149 o149)

    ;; order o150 ('m2', 'p2', 264, 276)
    (order_mine o150 m2)
    (order_port o150 p2)
    (not-delivered o150)

    ;; consist c150
    (available c150)
    (empty c150)
    (at c150 y)

    (compat c150 o150)

    ;; order o151 ('m2', 'p2', 276, 288)
    (order_mine o151 m2)
    (order_port o151 p2)
    (not-delivered o151)

    ;; consist c151
    (available c151)
    (empty c151)
    (at c151 y)

    (compat c151 o151)

    ;; order o152 ('m2', 'p2', 288, 300)
    (order_mine o152 m2)
    (order_port o152 p2)
    (not-delivered o152)

    ;; consist c152
    (available c152)
    (empty c152)
    (at c152 y)

    (compat c152 o152)

    ;; order o153 ('m2', 'p2', 300, 312)
    (order_mine o153 m2)
    (order_port o153 p2)
    (not-delivered o153)

    ;; consist c153
    (available c153)
    (empty c153)
    (at c153 y)

    (compat c153 o153)

    ;; order o154 ('m2', 'p2', 312, 324)
    (order_mine o154 m2)
    (order_port o154 p2)
    (not-delivered o154)

    ;; consist c154
    (available c154)
    (empty c154)
    (at c154 y)

    (compat c154 o154)

    ;; order o155 ('m2', 'p2', 324, 336)
    (order_mine o155 m2)
    (order_port o155 p2)
    (not-delivered o155)

    ;; consist c155
    (available c155)
    (empty c155)
    (at c155 y)

    (compat c155 o155)

    ;; order o156 ('m2', 'p2', 336, 348)
    (order_mine o156 m2)
    (order_port o156 p2)
    (not-delivered o156)

    ;; consist c156
    (available c156)
    (empty c156)
    (at c156 y)

    (compat c156 o156)

    ;; order o157 ('m2', 'p2', 348, 360)
    (order_mine o157 m2)
    (order_port o157 p2)
    (not-delivered o157)

    ;; consist c157
    (available c157)
    (empty c157)
    (at c157 y)

    (compat c157 o157)

    ;; order o158 ('m2', 'p2', 360, 372)
    (order_mine o158 m2)
    (order_port o158 p2)
    (not-delivered o158)

    ;; consist c158
    (available c158)
    (empty c158)
    (at c158 y)

    (compat c158 o158)

    ;; order o159 ('m2', 'p2', 372, 384)
    (order_mine o159 m2)
    (order_port o159 p2)
    (not-delivered o159)

    ;; consist c159
    (available c159)
    (empty c159)
    (at c159 y)

    (compat c159 o159)

    ;; order o160 ('m2', 'p2', 384, 396)
    (order_mine o160 m2)
    (order_port o160 p2)
    (not-delivered o160)

    ;; consist c160
    (available c160)
    (empty c160)
    (at c160 y)

    (compat c160 o160)

    ;; order o161 ('m2', 'p2', 396, 408)
    (order_mine o161 m2)
    (order_port o161 p2)
    (not-delivered o161)

    ;; consist c161
    (available c161)
    (empty c161)
    (at c161 y)

    (compat c161 o161)

    ;; order o162 ('m2', 'p2', 408, 420)
    (order_mine o162 m2)
    (order_port o162 p2)
    (not-delivered o162)

    ;; consist c162
    (available c162)
    (empty c162)
    (at c162 y)

    (compat c162 o162)

    ;; order o163 ('m2', 'p2', 420, 432)
    (order_mine o163 m2)
    (order_port o163 p2)
    (not-delivered o163)

    ;; consist c163
    (available c163)
    (empty c163)
    (at c163 y)

    (compat c163 o163)

    ;; order o164 ('m2', 'p2', 432, 444)
    (order_mine o164 m2)
    (order_port o164 p2)
    (not-delivered o164)

    ;; consist c164
    (available c164)
    (empty c164)
    (at c164 y)

    (compat c164 o164)

    ;; order o165 ('m2', 'p2', 444, 456)
    (order_mine o165 m2)
    (order_port o165 p2)
    (not-delivered o165)

    ;; consist c165
    (available c165)
    (empty c165)
    (at c165 y)

    (compat c165 o165)

    ;; order o166 ('m2', 'p2', 456, 468)
    (order_mine o166 m2)
    (order_port o166 p2)
    (not-delivered o166)

    ;; consist c166
    (available c166)
    (empty c166)
    (at c166 y)

    (compat c166 o166)

    ;; order o167 ('m2', 'p2', 468, 480)
    (order_mine o167 m2)
    (order_port o167 p2)
    (not-delivered o167)

    ;; consist c167
    (available c167)
    (empty c167)
    (at c167 y)

    (compat c167 o167)

    ;; order o168 ('m2', 'p2', 480, 492)
    (order_mine o168 m2)
    (order_port o168 p2)
    (not-delivered o168)

    ;; consist c168
    (available c168)
    (empty c168)
    (at c168 y)

    (compat c168 o168)

    ;; order o169 ('m2', 'p2', 492, 504)
    (order_mine o169 m2)
    (order_port o169 p2)
    (not-delivered o169)

    ;; consist c169
    (available c169)
    (empty c169)
    (at c169 y)

    (compat c169 o169)

    ;; order o170 ('m2', 'p2', 504, 516)
    (order_mine o170 m2)
    (order_port o170 p2)
    (not-delivered o170)

    ;; consist c170
    (available c170)
    (empty c170)
    (at c170 y)

    (compat c170 o170)

    ;; order o171 ('m2', 'p2', 516, 528)
    (order_mine o171 m2)
    (order_port o171 p2)
    (not-delivered o171)

    ;; consist c171
    (available c171)
    (empty c171)
    (at c171 y)

    (compat c171 o171)

    ;; order o172 ('m2', 'p2', 528, 540)
    (order_mine o172 m2)
    (order_port o172 p2)
    (not-delivered o172)

    ;; consist c172
    (available c172)
    (empty c172)
    (at c172 y)

    (compat c172 o172)

    ;; order o173 ('m2', 'p2', 540, 552)
    (order_mine o173 m2)
    (order_port o173 p2)
    (not-delivered o173)

    ;; consist c173
    (available c173)
    (empty c173)
    (at c173 y)

    (compat c173 o173)

    ;; order o174 ('m2', 'p2', 552, 564)
    (order_mine o174 m2)
    (order_port o174 p2)
    (not-delivered o174)

    ;; consist c174
    (available c174)
    (empty c174)
    (at c174 y)

    (compat c174 o174)

    ;; order o175 ('m2', 'p2', 564, 576)
    (order_mine o175 m2)
    (order_port o175 p2)
    (not-delivered o175)

    ;; consist c175
    (available c175)
    (empty c175)
    (at c175 y)

    (compat c175 o175)

    ;; order o176 ('m2', 'p2', 576, 588)
    (order_mine o176 m2)
    (order_port o176 p2)
    (not-delivered o176)

    ;; consist c176
    (available c176)
    (empty c176)
    (at c176 y)

    (compat c176 o176)

    ;; order o177 ('m2', 'p2', 588, 600)
    (order_mine o177 m2)
    (order_port o177 p2)
    (not-delivered o177)

    ;; consist c177
    (available c177)
    (empty c177)
    (at c177 y)

    (compat c177 o177)

    ;; order o178 ('m2', 'p2', 600, 612)
    (order_mine o178 m2)
    (order_port o178 p2)
    (not-delivered o178)

    ;; consist c178
    (available c178)
    (empty c178)
    (at c178 y)

    (compat c178 o178)

    ;; order o179 ('m2', 'p2', 612, 624)
    (order_mine o179 m2)
    (order_port o179 p2)
    (not-delivered o179)

    ;; consist c179
    (available c179)
    (empty c179)
    (at c179 y)

    (compat c179 o179)

    ;; order o180 ('m2', 'p2', 624, 636)
    (order_mine o180 m2)
    (order_port o180 p2)
    (not-delivered o180)

    ;; consist c180
    (available c180)
    (empty c180)
    (at c180 y)

    (compat c180 o180)

    ;; order o181 ('m2', 'p2', 636, 648)
    (order_mine o181 m2)
    (order_port o181 p2)
    (not-delivered o181)

    ;; consist c181
    (available c181)
    (empty c181)
    (at c181 y)

    (compat c181 o181)

    ;; order o182 ('m2', 'p2', 648, 660)
    (order_mine o182 m2)
    (order_port o182 p2)
    (not-delivered o182)

    ;; consist c182
    (available c182)
    (empty c182)
    (at c182 y)

    (compat c182 o182)

    ;; order o183 ('m2', 'p2', 660, 672)
    (order_mine o183 m2)
    (order_port o183 p2)
    (not-delivered o183)

    ;; consist c183
    (available c183)
    (empty c183)
    (at c183 y)

    (compat c183 o183)

    ;; order o184 ('m2', 'p2', 672, 684)
    (order_mine o184 m2)
    (order_port o184 p2)
    (not-delivered o184)

    ;; consist c184
    (available c184)
    (empty c184)
    (at c184 y)

    (compat c184 o184)

    ;; order o185 ('m2', 'p2', 684, 696)
    (order_mine o185 m2)
    (order_port o185 p2)
    (not-delivered o185)

    ;; consist c185
    (available c185)
    (empty c185)
    (at c185 y)

    (compat c185 o185)

    ;; order o186 ('m2', 'p2', 696, 708)
    (order_mine o186 m2)
    (order_port o186 p2)
    (not-delivered o186)

    ;; consist c186
    (available c186)
    (empty c186)
    (at c186 y)

    (compat c186 o186)

    ;; order o187 ('m2', 'p2', 708, 720)
    (order_mine o187 m2)
    (order_port o187 p2)
    (not-delivered o187)

    ;; consist c187
    (available c187)
    (empty c187)
    (at c187 y)

    (compat c187 o187)

    ;; order o188 ('m2', 'p2', 720, 732)
    (order_mine o188 m2)
    (order_port o188 p2)
    (not-delivered o188)

    ;; consist c188
    (available c188)
    (empty c188)
    (at c188 y)

    (compat c188 o188)

    ;; order o189 ('m2', 'p2', 732, 744)
    (order_mine o189 m2)
    (order_port o189 p2)
    (not-delivered o189)

    ;; consist c189
    (available c189)
    (empty c189)
    (at c189 y)

    (compat c189 o189)

    ;; order o190 ('m2', 'p2', 744, 756)
    (order_mine o190 m2)
    (order_port o190 p2)
    (not-delivered o190)

    ;; consist c190
    (available c190)
    (empty c190)
    (at c190 y)

    (compat c190 o190)

    ;; order o191 ('m2', 'p2', 756, 768)
    (order_mine o191 m2)
    (order_port o191 p2)
    (not-delivered o191)

    ;; consist c191
    (available c191)
    (empty c191)
    (at c191 y)

    (compat c191 o191)

    ;; order o192 ('m2', 'p1', 0, 12)
    (order_mine o192 m2)
    (order_port o192 p1)
    (not-delivered o192)

    ;; consist c192
    (available c192)
    (empty c192)
    (at c192 y)

    (compat c192 o192)

    ;; order o193 ('m2', 'p1', 12, 24)
    (order_mine o193 m2)
    (order_port o193 p1)
    (not-delivered o193)

    ;; consist c193
    (available c193)
    (empty c193)
    (at c193 y)

    (compat c193 o193)

    ;; order o194 ('m2', 'p1', 24, 36)
    (order_mine o194 m2)
    (order_port o194 p1)
    (not-delivered o194)

    ;; consist c194
    (available c194)
    (empty c194)
    (at c194 y)

    (compat c194 o194)

    ;; order o195 ('m2', 'p1', 36, 48)
    (order_mine o195 m2)
    (order_port o195 p1)
    (not-delivered o195)

    ;; consist c195
    (available c195)
    (empty c195)
    (at c195 y)

    (compat c195 o195)

    ;; order o196 ('m2', 'p1', 48, 60)
    (order_mine o196 m2)
    (order_port o196 p1)
    (not-delivered o196)

    ;; consist c196
    (available c196)
    (empty c196)
    (at c196 y)

    (compat c196 o196)

    ;; order o197 ('m2', 'p1', 60, 72)
    (order_mine o197 m2)
    (order_port o197 p1)
    (not-delivered o197)

    ;; consist c197
    (available c197)
    (empty c197)
    (at c197 y)

    (compat c197 o197)

    ;; order o198 ('m2', 'p1', 72, 84)
    (order_mine o198 m2)
    (order_port o198 p1)
    (not-delivered o198)

    ;; consist c198
    (available c198)
    (empty c198)
    (at c198 y)

    (compat c198 o198)

    ;; order o199 ('m2', 'p1', 84, 96)
    (order_mine o199 m2)
    (order_port o199 p1)
    (not-delivered o199)

    ;; consist c199
    (available c199)
    (empty c199)
    (at c199 y)

    (compat c199 o199)

    ;; order o200 ('m2', 'p1', 96, 108)
    (order_mine o200 m2)
    (order_port o200 p1)
    (not-delivered o200)

    ;; consist c200
    (available c200)
    (empty c200)
    (at c200 y)

    (compat c200 o200)

    ;; order o201 ('m2', 'p1', 108, 120)
    (order_mine o201 m2)
    (order_port o201 p1)
    (not-delivered o201)

    ;; consist c201
    (available c201)
    (empty c201)
    (at c201 y)

    (compat c201 o201)

    ;; order o202 ('m2', 'p1', 120, 132)
    (order_mine o202 m2)
    (order_port o202 p1)
    (not-delivered o202)

    ;; consist c202
    (available c202)
    (empty c202)
    (at c202 y)

    (compat c202 o202)

    ;; order o203 ('m2', 'p1', 132, 144)
    (order_mine o203 m2)
    (order_port o203 p1)
    (not-delivered o203)

    ;; consist c203
    (available c203)
    (empty c203)
    (at c203 y)

    (compat c203 o203)

    ;; order o204 ('m2', 'p1', 144, 156)
    (order_mine o204 m2)
    (order_port o204 p1)
    (not-delivered o204)

    ;; consist c204
    (available c204)
    (empty c204)
    (at c204 y)

    (compat c204 o204)

    ;; order o205 ('m2', 'p1', 156, 168)
    (order_mine o205 m2)
    (order_port o205 p1)
    (not-delivered o205)

    ;; consist c205
    (available c205)
    (empty c205)
    (at c205 y)

    (compat c205 o205)

    ;; order o206 ('m2', 'p1', 168, 180)
    (order_mine o206 m2)
    (order_port o206 p1)
    (not-delivered o206)

    ;; consist c206
    (available c206)
    (empty c206)
    (at c206 y)

    (compat c206 o206)

    ;; order o207 ('m2', 'p1', 180, 192)
    (order_mine o207 m2)
    (order_port o207 p1)
    (not-delivered o207)

    ;; consist c207
    (available c207)
    (empty c207)
    (at c207 y)

    (compat c207 o207)

    ;; order o208 ('m2', 'p1', 192, 204)
    (order_mine o208 m2)
    (order_port o208 p1)
    (not-delivered o208)

    ;; consist c208
    (available c208)
    (empty c208)
    (at c208 y)

    (compat c208 o208)

    ;; order o209 ('m2', 'p1', 204, 216)
    (order_mine o209 m2)
    (order_port o209 p1)
    (not-delivered o209)

    ;; consist c209
    (available c209)
    (empty c209)
    (at c209 y)

    (compat c209 o209)

    ;; order o210 ('m2', 'p1', 216, 228)
    (order_mine o210 m2)
    (order_port o210 p1)
    (not-delivered o210)

    ;; consist c210
    (available c210)
    (empty c210)
    (at c210 y)

    (compat c210 o210)

    ;; order o211 ('m2', 'p1', 228, 240)
    (order_mine o211 m2)
    (order_port o211 p1)
    (not-delivered o211)

    ;; consist c211
    (available c211)
    (empty c211)
    (at c211 y)

    (compat c211 o211)

    ;; order o212 ('m2', 'p1', 240, 252)
    (order_mine o212 m2)
    (order_port o212 p1)
    (not-delivered o212)

    ;; consist c212
    (available c212)
    (empty c212)
    (at c212 y)

    (compat c212 o212)

    ;; order o213 ('m2', 'p1', 252, 264)
    (order_mine o213 m2)
    (order_port o213 p1)
    (not-delivered o213)

    ;; consist c213
    (available c213)
    (empty c213)
    (at c213 y)

    (compat c213 o213)

    ;; order o214 ('m2', 'p1', 264, 276)
    (order_mine o214 m2)
    (order_port o214 p1)
    (not-delivered o214)

    ;; consist c214
    (available c214)
    (empty c214)
    (at c214 y)

    (compat c214 o214)

    ;; order o215 ('m2', 'p1', 276, 288)
    (order_mine o215 m2)
    (order_port o215 p1)
    (not-delivered o215)

    ;; consist c215
    (available c215)
    (empty c215)
    (at c215 y)

    (compat c215 o215)

    ;; order o216 ('m2', 'p1', 288, 300)
    (order_mine o216 m2)
    (order_port o216 p1)
    (not-delivered o216)

    ;; consist c216
    (available c216)
    (empty c216)
    (at c216 y)

    (compat c216 o216)

    ;; order o217 ('m2', 'p1', 300, 312)
    (order_mine o217 m2)
    (order_port o217 p1)
    (not-delivered o217)

    ;; consist c217
    (available c217)
    (empty c217)
    (at c217 y)

    (compat c217 o217)

    ;; order o218 ('m2', 'p1', 312, 324)
    (order_mine o218 m2)
    (order_port o218 p1)
    (not-delivered o218)

    ;; consist c218
    (available c218)
    (empty c218)
    (at c218 y)

    (compat c218 o218)

    ;; order o219 ('m2', 'p1', 324, 336)
    (order_mine o219 m2)
    (order_port o219 p1)
    (not-delivered o219)

    ;; consist c219
    (available c219)
    (empty c219)
    (at c219 y)

    (compat c219 o219)

    ;; order o220 ('m2', 'p1', 336, 348)
    (order_mine o220 m2)
    (order_port o220 p1)
    (not-delivered o220)

    ;; consist c220
    (available c220)
    (empty c220)
    (at c220 y)

    (compat c220 o220)

    ;; order o221 ('m2', 'p1', 348, 360)
    (order_mine o221 m2)
    (order_port o221 p1)
    (not-delivered o221)

    ;; consist c221
    (available c221)
    (empty c221)
    (at c221 y)

    (compat c221 o221)

    ;; order o222 ('m2', 'p1', 360, 372)
    (order_mine o222 m2)
    (order_port o222 p1)
    (not-delivered o222)

    ;; consist c222
    (available c222)
    (empty c222)
    (at c222 y)

    (compat c222 o222)

    ;; order o223 ('m2', 'p1', 372, 384)
    (order_mine o223 m2)
    (order_port o223 p1)
    (not-delivered o223)

    ;; consist c223
    (available c223)
    (empty c223)
    (at c223 y)

    (compat c223 o223)

    ;; order o224 ('m2', 'p1', 384, 396)
    (order_mine o224 m2)
    (order_port o224 p1)
    (not-delivered o224)

    ;; consist c224
    (available c224)
    (empty c224)
    (at c224 y)

    (compat c224 o224)

    ;; order o225 ('m2', 'p1', 396, 408)
    (order_mine o225 m2)
    (order_port o225 p1)
    (not-delivered o225)

    ;; consist c225
    (available c225)
    (empty c225)
    (at c225 y)

    (compat c225 o225)

    ;; order o226 ('m2', 'p1', 408, 420)
    (order_mine o226 m2)
    (order_port o226 p1)
    (not-delivered o226)

    ;; consist c226
    (available c226)
    (empty c226)
    (at c226 y)

    (compat c226 o226)

    ;; order o227 ('m2', 'p1', 420, 432)
    (order_mine o227 m2)
    (order_port o227 p1)
    (not-delivered o227)

    ;; consist c227
    (available c227)
    (empty c227)
    (at c227 y)

    (compat c227 o227)

    ;; order o228 ('m2', 'p1', 432, 444)
    (order_mine o228 m2)
    (order_port o228 p1)
    (not-delivered o228)

    ;; consist c228
    (available c228)
    (empty c228)
    (at c228 y)

    (compat c228 o228)

    ;; order o229 ('m2', 'p1', 444, 456)
    (order_mine o229 m2)
    (order_port o229 p1)
    (not-delivered o229)

    ;; consist c229
    (available c229)
    (empty c229)
    (at c229 y)

    (compat c229 o229)

    ;; order o230 ('m2', 'p1', 456, 468)
    (order_mine o230 m2)
    (order_port o230 p1)
    (not-delivered o230)

    ;; consist c230
    (available c230)
    (empty c230)
    (at c230 y)

    (compat c230 o230)

    ;; order o231 ('m2', 'p1', 468, 480)
    (order_mine o231 m2)
    (order_port o231 p1)
    (not-delivered o231)

    ;; consist c231
    (available c231)
    (empty c231)
    (at c231 y)

    (compat c231 o231)

    ;; order o232 ('m2', 'p1', 480, 492)
    (order_mine o232 m2)
    (order_port o232 p1)
    (not-delivered o232)

    ;; consist c232
    (available c232)
    (empty c232)
    (at c232 y)

    (compat c232 o232)

    ;; order o233 ('m2', 'p1', 492, 504)
    (order_mine o233 m2)
    (order_port o233 p1)
    (not-delivered o233)

    ;; consist c233
    (available c233)
    (empty c233)
    (at c233 y)

    (compat c233 o233)

    ;; order o234 ('m2', 'p1', 504, 516)
    (order_mine o234 m2)
    (order_port o234 p1)
    (not-delivered o234)

    ;; consist c234
    (available c234)
    (empty c234)
    (at c234 y)

    (compat c234 o234)

    ;; order o235 ('m2', 'p1', 516, 528)
    (order_mine o235 m2)
    (order_port o235 p1)
    (not-delivered o235)

    ;; consist c235
    (available c235)
    (empty c235)
    (at c235 y)

    (compat c235 o235)

    ;; order o236 ('m2', 'p1', 528, 540)
    (order_mine o236 m2)
    (order_port o236 p1)
    (not-delivered o236)

    ;; consist c236
    (available c236)
    (empty c236)
    (at c236 y)

    (compat c236 o236)

    ;; order o237 ('m2', 'p1', 540, 552)
    (order_mine o237 m2)
    (order_port o237 p1)
    (not-delivered o237)

    ;; consist c237
    (available c237)
    (empty c237)
    (at c237 y)

    (compat c237 o237)

    ;; order o238 ('m2', 'p1', 552, 564)
    (order_mine o238 m2)
    (order_port o238 p1)
    (not-delivered o238)

    ;; consist c238
    (available c238)
    (empty c238)
    (at c238 y)

    (compat c238 o238)

    ;; order o239 ('m2', 'p1', 564, 576)
    (order_mine o239 m2)
    (order_port o239 p1)
    (not-delivered o239)

    ;; consist c239
    (available c239)
    (empty c239)
    (at c239 y)

    (compat c239 o239)

    ;; order o240 ('m2', 'p1', 576, 588)
    (order_mine o240 m2)
    (order_port o240 p1)
    (not-delivered o240)

    ;; consist c240
    (available c240)
    (empty c240)
    (at c240 y)

    (compat c240 o240)

    ;; order o241 ('m2', 'p1', 588, 600)
    (order_mine o241 m2)
    (order_port o241 p1)
    (not-delivered o241)

    ;; consist c241
    (available c241)
    (empty c241)
    (at c241 y)

    (compat c241 o241)

    ;; order o242 ('m2', 'p1', 600, 612)
    (order_mine o242 m2)
    (order_port o242 p1)
    (not-delivered o242)

    ;; consist c242
    (available c242)
    (empty c242)
    (at c242 y)

    (compat c242 o242)

    ;; order o243 ('m2', 'p1', 612, 624)
    (order_mine o243 m2)
    (order_port o243 p1)
    (not-delivered o243)

    ;; consist c243
    (available c243)
    (empty c243)
    (at c243 y)

    (compat c243 o243)

    ;; order o244 ('m2', 'p1', 624, 636)
    (order_mine o244 m2)
    (order_port o244 p1)
    (not-delivered o244)

    ;; consist c244
    (available c244)
    (empty c244)
    (at c244 y)

    (compat c244 o244)

    ;; order o245 ('m2', 'p1', 636, 648)
    (order_mine o245 m2)
    (order_port o245 p1)
    (not-delivered o245)

    ;; consist c245
    (available c245)
    (empty c245)
    (at c245 y)

    (compat c245 o245)

    ;; order o246 ('m2', 'p1', 648, 660)
    (order_mine o246 m2)
    (order_port o246 p1)
    (not-delivered o246)

    ;; consist c246
    (available c246)
    (empty c246)
    (at c246 y)

    (compat c246 o246)

    ;; order o247 ('m2', 'p1', 660, 672)
    (order_mine o247 m2)
    (order_port o247 p1)
    (not-delivered o247)

    ;; consist c247
    (available c247)
    (empty c247)
    (at c247 y)

    (compat c247 o247)

    ;; order o248 ('m2', 'p1', 672, 684)
    (order_mine o248 m2)
    (order_port o248 p1)
    (not-delivered o248)

    ;; consist c248
    (available c248)
    (empty c248)
    (at c248 y)

    (compat c248 o248)

    ;; order o249 ('m2', 'p1', 684, 696)
    (order_mine o249 m2)
    (order_port o249 p1)
    (not-delivered o249)

    ;; consist c249
    (available c249)
    (empty c249)
    (at c249 y)

    (compat c249 o249)

    ;; order o250 ('m2', 'p1', 696, 708)
    (order_mine o250 m2)
    (order_port o250 p1)
    (not-delivered o250)

    ;; consist c250
    (available c250)
    (empty c250)
    (at c250 y)

    (compat c250 o250)

    ;; order o251 ('m2', 'p1', 708, 720)
    (order_mine o251 m2)
    (order_port o251 p1)
    (not-delivered o251)

    ;; consist c251
    (available c251)
    (empty c251)
    (at c251 y)

    (compat c251 o251)

    ;; order o252 ('m2', 'p1', 720, 732)
    (order_mine o252 m2)
    (order_port o252 p1)
    (not-delivered o252)

    ;; consist c252
    (available c252)
    (empty c252)
    (at c252 y)

    (compat c252 o252)

    ;; order o253 ('m2', 'p1', 732, 744)
    (order_mine o253 m2)
    (order_port o253 p1)
    (not-delivered o253)

    ;; consist c253
    (available c253)
    (empty c253)
    (at c253 y)

    (compat c253 o253)

    ;; order o254 ('m2', 'p1', 744, 756)
    (order_mine o254 m2)
    (order_port o254 p1)
    (not-delivered o254)

    ;; consist c254
    (available c254)
    (empty c254)
    (at c254 y)

    (compat c254 o254)

    ;; order o255 ('m2', 'p1', 756, 768)
    (order_mine o255 m2)
    (order_port o255 p1)
    (not-delivered o255)

    ;; consist c255
    (available c255)
    (empty c255)
    (at c255 y)

    (compat c255 o255)

)
(:goal
(and
  (delivered o0)
  (at c0 y)
  (delivered o1)
  (at c1 y)
  (delivered o2)
  (at c2 y)
  (delivered o3)
  (at c3 y)
  (delivered o4)
  (at c4 y)
  (delivered o5)
  (at c5 y)
  (delivered o6)
  (at c6 y)
  (delivered o7)
  (at c7 y)
  (delivered o8)
  (at c8 y)
  (delivered o9)
  (at c9 y)
  (delivered o10)
  (at c10 y)
  (delivered o11)
  (at c11 y)
  (delivered o12)
  (at c12 y)
  (delivered o13)
  (at c13 y)
  (delivered o14)
  (at c14 y)
  (delivered o15)
  (at c15 y)
  (delivered o16)
  (at c16 y)
  (delivered o17)
  (at c17 y)
  (delivered o18)
  (at c18 y)
  (delivered o19)
  (at c19 y)
  (delivered o20)
  (at c20 y)
  (delivered o21)
  (at c21 y)
  (delivered o22)
  (at c22 y)
  (delivered o23)
  (at c23 y)
  (delivered o24)
  (at c24 y)
  (delivered o25)
  (at c25 y)
  (delivered o26)
  (at c26 y)
  (delivered o27)
  (at c27 y)
  (delivered o28)
  (at c28 y)
  (delivered o29)
  (at c29 y)
  (delivered o30)
  (at c30 y)
  (delivered o31)
  (at c31 y)
  (delivered o32)
  (at c32 y)
  (delivered o33)
  (at c33 y)
  (delivered o34)
  (at c34 y)
  (delivered o35)
  (at c35 y)
  (delivered o36)
  (at c36 y)
  (delivered o37)
  (at c37 y)
  (delivered o38)
  (at c38 y)
  (delivered o39)
  (at c39 y)
  (delivered o40)
  (at c40 y)
  (delivered o41)
  (at c41 y)
  (delivered o42)
  (at c42 y)
  (delivered o43)
  (at c43 y)
  (delivered o44)
  (at c44 y)
  (delivered o45)
  (at c45 y)
  (delivered o46)
  (at c46 y)
  (delivered o47)
  (at c47 y)
  (delivered o48)
  (at c48 y)
  (delivered o49)
  (at c49 y)
  (delivered o50)
  (at c50 y)
  (delivered o51)
  (at c51 y)
  (delivered o52)
  (at c52 y)
  (delivered o53)
  (at c53 y)
  (delivered o54)
  (at c54 y)
  (delivered o55)
  (at c55 y)
  (delivered o56)
  (at c56 y)
  (delivered o57)
  (at c57 y)
  (delivered o58)
  (at c58 y)
  (delivered o59)
  (at c59 y)
  (delivered o60)
  (at c60 y)
  (delivered o61)
  (at c61 y)
  (delivered o62)
  (at c62 y)
  (delivered o63)
  (at c63 y)
  (delivered o64)
  (at c64 y)
  (delivered o65)
  (at c65 y)
  (delivered o66)
  (at c66 y)
  (delivered o67)
  (at c67 y)
  (delivered o68)
  (at c68 y)
  (delivered o69)
  (at c69 y)
  (delivered o70)
  (at c70 y)
  (delivered o71)
  (at c71 y)
  (delivered o72)
  (at c72 y)
  (delivered o73)
  (at c73 y)
  (delivered o74)
  (at c74 y)
  (delivered o75)
  (at c75 y)
  (delivered o76)
  (at c76 y)
  (delivered o77)
  (at c77 y)
  (delivered o78)
  (at c78 y)
  (delivered o79)
  (at c79 y)
  (delivered o80)
  (at c80 y)
  (delivered o81)
  (at c81 y)
  (delivered o82)
  (at c82 y)
  (delivered o83)
  (at c83 y)
  (delivered o84)
  (at c84 y)
  (delivered o85)
  (at c85 y)
  (delivered o86)
  (at c86 y)
  (delivered o87)
  (at c87 y)
  (delivered o88)
  (at c88 y)
  (delivered o89)
  (at c89 y)
  (delivered o90)
  (at c90 y)
  (delivered o91)
  (at c91 y)
  (delivered o92)
  (at c92 y)
  (delivered o93)
  (at c93 y)
  (delivered o94)
  (at c94 y)
  (delivered o95)
  (at c95 y)
  (delivered o96)
  (at c96 y)
  (delivered o97)
  (at c97 y)
  (delivered o98)
  (at c98 y)
  (delivered o99)
  (at c99 y)
  (delivered o100)
  (at c100 y)
  (delivered o101)
  (at c101 y)
  (delivered o102)
  (at c102 y)
  (delivered o103)
  (at c103 y)
  (delivered o104)
  (at c104 y)
  (delivered o105)
  (at c105 y)
  (delivered o106)
  (at c106 y)
  (delivered o107)
  (at c107 y)
  (delivered o108)
  (at c108 y)
  (delivered o109)
  (at c109 y)
  (delivered o110)
  (at c110 y)
  (delivered o111)
  (at c111 y)
  (delivered o112)
  (at c112 y)
  (delivered o113)
  (at c113 y)
  (delivered o114)
  (at c114 y)
  (delivered o115)
  (at c115 y)
  (delivered o116)
  (at c116 y)
  (delivered o117)
  (at c117 y)
  (delivered o118)
  (at c118 y)
  (delivered o119)
  (at c119 y)
  (delivered o120)
  (at c120 y)
  (delivered o121)
  (at c121 y)
  (delivered o122)
  (at c122 y)
  (delivered o123)
  (at c123 y)
  (delivered o124)
  (at c124 y)
  (delivered o125)
  (at c125 y)
  (delivered o126)
  (at c126 y)
  (delivered o127)
  (at c127 y)
  (delivered o128)
  (at c128 y)
  (delivered o129)
  (at c129 y)
  (delivered o130)
  (at c130 y)
  (delivered o131)
  (at c131 y)
  (delivered o132)
  (at c132 y)
  (delivered o133)
  (at c133 y)
  (delivered o134)
  (at c134 y)
  (delivered o135)
  (at c135 y)
  (delivered o136)
  (at c136 y)
  (delivered o137)
  (at c137 y)
  (delivered o138)
  (at c138 y)
  (delivered o139)
  (at c139 y)
  (delivered o140)
  (at c140 y)
  (delivered o141)
  (at c141 y)
  (delivered o142)
  (at c142 y)
  (delivered o143)
  (at c143 y)
  (delivered o144)
  (at c144 y)
  (delivered o145)
  (at c145 y)
  (delivered o146)
  (at c146 y)
  (delivered o147)
  (at c147 y)
  (delivered o148)
  (at c148 y)
  (delivered o149)
  (at c149 y)
  (delivered o150)
  (at c150 y)
  (delivered o151)
  (at c151 y)
  (delivered o152)
  (at c152 y)
  (delivered o153)
  (at c153 y)
  (delivered o154)
  (at c154 y)
  (delivered o155)
  (at c155 y)
  (delivered o156)
  (at c156 y)
  (delivered o157)
  (at c157 y)
  (delivered o158)
  (at c158 y)
  (delivered o159)
  (at c159 y)
  (delivered o160)
  (at c160 y)
  (delivered o161)
  (at c161 y)
  (delivered o162)
  (at c162 y)
  (delivered o163)
  (at c163 y)
  (delivered o164)
  (at c164 y)
  (delivered o165)
  (at c165 y)
  (delivered o166)
  (at c166 y)
  (delivered o167)
  (at c167 y)
  (delivered o168)
  (at c168 y)
  (delivered o169)
  (at c169 y)
  (delivered o170)
  (at c170 y)
  (delivered o171)
  (at c171 y)
  (delivered o172)
  (at c172 y)
  (delivered o173)
  (at c173 y)
  (delivered o174)
  (at c174 y)
  (delivered o175)
  (at c175 y)
  (delivered o176)
  (at c176 y)
  (delivered o177)
  (at c177 y)
  (delivered o178)
  (at c178 y)
  (delivered o179)
  (at c179 y)
  (delivered o180)
  (at c180 y)
  (delivered o181)
  (at c181 y)
  (delivered o182)
  (at c182 y)
  (delivered o183)
  (at c183 y)
  (delivered o184)
  (at c184 y)
  (delivered o185)
  (at c185 y)
  (delivered o186)
  (at c186 y)
  (delivered o187)
  (at c187 y)
  (delivered o188)
  (at c188 y)
  (delivered o189)
  (at c189 y)
  (delivered o190)
  (at c190 y)
  (delivered o191)
  (at c191 y)
  (delivered o192)
  (at c192 y)
  (delivered o193)
  (at c193 y)
  (delivered o194)
  (at c194 y)
  (delivered o195)
  (at c195 y)
  (delivered o196)
  (at c196 y)
  (delivered o197)
  (at c197 y)
  (delivered o198)
  (at c198 y)
  (delivered o199)
  (at c199 y)
  (delivered o200)
  (at c200 y)
  (delivered o201)
  (at c201 y)
  (delivered o202)
  (at c202 y)
  (delivered o203)
  (at c203 y)
  (delivered o204)
  (at c204 y)
  (delivered o205)
  (at c205 y)
  (delivered o206)
  (at c206 y)
  (delivered o207)
  (at c207 y)
  (delivered o208)
  (at c208 y)
  (delivered o209)
  (at c209 y)
  (delivered o210)
  (at c210 y)
  (delivered o211)
  (at c211 y)
  (delivered o212)
  (at c212 y)
  (delivered o213)
  (at c213 y)
  (delivered o214)
  (at c214 y)
  (delivered o215)
  (at c215 y)
  (delivered o216)
  (at c216 y)
  (delivered o217)
  (at c217 y)
  (delivered o218)
  (at c218 y)
  (delivered o219)
  (at c219 y)
  (delivered o220)
  (at c220 y)
  (delivered o221)
  (at c221 y)
  (delivered o222)
  (at c222 y)
  (delivered o223)
  (at c223 y)
  (delivered o224)
  (at c224 y)
  (delivered o225)
  (at c225 y)
  (delivered o226)
  (at c226 y)
  (delivered o227)
  (at c227 y)
  (delivered o228)
  (at c228 y)
  (delivered o229)
  (at c229 y)
  (delivered o230)
  (at c230 y)
  (delivered o231)
  (at c231 y)
  (delivered o232)
  (at c232 y)
  (delivered o233)
  (at c233 y)
  (delivered o234)
  (at c234 y)
  (delivered o235)
  (at c235 y)
  (delivered o236)
  (at c236 y)
  (delivered o237)
  (at c237 y)
  (delivered o238)
  (at c238 y)
  (delivered o239)
  (at c239 y)
  (delivered o240)
  (at c240 y)
  (delivered o241)
  (at c241 y)
  (delivered o242)
  (at c242 y)
  (delivered o243)
  (at c243 y)
  (delivered o244)
  (at c244 y)
  (delivered o245)
  (at c245 y)
  (delivered o246)
  (at c246 y)
  (delivered o247)
  (at c247 y)
  (delivered o248)
  (at c248 y)
  (delivered o249)
  (at c249 y)
  (delivered o250)
  (at c250 y)
  (delivered o251)
  (at c251 y)
  (delivered o252)
  (at c252 y)
  (delivered o253)
  (at c253 y)
  (delivered o254)
  (at c254 y)
  (delivered o255)
  (at c255 y)
))
(:metric minimize (total-time)))

