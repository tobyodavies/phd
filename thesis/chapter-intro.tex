\chapter{Introduction}

Planning is the problem of finding a sequence of actions an agent can
perform that leads them from some initial state to a state satisfying
some desired goal condition.
A simple example of such a problem is finding a sequence of actions
that would enable a robot to move a ball between two rooms.
Such a plan might be: pick up the ball, move to the other room,
then drop the ball.
This is an obviously contrived example, but planning has industrial
applications too: for example if the robot was a train, the ball cargo,
and the rooms cities or ports. Especially if the train has to somehow
coordinate with a number of other trains sharing a busy track network
so as not to crash.

Search is one of the most pervasive approaches to planning and
scheduling problems, in simple terms, search consists of interleaving
guesses with reasoning, to try to incrementally build a large number
of different solutions.
A conflict occurs when reasoning determines that one or more of the
guesses made were wrong.
For example, when planning a set of train services, a planner may
guess that two trains should head towards each other at the same time,
if there is no siding to pass on, this will inevitably lead to a
crash.

Learning from this conflict might involve realising that the
underlying cause is the relative order of the trains departing their
stations and passing one-another.
One might reason, for example, that the colour of those trains, who is
driving, the particular cargo loaded onto them, or indeed the precise
time of departure is not relevant to the conflict.
Recognising what is and is not responsible for conflict allows
algorithms to avoid making the same mistake many times in many
superficially distinct states generated during search.

The types of guesses made during search, and their interactions with
the reasoning techniques employed determine the types of conflict that
can be detected, which in turn define the kinds of knowledge that may
be derived from these conflicts. 

The focus of this thesis is on applying and extending this kind of
conflict-directed reasoning to several important variants of the
planning problem.  To this end we introduce a number of novel
algorithms, associated kinds of conflict, and representations of
knowledge derived from these conflicts.
These algorithms are summarised in table \ref{intro:contribs}.
All of the approaches we introduce are capable of finding the optimal
plan and proving its optimality.

\begin{table}
\begin{tabular}{l | l | l }
  Algorithm  &   Conflict    & Knowledge Learned              \\
\hline
 \textbf{FragPlan} (Ch. \ref{chp:fragplan}+\ref{chp:orsc})  &  Resource limitations     &  Improving Agent Plan         \\
 \textbf{OpSeq} (Ch. \ref{chp:opseq})  & Unsequencable            &  Generalised Landmark       \\
 \textbf{CDHL} (Ch. \ref{chp:cdhl})    &  Suboptimality            &  Clause                \\
 \textbf{MznLBBD} (Ch. \ref{chp:autolbbd})  &  Unschedulable            &  Benders Cut                \\
 \textbf{OpSched} (Sec. \ref{chp:opsched})  & Unschedulable          & More Events Needed  \\
\end{tabular}
\caption{Summary of contributions\label{intro:contribs}}
\end{table}

\section*{Thesis Organisation}

The next chapter covers related work, expands on our view of the
relationship between conflict and learning, and highlights the bigger
questions asked and answered by this thesis.
The chapters comprising the body of the thesis are intended to be
self contained. A reader should be able to read any chapter
without the need to refer to any other much like any paper.
These chapters are grouped into 3 parts, the introductions to each of
these parts highlight the common themes, the relationships between the
chapters and the thesis as a whole.

Part I considers multi-agent planning problems, the agents in these
chapters can be black boxes, but we use the Golog language to describe
and analyse them.
This section covers the \textbf{FragPlan} algorithm which enables
agents to plan largely independently, and to learn from the conflicts
they cause in terms of shared resources.
 
Part II considers classical planning problems, and covers both
Operator Sequencing and Conflict-Directed Heuristic Learning.
Operator Sequencing (\textbf{OpSeq}) separates the choice of the count
of actions in a plan from the order of those actions, then learns
information about the former when conflicts are detected while
searching for the latter.
Conflict-Directed Heuristic Learning (\textbf{CDHL}) attempts to learn
how far from a goal each state is in terms of desirable properties
that do not yet hold in a state.

Part III considers temporal problems, starting with Automatic LBBD for
MiniZinc, which solves alternative scheduling problems, a half-way
point between temporal planning and scheduling.  We then move on to
Operator Scheduling, a theoretical extension of the automatic LBBD
approach to tackle true temporal planning problems.

Chapter \ref{chp:fragplan} is derived from the paper \citemypaper{davies2014}.

Chapter \ref{chp:orsc} is derived from the paper \citemypaper{davies2015a}.

Chapter \ref{chp:opseq} is derived from the paper \citemypaper{davies2015b}.
This paper was also reproduced with permission from AAAI in abridged form as \citemypaper{davies2016}.

Chapter \ref{chp:autolbbd} is derived from the paper \citemypaper{davies2017}.
