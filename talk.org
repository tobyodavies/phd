#+TITLE:     Fragment-Based Planning
#+AUTHOR:    Toby Davies
#+EMAIL:     toby.davies@nicta.com.au
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LaTeX_CLASS_OPTIONS: [bigger]
#+BEAMER_FRAME_LEVEL: 2
#+latex_header: \mode<beamer>{\usetheme{Madrid}}
#+latex_header: \AtBeginSection[]{\begin{frame}<beamer>\frametitle{Topic}\tableofcontents[currentsection]\end{frame}}
#+LaTeX_HEADER: \institute[NICTA]{NICTA Optimisation Research Group}
#+COLUMNS: %40ITEM %10BEAMER_env(Env) %9BEAMER_envargs(Env Args) %4BEAMER_col(Col) %10BEAMER_extra(Extra)


* Bulk Freight Rail Scheduling
** A quick introduction
Given a set of partially specified desired services to run on a rail
network, fill in the exact timings.

- No two trains at the same place simultaneously
- Maximize tonnes throughput

** Why model in Golog

- Easy to model the single-agent version
- Efficient to find solutions as program leaves little branching

** How to model in Golog
*** BFRSP Golog code 					      :BMCOL:B_block:
    :PROPERTIES:
    :BEAMER_env: block
    :END:
\begin{align*}
& \texttt{proc deliverservice(a, o):} \\
& \quad \texttt{foreach } (b, t0, t1) \texttt{ in } o \texttt{ do }\\
& \quad \quad (\pi dt \in [0..t1-\texttt{time}()). \texttt{wait}(a, dt)); \\
& \quad \quad \texttt{travelto}(a, o, b, t0, t1) \\
& \quad \texttt{end}\\
& \texttt{end}
\end{align*}



** Optimisation in Golog
Even the single agent problem requires optimisation (shortest path)

- Implemented a naive branch-and-bound search algorithm for Golog
- Cost-optimal planning in Golog is surprisingly easy

** Multi-agent combinatiorial explosion 
Early choices have significant cost implications, and there are a *lot* of possibilities:

- Partitioning $n$ orders to assign to $k$ different agents gives us $2^n$ assignments
- Each agent must then order its approx. $n/k$ orders in one of $(n/k)!$ ways

Best case combinations: $2^n \cdot (n/k)!$ (assuming $n >> k$)

** How can this ever be tractable?

Most of these sequences and partitions are obviously bad given a
heuristic solution.

- We never need to investigate most of these partitions
  - But planners may only see the consequences deep in the search
  - Heuristics likely accurate for path-finding

We would like our agents to learn how to cooperate.
Perhaps by knowing what one agent's actions cost another.


* Linear Programming and Column Generation
** Integer programming general case

\begin{align*}
&\text{Maximize:}                                     & \\
&\qquad \sum \alpha_i \cdot x_i        & \\
&\text{Subject To:}                                   & \\
&\qquad \sum c_{ir} \cdot x_i \le a_r   & \forall r \in R \\
&\qquad x_i \in \{0, 1\}                              & 
\end{align*}

** BFRSP example

\begin{align*}
&\text{Maximize:}                                                & \\
&\qquad \sum\limits_{f \in F} (M - d_f) \cdot x_f    & \\
&\text{Subject To:} &\\
&\qquad \sum\limits_{f \in F_{bt}} x_f \le 1                        & \forall b \in B \,, \forall t \in T \\
&\qquad \sum\limits_{f \in F_{o}} x_f \le 1                         & \forall o \in O\\
&\qquad x_f \in \{0, 1\}                                         & \forall f \in F
\end{align*}


Where $x_f$ is $1$ iff $f$ should be executed.
$F_o \subseteq F$ satify order $o$, $F_{bt}
\subseteq F$ are on block $b$ at time $t$
and $d_f$ is the duration of $f$.

** Lazy column generation
   Enumerating F is prohibitive: 

    - $>> 2^n$ possible ways to deliver each order

   We'd like to start with only the best way to satisfy each order,
   then add interesting columns, which improve our global solution*.
 
** LP duality
 - Q: What's the cost of consuming 1 unit of $a_r$?
 - A: $\pi_r$, computing this is a side-effect of computing the linear relaxation.
      $\pi_r$ is known as the dual price of $r$.

 If we know what shared resources actions consume, we can learn the
 global cost function as we get closer to the optimal solution.

** Reduced cost
- Reduced cost is \[ \text{intrinsic cost} + \sum\limits_{r \in R} u_r \cdot \pi_r\]
- Reduced cost is a relaxation of how much a fragment can improve the current optimum solution.
- If a fragment has non-negative reduced cost, it cannot improve the global solution.

** Column Generation

- Solve each subgoal independently.
  - We are only interested in plans with negative reduced cost.
- Solve linear relaxation
- Modify action costs using \pi_r values
- Repeat

  When we iterate this process we eventually converge to the *linear* optimum.

** Branch and Price

   Convergance to the linear optimum means we may be asked to execute a fraction of a plan.
   
   To reach an integer solution we need to use:

   - IP heuristics
   - Branch-and-Bound:
     - CG to get relaxed solution
     - Maybe try to generate a new heuristic solution
     - Prune tree if worse than current heuristic
     - Fathom tree if the CG solution happens to be integer

* Preliminary Results
** Preliminary results for BFRS
   - Find a feasible solution using a CG based heuristic to 160 orders over 480 (minimum) time-steps in 108s.
     - Evaluates under 180 fragments, < 0.1% of potential fragments.
   - Find a solution within 2% of optimal in ~20 minutes.
     - Evaluates < 1% of potential fragments.

   Still lots of algorithmic and heuristic improvements to be made.
