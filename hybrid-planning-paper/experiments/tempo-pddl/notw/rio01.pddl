(define (problem bfrs_44)
(:domain bfrs)
(:objects
    m_tp - location
    m_mj - location
    m_mnd - location
    m_ya - location
    m_b2 - location
    m_ma - location
    j_b - location
    m_b4 - location
    m_yac - location
    m_wa - location
    p_dampier - location
    m_hd - location
    y - location
    j_wa - location
    p_capelambert - location
    m_pbd - location
    o0 - order
    o1 - order
    o2 - order
    o3 - order
    o4 - order
    o5 - order
    o6 - order
    o7 - order
    o8 - order
    o9 - order
    o10 - order
    o11 - order
    o12 - order
    o13 - order
    o14 - order
    o15 - order
    o16 - order
    o17 - order
    o18 - order
    o19 - order
    o20 - order
    o21 - order
    o22 - order
    o23 - order
    o24 - order
    o25 - order
    o26 - order
    o27 - order
    o28 - order
    o29 - order
    o30 - order
    o31 - order
    o32 - order
    o33 - order
    o34 - order
    o35 - order
    o36 - order
    o37 - order
    o38 - order
    o39 - order
    o40 - order
    o41 - order
    o42 - order
    o43 - order
    c0 - consist
    c1 - consist
    c2 - consist
    c3 - consist
    c4 - consist
    c5 - consist
    c6 - consist
    c7 - consist
    c8 - consist
    c9 - consist
    c10 - consist
    c11 - consist
    c12 - consist
    c13 - consist
    c14 - consist
    c15 - consist
    c16 - consist
    c17 - consist
    c18 - consist
    c19 - consist
    c20 - consist
    c21 - consist
    c22 - consist
    c23 - consist
    c24 - consist
    c25 - consist
    c26 - consist
    c27 - consist
    c28 - consist
    c29 - consist
    c30 - consist
    c31 - consist
    c32 - consist
    c33 - consist
    c34 - consist
    c35 - consist
    c36 - consist
    c37 - consist
    c38 - consist
    c39 - consist
    c40 - consist
    c41 - consist
    c42 - consist
    c43 - consist
)
(:init
  (is_yard y)
    (clear m_tp)
    (clear m_mj)
    (clear m_mnd)
    (clear m_ya)
    (clear m_b2)
    (clear m_ma)
    (clear j_b)
    (clear m_b4)
    (clear m_yac)
    (clear m_wa)
    (clear p_dampier)
    (clear m_hd)
    (clear y)
    (clear j_wa)
    (clear p_capelambert)
    (clear m_pbd)

    (conn m_tp j_b)
    (conn j_b m_tp)
    (conn m_tp m_pbd)
    (conn m_pbd m_tp)
    (conn m_mj y)
    (conn y m_mj)
    (conn m_mj m_ma)
    (conn m_ma m_mj)
    (conn m_mnd j_b)
    (conn j_b m_mnd)
    (conn m_mnd m_ya)
    (conn m_ya m_mnd)
    (conn m_ya m_yac)
    (conn m_yac m_ya)
    (conn m_b2 j_b)
    (conn j_b m_b2)
    (conn m_b2 m_b4)
    (conn m_b4 m_b2)
    (conn j_b y)
    (conn y j_b)
    (conn j_b j_wa)
    (conn j_wa j_b)
    (conn m_wa j_wa)
    (conn j_wa m_wa)
    (conn p_dampier y)
    (conn y p_dampier)
    (conn m_hd j_wa)
    (conn j_wa m_hd)
    (conn y p_capelambert)
    (conn p_capelambert y)

    ;; order o0 ('m_tp', 'p_dampier', 0, 45)
    (order_mine o0 m_tp)
    (order_port o0 p_dampier)
    (not-delivered o0)

    ;; consist c0
    (available c0)
    (empty c0)
    (at c0 y)

    (compat c0 o0)

    ;; order o1 ('m_tp', 'p_dampier', 45, 90)
    (order_mine o1 m_tp)
    (order_port o1 p_dampier)
    (not-delivered o1)

    ;; consist c1
    (available c1)
    (empty c1)
    (at c1 y)

    (compat c1 o1)

    ;; order o2 ('m_tp', 'p_capelambert', 0, 45)
    (order_mine o2 m_tp)
    (order_port o2 p_capelambert)
    (not-delivered o2)

    ;; consist c2
    (available c2)
    (empty c2)
    (at c2 y)

    (compat c2 o2)

    ;; order o3 ('m_tp', 'p_capelambert', 45, 90)
    (order_mine o3 m_tp)
    (order_port o3 p_capelambert)
    (not-delivered o3)

    ;; consist c3
    (available c3)
    (empty c3)
    (at c3 y)

    (compat c3 o3)

    ;; order o4 ('m_mj', 'p_dampier', 0, 45)
    (order_mine o4 m_mj)
    (order_port o4 p_dampier)
    (not-delivered o4)

    ;; consist c4
    (available c4)
    (empty c4)
    (at c4 y)

    (compat c4 o4)

    ;; order o5 ('m_mj', 'p_dampier', 45, 90)
    (order_mine o5 m_mj)
    (order_port o5 p_dampier)
    (not-delivered o5)

    ;; consist c5
    (available c5)
    (empty c5)
    (at c5 y)

    (compat c5 o5)

    ;; order o6 ('m_mj', 'p_capelambert', 0, 45)
    (order_mine o6 m_mj)
    (order_port o6 p_capelambert)
    (not-delivered o6)

    ;; consist c6
    (available c6)
    (empty c6)
    (at c6 y)

    (compat c6 o6)

    ;; order o7 ('m_mj', 'p_capelambert', 45, 90)
    (order_mine o7 m_mj)
    (order_port o7 p_capelambert)
    (not-delivered o7)

    ;; consist c7
    (available c7)
    (empty c7)
    (at c7 y)

    (compat c7 o7)

    ;; order o8 ('m_mnd', 'p_dampier', 0, 45)
    (order_mine o8 m_mnd)
    (order_port o8 p_dampier)
    (not-delivered o8)

    ;; consist c8
    (available c8)
    (empty c8)
    (at c8 y)

    (compat c8 o8)

    ;; order o9 ('m_mnd', 'p_dampier', 45, 90)
    (order_mine o9 m_mnd)
    (order_port o9 p_dampier)
    (not-delivered o9)

    ;; consist c9
    (available c9)
    (empty c9)
    (at c9 y)

    (compat c9 o9)

    ;; order o10 ('m_mnd', 'p_capelambert', 0, 45)
    (order_mine o10 m_mnd)
    (order_port o10 p_capelambert)
    (not-delivered o10)

    ;; consist c10
    (available c10)
    (empty c10)
    (at c10 y)

    (compat c10 o10)

    ;; order o11 ('m_mnd', 'p_capelambert', 45, 90)
    (order_mine o11 m_mnd)
    (order_port o11 p_capelambert)
    (not-delivered o11)

    ;; consist c11
    (available c11)
    (empty c11)
    (at c11 y)

    (compat c11 o11)

    ;; order o12 ('m_ya', 'p_dampier', 0, 45)
    (order_mine o12 m_ya)
    (order_port o12 p_dampier)
    (not-delivered o12)

    ;; consist c12
    (available c12)
    (empty c12)
    (at c12 y)

    (compat c12 o12)

    ;; order o13 ('m_ya', 'p_dampier', 45, 90)
    (order_mine o13 m_ya)
    (order_port o13 p_dampier)
    (not-delivered o13)

    ;; consist c13
    (available c13)
    (empty c13)
    (at c13 y)

    (compat c13 o13)

    ;; order o14 ('m_ya', 'p_capelambert', 0, 45)
    (order_mine o14 m_ya)
    (order_port o14 p_capelambert)
    (not-delivered o14)

    ;; consist c14
    (available c14)
    (empty c14)
    (at c14 y)

    (compat c14 o14)

    ;; order o15 ('m_ya', 'p_capelambert', 45, 90)
    (order_mine o15 m_ya)
    (order_port o15 p_capelambert)
    (not-delivered o15)

    ;; consist c15
    (available c15)
    (empty c15)
    (at c15 y)

    (compat c15 o15)

    ;; order o16 ('m_b2', 'p_dampier', 0, 45)
    (order_mine o16 m_b2)
    (order_port o16 p_dampier)
    (not-delivered o16)

    ;; consist c16
    (available c16)
    (empty c16)
    (at c16 y)

    (compat c16 o16)

    ;; order o17 ('m_b2', 'p_dampier', 45, 90)
    (order_mine o17 m_b2)
    (order_port o17 p_dampier)
    (not-delivered o17)

    ;; consist c17
    (available c17)
    (empty c17)
    (at c17 y)

    (compat c17 o17)

    ;; order o18 ('m_b2', 'p_capelambert', 0, 45)
    (order_mine o18 m_b2)
    (order_port o18 p_capelambert)
    (not-delivered o18)

    ;; consist c18
    (available c18)
    (empty c18)
    (at c18 y)

    (compat c18 o18)

    ;; order o19 ('m_b2', 'p_capelambert', 45, 90)
    (order_mine o19 m_b2)
    (order_port o19 p_capelambert)
    (not-delivered o19)

    ;; consist c19
    (available c19)
    (empty c19)
    (at c19 y)

    (compat c19 o19)

    ;; order o20 ('m_ma', 'p_dampier', 0, 45)
    (order_mine o20 m_ma)
    (order_port o20 p_dampier)
    (not-delivered o20)

    ;; consist c20
    (available c20)
    (empty c20)
    (at c20 y)

    (compat c20 o20)

    ;; order o21 ('m_ma', 'p_dampier', 45, 90)
    (order_mine o21 m_ma)
    (order_port o21 p_dampier)
    (not-delivered o21)

    ;; consist c21
    (available c21)
    (empty c21)
    (at c21 y)

    (compat c21 o21)

    ;; order o22 ('m_ma', 'p_capelambert', 0, 45)
    (order_mine o22 m_ma)
    (order_port o22 p_capelambert)
    (not-delivered o22)

    ;; consist c22
    (available c22)
    (empty c22)
    (at c22 y)

    (compat c22 o22)

    ;; order o23 ('m_ma', 'p_capelambert', 45, 90)
    (order_mine o23 m_ma)
    (order_port o23 p_capelambert)
    (not-delivered o23)

    ;; consist c23
    (available c23)
    (empty c23)
    (at c23 y)

    (compat c23 o23)

    ;; order o24 ('m_b4', 'p_dampier', 0, 45)
    (order_mine o24 m_b4)
    (order_port o24 p_dampier)
    (not-delivered o24)

    ;; consist c24
    (available c24)
    (empty c24)
    (at c24 y)

    (compat c24 o24)

    ;; order o25 ('m_b4', 'p_dampier', 45, 90)
    (order_mine o25 m_b4)
    (order_port o25 p_dampier)
    (not-delivered o25)

    ;; consist c25
    (available c25)
    (empty c25)
    (at c25 y)

    (compat c25 o25)

    ;; order o26 ('m_b4', 'p_capelambert', 0, 45)
    (order_mine o26 m_b4)
    (order_port o26 p_capelambert)
    (not-delivered o26)

    ;; consist c26
    (available c26)
    (empty c26)
    (at c26 y)

    (compat c26 o26)

    ;; order o27 ('m_b4', 'p_capelambert', 45, 90)
    (order_mine o27 m_b4)
    (order_port o27 p_capelambert)
    (not-delivered o27)

    ;; consist c27
    (available c27)
    (empty c27)
    (at c27 y)

    (compat c27 o27)

    ;; order o28 ('m_yac', 'p_dampier', 0, 45)
    (order_mine o28 m_yac)
    (order_port o28 p_dampier)
    (not-delivered o28)

    ;; consist c28
    (available c28)
    (empty c28)
    (at c28 y)

    (compat c28 o28)

    ;; order o29 ('m_yac', 'p_dampier', 45, 90)
    (order_mine o29 m_yac)
    (order_port o29 p_dampier)
    (not-delivered o29)

    ;; consist c29
    (available c29)
    (empty c29)
    (at c29 y)

    (compat c29 o29)

    ;; order o30 ('m_yac', 'p_capelambert', 0, 45)
    (order_mine o30 m_yac)
    (order_port o30 p_capelambert)
    (not-delivered o30)

    ;; consist c30
    (available c30)
    (empty c30)
    (at c30 y)

    (compat c30 o30)

    ;; order o31 ('m_yac', 'p_capelambert', 45, 90)
    (order_mine o31 m_yac)
    (order_port o31 p_capelambert)
    (not-delivered o31)

    ;; consist c31
    (available c31)
    (empty c31)
    (at c31 y)

    (compat c31 o31)

    ;; order o32 ('m_wa', 'p_dampier', 0, 45)
    (order_mine o32 m_wa)
    (order_port o32 p_dampier)
    (not-delivered o32)

    ;; consist c32
    (available c32)
    (empty c32)
    (at c32 y)

    (compat c32 o32)

    ;; order o33 ('m_wa', 'p_dampier', 45, 90)
    (order_mine o33 m_wa)
    (order_port o33 p_dampier)
    (not-delivered o33)

    ;; consist c33
    (available c33)
    (empty c33)
    (at c33 y)

    (compat c33 o33)

    ;; order o34 ('m_wa', 'p_capelambert', 0, 45)
    (order_mine o34 m_wa)
    (order_port o34 p_capelambert)
    (not-delivered o34)

    ;; consist c34
    (available c34)
    (empty c34)
    (at c34 y)

    (compat c34 o34)

    ;; order o35 ('m_wa', 'p_capelambert', 45, 90)
    (order_mine o35 m_wa)
    (order_port o35 p_capelambert)
    (not-delivered o35)

    ;; consist c35
    (available c35)
    (empty c35)
    (at c35 y)

    (compat c35 o35)

    ;; order o36 ('m_hd', 'p_dampier', 0, 45)
    (order_mine o36 m_hd)
    (order_port o36 p_dampier)
    (not-delivered o36)

    ;; consist c36
    (available c36)
    (empty c36)
    (at c36 y)

    (compat c36 o36)

    ;; order o37 ('m_hd', 'p_dampier', 45, 90)
    (order_mine o37 m_hd)
    (order_port o37 p_dampier)
    (not-delivered o37)

    ;; consist c37
    (available c37)
    (empty c37)
    (at c37 y)

    (compat c37 o37)

    ;; order o38 ('m_hd', 'p_capelambert', 0, 45)
    (order_mine o38 m_hd)
    (order_port o38 p_capelambert)
    (not-delivered o38)

    ;; consist c38
    (available c38)
    (empty c38)
    (at c38 y)

    (compat c38 o38)

    ;; order o39 ('m_hd', 'p_capelambert', 45, 90)
    (order_mine o39 m_hd)
    (order_port o39 p_capelambert)
    (not-delivered o39)

    ;; consist c39
    (available c39)
    (empty c39)
    (at c39 y)

    (compat c39 o39)

    ;; order o40 ('m_pbd', 'p_dampier', 0, 45)
    (order_mine o40 m_pbd)
    (order_port o40 p_dampier)
    (not-delivered o40)

    ;; consist c40
    (available c40)
    (empty c40)
    (at c40 y)

    (compat c40 o40)

    ;; order o41 ('m_pbd', 'p_dampier', 45, 90)
    (order_mine o41 m_pbd)
    (order_port o41 p_dampier)
    (not-delivered o41)

    ;; consist c41
    (available c41)
    (empty c41)
    (at c41 y)

    (compat c41 o41)

    ;; order o42 ('m_pbd', 'p_capelambert', 0, 45)
    (order_mine o42 m_pbd)
    (order_port o42 p_capelambert)
    (not-delivered o42)

    ;; consist c42
    (available c42)
    (empty c42)
    (at c42 y)

    (compat c42 o42)

    ;; order o43 ('m_pbd', 'p_capelambert', 45, 90)
    (order_mine o43 m_pbd)
    (order_port o43 p_capelambert)
    (not-delivered o43)

    ;; consist c43
    (available c43)
    (empty c43)
    (at c43 y)

    (compat c43 o43)

)
(:goal
(and
  (delivered o0)
  (at c0 y)
  (delivered o1)
  (at c1 y)
  (delivered o2)
  (at c2 y)
  (delivered o3)
  (at c3 y)
  (delivered o4)
  (at c4 y)
  (delivered o5)
  (at c5 y)
  (delivered o6)
  (at c6 y)
  (delivered o7)
  (at c7 y)
  (delivered o8)
  (at c8 y)
  (delivered o9)
  (at c9 y)
  (delivered o10)
  (at c10 y)
  (delivered o11)
  (at c11 y)
  (delivered o12)
  (at c12 y)
  (delivered o13)
  (at c13 y)
  (delivered o14)
  (at c14 y)
  (delivered o15)
  (at c15 y)
  (delivered o16)
  (at c16 y)
  (delivered o17)
  (at c17 y)
  (delivered o18)
  (at c18 y)
  (delivered o19)
  (at c19 y)
  (delivered o20)
  (at c20 y)
  (delivered o21)
  (at c21 y)
  (delivered o22)
  (at c22 y)
  (delivered o23)
  (at c23 y)
  (delivered o24)
  (at c24 y)
  (delivered o25)
  (at c25 y)
  (delivered o26)
  (at c26 y)
  (delivered o27)
  (at c27 y)
  (delivered o28)
  (at c28 y)
  (delivered o29)
  (at c29 y)
  (delivered o30)
  (at c30 y)
  (delivered o31)
  (at c31 y)
  (delivered o32)
  (at c32 y)
  (delivered o33)
  (at c33 y)
  (delivered o34)
  (at c34 y)
  (delivered o35)
  (at c35 y)
  (delivered o36)
  (at c36 y)
  (delivered o37)
  (at c37 y)
  (delivered o38)
  (at c38 y)
  (delivered o39)
  (at c39 y)
  (delivered o40)
  (at c40 y)
  (delivered o41)
  (at c41 y)
  (delivered o42)
  (at c42 y)
  (delivered o43)
  (at c43 y)
))
(:metric minimize (total-time)))

