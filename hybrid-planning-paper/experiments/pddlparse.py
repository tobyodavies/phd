import sexpdata

def alist_lookup_all(sexp, kw):
    for pair in sexp:
        head = pair[0]
        if head == kw:
            yield pair

def alist_lookup(sexp, kw):
    return next(alist_lookup_all(sexp, kw), None)

def to_tuples(facts):
    return [tuple(sexpdata.dumps(sym)
                          for sym in fact)
                   for fact in facts]

def load_prob(fname):
    with open(fname) as f:
        sexp = sexpdata.load(f)
        init = to_tuples(alist_lookup(sexp[1:], sexpdata.Symbol(':init'))[1:])
        goal = to_tuples(alist_lookup(sexp[1:], sexpdata.Symbol(':goal'))[1][1:])
    return init, goal

def binfunc2dict(facts, name):
    return {key: val
            for _, key, val in alist_lookup_all(facts, name)}

def parse_blocksworld(pfile):
    init, goal = load_prob(pfile)

    init_on = binfunc2dict(init, 'on')
    init_on.update({b: None
                    for _, b in alist_lookup_all(init, 'ontable')})

    goal_on = {b: None for b in init_on}
    goal_on.update(binfunc2dict(goal, 'on'))

    return init_on, goal_on
