#include "landmark.hxx"

#define MAX_OPS 10000

namespace lml {
typedef int OpID;
typedef unsigned FluentID;

using aptk::State;
using aptk::Fluent;
using aptk::STRIPS_Problem;
using aptk::WatchedLitSuccGen;
using aptk::Bit_Set;

template <class Container, class Filter>
void filter(Container& c, Filter f){
	for(int i = c.size() - 1; i >= 0; i--) {
		if(!f(c[i])){
			c[i] = std::move(c.back());
			c.pop_back();
		}
	}
}

Bit_Set regress_maximal(const STRIPS_Problem& prob, OpID op, Bit_Set max){
	auto act = prob.actions()[op];

	// auto edels = act->edel_set();
	// edels.remove(act->del_set());
	// max.set_union(edels);

	max.remove(act->add_set());
	max.set_union(act->prec_set());
	return max;
}

// double cost_efficiency(LMDB& lmdb, Landmark* lm) {
// 	double total_cost = 0.0;
// 	double min_cost = 1e9;
// 	for(auto op_bound : lm->op_counts) {
// 		double rc = lmdb.rc(op_bound.first, op_bound.second);
// 		total_cost += rc;
// 		min_cost = std::min(min_cost, rc);
// 	}
// 	return min_cost / total_cost;
// }

void LMDB::setup(){
	wl.init();
	disabled_lms.resize(prob.num_fluents());
	reduced_cost.resize(prob.num_actions());
	for(auto act : prob.actions()){
		reduced_cost[act->index()].clear();
		reduced_cost[act->index()].push_back(act->cost());
	}
	relevant_facts.resize(prob.num_fluents()-1);
	block_count.resize(prob.num_fluents());
}

void LMDB::applicable_actions(const State& s, std::vector<OpID>& ops) {
	wl.applicable_actions(s, ops);
	filter(ops, [&](OpID op){ return rc(op, 1) < 1e-6; });
	return;
}

Landmark* LMDB::add_lm(State s, std::vector<OpID> lm_vec) {

	all_lms.emplace_back(new Landmark());
	auto lm = all_lms.back().get();
	lm->id = all_lms.size();
	for(auto op : lm_vec)
		lm->op_counts[op] = 999;
	wl.reachable(s, [&](OpID op, State& s1){
			if(lm->op_counts.count(op)){
				lm->op_counts[op] = 1;
				return false;
			}
			return true;
		});
	for(auto op : lm_vec){
		if(lm->op_counts[op] > 1)
			lm->op_counts.erase(op);
	}
	lm->maximal = s.fluent_set();
	lm->cost_assigned = 0;
	applicable_lms.push_back(lm);
	return lm;
}

double LMDB::eval(State s) {
	filter(applicable_lms, [&](Landmark* lm) {
			if(!lm->applies(s)){
				add_watcher(s, lm);
				return false;
			}
			return true;
		});

	for(unsigned f = 0 ; f < prob.num_fluents(); f++) {
		if (!disabled_lms[f].empty() && !s.entails(f)) {
			filter(disabled_lms[f], [&](Landmark* lm){
					if(lm->applies(s)){
						lm->cost_assigned = 0.0;
						applicable_lms.push_back(lm);
						return false;
					}
					return true;
				});
		}
	}
	std::sort(applicable_lms.begin(), applicable_lms.end(),
		  [&](Landmark* lm0, Landmark* lm1){
			  return lm0->id < lm1->id;
		  });

	return incr_eval(s);
}

double LMDB::lmcut(State s) {

	for(unsigned f = 0; f < prob.num_fluents(); f++){
		std::vector<OpID> cut;
		State s2(s);
		if(!wl.reachable(s2, [&](OpID op, State& _s){
					if(prob.actions()[op]->add_set().isset(f)){
						cut.push_back(op);
						return false;
					}
					return true;
				})) {
			add_lm(s, cut);
		}
	}

	eval(s);
	std::vector<double> rc_for_lmcut(prob.num_actions());
	for(auto act : prob.actions()){
		rc_for_lmcut[act->index()] = rc(act->index(), 1);
	}
	
	wl.lmcut(s,
		 rc_for_lmcut,
		 [&](std::vector<int> cut){
			 add_lm(s, cut);
		 });

	return incr_eval(s);

}

double LMDB::incr_eval(const State& s) {
	double cost=0.0;
	for(auto lm : applicable_lms){
		if(true || lm->cost_assigned <= 1e-6) {
			double dcost = lm_cost(s, lm);
			lm->cost_assigned += dcost;
			discount_lm(lm, dcost);
			// if(dcost < 1) {
			// 	dump_lm(prob, lm, s);
			// 	std::cerr << "costs " << lm->cost_assigned 
			// 		  << ", can increase by " << lm_cost(s, lm)
			// 		  << std::endl;
			// }
		}
		cost += lm->cost_assigned;
	}
	return cost;
}

double LMDB::lm_cost(const State& s, Landmark* lm){
	double min_cost = 1e9;
	for(auto op_bound : lm->op_counts) {
		min_cost = std::min(min_cost,
				    rc(op_bound.first, op_bound.second));
	}
	return min_cost;
}

unsigned LMDB::num_active() const {
	return applicable_lms.size();
}


void LMDB::add_watcher(const State& s, Landmark* lm) {
	aptk::Bit_Set fs = s.fluent_set();
	fs.remove(lm->maximal);
	auto min_f = fs.min_elem();
	if (min_f >= prob.num_fluents()){
		min_f = lm->maximal.min_elem();
	}
	disabled_lms[min_f].push_back(lm);
}

double LMDB::rc(OpID op, unsigned occ) const {
	double cost = 0.0;
	unsigned i;
	for(i = 0; i < reduced_cost[op].size() && i < occ; i++){
		cost += reduced_cost[op][i];
	}
	if(i < occ)
		cost += (occ - i)*(prob.actions()[op]->cost());
	return cost;
}

void LMDB::discount_lm(Landmark* lm, double by){
	for(auto bound : lm->op_counts){
		discount_op(bound.first, bound.second, by);
	}
}

void LMDB::discount_op(OpID op, unsigned occ, double by){
	reduced_cost[op].resize(occ, prob.actions()[op]->cost());
	for(; occ > 0; occ--){
		if(by <= reduced_cost[op][occ-1]) {
			reduced_cost[op][occ-1] -= by;
			break;
		}
		by -= reduced_cost[op][occ-1];
		reduced_cost[op][occ-1] = 0;
	}
}

void LMDB::undiscount_op(OpID op, unsigned occ, double by){
	double total_cost = prob.actions()[op]->cost();
	for(; occ > 0; occ--){
		if(reduced_cost[op][occ-1] + by <= total_cost) {
			reduced_cost[op][occ-1] += by;
			break;
		}
		by -= (total_cost - reduced_cost[op][occ-1]);
		reduced_cost[op][occ-1] = total_cost;
	}
}

void LMDB::undiscount_lm(Landmark* lm, double by){
	for(auto bound : lm->op_counts) {
		undiscount_op(bound.first, bound.second, by);
	}
}


Landmark* LMDB::progress_lms(State s, OpID op) {
	Landmark* lm_nogood = NULL;
	auto& av = prob.actions()[op]->add_vec();
	auto& dv = prob.actions()[op]->del_vec();
	filter(applicable_lms, [&](Landmark* lm) {
			if(lm->op_counts.count(op) > 0){
				undiscount_lm(lm,
					      lm->cost_assigned);
				lm->cost_assigned = 0.0;
				add_watcher(s, lm);
				return false;
			}
			return true;
		});

	for(auto df : dv) {
		filter(disabled_lms[df], [&](Landmark* lm){
				if(lm->applies(s)) {
					lm->cost_assigned = 0.0;
					if(lm_nogood == NULL) // || lm_cost(s, lm) > lm_cost(s, lm_nogood))
						lm_nogood = lm;
					applicable_lms.push_back(lm);
					return false;
				}
				return true;
			});
	}
	incr_eval(s);
	if(lm_nogood && lm_nogood->cost_assigned > 1e-6)
		return lm_nogood;
	auto zrc = [&](OpID op){
		return rc(op, 1) <= 1e-6;
	};
	if(!wl.reachable(s, [&](OpID op, State& s){return zrc(op);})) {
		std::vector<int> cut;
		wl.mincut(s, cut, zrc);
		lm_nogood = add_lm(s, cut);
	}
	return lm_nogood;
}

void LMDB::backtrack(const State& s, OpID op) {
	auto& av = prob.actions()[op]->add_vec();
	auto& dv = prob.actions()[op]->del_vec();
	for(auto f : av){
		filter(disabled_lms[f], [&](Landmark* lm){
				if(lm->applies(s)){
					lm->cost_assigned = 0;
					applicable_lms.push_back(lm);
				} else {
					add_watcher(s, lm);
				}
				return false;

			});
	}
	filter(applicable_lms, [&](Landmark* lm){
			for(auto df : dv){
				if(!lm->maximal.isset(df)){
					undiscount_lm(lm, lm->cost_assigned);
					lm->cost_assigned = 0.0;
					add_watcher(s, lm);
					return false;
				}
			}
			return true;
		});
	for(auto df : dv){
		wl.map_watching(s, df, [&](WatchedLitSuccGen::watcher& w){
				return !w.triggers(prob, s);
			});
	}
}

Landmark* LMDB::regress(const State& s, const std::vector< std::pair<OpID, Landmark*> >& succ_lms) {
	// // Sort lms by the number of syntactic states we expect them to match.
	// std::sort(succ_lms.begin(), succ_lms.end(),
	// 	  [&](std::pair<OpID, Landmark*> p0, p1){
	// 		  unsigned b0 = p0.second->maximal.bits().count_elements();
	// 		  unsigned b1 = p1.second->maximal.bits().count_elements();
	// 		  return b0 > b1;
	// 	  });

	all_lms.emplace_back(new Landmark());
	auto& glm = all_lms.back();
	glm->id = all_lms.size();
	glm->maximal.resize(prob.num_fluents()-1);
	glm->maximal.set_all();
	for(auto  succ_lm : succ_lms){
		if(succ_lm.second == NULL)
			continue;
		OpID op = succ_lm.first;
		Landmark* slm = succ_lm.second;
		glm->maximal.set_intersection(regress_maximal(prob, op, slm->maximal));
		for(auto lm_op : slm->op_counts) {
			auto existing_lb = glm->op_counts.count(lm_op.first)?
				glm->op_counts[lm_op.first]:
				MAX_OPS;
			glm->op_counts[lm_op.first] = std::min(existing_lb,
							       lm_op.second + int(lm_op.first == op));
		}
	}

	for(unsigned f : glm->maximal ) {
		wl.map_watching(s, f, [&](WatchedLitSuccGen::watcher w){
				int op = w.op;
				auto act = prob.actions()[op];
				if(s.entails(w.blocker) && s.fluent_set().contains(act->prec_set()) && rc(op, 1) >= 0){
					glm->op_counts[op] = 1;
				} else if(glm->maximal.isset(w.blocker) && glm->maximal.contains(act->prec_set())){
					glm->maximal.unset(f);
				}
				return false;
			});
	}
	applicable_lms.push_back(glm.get());
	return glm.get();
}

}
