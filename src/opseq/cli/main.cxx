/*
Lightweight Automated Planning Toolkit
Copyright (C) 2012
Miquel Ramirez <miquel.ramirez@rmit.edu.au>
Nir Lipovetzky <nirlipo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <strips_prob.hxx>
#include <action.hxx>
#include <strips_state.hxx>
#include <fd_to_aptk.hxx>
#include <watched_lit_succ_gen.hxx>
#include <aptk/time.hxx>
#include <reachability.hxx>

#include <iostream>
#include <iterator>
#include <boost/program_options.hpp>
#include <vector>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <utility>
#include <string>

#include <minisat/simp/SimpSolver.h>
typedef Minisat::SimpSolver CNF;
using Minisat::var;
using Minisat::mkLit;

#include "opseq/opcount.hxx"
#include "opseq/flow_abstraction.hxx"
#include "opseq/opsetsat.hxx"

namespace po = boost::program_options;

using aptk::STRIPS_Problem;
using aptk::State;
using opseq::OpSetEngine;
using opseq::OpCounter;
using opseq::Abstraction;
using aptk::WatchedLitSuccGen;

struct Timer {
	std::string msg;
	clock_t t0;
	Timer(std::string msg_):msg(msg_), t0(clock()){
		std::cerr << "Start " << msg << std::endl;
	}
	~Timer(){
		std::cerr << msg << " took " << 1.0*(clock() - t0)/CLOCKS_PER_SEC << " seconds"  << std::endl;
	}
};


void process_command_line_options( int ac, char** av, po::variables_map& vars ) {
	po::options_description desc( "Options:" );
	
	desc.add_options()
		( "help", "Show help message" )
		( "sas-file",
		  po::value<std::string>()->default_value("output"),
		  "FD preprocessor output file" )
	;
	
	try {
		po::store( po::parse_command_line( ac, av, desc ), vars );
		po::notify( vars );
	}
	catch ( std::exception& e ) {
		std::cerr << "Error: " << e.what() << std::endl;
		std::exit(1);
	}

	if ( vars.count("help") ) {
		std::cout << desc << std::endl;
		std::exit(0);
	}

}

typedef std::unordered_multimap<unsigned, std::pair<unsigned, unsigned> > op_prevail_pairs;

op_prevail_pairs pairsToMerge(STRIPS_Problem& prob, std::vector<aptk::FD_Parser::DTG>& dtgs){
	std::map<unsigned, unsigned> fluent_var;

	unsigned var = 0;
	for(auto& dtg : dtgs){
		for(unsigned src_idx = 0; src_idx < dtg.values.size(); src_idx++){
			fluent_var[dtg.values[src_idx]] = var;
		}
		var++;
	}
	op_prevail_pairs relevant_pairs;
	var = 0;
	for(auto& dtg : dtgs){
		for(auto op_trans : dtg.ops){
			for(auto prec : prob.actions()[op_trans.first]->prec_vec()){
				if(prob.actions()[op_trans.first]->del_set().isset(prec))
					continue;
				auto prev_var = fluent_var[prec];
				if(prev_var != var){
					int d_size = dtg.values.size() - dtgs[prev_var].values.size();
					bool smaller = d_size < 0 || (d_size == 0 && var < prev_var);
					auto min_var =  smaller ? var : prev_var;
					auto max_var = !smaller ? var : prev_var;
					assert(min_var != max_var);
					assert(min_var == var || min_var == prev_var);
					assert(max_var == var || max_var == prev_var);
					auto key = std::make_pair(op_trans.first,
								  std::make_pair(min_var, max_var));
					auto ins = relevant_pairs.insert(key);
				}
			}
		}
		var++;
	}
	std::cerr << "Found " << relevant_pairs.size() << " pairs." << std::endl;
	return relevant_pairs;
}

typedef std::vector<Abstraction> flows_t;

void addSingleFlows(STRIPS_Problem& prob, opseq::OpCounter& oc, std::vector<aptk::FD_Parser::DTG>& dtgs, flows_t& flows){
	unsigned i = 0;
	for(auto& dtg : dtgs){
		std::cerr << "DTG: " << i << std::endl;
		std::cerr << dtg.values.size() << std::endl;
		flows.emplace_back(oc, dtg.values, dtg.init_idx + 1, dtg.goal_idx + 1);
		Abstraction& flow = flows.back();
		for(auto& op_tr : dtg.ops){
			unsigned op = op_tr.first;
			int src_idx = op_tr.second.src;
			int dst_idx = op_tr.second.dst;

			flow.addTransition(op, src_idx + 1, dst_idx + 1);
		}
		flow.conserveAll();
	}
}

void addPairFlows(STRIPS_Problem& prob, opseq::OpCounter& oc, std::vector<aptk::FD_Parser::DTG>& dtgs, flows_t& flows){
	oc.optimize();
	//oc.addDelayed();
	auto relevant_pairs = pairsToMerge(prob, dtgs);
	int n = 0;
	for(auto to_merge : relevant_pairs){
		unsigned op, i, j;
		op = to_merge.first;
		std::tie(i, j) = to_merge.second;
		if(oc.countVal(op) < .01)
			continue;
		std::cerr << "Merging " << i << "x" << j
			  << "(" << ++n << "/" << relevant_pairs.size() << ")"
			  << std::endl;
		auto& dtg0 = flows.at(i);
		auto& dtg1 = flows.at(j);
		std::cerr << "Size = " << dtg0.size() << "x" << dtg1.size() << " = "<< dtg0.size()*dtg1.size() << std::endl;
		Abstraction flow(dtg0, dtg1);

		flow.constrainOp(op);
		std::cerr << "Generating Constraints for" << prob.actions()[op]->signature() << std::endl;
	}
	std::cerr << "Merging finished" << std::endl;
}

template<class C>
bool HittingSet(WatchedLitSuccGen& wl, STRIPS_Problem& prob, State& s0, std::vector<unsigned> goal, GRBLinExpr& lm, C& oc){
	return HittingSet(wl, prob, s0, goal, lm, oc,
			  [&](unsigned op){
				  return oc.countVal(op) > 1e-5;
			  });
}

template<class C>
bool HittingSet(WatchedLitSuccGen& wl, STRIPS_Problem& prob, State& s0, std::vector<unsigned> goal, GRBLinExpr& lm, C& oc,
		std::function<bool(unsigned)> usable){


	std::vector<bool> cut(prob.num_actions());
	std::vector<unsigned> cut_vec;

	if(wl.reachable(
		s0,
		[&](unsigned op, const State& s2){
			if(s2.entails(goal))
				return false;
			if(!usable(op)) {
				cut[op] = true;
				cut_vec.push_back(op);
				return false;
			}
			return true;
		})){
		return false;
	}
	State s(prob);
	for(auto uncut_op : cut_vec){
		if(!cut[uncut_op])
			continue;
		auto act = prob.actions()[uncut_op];
		
		cut[uncut_op] = false;

		s.reset();
		s.set(s0.fluent_vec());
		s.set(act->add_vec());
		bool goal_reached = 
			wl.reachable(
				s,
				s0.fluent_vec().size(),
				[&](unsigned op, const State& s2){
					if(s2.entails(goal) || s2.entails(prob.actions()[op]->add_vec())){
						return false;
					}
					if(cut[op]){
						return false;
					}
					return true;
				});
		if(!goal_reached){
			s0.set(s.fluent_vec());
		} else {
			lm += oc.countGE(uncut_op, 1);
			cut[uncut_op] = true;
		}
	}
	return true;
}

template<class C>
bool HittingSet(WatchedLitSuccGen& wl, STRIPS_Problem& prob, std::vector<unsigned> goal, GRBLinExpr& lm, C& oc){
	State s0(prob);
	s0.set(prob.init());
	return HittingSet(wl, prob, s0, goal, lm, oc);
}

struct MyCB : public GRBCallback {
	struct InCBSentry {
		bool& val;
		InCBSentry(bool& val): val(val) {
			val = true;
		}
		~InCBSentry(){ val = false; }
	};


	MyCB(STRIPS_Problem& prob, OpCounter& oc):
		prob(prob), oc(oc), s0(prob), wl(prob), seq(prob), cut(prob.num_actions()-1), dr_cut(prob.num_actions()-1), best_cost(1e9)
	{
		oc.getModel().getEnv().set(GRB_IntParam_LazyConstraints, 1);
		oc.getModel().setCallback(this);
		seq.add_relaxation();
		seq.ensure_layers(5);
	}

	~MyCB(){
		oc.getModel().setCallback(NULL);
	}

	GRBVar countGE(unsigned op, unsigned lb){
		return oc.countGE(op, lb);
	}

	double countVal1(unsigned op){
		if (where == GRB_CB_MIPSOL)
			return getSolution(oc.countGE(op, 1));
		if (where == GRB_CB_MIPNODE)
			return getNodeRel(oc.countGE(op, 1));
		return oc.countGE(op, 1).get(GRB_DoubleAttr_X);
	}

	double countVal(unsigned op){
		if (in_cb && where == GRB_CB_MIPSOL)
			return getSolution(oc.count(op));
		if (in_cb && where == GRB_CB_MIPNODE)
			return getNodeRel(oc.count(op));
		return oc.countVal(op);
	}

	// This seems to just hinder solving
	void addBasicPrevFlows(){
		return;
		compute_ops();
		op_prevail_pairs::const_iterator it, end;
		for(auto to_merge : pairs){
			unsigned op, i, j;
			op = to_merge.first;
			if(ops[op] == 0)
				continue;
			std::tie(i, j) = to_merge.second;
			auto& dtg0 = flows.at(i);
			auto& dtg1 = flows.at(j);
			if(!merged_flows.count(std::make_pair(i,j))){
				merged_flows.insert(
					std::make_pair(
						std::make_pair(i, j),
						Abstraction(dtg0, dtg1)));
			}
			auto& flow = merged_flows.at(std::make_pair(i,j));
			unsigned node = flow.prev_op_node[op];
			if(flow.merged_nodes.count(flow.prev_op_node[op]) == 0){
				std::cerr << "Merging " << op << std::endl;
				flow.conserveAll();
				// flow.conserveFlow(node);
				// for( auto in_op : flow.inflow_ops[node]){
				// 	flow.constrainOp(in_op);
				// }
				// for( auto out_op : flow.outflow_ops[node]){
				// 	flow.constrainOp(out_op);
				// }
				// if(flow.merged_nodes.count(flow.prev_op_node[op]) == 0){
				// 	std::cerr << "WTF?\n";
				// }
				abort();
			}
		}
		for(auto op : ops)
			if(op.second > 0)
				pairs.erase(op.first);
	}

	void callback() {
		if(!(where == GRB_CB_MIPSOL
		     ||
		     (where == GRB_CB_MIPNODE
		      &&
		      getIntInfo(GRB_CB_MIPNODE_STATUS) == GRB_OPTIMAL)
		    )
		   )
			return;
		double lb_cost = (where == GRB_CB_MIPSOL)?
			getDoubleInfo(GRB_CB_MIPSOL_OBJBND):
			getDoubleInfo(GRB_CB_MIPNODE_OBJBND);
		oc.setLB(lb_cost);
		
		//Timer t("Callback");
		InCBSentry sentry(in_cb);

		s0.reset();
		s0.set(prob.init());
		GRBLinExpr lm;
		if(HittingSet(wl, prob, s0, prob.goal(), lm, *this)) {
			addLazy(lm >= 1);
			oc.addConstr(lm >= 1);
			return;
		}

		// if(where == GRB_CB_MIPNODE
		//    &&
		//    getIntInfo(GRB_CB_MIPNODE_STATUS) == GRB_OPTIMAL)
		// 	addBasicPrevFlows();

		if(!(where == GRB_CB_MIPSOL))
			return;
		// if(1.15*oc.getLB() < getDoubleInfo(GRB_CB_MIPSOL_OBJ))
		// 	return;



		cut.reset();
		if(!seq.search(ops, plan, cut, 100)) {
			bool safe = true;
			GRBLinExpr glm;
			int cutsize = 0;
			for(unsigned op : cut) {
				cutsize++;
				if(!oc.safeCount(op,  ops[op] + 1)){
					safe = false;
				}
				glm += oc.countGE(op, ops[op] + 1);
			}
			if (safe){
				if(!cutsize)
					return;
				std::cerr << "Add Lazy" << std::endl;
				addLazy(glm >= 1);
				oc.addConstr(glm >= 1);
			} else {
				std::cerr << "Abort" << std::endl;
				abort();
			}

		} // else {
		// 	std::cerr << "Plan found" << std::endl;
		// 	int cost = plan_cost(plan);
		// 	if( cost < best_cost){
		// 		best_plan = plan;
		// 		best_cost = cost;
		// 	}
		// }
	}

	int compute_ops(){
		int num_ops = 0;
		int exp_cost = 0;
		for(unsigned op = 0 ; op < prob.num_actions(); op++){
			ops[op] = (unsigned)(countVal(op) + .95);
			num_ops += ops[op];
			exp_cost += prob.actions()[op]->cost();
		}
		return exp_cost;
	}

	void compute_ops_from_mip(){
		for(unsigned op = 0 ; op < prob.num_actions(); op++){
			ops[op] = (unsigned)(oc.countVal(op) + .95);
		}	
	}

	bool sequence(GRBLinExpr& glm){
		plan.clear();
		cut.reset();
		compute_ops();
		if(!seq.search(ops, plan, cut)) {

			for(unsigned op : cut) {
				if(oc.safeCount(op,  ops[op] + 1)){
					glm += oc.countGE(op, ops[op] + 1);
					std::cerr << "oc.countGE(" 
						  << op
						  << ","
						  << ops[op] + 1
						  << ") + ";
				}
			}
			std::cerr << "0 >= 1" << std::endl;
			return false;
		}
		std::cerr << "Plan found" << std::endl;
		int cost = plan_cost(plan);
		if( cost < best_cost){
			best_plan = plan;
			best_cost = cost;
			std::cerr << "Gap: " << int(best_cost - oc.getLB()) << std::endl;
		}
		return true;
	}

	int plan_cost(const std::vector<int>& p) {
		int sum = 0;
		for( auto op : p ){
			sum += prob.actions()[op]->cost();
		}
		return sum;
	}

	void print_plan(std::ostream& out) {
		for(auto op : best_plan){
			out << prob.actions()[op]->signature() << std::endl;
		}
	}

	STRIPS_Problem& prob;
	OpCounter& oc;
	unsigned i;
	State s0;
	WatchedLitSuccGen wl;
	OpSetEngine<STRIPS_Problem> seq;
	bool in_cb;

	int best_cost;
	std::vector<int> plan;
	std::vector<int> best_plan;

	opseq::OpSet ops;
	aptk::Bit_Set cut;
	aptk::Bit_Set dr_cut;

	flows_t flows;
	op_prevail_pairs pairs;
	std::map< std::pair<unsigned, unsigned>, Abstraction > merged_flows;
};

int main( int argc, char** argv ) {
	Timer search_timer("Search");
	po::variables_map vm;

	process_command_line_options( argc, argv, vm );

	STRIPS_Problem	prob;
	std::vector<aptk::FD_Parser::DTG> dtgs;
	aptk::FD_Parser::get_problem_description( vm["sas-file"].as<std::string>(), prob, dtgs );
	
	std::cout << "PDDL problem description loaded: " << std::endl;
	std::cout << "\t#Actions: " << prob.num_actions() << std::endl;
	std::cout << "\t#Fluents: " << prob.num_fluents() << std::endl;
	std::cout << "\t#Mutexes: " << prob.mutexes().num_groups() << std::endl;
	std::cout << "\t#DTGs: " << dtgs.size() << std::endl;
	prob.make_action_tables();
	prob.compute_edeletes();

	opseq::OpCounter oc(prob.actions(), prob.fluents());
	try {
		MyCB cb(prob, oc);
		addSingleFlows(prob, oc, dtgs, cb.flows);
		cb.pairs = pairsToMerge(prob, dtgs);
		{
			opseq::LinRelaxSentry relax(oc.getModel());
			oc.optimize();
		}
		while (true) {
			GRBLinExpr glm;
			if (!oc.optimize()){
				oc.getModel().write("model.lp");
				throw std::runtime_error("Optimisation failed");
			}
			if(!cb.sequence(glm)){
				oc.addConstr(glm >= 1);
				cb.addBasicPrevFlows();
			}
			if( !cb.best_plan.empty() ) {
				cb.print_plan(std::cout);
				break;
			}
		}

	} catch (GRBException& e) {
		oc.getModel().write("model.lp");
		std::cerr << "Error: " << e.getMessage() << std::endl;
	}
	return 0;
}
