import math
import networkx as nx
import itertools as it
import time

import pymas
import pymas.test.bulkfreight as bfrs

bfrs.BulkFreight.orders = [('m1', 'p2', 0, 12), ('m1', 'p2', 12, 24), ('m1', 'p1', 0, 12), ('m1', 'p1', 12, 24), ('m2', 'p2', 0, 12), ('m2', 'p2', 12, 24), ('m2', 'p1', 0, 12), ('m2', 'p1', 12, 24)]
bfrs.BulkFreight.graph = nx.from_edgelist([('p2', 'y'), ('p1', 'y'), ('d', 'y'), ('d', 'x'), ('d', 'm1'), ('d', 'm2'), ('y', 'x')])

start_t = time.time()
init = list(bfrs.solve(nfrags=int(2*math.log(8)+2)))
selector = pymas.FragSelector(init, bfrs.solve)
print "#frags:", len(selector.fragments)
initial_cost, selection = selector.heuristic_select()
print "Delivered:", len(selection)
print "Time:", time.time() - start_t
exes = [{sit.time(): act
         for (act, sit) in frag.sit.history}
        for frag in selection]
jexe = [(t, [exes[i].get(t) for i in xrange(len(selection))])
        for t in xrange(max(max(ex) for ex in exes))]
print "\n".join(str(e) for e in jexe)

