#include <siw_planner.hxx>
#include <strips_state.hxx>
#include <landmark_graph.hxx>
#include <landmark_graph_generator.hxx>

#include <aptk/resources_control.hxx>
#include <aptk/string_conversions.hxx>

#include <aptk/bit_array.hxx>
#include <opsetsat.hxx>

#include <random>
#include <iostream>
#include <fstream>

using	aptk::agnostic::Fwd_Search_Problem;
using 	aptk::agnostic::Landmarks_Graph_Generator;
using 	aptk::agnostic::Landmarks_Graph;

typedef         Landmarks_Graph_Generator<Fwd_Search_Problem>     	Gen_Lms_Fwd;


//typedef		Serialized_Search< Fwd_Search_Problem, IW_Fwd, IW_Node >        SIW_Fwd;

OpSetPlanner::OpSetPlanner()
	: STRIPS_Problem(), cut(n_actions()), m_log_filename( "iw.log"), m_plan_filename( "plan.ipc" ), opengine(*instance()) {
}

OpSetPlanner::OpSetPlanner( std::string domain_file, std::string instance_file )
	: STRIPS_Problem( domain_file, instance_file ), cut(n_actions()), m_log_filename( "iw.log" ), m_plan_filename( "plan.ipc" ), opengine(*instance()) {
}

OpSetPlanner::~OpSetPlanner() {
}


void	
OpSetPlanner::setup() {
	// MRJ: Call superclass method, then do you own thing here
	STRIPS_Problem::setup();
	std::cout << "PDDL problem description loaded: " << std::endl;
	std::cout << "\tDomain: " << instance()->domain_name() << std::endl;
	std::cout << "\tProblem: " << instance()->problem_name() << std::endl;
	std::cout << "\t#Actions: " << instance()->num_actions() << std::endl;
	std::cout << "\t#Fluents: " << instance()->num_fluents() << std::endl;
}


// aptk::search::opset::OpSet OpSetPlanner::landmark_cover(Fwd_Search_Problem search_prob, Landmarks_Graph& graph) {
// 	using namespace aptk;
// 	using namespace aptk::search::opset;
// 	OpSet opset;
// 	auto fluents = instance()->fluents();
// 	for (auto f = fluents.begin() ; f != fluents.end() ; f++){
// 		if(!graph.is_landmark((*f)->index())) continue;
// 		auto ops_adding = instance()->actions_adding((*f)->index());
// 		if(ops_adding.size() == 0){
// 			std::cout<< "Uncoverable fluent: " << (*f)->signature() << std::endl;
// 			continue;
// 		}
// 		bool covered = false;
// 		for (auto op = ops_adding.begin(); op != ops_adding.end(); op++){
// 			if(opset[(*op)->index()] > 0) {
// 				covered = true;
// 				std::cout << "Cover " << (*f)->signature() << " by re-using " << ops_adding[0]->signature() << std::endl;
// 				break;
// 			}
// 		}
// 		if (!covered){
// 			std::cout << "Cover " << (*f)->signature() << " with " << ops_adding[0]->signature() << std::endl;
// 			opset[ops_adding[0]->index()] = 1;
// 		}
// 	}
// 	return opset;
// }

bool OpSetPlanner::solve(int n_layers) {
	plan.clear();
	cut.reset();
	cut.resize(n_actions()-1);
	plan_found = opengine.search(opset, plan, cut, n_layers);
	return plan_found;
}

// void
// OpSetPlanner::solve() {
// 	using namespace aptk;
// 	std::cout << "#mutexes: " << instance()->mutexes().num_groups() << std::endl;

// 	Fwd_Search_Problem	search_prob( instance() );
// 	float t0 = aptk::time_used();
// 	instance()->compute_edeletes();

// 	Gen_Lms_Fwd    gen_lms( search_prob );
// 	Landmarks_Graph graph( *instance() );

// 	gen_lms.compute_lm_graph_set_additive( graph );
	
// 	std::cout << "Landmarks found: " << graph.num_landmarks() << std::endl;

// 	aptk::search::opset::OpSetEngine<aptk::STRIPS_Problem> opengine(*instance());
// 	aptk::search::opset::OpSet opset = landmark_cover(search_prob, graph);
	
// 	aptk::Bit_Array cut(instance()->num_actions());

// 	std::vector<Action_Idx> plan;
// 	t0 = aptk::time_used();
// 	int i = 0;
// 	while (!opengine.search(opset, plan, cut)){
// 		std::cout << "bounded search iter "<< i++ <<" took " << aptk::time_used() - t0 << std::endl;
// 		int cut_elems = cut.count_elements();
// 		int inc_budget_act_idx = rand()%cut_elems;
// 		int inc_budget_act = 0;
// 		std::cout << "Cut elems " << cut_elems << std::endl;
// 		for (int op = 0, i = 0 ; op < search_prob.num_actions(); op++){
// 			auto act = instance()->actions()[op];
// 			if(cut.isset(op)){
// 				if(i++ == inc_budget_act_idx){
// 					inc_budget_act = op;
// 				}
// 				std::cout<< "[" << act->signature() <<">" << opset[op] << "] + ";
// 			}
// 		}
// 		std::cout << "-1 >= 0" <<std::endl;
// 		std::cout << "Increase budget for " << inc_budget_act << std::endl;
// 		opset[inc_budget_act]++;
// 		std::cout << instance()->actions()[inc_budget_act]->signature() << std::endl;
// 		std::cout << "to " << opset[inc_budget_act] << std::endl;
// 		t0 = aptk::time_used();
// 		plan.clear();
// 	}
// 	std::cout << "final bounded search iter "<< i++ <<" took " << aptk::time_used() - t0 << std::endl;
// 	std::cout << "GOAL" << std::endl;
// 	for (auto p_op = begin(plan); p_op != end(plan); p_op++){
// 		std::cerr << instance()->actions()[*p_op]->signature() << std::endl;
// 	}

// }

