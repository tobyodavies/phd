/*
Lightweight Automated Planning Toolkit
Copyright (C) 2014
Toby Davies <tobyodavies@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __DEPTH_FIRST_SEARCH__
#define __DEPTH_FIRST_SEARCH__

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <iostream>
#include <aptk/search_prob.hxx>
#include <aptk/bit_array.hxx>



namespace aptk {

namespace search {

namespace opset {

typedef std::unordered_map<Action_Idx, unsigned>  OpSet;

template<typename State>
class ClosedList {
public:
	struct Hash {
		std::size_t operator() (State* os) const {
			return os->hash();
		}
	};

	struct Eq {
		bool operator() (State* const& lhs, State* const& rhs) const {
			return  lhs == rhs || *(lhs) == *(rhs);
		}
	};

	bool close(State* s, const OpSet& ops){
		//std::cout << "Check closed" << std::endl;
		auto range = closed.equal_range(s);
		//std::cout << "Closed candidates: " << closed.count(s) <<std::endl;
		for(auto it = range.first; it != range.second; ++it){
			bool subsumes = true;
			const OpSet& cached_ops = it->second;
			for(auto op = begin(ops); op != end(ops); ++op){
				Action_Idx op_idx = op->first;
				unsigned cached_count = cached_ops.count(op_idx)?cached_ops.at(op_idx):0;
				unsigned new_count = op->second;
				subsumes = subsumes && (cached_count >= new_count);
			}
			if (subsumes){
				return true;
			}
		}
		closed.insert(std::make_pair(s, ops));
		return false;
	}

private:
	std::unordered_multimap<State*, const OpSet, Hash, Eq> closed;
};

template<typename SearchProb>
class OpSetEngine {
private:

	typedef typename SearchProb::State_Type State;

	SearchProb& prob;
	OpSet opset;
	double expansions;
	Bit_Array cut;
	ClosedList<State> closed;

public:
	OpSetEngine(SearchProb& prob):prob(prob), cut(prob.num_actions()-1) {
		cut.reset();
	}
	virtual ~OpSetEngine(){
		// for (auto it = begin(closed); it != end(closed) ; it++){
		// 	delete it->second;
		// }
	}

	OpSet& op_bounds() { return opset; }
	Bit_Array get_cut() const { return cut;}

	bool opset_search(std::vector<Action_Idx>& plan, State* s0){
		assert(s0 != NULL);
		// Check if we have achieved the goal
		if (prob.goal(*s0)) {
			return true;
		}
		//std::cout << "beginning opset search" << std::endl;
		// Check we're not closed
		if (closed.close(s0, opset)) {
			//std::cout << "Prune closed node" << std::endl;
			delete s0;
			return false;
		}
		//std::cout << "expanding" << std::endl;
		std::vector<Action_Idx> app_ops;
		prob.applicable_set_v2(*s0, app_ops);
		expansions += app_ops.size();
		for(auto op = app_ops.begin(); op != app_ops.end(); op++){
			if (opset.count(*op) == 0 || opset[*op] <= 0 ){
				//std::cout << "Prune " << *op << std::endl;
				cut.set(*op);
				continue;
			}
		
			--opset[*op];
			plan.push_back(*op);
			auto s1 = prob.next(*s0, *op);
			bool success = opset_search(plan, s1);
			++opset[*op];
			if(success) {
				return true;
			}
			plan.pop_back();
		}
		return false;
	}
	bool search(std::vector<Action_Idx>& plan){
		//std::cout << "reset cut" << std::endl;
		cut.reset();
		return opset_search(plan, prob.init());
	}
};
}
}
}
#endif
