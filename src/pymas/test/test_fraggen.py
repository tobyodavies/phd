import pytest

from pymas import situation, fluent, action, NOOP, Var, VarChoice

## Domains

class TestSit(situation('TestSit')):
    @action
    def lock(sit, res_id):
        """
        A simple mutex style resource
        """
        return 1e-10, {sit.shared(res_id): 1}

    @action
    def unlock(sit, res_id):
        """
        A simple mutex style resource
        """
        return 1e-10, {sit.shared(res_id): -1}


import bulkfreight
from bulkfreight import BulkFreight

## /Domains


def test_noop():
    s0 = BulkFreight.s0()
    assert NOOP.final(s0)
    assert len(list(NOOP.trans(s0))) == 0


def test_seq():
    s0 = BulkFreight.s0()
    prog = s0.enter('d') + s0.enter('m1') + s0.load(('m1', 'p1', 0, 1000))
    print prog, s0.load(('m1', 'p1', 0, 1000)).fraggen()
    print prog.children, [child.final(s0) for child in prog.children]
    assert not prog.final(s0)
    transitions = list(prog.trans(s0))
    print transitions
    print s0, [sit for sit, rprog in transitions if sit == s0]
    assert s0 not in [sit for sit, rprog in transitions]
    assert len(transitions) == 1


def test_alt():
    s0 = BulkFreight.s0()
    enter = BulkFreight.enter
    prog = enter('d') | enter('p1') | enter('p2')
    print prog
    assert not prog.final(s0)
    assert (prog | NOOP).final(s0)
    assert len(list(prog.trans(s0))) > 1


def test_choice():
    s0 = BulkFreight.s0()
    edge = Var(s0.graph.nodes())
    prog = VarChoice(edge, s0.enter(edge))
    assert not prog.final(s0)
    print list(prog.trans(s0))
    assert 1 < len(list(prog.trans(s0))) <= len(edge.domain)

def test_iter():
    s0 = BulkFreight.s0()
    edge = Var(s0.graph.nodes())
    prog = VarChoice(edge, s0.enter(edge)).repeat()
    assert prog.final(s0)
    print list(prog.trans(s0))
    assert 1 < len(list(prog.trans(s0))) <= len(edge.domain) + 1


def test_poss():
    s0 = BulkFreight.s0()
    prog = s0.enter('m1').fraggen()
    assert len(list(prog.trans(s0))) == 0

### Test search

def test_search_1():
    s0 = BulkFreight.s0()
    prog = s0.travel_to('m1') + s0.load(('m1', 'p1', 0, 1000))
    all_solns = list(prog.solutions(s0))
    assert len(all_solns) >= 1


def test_search_1():
    s0 = BulkFreight.s0()
    edge = Var(s0.graph.nodes())
    prog = VarChoice(edge, s0.enter(edge)).repeat() + s0.load(('m1', 'p1', 0, 1000))
    soln = next(prog.solutions(s0, solution=s0), None)
    assert soln is not None

def test_search_1_5():
    s0 = BulkFreight.s0()
    edge = Var(s0.graph.nodes())
    prog = s0.travel_to('m1') + s0.load(('m1', 'p1', 0, 1000))
    soln = next(prog.solutions(s0, solution=s0))
    #assert len(all_solns) >= 1
    assert soln.at('m1')

def test_search_2():
    s0 = BulkFreight.s0()
    prog = s0.satisfy_order(('m1', 'p1', 0, 1000))
    all_solns = [s for (_, s) in zip(xrange(1), prog.solutions(s0, solution=s0))]
    print len(all_solns)
    assert len(all_solns) >= 1
    soln = all_solns[0]
    frag = soln.to_fragment()
    print [(act, act.cost(sit)) for act, sit in soln.history]
    assert frag.cost == sum(act.cost(sit) for (act, sit) in soln.history)
    assert frag.cost < 0
    assert len(frag.resources) >= 5

def test_search_3():
    assert 3 <= len(list(bulkfreight.solve(nfrags=4))) <= 5

if __name__ == '__main__':
    s0 = BulkFreight.s0()
    prog = s0.satisfy_order(('m1', 'p1'))
    print "xxx",
    print next(prog.solutions(s0, solution=s0))
    print "test2:",
    print test_search_2()
    print "test3:",
    print test_search_3()
