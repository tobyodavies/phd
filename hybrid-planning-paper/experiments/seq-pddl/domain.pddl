(define (domain Toby)
(:requirements :TYPING :action-costs
 )
(:types		
	location
	order
	consist
)
(:predicates 

	(available ?c)
       	(at ?c - consist ?loc - location)
	(loaded ?c - consist ?o - order)
	(empty ?c - consist)
	(clear ?loc - location)
	(delivered ?o - order)
	(not-delivered ?o - order)
	(conn ?from ?to - location)
	
	(order_mine ?o - order ?loc - location)	
	(order_port ?o - order ?loc - location)	

)
(:constants
	y - location
)


(:functions
	(total-cost) - number
	(current_time) - number
	(load_after_time ?o - order) - number
	(unload_before_time ?o - order) - number

)


(:action enter_section 
:parameters (?c - consist ?curpos ?nextpos - location) 
:precondition (and 
	         (available ?c)
		 (at ?c ?curpos)
		 (conn ?curpos ?nextpos) 
		 (clear ?nextpos) 
	   ) 
:effect (and  
              (not (available ?c))
	      (at ?c ?nextpos)   (not (at ?c ?curpos))  
            (not (clear ?nextpos))   (clear ?curpos) 
	    (increase (total-cost) 1)
	 ) 
) 

(:action enter_yard 
:parameters (?c - consist ?curpos - location) 
:precondition (and  
	      (at ?c ?curpos)  
	      (conn ?curpos y)  
     	      (available ?c)
	      ) 
:effect (and  
              (not (available ?c))
	      (at ?c y)   (not (at ?c ?curpos))
   	      (increase (total-cost) 1)
	 ) 
) 

(:action next_step 
:parameters ( ) 
:precondition (and  )
:effect (and  
	    (forall (?c - consist ) (and  (available ?c)) )
	    (increase (total-cost) 0)
	    (increase (current_time) 1) ;; REMEMBER CHANGE TO cross_time FUNCTION!!!

	)
) 


(:action load 
:parameters (?c - consist ?o - order ?curpos - location) 
:precondition (and 
	         (available ?c)
	   	 (empty ?c)
		 (not-delivered ?o)
		 (at ?c ?curpos)
		 (order_mine ?o ?curpos)
		 (>= (current_time) (load_after_time ?o ))
	   ) 
:effect (and  
              (not (available ?c))
	      (loaded ?c ?o)   (not (empty ?c))
  	      (increase (total-cost) 1)

	 ) 
) 

(:action unload 
:parameters (?c - consist ?o - order ?curpos - location) 
:precondition (and 
	         (available ?c)
		 (not-delivered ?o)
	   	 (loaded ?c ?o) 
		 (at ?c ?curpos)
		 (order_port ?o ?curpos)
		 (<= (current_time) (unload_before_time ?o))
	   ) 
:effect (and  
              (not (available ?c))
	      (not (loaded ?c ?o))   (empty ?c)   (delivered ?o)
              (not (not-delivered ?o))
	      (increase (total-cost) 1)
	 ) 
) 



)



