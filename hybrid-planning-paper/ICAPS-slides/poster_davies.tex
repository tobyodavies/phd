\documentclass[presentation]{beamer}
%\documentclass[handout,notes=show]{beamer}
\usetheme[sidebar=false]{NICTA}
%for default nicta style use:
\usepackage{etex}
\usepackage[labelformat=empty]{caption}

% Required Packages
\usepackage{times}
\usepackage{helvet}
\usepackage{courier}
\usepackage{verbatim}
\usepackage{natbib}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{bm} 

\usepackage[noend]{algpseudocode}
\usepackage{algorithm}
\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{arrows,automata}
\usepackage{xifthen}
%% \AtBeginSection[]
%% {
%%    \begin{frame}
%%        \frametitle{Outline}
%%        \tableofcontents[currentsection]
%%    \end{frame}
%% }


\input{../macros-sebastian}

\title[Fragment-based Planning]{Fragment-based Planning \\ using Column Generation}

\author[Davies et al.]{Toby O. Davies\inst{1}\\ \and Adrian R. Pearce\inst{1} \and Peter Stuckey\inst{1}
        \and Harald S\o{}ndergaard \inst{2}}
\institute[NICTA]{\inst{1} NICTA and The University of Melbourne \and %
                  \inst{2} The University of Melbourne}

\date{25th June 2014}


% user defined
\newcommand{\dwd}{Dantzig-Wolfe Decomposition}
\newcommand{\Bfrsp}{Bulk Freight Rail Scheduling Problem}
\newcommand{\bfrsp}{BFRSP}
\newcommand{\Fbp}{fragment-based planning}
\newcommand{\FBP}{Fragment-Based Planning}
\newcommand{\fbp}{FBP}

%\newcommand{\tablefont}{\small}
\newcommand{\tablefont}{\normalsize}

\newcommand{\quine}[1]{[\![#1]\!]}
\newcommand{\id}[1]{\mbox{\textit{#1}}}
\newcommand{\Frags}{\id{Frags}}
\newcommand{\Res}{\id{Res}}
\newcommand{\Goals}{\id{Goals}}
\newcommand{\LowBound}{\id{LowBound}}
\newcommand{\UpBound}{\id{UpBound}}
\newcommand{\SolveLP}{\id{SolveLP}}
\newcommand{\SolveIP}{\id{SolveIP}}
\newcommand{\LinFBP}{\id{LinFBP}}
\newcommand{\Queue}{\id{Queue}}
\newcommand{\initfrags}{\id{initfrags}}
\newcommand{\Branches}{\id{Branches}}
\newcommand{\BranchRs}{\id{BranchRs}}
\newcommand{\LRs}{\id{LRs}}
\newcommand{\Fs}{\id{Fs}}
\newcommand{\Gs}{\id{Gs}}
\newcommand{\Rs}{\id{Rs}}
\newcommand{\Clear}{\id{Clear}}
\newcommand{\Occupied}{\id{Occupied}}
\newcommand{\plusplus}{\mathbin{\mathtt{++}}}


\newcommand{\dcol}[1]{\multicolumn{2}{|c|}{#1}}

\newcommand{\maybehighlight}[2]{\ifthenelse{\isin{#2}{#1}}{\tikzset{fill=red!50}}{}}

\tikzset{
    Y/.code={\maybehighlight{#1}{Y}},
    D/.code={\maybehighlight{#1}{D}},
    M1/.code={\maybehighlight{#1}{M1}},
    M2/.code={\maybehighlight{#1}{M2}},
    P1/.code={\maybehighlight{#1}{P1}},
    P2/.code={\maybehighlight{#1}{P2}},
    state/.style={draw=black,text=black,shape=circle},
    branch/.style={draw=black,text=black},
}
\newcommand{\track}[1]{
  \begin{tikzpicture}[-,node distance=1.3cm,semithick]
    \node[state, Y=#1] (Y)                    {$y$};
    \node[state, D=#1] (D)  [left of=Y]       {$d$};
    \node[state,M1=#1] (M1) [above left of=D] {$m_1$};
    \node[state,M2=#1] (M2) [below left of=D] {$m_2$};
    \node[state,P1=#1] (P1) [above right of=Y]{$p_1$};
    \node[state,P2=#1] (P2) [below right of=Y]{$p_2$};
    
    \path (Y)  edge (D)
          (D)  edge (M1)
          (M2) edge (D)
          (Y)  edge (P1)
          (Y)  edge (P2)
          (Y)  edge (D)
          (D)  edge (M1)
          (Y)  edge (P1)
          (Y)  edge (D)
          (D)  edge (M2)
          (Y)  edge (P2);
  \end{tikzpicture}
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}
\note{introduce myself}

\begin{frame}
\frametitle{Fragment-Based Planning}
%(Using column generation)
Solves \textit{hybrid} Planning and Scheduling problems by combining:
  \begin{itemize}
  \item State-based search
  \item Mixed-integer programming
  \item Domain Control Knowledge (A Golog Program)
  \end{itemize}

Solved by \textbf{decomposition}, but retains \textbf{optimality}.
\end{frame}

\note{
  The paper discusses how we combine these three techniques using a decomposition.

  This paper isn't about a domain per-se, but the FBP algorithm was
  heavily inspired by a pattern I observed while building optimisation
  tools for Mining and Logistics companies.

  So I will explain how it works in one of those domains.
  FBP relies on a certain problem structure that I'm calling hybrid planning/scheduling.
}

\begin{frame}
\frametitle{What makes a hybrid problem?}
  \begin{columns}
    \begin{column}{.6\textwidth}
      \Large
      \begin{itemize}
      \item Independent subtasks.
      \item Shared resources.
      \item Choice of how to do them.
      \item Choice of when to do them.
      \end{itemize}
    \end{column}
    \begin{column}{.4\textwidth}
      \includegraphics[width=.85\columnwidth]{rail2.jpg}
      \\
      \includegraphics[width=.85\columnwidth]{rail3.jpg}
      \\
      {\tiny Photographs Courtesy of Rio Tinto Iron Ore}
    \end{column}
  \end{columns}
\end{frame}
\note{
  \bfrsp{} is a great example of this class of problems, but the same
  patterns occur in rail crew, airline cargo, \& field maintainance
  scheduling.

  Photos highlight these properties:
  Many services operate concurrently,
  sharing the track network.
  The choice of path affects the subset of the network resources a service uses.
  Chosing when and where to do nothing is often the most important choice. 
}



\begin{frame}
\frametitle{\Bfrsp{}}

\begin{columns}
  \begin{column}{.5\textwidth}
    \begin{exampleblock}{A simple track network}
      \centering
      \only<1>{\track{None}}%
      \only<2>{\track{Y}}%
      \only<3>{\track{D}}%
      \only<4>{\track{M1}}%
      \only<5>{\track{D}}%
      \only<6>{\track{Y}}%
      \only<7>{\track{P1}}%
      \only<8>{\track{Y}}%
    \end{exampleblock}

    Orders: $[\tup{\id{location}, \id{earliestT}, \id{latestT}}, \cdots ]$ \\
    $o_1=[\tup{m_1, 0, 11}, \tup{p_1, 0, 11}, \tup{y, 0, 11}]$
    $o_2=[\tup{m_2, 0, 11}, \tup{p_2, 0, 11}, \tup{y, 0, 11}]$
  \end{column}

  \begin{column}{.3\textwidth}
    \begin{align*}
      &\texttt{service}(c_1, o_1) || \texttt{service}(c_2, o_2)
    \end{align*}
    \begin{align*}
      & \texttt{proc service}(c, o): \\
      & \quad \texttt{foreach } l \texttt{ in } o \texttt{ do }\\
      & \quad \quad \pi(t). \textbf{waituntil}(t); \\
      & \quad \quad \texttt{travelto}(c, o, l)
    \end{align*}
    \begin{align*}
      & \texttt{proc travelto}(c, o, l): \\
      & \quad \texttt{while } \lnot \texttt{At}(c, l) \texttt{ do} \\
      & \quad \quad \pi(e \in E). \, \textbf{move}(c, e); \\  
      & \quad \pi(t \in [t0, t1]). \, \textbf{waituntil}(t); \\
      & \quad \textbf{visit}(c, o, l)
    \end{align*}
  \end{column}
\end{columns}
\end{frame}
\note{
This is the smallest interesting track network.
$m_i$ are mines, $p_i$ are ports. 
Each node has a $cap(n) = 1$ except $cap(y) = \infty$.

I should emphasise that scheduling approaches do work here, but they give fragile models,
the choice of what to do really hampers a lot of scheduling approaches.
I spent 9 months fixing a model because they built a port between $y$ and $d$.
No planning-based model would care about this, but because it affected the subset of resources a service used,
all the complex pre-processing of partial paths was now wrong.

We see node $d$ is a bottleneck, *when* it's a bottleneck may change,
but I can plan each service as if it were alone, then see when they overlap.

This is the intuition of FBP: learn how each agent's actions impact other agents,
by assigning costs to resources, and solving each agent's subproblem independently.
This decomposes the problem!

How do we assign these costs? We use Linear Programming, specifically the Dual.
}

\begin{frame}\frametitle{Multi-Agent Planning as a MIP}
    \begin{block}{Set of all possible per-agent plan fragments \vect{x}\\
                  Constrained by a set of resources $R$}
    \begin{align*}
      \text{Minimize:}&\qquad \sum c_i \cdot x_i                & \\
      \text{Subject To:}&\qquad \sum u_{r,i} \cdot x_i \quad \le a_r & \forall r \in R 
    \end{align*}
  \end{block}
\end{frame}
%% \begin{frame}
%% \frametitle{A (MI)LP refresher}
%%   \begin{block}{A (MI)LP with decision vector \vect{x} and set of resources $R$}
%%     \begin{align*}
%%       \text{Minimize:}&\qquad \sum c_i \cdot x_i                & \\
%%       \text{Subject To:}&\qquad \sum u_{r,i} \cdot x_i \quad \le a_r & \forall r \in R 
%%     \end{align*}
%%   \end{block}
%% \end{frame}
\note{
A quick refresher on MIPs this can be read:

Minimize the sum of decision costs, ensuring usage of any resource does not exceed availability.
Some ``resources'' have negative availability, making them subgoals.}

\begin{frame}
\frametitle{The Dual}
Each resource $r$ has a ``dual-price'' $\lpdual_r$, a local upper bound on the value of an extra unit of $r$
  \begin{block}{Reduced Cost: How much can $x_i$ improve the objective}
     \[ \gamma(i, \vect{\lpdual}) = c_i - \sum\limits_{r\in R} \lpdual_r \cdot u_{r,i} \]
  \end{block}

  \begin{itemize}
  \item LPs are \textbf{convex}: local improvement \textbf{will} find the optimum.
  \item If no $x_i$ has negative $\gamma(i, \vect{\lpdual})$, we have found optimum.
  \end{itemize}
\end{frame}
\note{
  This is more or less exactly what the simplex algorithm does: pick a
  -ive $\gamma$ column, make it basic (non-zero) and re-calculate duals.

  So maybe we can use an LP to assign costs to the shared resources so we iterate towards an optimum,
  but what do the columns represent?

  A column is a plan fragment, that is a single-agent's (or subtask's) plan.

  But there are exponentially many of them.
}



\begin{frame}
  \frametitle{Who says we need \textbf{all} the fragments?}

  \begin{block}{Column Generation}
    \begin{itemize}
    \item Start with some feasible $\vect{x}$
    \item Find the optimal duals
    \item \textbf{Search} for negative reduced cost choices and append to $\vect{x}$
    \item Repeat
    \end{itemize}
  \end{block}
  Note: This is called column generation because each $x_i$ is a column in $\vect{x}$.
\end{frame}
\note{
We can ignore huge numbers of columns as we approach feasibility
}

\begin{frame}\frametitle{FBP as Column Generation}
  Each choice is a partial plan or ``fragment'' representing a complete plan for a single service.

  \begin{block}{FBP}
    \begin{itemize}
    \item Start with the empty set of plan fragments.
    \item Find the optimal duals.
    \item \textbf{Search} for negative reduced cost fragments.
    \item Repeat.
    \end{itemize}
  \end{block}

  To ensure the empty set is a feasible solution in the LP, we
  ``soften'' the constraint that all services are delivered with a
  penalty of $M$, where $M$ is some sufficiently large number.
\end{frame}
\note{
  
}

\begin{frame}\frametitle{FBP example: Iteration 1}
$t= \only<1>{0}
    \only<2>{1}
    \only<3>{2}
    \only<4>{3}
    \only<5>{4}
    \only<6>{5}
    \only<7>{6}
    \only<8>{7}
    \only<9->{8}
$

\begin{columns}[T]
  \begin{column}{.48\textwidth}
    \only<1>{\track{Y}}
    \only<2>{\track{D}}
    \only<3-4>{\track{M1}}
    \only<5>{\track{D}}
    \only<6>{\track{Y}}
    \only<7-8>{\track{P1}}
    \only<9>{\track{Y}}
    \only<10->{\track{None}}

    Reduced Cost: $
    \only<1>{0}
    \only<2>{1}
    \only<3>{2}
    \only<4>{3}
    \only<5>{4}
    \only<6>{5}
    \only<7>{6}
    \only<8>{7}
    \only<9->{8}
    - M - \sum{\lpdual}  \only<11->{=0}$

    Resources:
    \footnotesize
    \begin{enumerate}
    \item<2-> $\id{At}(d,   t=1)$;   \only<-10>{$\lpdual_{1}=0$} \only<11->{$\lpdual_{2}=8-M$}
    \item<3-> $\id{At}(m_1, t=2)$;   \only<-10>{$\lpdual_{1}=0$} \only<11->{$\lpdual_{2}=0$}
    \item<5-> $\id{At}(d, t=4)$;     \only<-10>{$\lpdual_{1}=0$} \only<11->{$\lpdual_{2}=0$}
    \item<7-> $\id{At}(p_1, t=6)$;   \only<-10>{$\lpdual_{1}=0$} \only<11->{$\lpdual_{2}=0$}
    \item<9-> $\id{Delivered}(o_2)$;   \only<-10>{$\lpdual_{1}=0$} \only<11->{$\lpdual_{2}=0$}
    \end{enumerate}
  \end{column}

  \begin{column}{.48\textwidth}
    \only<1>{\track{Y}}
    \only<2>{\track{D}}
    \only<3-4>{\track{M2}}
    \only<5>{\track{D}}
    \only<6>{\track{Y}}
    \only<7-8>{\track{P2}}
    \only<9>{\track{Y}}
    \only<10->{\track{None}}

    Reduced Cost: $
    \only<1>{0}
    \only<2>{1}
    \only<3>{2}
    \only<4>{3}
    \only<5>{4}
    \only<6>{5}
    \only<7>{6}
    \only<8>{7}
    \only<9->{8}
    -M - \sum{\lpdual} \only<11->{=0}$
    
%    \only<11->{Reduced Cost #2: $8 - (8-M) - M = 0$}

    Resources:
    \footnotesize
    \begin{enumerate}
    \item<2-> $\id{At}(d,   t=1)$;   \only<-10>{$\lpdual_{1}=0$} \only<11->{$\lpdual_{2}=8-M$}
    \item<3-> $\id{At}(m_2, t=2)$;   \only<-10>{$\lpdual_{1}=0$} \only<11->{$\lpdual_{2}=0$}
    \item<5-> $\id{At}(d, t=4)$;     \only<-10>{$\lpdual_{1}=0$} \only<11->{$\lpdual_{2}=0$}
    \item<7-> $\id{At}(p_2, t=6)$;   \only<-10>{$\lpdual_{1}=0$} \only<11->{$\lpdual_{2}=0$}
    \item<9-> $\id{Delivered}(o_2)$;   \only<-10>{$\lpdual_{1}=0$} \only<11->{$\lpdual_{2}=0$}
    \end{enumerate}
  \end{column}
\end{columns}
\end{frame}
\begin{frame}\frametitle{FBP example: Iteration 2}
$t= \only<1>{1}
    \only<2>{2}
    \only<3>{3}
    \only<4>{4}
    \only<5>{5}
    \only<6>{6}
    \only<7>{7}
    \only<8>{8}
    \only<9->{9}
$

\begin{columns}[T]
  \begin{column}{.48\textwidth}
    \only<1>{\track{Y}}
    \only<2>{\track{D}}
    \only<3-4>{\track{M1}}
    \only<5>{\track{D}}
    \only<6>{\track{Y}}
    \only<7-8>{\track{P1}}
    \only<9>{\track{Y}}
    \only<10->{\track{None}}

    Reduced Cost: $
    \only<1>{1}
    \only<2>{2}
    \only<3>{3}
    \only<4>{4}
    \only<5>{5}
    \only<6>{6}
    \only<7>{7}
    \only<8>{8}
    \only<9->{9}
    - M - \sum{\lpdual}  \only<11->{=0}$

    Resources:
    \footnotesize
    \begin{enumerate}
    \item<2-> $\id{At}(d,   t=2)$;   \only<-10>{$\lpdual_{2}=0$} \only<11->{$\lpdual_{3}=8-M$}
    \item<3-> $\id{At}(m_1, t=3)$;   \only<-10>{$\lpdual_{2}=0$} \only<11->{$\lpdual_{3}=0$}
    \item<5-> $\id{At}(d, t=5)$;     \only<-10>{$\lpdual_{2}=0$} \only<11->{$\lpdual_{3}=0$}
    \item<7-> $\id{At}(p_1, t=7)$;   \only<-10>{$\lpdual_{2}=0$} \only<11->{$\lpdual_{3}=0$}
    \item<9-> $\id{Delivered}(o_1)$;   \only<-10>{$\lpdual_{2}=0$} \only<11->{$\lpdual_{3}=M-9$}
    \end{enumerate}
  \end{column}

  \begin{column}{.48\textwidth}
    \only<1>{\track{Y}}
    \only<2>{\track{D}}
    \only<3-4>{\track{M2}}
    \only<5>{\track{D}}
    \only<6>{\track{Y}}
    \only<7-8>{\track{P2}}
    \only<9>{\track{Y}}
    \only<10->{\track{None}}

    Reduced Cost: $
    \only<1>{1}
    \only<2>{2}
    \only<3>{3}
    \only<4>{4}
    \only<5>{5}
    \only<6>{6}
    \only<7>{7}
    \only<8>{8}
    \only<9->{9}
    -M - \sum{\lpdual} \only<11->{=0}$
    
%    \only<11->{Reduced Cost #2: $8 - (8-M) - M = 0$}

    Resources:
    \footnotesize
    \begin{enumerate}
    \item<2-> $\id{At}(d,   t=2)$;   \only<-10>{$\lpdual_{2}=0$} \only<11->{$\lpdual_{3}=0$}
    \item<3-> $\id{At}(m_2, t=3)$;   \only<-10>{$\lpdual_{2}=0$} \only<11->{$\lpdual_{3}=0$}
    \item<5-> $\id{At}(d, t=5)$;     \only<-10>{$\lpdual_{2}=0$} \only<11->{$\lpdual_{3}=0$}
    \item<7-> $\id{At}(p_2, t=7)$;   \only<-10>{$\lpdual_{2}=0$} \only<11->{$\lpdual_{3}=0$}
    \item<9-> $\id{Delivered}(o_2)$;   \only<-10>{$\lpdual_{2}=0$} \only<11->{$\lpdual_{3}=M-9$}
    \end{enumerate}
  \end{column}
\end{columns}
\centering
\footnotesize
\only<11->{$\id{At}(d, t=1); \lpdual_{3}=1$}

\end{frame}
%% \begin{frame}\frametitle{Worked example}
%% \begin{columns}
%% \begin{column}{.33\textwidth}

%% \footnotesize
%% \[%
%% \setlength{\arraycolsep}{2pt}
%% \begin{array}{rcc}
%%    f_1 & = & \left[
%% 	\begin{array}{l}
%% 	   move(\tup{y,d}); \\ move(\tup{d,m_1}); 
%%         \\ visit(m_1,o_1);  \\ move(\tup{m_1,d});
%%         \\ move(\tup{d,y}); \\ move(\tup{y,p_1});
%% 	\\ visit(p_1,o_1);  \\ move(\tup{p_1,y});
%%         \\ visit(y,o_1)
%% 	\end{array}
%% 	\right]
%% \\ f_2 & = & \left[
%% 	\begin{array}{l}
%% 	   move(\tup{y,d}); \\ move(\tup{d,m_2});
%%         \\ visit(m_2,o_2);  \\ move(\tup{m_2,d});
%%         \\ move(\tup{d,y}); \\ move(\tup{y,p_2});
%% 	\\ visit(p_2,o_2);  \\ move(\tup{p_2,y});
%%         \\ visit(y,o_2)
%% 	\end{array}
%% 	\right]
%% \end{array}
%% \]
%% \end{column}
%% \begin{column}{.65\textwidth}
%%   \begin{table}
%% \begin{tabular}{ c | c | c }
%% \hline
%%                    &  $\vect{\pi}$          & Fragments \\
%% \hline
%% \onslide<1->{
%%   $\theta=200$     &                        & $f_1, f_2$ \\
%%  $\gamma(f_1)=-91$ &                        & \\
%%  $\gamma(f_2)=-91$ &                        & 
%% \\\hline
%% }

%% \onslide<2->{
%%   $\theta=109$     & $\pi_{d,0} = -91$      & $[wait(1)] \plusplus f_1$ \\
%% $\gamma(f_3)=-90$  &                        & $[wait(1)] \plusplus f_2$\\
%% $\gamma(f_4)=-90$  &                        &  \\
%% \hline
%% }
%% \onslide<3>{
%%   $\theta=19$      & $\pi_{d,3} = -1$       & \\
%%                    & $\pi_{p_1,o_1} = -90$  & \\
%%                    & $\pi_{p_2,o_2} = -90$  &
%% }%
%% \\\hline
%% \end{tabular}
%% \end{table}
%% \end{column}
%% \end{columns}
%% \end{frame}
\note{
  \begin{enumerate}
  \item The optimal plans for the two sub-goals are the shortest
    paths from $y$ to one of the mines $m_1$ or $m_2$, then one of the
    ports $p_1$ or $p_2$, then to the yard.

  \item  The dual detects a bottlenecks at block $d$ at time $0$, overestimating that over-use of this resource costs $91$ units. 
    The new minimum cost way to achieve each goal now waits.

  \item  Now the dual shows that unloads are bottlenecks 
    (we now have two ways to satisfy each goal).
    These cannot be avoided, however at a cost of only $90$,
    search must prove no such fragment exists.
  \end{enumerate}
}


%% \begin{frame}
%% \begin{algorithmic}
%% \Function{LinFBP}{$Frags, Res, Goals, \delta$}
%% \State $\LowBound \gets 0$
%% \State $\UpBound  \gets M\cdot\norm{\Goals}$
%% \While{$ (1-\epsilon) \cdot \UpBound > \LowBound$}
%%       \State $\theta, \vect{x},\vect{\lpdual}\gets \SolveLP(\Frags, \Res)$

%%       \State $F     \gets Do(\pi(g\in \Goals).\delta|noop, \vect{\lpdual})$
%%       %\Comment{min $\gamma(F, \vect{\lpdual})$ plan}
%%       \State $\Frags \gets \Frags \cup \{F\}$

%%       \State $d\theta \gets \norm{\Goals} \cdot \gamma(F, \vect{\lpdual})$
%%       \State $\UpBound  \gets \theta$
%%       \State $\LowBound \gets max(\LowBound, \theta + d\theta)$
%%       \ForAll{$r \in F$}
%%           \State $\quine{e \le a} \gets \Res[r]$
%%           \State $\Res[r] \gets \quine{e + u_{F,r} \cdot x_F \le a}$
%%       \EndFor
%% \EndWhile
%% \EndFunction
%% \end{algorithmic}
%% \end{frame}

\begin{frame}
\begin{exampleblock}{Comparisons}
\begin{itemize}
\item \textbf{FBP:} Fragment-based planner.
\item \textbf{CPX:} Constraint programming solver using Lazy Clause Generation.
\item \textbf{POPF2:} A heuristic satisficing planner.
\item \textbf{Golog:} High-level Automated planning language.
\end{itemize}
\end{exampleblock}

\end{frame}
\note{
Of the IPC2011 temporal planners, only popf2 supports TILs or numeric fluents.
}

\begin{frame}
	\frametitle{FBP Results}
\begin{table}[h]
\centering
\small
%\tablefont
\begin{tabular}{|c c c | r r r r |}
\hline
$\card{V}$ &$\card{O}$&       m/p &      \Golog{} &        \tt popf2 & \tt     cpx   & \tt  \textbf{FBP} \\
[0.4ex]
\hline
  6        &        2 &         1 &          0.4  &    \textbf{0.3}  &          0.7  &           1.6   \\
  6        &        4 &         1 &          ---  &            ---   &  \textbf{2.3} &           3.3   \\
\hline
  6        &        4 &         2 &          ---  &            ---   &  \textbf{1.7} &           2.0   \\
  6        &        8 &         2 &          ---  &            ---   &  \textbf{7.5} &           7.6   \\
  6        &       16 &         2 &          ---  &            ---   &         37.9  &  \textbf{29.7}  \\
  6        &       32 &         2 &          ---  &            ---   &          ---  &  \textbf{50.3}  \\
  6        &       64 &         2 &          ---  &            ---   &          ---  & \textbf{150.3}  \\
  6        &      128 &         2 &          ---  &            ---   &          ---  & \textbf{418.8}  \\
  6        &      256 &         2 &          ---  &            ---   &          ---  & \textbf{589.7}  \\
\hline
  16       &       44 &        11 &          ---  &            ---   &          --- & \textbf{315.0}  \\
  16       &       88 &        11 &          ---  &            ---   &          --- & \textbf{363.0}  \\
[0.1ex]
\hline
\end{tabular}
%\caption{\bfrsp{}: Time to first solution in seconds (1800s time limit, 4GB memory)}
\end{table}
\end{frame}
\note{The exponential growth of the orders. Largest problem reqs >256*6 ~ 1500 actions}

\begin{frame}
\frametitle{Problem: Time Windows are Complicating}
\begin{exampleblock}{Comparisons Without Time Windows}
  \begin{itemize}
  \item \textbf{POPF2:} A heuristic satisficing planner.
  \item \textbf{yahsp2:} ``Yet Another Heuristic Search Planner''.
  \item \textbf{cpt4:} A CP-based optimal temporal planner.
  \end{itemize}
\end{exampleblock}

NB: \bfrsp{} with no time windows is polynomially satisfiable

\end{frame}
\note{
Sequentially apply shortest path to each way point in any serialisation of orders to solve polynomially.

cpt is an optimal planner, so it enforces its own time window that make the problem hard again.
}

\begin{frame}
\frametitle{FBP Results Without Time Windows}
  \begin{table}[h]
\centering
\small
\begin{tabular}{|c@{~} c c | r@{~}r@{~} r | r |}
\hline
$\card{V}$ &$\card{O}$& m/p &  \tt  popf2 & \tt     yahsp2 &    \tt  cpt4  &    \tt   FBP*   \\
[0.4ex]
\hline
   6       &        2 & 1   &       0.3   &   \textbf{0.0} &  \textbf{0.0} &          1.6    \\
   6       &        4 & 1   &       8.4   &   \textbf{0.0} &          0.8  &          3.3    \\
\hline
   6       &        4 & 2   &       ---   &   \textbf{0.0} &          0.7  &          2.0    \\
   6       &        8 & 2   &       ---   &   \textbf{0.3} &         ---   &          7.6    \\
   6       &       16 & 2   &       ---   &          42.5  &         ---   & \textbf{29.7}   \\
   6       &       32 & 2   &       ---   &          ---   &         ---   & \textbf{50.3}   \\
[0.1ex]
\hline
\end{tabular}
\caption{* FBP results are from the non-relaxed problem}
\end{table}
\end{frame}
\note{No best taken from FBP, but yahsp now outperforms where CPX was good}

\begin{frame}
  \frametitle{Extending FBP to other domains}
  The idea of ``resources'' is quite general.

  In Blocks-World:
  \begin{itemize}
  \item Clearness of a block is a resource.
  \item A fragment is the actions on a single block.
  \end{itemize}
\end{frame}
\note{
This is bad for FBP: it splits the work unevenly between master and subproblems, and
relies on the LP to detect impossible sequences, something that search should be better at.

But, opt results promising:
}

\begin{frame}
\frametitle{Multi-Arm Blocks-World Results}

\begin{table}[h]
\centering
\begin{tabular}{| c | r r | r r | r | r r |}
\hline
Blocks    &  \dcol{\tt popf2}  & \dcol{\tt yahsp2}  &  {\tt cpt}   & \dcol{\tt FBP}    \\
[0.4ex]
% \hline
 & 1st & opt & 1st & opt & opt & 1st & opt \\
\hline
  3       &  0.0  &   3.5 &    0.0 &   0.0 &     0.0  &   0.3  &  0.5  \\
  4       &  0.0  &  53.0 &    0.0 &   0.0 &     0.0  &   0.3  &  0.7  \\
  5       &  0.0  &   --- &    0.0 &   0.1 &     0.1  &   0.2  &  0.6  \\
  6       &  0.1  &   --- &    0.0 &   6.4 &     0.1  &   0.5  &  0.8  \\
  7       &  0.9  &   --- &    0.1 &   --- &     0.2  &   0.6  &  0.8  \\
  8       &  0.1  &   --- &    0.0 &   --- &     0.4  &   1.0  &  1.9  \\
  9       &  17.9 &   --- &    0.0 &   --- &     1.7  &   5.0  &  6.2  \\
  10      &  0.3  &   --- &    0.0 &   --- &     1.7  &   1.3  &  2.0  \\
  11      &  1.4  &   --- &    0.0 &   --- &     5.2  &   2.0  &  3.8  \\
  12      &  ---  &   --- &    0.0 &   --- &    12.6  &   4.9  &  6.8  \\
[1ex]
\hline
\end{tabular}
\caption{time limit 120s, 4GB memory}
\end{table}
\end{frame}
\note{
It doesn't do badly, seems to fare well with high concurrency, like CPT
}

\begin{frame}
  \frametitle{Conclusions \& Further work}
FBP is interesting:
  \begin{itemize}
  \item Enables planning-like models for complex scheduling algorithms.
  \item \textbf{Many} engineering improvements can be made.
  \item More work needed on more general models. (In progress)
  \item Nogood Learning: similar problems solved 100s of times. (In progress)
%  \item UNSAT \textbf{must} be feasible to prove in sub-problems.
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Questions?}
\end{frame}


\begin{frame}\frametitle{Branch \& Price}
  \centering
  \begin{itemize}
  \item Column Generation guarantees we will find the \textbf{linear} optimum. (i.e. we may have to run 0.5 of a train)
  \item But we can add branching constraints, and re-run Column Generation for each branch.
  \end{itemize}

  \begin{tikzpicture}[-,node distance=3cm,semithick]
    \node[branch] (O)                     {$x_i \in [0, 1]$};
    \node[branch] (L)  [below left of=O]  {$x_i \leq 0$};
    \node[branch] (R)  [below right of=O] {$x_i \geq 1$};
    \path (O) edge (L)
          (O) edge (R);
  \end{tikzpicture}

  A node's optimal solution is the best of the solutions to its children

\end{frame}

\end{document}
