#include "pdb.hxx"

#include "action.hxx"
#include "strips_state.hxx"

#include "gtest/gtest.h"

#define DIM 8
#define AREA (DIM*DIM)

#define ITERS 2

using namespace aptk;
using namespace cdhl;

#include "argmin.hxx"

namespace cdhl {
class PDBTest : public ::testing::Test {
public:
	STRIPS_Problem prob;
	LearningPDB pdb;

	unsigned at[DIM][DIM];
	unsigned vis[DIM][DIM];
	unsigned nvis[DIM][DIM];
	State* s0;

	PDBTest(): prob(), pdb(prob) {
		std::vector<unsigned> goal;
		std::vector<unsigned> loc_mutex;
		std::vector<unsigned> init;
		for(int i = 0; i < DIM ; i++){
			for(int j = 0; j < DIM ; j++){
				at[i][j] = STRIPS_Problem::add_fluent(prob,
					"at-"
					+ std::to_string(i) + "-"
					+ std::to_string(j));
				vis[i][j] = STRIPS_Problem::add_fluent(
					prob,
					"visited-"
					+ std::to_string(i) + "-"
					+ std::to_string(j));
				nvis[i][j] = STRIPS_Problem::add_fluent(
					prob,
					"not-visited-"
					+ std::to_string(i) + "-"
					+ std::to_string(j));
				goal.push_back(vis[i][j]);
				init.push_back(nvis[i][j]);
				loc_mutex.push_back(at[i][j]);
			}
		}
		for(int i = 0; i < DIM ; i++){
			for(int j = 0; j < DIM ; j++){
				for(int di = -1 ; di <= 1; di+=2){
					if(i+di >= 0 && i+di <DIM)
						STRIPS_Problem::add_action(
							prob,
							"move-"
							+ std::to_string(i) + "-"
							+ std::to_string(j)
							+ "-to-"
							+ std::to_string(i+di) + "-"
							+ std::to_string(j),
							{at[i][j]},
							{at[i+di][j], vis[i+di][j]},
							{at[i][j]},
							{}
						);
				}
				for(int dj = -1 ; dj <= 1; dj+=2){
					if(j+dj >= 0 && j+dj <DIM)
						STRIPS_Problem::add_action(
							prob,
							"move-"
							+ std::to_string(i) + "-"
							+ std::to_string(j)
							+ "-to-"
							+ std::to_string(i) + "-"
							+ std::to_string(j+dj),
							{at[i][j]},
							{at[i][j+dj], vis[i][j+dj]},
							{at[i][j], nvis[i][j+dj]},
							{}
						);
				}
			}
		}
		goal.push_back(at[0][0]);
		init.push_back(at[0][0]);

		STRIPS_Problem::set_init(prob, init);
		STRIPS_Problem::set_goal(prob, goal);
		prob.mutexes().add(loc_mutex);
		std::vector<unsigned> trivial_mutex(2);
		for(int i = 0; i < DIM ; i++){
			for(int j = 0; j < DIM ; j++){
				trivial_mutex[0] = vis[i][j];
				trivial_mutex[1] = nvis[i][j];
				prob.mutexes().add(trivial_mutex);
			}
		}
		prob.make_action_tables(true);
		s0 = new State(prob);
		s0->set(prob.init());
		prob.compute_edeletes();
		pdb.setup();
        }

	virtual ~PDBTest() {
	}

};

TEST_F(PDBTest, Eval) {
	LearningPDB::PathInfo bounds = {min_h: 0, max_h: 2, g: std::make_pair((unsigned)0, (unsigned) 1)};
	EXPECT_LE(2, pdb.eval(*s0, bounds));
	// Ensure eval is idempotent
	for(int i = 1 ; i < ITERS; i++){
		auto act = prob.actions()[1637*i%prob.num_actions()];
		s0->unset(act->edel_vec());
		s0->set(act->add_vec());
		ASSERT_LE((int) !s0->entails(prob.goal()), pdb.eval(*s0, bounds))
			<< "iter " << i;
	}
}

TEST_F(PDBTest, EvalGoal){
	LearningPDB::PathInfo bounds = {min_h: 0, max_h: 2, g: std::make_pair((unsigned)0, (unsigned) 1)};
	//int applicable = AREA - 1;
	for(auto g : prob.goal()){
		// Ensure eval is idempotent
		for(int i = 0 ; i < ITERS / prob.goal().size(); i++)
			ASSERT_LE((int) !s0->entails(prob.goal()), pdb.eval(*s0, bounds))
				<< "iter " << i;
		//ASSERT_EQ(applicable, pdb.applicable_expls.size());
		//applicable -= !s0->entails(g);
		s0->set(g);
	}
	bounds.max_h = 1;
	EXPECT_EQ(0, pdb.eval(*s0, bounds));	
}


TEST_F(PDBTest, BestActionNoEscape) {
	LearningPDB::PathInfo bounds = {min_h: 0, max_h: 2, g: std::make_pair((unsigned)0, (unsigned) 1)};
	auto b = pdb.builder();
	b.state_ub(s0->fluent_set()).lb(1).lso(prob.num_actions()-1);
	auto e = pdb.addCube(b);
	EXPECT_EQ(e, pdb.best_expl(*s0, bounds));
	for(int i = 0 ; i < ITERS; i++){
		unsigned lb, op;
		std::tie(lb, op) = pdb.best_action(*s0, bounds);
		ASSERT_LE(1, e->lb);
		ASSERT_LE(e->least_succ_op, op);
	}
}

TEST_F(PDBTest, BestAction) {
	LearningPDB::PathInfo bounds = {min_h: 0, max_h: 2, g: std::make_pair((unsigned)0, (unsigned) 1)};
	s0->set(prob.goal());
	for(auto g : prob.goal()){
		s0->unset(g);
		auto act = argmin(prob.actions_adding(g),
				  [&](const Action* act){
					  return 1e4*act->cost() + act->index();
				  });
		s0->set(act->prec_vec());
		ASSERT_LE(1, pdb.eval(*s0, bounds));
		for(int i = 0 ; i < ITERS/prob.goal().size(); i++){
			int lb, op;
			std::tie(lb, op) = pdb.best_action(*s0, bounds);
			ASSERT_GE(act->index(), op)
				<< prob.fluents()[g]->signature() << " "
				<< prob.actions()[op]->signature() << " "
				<< act->signature();
		}
		s0->set(g);
	}
}

TEST_F(PDBTest, IncrEval) {
	LearningPDB::PathInfo bounds = {min_h: 0, max_h: 2, g: std::make_pair((unsigned)0, (unsigned) 1)};
	std::vector<int> actions;
	EXPECT_LE(1, pdb.eval(*s0, bounds));
	for(int i = 0 ; i < ITERS; i++){
		actions.clear();
		prob.applicable_actions_v2(*s0, actions);
		int op = actions[0];
		auto act = prob.actions()[op];
		ASSERT_LE(2, pdb.incr_eval(*s0, bounds, op));
	}
}

TEST_F(PDBTest, Cubes) {
	State s1(*s0);
	LearningPDB::PathInfo bounds = {min_h: 0, max_h: 1000000, g: std::make_pair((unsigned)0, (unsigned) 1)};
	s1.unset(at[0][0]);
	s1.set(at[0][1]); s1.set(vis[0][1]);
	s1.set(at[1][0]); s1.set(vis[1][0]);
	auto b = pdb.builder();
	b.state_ub(s1.fluent_set()).lb(23);
	pdb.addCube(b);

	auto e0 = pdb.best_expl(*s0, bounds);
	b.state_ub(s0->fluent_set()).lb(2);
	pdb.addCube(b);
	auto e1 = pdb.best_expl(*s0, bounds);
	EXPECT_NE(e0, e1);
	b.state_ub(s0->fluent_set()).lb(2).lso(1);
	pdb.addCube(b);
	auto e2 = pdb.best_expl(*s0, bounds);
	EXPECT_NE(e1, e2);
}

TEST_F(PDBTest, UIP) {
	LearningPDB::PathInfo bounds = {min_h: 0, max_h: 1000000, g: std::make_pair((unsigned)0, (unsigned) 1)};
	pdb.use_uip = true;
	pdb.strengthen = false;
	int lb, op;
	for(int i = 1 ; i < ITERS; i++){
		std::tie(lb, op) = pdb.best_action(*s0, bounds);
		ASSERT_LE(2, lb);
		ASSERT_GE(AREA, lb);
		ASSERT_EQ(lb, pdb.best_expl(*s0, bounds)->lb);
	}
}

TEST_F(PDBTest, UIP_STRENGTHEN) {
	LearningPDB::PathInfo bounds = {min_h: 0, max_h: 1000000, g: std::make_pair((unsigned)0, (unsigned) 1)};
	pdb.use_uip = true;
	pdb.strengthen = true;
	int lb, op;
	for(int i = 1 ; i < ITERS; i++){
		std::tie(lb, op) = pdb.best_action(*s0, bounds);
		ASSERT_LE(2, lb);
		ASSERT_GE(AREA, lb);
		ASSERT_EQ(lb, pdb.best_expl(*s0, bounds)->lb);
	}
}

// TEST_F(PDBTest, Regress) {
// 	State s1(*s0);
// 	s1.unset(at[0][0]);
// 	s1.set(at[0][1]); s1.set(vis[0][1]);
// 	s1.set(at[1][0]); s1.set(vis[1][0]);
// 	pdb.addCube(s1.fluent_set(), 23);

// 	auto lb0 = pdb.best_expl(*s0)->lb;
// 	auto o0 = pdb.best_expl(*s0)->least_succ_op;
// 	pdb.addCube(s0->fluent_set(), 2);
// 	auto lb1 = pdb.best_expl(*s0)->lb;
// 	auto o1 = pdb.best_expl(*s0)->least_succ_op;

// 	EXPECT_LT(lb0, lb1);

// 	EXPECT_EQ(0, o1);
// 	pdb.regress(*s0, o1);

// 	auto o2 = pdb.best_expl(*s0)->least_succ_op;
// 	EXPECT_LT(o1, o2);
// 	EXPECT_EQ(1, o2);

// 	pdb.regress(*s0, o2);
// 	EXPECT_EQ(2, pdb.best_expl(*s0)->least_succ_op);

// 	EXPECT_EQ(24, pdb.explain(*s0, 24)->lb);
// 	EXPECT_EQ(24, pdb.eval(*s0, 25));
// 	EXPECT_EQ(0, pdb.best_action(*s0, 2));

// }

// TEST_F(PDBTest, Explain) {
// 	EXPECT_GT(7, pdb.eval(*s0, 8));
// 	auto e = pdb.addCube(s0->fluent_set(), 2, 3);
// 	e->next_lb = 7;
// 	EXPECT_EQ(7, pdb.explain(*s0, 6)->lb);
// 	EXPECT_EQ(7, pdb.eval(*s0, 8));
// }

// TEST_F(PDBTest, ExplainFixPoint) {
// 	EXPECT_EQ(1, pdb.eval(*s0));
// 	int lb = pdb.eval(*s0);
// 	int i = 1;
// 	State s(*s0);
// 	while(pdb.explain(*s0, lb+1) > lb){
// 		lb = pdb.eval(*s0);
// 		ASSERT_EQ(++i, lb) << " iter " << i;
// 		ASSERT_GE(AREA, lb) << " iter " << i;

// 		auto op = pdb.best_action(s);
// 		while(lb == incr_eval(*s0)){
// 			s->set(prob.actions()[op]
// 		}
// 	}
// 	EXPECT_LE(DIM+DIM, pdb.eval(*s0));
// 	EXPECT_GE(AREA, lb);
// }


// TEST_F(PDBTest, Cycles) {
// 	EXPECT_TRUE(false);
// }


// TEST_F(PDBTest, ProgressEvalBacktrack) {
// 	EXPECT_TRUE(false);
// }

}
