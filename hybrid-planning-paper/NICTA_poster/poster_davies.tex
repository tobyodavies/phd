\documentclass[final]{beamer}
\usetheme[ORG]{NICTAposter}
%for default nicta style use:
%\usetheme{NICTAposter}
\usepackage{etex}
\usepackage[orientation=portrait,size=a1,scale=.9,debug]{beamerposter}
\usepackage[labelformat=empty]{caption}

% Required Packages
\usepackage{times}
\usepackage{helvet}
\usepackage{courier}
\usepackage{verbatim}
\usepackage{natbib}

\setlength{\TPHorizModule}{1cm}
\setlength{\TPVertModule}{1cm}

\bibliographystyle{apalike}
\begin{filecontents}{\jobname.bib}
@inproceedings{Davies2014,
  title={Fragment-based Planning using Column Generation},
  author={Davies, Toby and Pearce, Adrian R. and Stuckey, Peter J. and S{\o}ndergaard, Harald},
  booktitle = {Proceedings of the Twenty-Fourth International Conference on Automated Planning and Scheduling (ICAPS)},
  pages={83-91},
  year={2014},
}
\end{filecontents}

% can make bibliography entries smaller
%\renewcommand\bibfont{\scriptsize}
% If you have more than one page of references, you want to tell beamer
% to put the continuation section label from the second slide onwards
\setbeamertemplate{frametitle continuation}[from second]
% Now get rid of all the colours
\setbeamercolor*{bibliography entry title}{fg=black}
\setbeamercolor*{bibliography entry author}{fg=black}
\setbeamercolor*{bibliography entry location}{fg=black}
\setbeamercolor*{bibliography entry note}{fg=black}
% and kill the abominable icon
\setbeamertemplate{bibliography item}{}

\title{Fragment-based Planning using \newline Column Generation}

%\author{\ \linebreak[4] 
%\author{\ \vspace{10mm} 
\author{\ \\[5ex]
Toby Davies -- PhD Student ~ \emph{Supervisors:} A Prof Adrian Pearce; Prof. Peter Stuckey; A Prof Harald S{\o}ndergaard
}
%\vskip.01\textheight	

% user defined
\newcommand{\propername}[1]{\textit{#1}}
\newcommand{\card}[1]{|{#1}|}                     % cardinality of a set
\newcommand{\Golog}{\propername{Golog}}
\newcommand{\ConGolog}{\propername{ConGolog}}
\newcommand{\mindigolog}{MIndiGolog}
\newcommand{\dwd}{Dantzig-Wolfe Decomposition}
\newcommand{\Bfrsp}{Bulk Freight Rail Scheduling Problem}
\newcommand{\bfrsp}{BFRSP}
\newcommand{\Fbp}{fragment-based planning}
\newcommand{\FBP}{Fragment-Based Planning}
\newcommand{\fbp}{FBP}
%\newcommand{\tablefont}{\small}
\newcommand{\tablefont}{\normalsize}

% Sitcalc Golog/ConGolog/IndiGolog programs
\newcommand{\mif}{\mbox{\bf if}}
\newcommand{\mwhile}{\mbox{\bf while}}
\newcommand{\mreturn}{\mbox{\bf return}}
\newcommand{\mthen}{\mbox{\bf then}}
\newcommand{\melse}{\mbox{\bf else}}
\newcommand{\mdo}{\mbox{\bf do}}
\newcommand{\mnoOp}{\mbox{\bf $noOp$}}
\newcommand{\mproc}{\mbox{\bf proc}}
\newcommand{\mend}{\mbox{\bf end}}
\newcommand{\mendproc}{\mbox{\bf endProc}}
\newcommand{\mendif}{\mbox{\bf endIf}}
\newcommand{\mendwhile}{\mbox{\bf endWhile}}
\newcommand{\mendfor}{\mbox{\bf endFor}}
\newcommand{\mfor}{\mbox{\bf for}}
\def\prparallel{\mathrel{\rangle\!\rangle}}
\def\supparallel{\mathord{|\!|}}
\newcommand{\ndet}{\mbox{$|$}}
\newcommand{\conc}{\mbox{$\parallel$}}
\newcommand{\pconc}{\mbox{$\prparallel$}}
\newcommand{\search}{\mbox{$\Sigma$}}
\newcommand{\searchO}{\mbox{$\Sigma_o$}}
\newcommand{\searchOM}{\mbox{$\Sigma_o^M$}}
\newcommand{\searchCR}{\mbox{$\Sigma_{cr}$}}
\newcommand{\searchR}{\mbox{$\Sigma_{r}$}}
\newcommand{\searchM}{\mbox{$\Sigma^M$}}
\newcommand{\searchC}{\mbox{$\Sigma_c$}}
\newcommand{\searchCM}{\mbox{$\searchC^M$}}
\newcommand{\searchCB}{\mbox{$\Sigma_{cb}$}}
\newcommand{\searchD}{\mbox{$\Delta$}}
\newcommand{\searchE}{\mbox{$\Delta_e$}}
\newcommand{\searchEM}{\mbox{$\Delta_e^M$}}
\newcommand{\searchL}{\mbox{$\Delta_l$}}
\newcommand{\searchER}{\mbox{$\Delta_r$}}
\newcommand{\searchERM}{\mbox{$\Delta_r^M$}}
\newcommand{\mnt}{\mbox{$mnt$}}



\begin{document}
\begin{frame}
\begin{columns}

\begin{column}{0.48\linewidth}
	\begin{block}{The Bulk Freight Rail Scheduling Problem (\bfrsp)}

\includegraphics[width=1.0\textwidth]{rail.jpg}\\[0ex]
\includegraphics[width=0.5\textwidth]{rail2.jpg}\includegraphics[width=0.5\textwidth]{rail3.jpg}\\
{\tiny Photographs Courtesy of Rio Tinto Iron Ore}

\begin{itemize}
\item A \textbf{Service} is a sequence of movements by a \textbf{consist} (a locomotive and wagons) over the rail network
\item \textbf{Objective}: satisfy as many \textbf{orders} as possible with minimum sum of service durations
\end{itemize}

	\end{block}
	
	\vskip.007\textheight		
	
	\begin{alertblock}{Key Technique \& Advantages}
		\begin{itemize}
			\item Linear programming, in particular the \emph{dual}, allows us to accurately predict the cost of resource consumption---we use \emph{both} the primal and the dual in feedback with planner
%significantly speeds proof of optimality
\item  MIP-Planner decomposition \alert{enables scaling to far larger problems}, when compared to other anytime algorithms (see results)
\item High-level temporal planning language is \alert{concise and easy to modify}
\end{itemize}
	\vskip.013\textheight		
\textbf{Potential application domains:} \emph{state-dependent} resource constrained problems involving time (e.g. Transportation, Mine Scheduling, Production scheduling, etc.)
	\vskip.005\textheight		
	\end{alertblock}

	
	\begin{exampleblock}{References}

\bibliography{master}{}

	\end{exampleblock}
\end{column}

\begin{column}{0.48\linewidth}

	\begin{block}{The Problem: motivated by industry experience}
\textbf{Resource constrained planning problems (e.g. \bfrsp):} known to be \alert{challenging to solve} using current technology, even non-temporal ones \\[0.5ex]
			\textbf{Industry experience:} small modification to \bfrsp\ problem took 9~months  to amend the 10,000 lines of C++ code!

	\vskip.007\textheight		

		\begin{itemize}
\item \textbf{High-level temporal planning languages:} promises a \alert{more concise} way of expressing the temporal domain, such as concurrent \Golog\ \cite{De_Giacomo2000}; to solve \alert{larger problems}
\item \textbf{Linear programming:} for automated planning \emph{heuristics}, \cite{VanDenBriel2007} exploits primal solutions to LP
%, however $\ldots$
%\item \textbf{Linear programming:} has shown promise for automated planning \emph{heuristics} \cite{VanDenBriel2007, Coles2008, Bonet2013}, exploiting primal solutions to the LP, however $\ldots$
		\end{itemize}

	\vskip.004\textheight		

\textbf{\alert{Challenge:}} \emph{Resource constrained temporal planning problems are typically beyond the scope of current planning technology!}

	\end{block}
	
	\vskip.005\textheight		
	
	\begin{alertblock}{The Solution: combines strengths of MIP \& Planning}
%High-level multi-agent temporal planning language\ \cite{Kelly2006} translated automatically to 
Automated translation into MIP/planning decomposition:
\begin{itemize}
\item \textbf{Automated planner:} Domain specific temporal planning language guides search for \emph{feasible} action sequences (\bfrsp\ services)
\item \textbf{MIP master problem:} Global mixed integer linear program relaxation predicts costs (resource consumption) based on \emph{dual} 
information
\end{itemize}



%\includegraphics[width=1.0\textwidth]{diagram.pdf}
\includegraphics[width=1.0\textwidth]{diagram.jpg}

For details, see \cite{Davies2014}.

	\end{alertblock}
	
	\begin{exampleblock}{Comparative Results: increasing network size \& orders}

	\vskip.005\textheight		

\begin{table}[h]
\centering
%\tablefont
\begin{tabular}{|c c c | r r r r |}
\hline
$\card{V}$ &$\card{O}$&       m/p &      \Golog{} &        \tt popf2 & \tt     cpx   & \tt  \textbf{FBP} \\
[0.4ex]
\hline
  6        &        2 &         1 &          0.4  &    \textbf{0.3}  &          0.7  &           1.6   \\
  6        &        4 &         1 &          ---  &            ---   &  \textbf{2.3} &           3.3   \\
\hline
  6        &        4 &         2 &          ---  &            ---   &  \textbf{1.7} &           2.0   \\
  6        &        8 &         2 &          ---  &            ---   &  \textbf{7.5} &           7.6   \\
  6        &       16 &         2 &          ---  &            ---   &         37.9  &  \textbf{29.7}  \\
  6        &       32 &         2 &          ---  &            ---   &          ---  &  \textbf{50.3}  \\
  6        &       64 &         2 &          ---  &            ---   &          ---  & \textbf{150.3}  \\
  6        &      128 &         2 &          ---  &            ---   &          ---  & \textbf{418.8}  \\
  6        &      256 &         2 &          ---  &            ---   &          ---  & \textbf{589.7}  \\
\hline
  16       &       44 &        11 &          ---  &            ---   &          --- & \textbf{315.0}  \\
  16       &       88 &        11 &          ---  &            ---   &          --- & \textbf{363.0}  \\
[0.1ex]
\hline
\end{tabular}
\caption{%
\bfrsp{}: Time to first solution for increasing track network
vertices, $\card{V}$, and orders , $\card{O}$, in seconds (1800s time limit, 4GB memory)
}
\label{bfrs-res}

\end{table}

\textbf{FBP:} Fragment-based planner \cite{Davies2014}\\
\textbf{CPX:} Constraint programming solver using Lazy Clause Generation \cite{lcg}\\
\textbf{POPF2:} A heuristic satisficing planner, \cite{Coles2010} \\%Coles et al. (2010)
\textbf{Golog:} High-level Automated planning language, Kelly \& Pearce (2006)

\end{exampleblock}

\end{column}

\end{columns}
\end{frame}
\end{document}
