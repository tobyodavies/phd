
#include <unordered_set>
#include <algorithm>
#include <queue>

#include "pdb.hxx"

#include "action.hxx"

using namespace aptk;
using namespace cdhl;

#define NDEBUG_LOG

#define ARENA_SIZE 1024

#ifdef DEBUG
#define LOG(X) std::cerr << X << std::endl
#else
#define LOG(X)
#endif

#include "argmin.hxx"
namespace cdhl {
void dump_state(STRIPS_Problem& prob, const Bit_Set& s){
	std::cerr << "{";
	for(auto f : prob.fluents()){
		if(s.isset(f->index()))
			std::cerr << " [" << f->signature() <<"]";
	}
	std::cerr << "}" << std::endl;
}

void dump_cube(STRIPS_Problem& prob, const LearningPDB::LBCube* expl){
	auto& s = expl->state_ub;
	std::cerr << expl << " = {";
	for(auto f : prob.fluents()){
		if(!s.isset(f->index()))
			std::cerr << " ~[" << f->signature() << "]";
	}
	std::cerr << "} >= " << expl->lb
		  << " (next_lb = " << expl->next_lb << ", "
		  << "lso = " << expl->least_succ_op << " "
		  << "(" << (expl->least_succ_op < prob.num_actions()? prob.actions()[expl->least_succ_op]->signature() : "NULL") << "), "
		  << "min_g = " << expl->min_g.first << ", "
		  << "depth = " << expl->min_g.second
		  << ")"
		  << std::endl;
}

int regress_intersect_size(STRIPS_Problem& prob, Bit_Set& s, Action_Idx op, const Bit_Set& s1){
	auto act = prob.actions()[op];

	auto sps = s.bits().packs();
	auto s1ps = s1.bits().packs();
	auto del_ps = act->edel_set().bits().packs();
	int size = 0;
	for(int i = 0; i < (int) s.bits().npacks() ; i++){
		size += Bit_Set::bits_in_word(sps[i] & (s1ps[i] | del_ps[i]));
	}
	//std::cerr << s.size() << " " << size << std::endl;
	return size;
}


bool LearningPDB::OpOrder::operator()(Action_Idx op0, Action_Idx op1){
	int c0 = op0 >= (int)prob.num_actions() ? BigM : prob.actions()[op0]->cost();
	int c1 = op1 >= (int)prob.num_actions() ? BigM : prob.actions()[op1]->cost();
	if(c0 == c1)
		return op0 < op1;
	return c0 > c1;
}

bool LearningPDB::CubeOrder::operator()(LBCube const* c0, LBCube const* c1){
	if(c0 == NULL)
		return c1 != NULL;
	if(c1 == NULL)
		return false;
	if(c0->lb == c1->lb){
		if(c0->least_succ_op != c1->least_succ_op)
			return op_lt(c0->least_succ_op, c1->least_succ_op);
		return c0->activity < c1->activity;
	}
	return c0->lb < c1->lb;
}

bool LearningPDB::CubeSimilarityOrder::operator()(LBCube const* c0, LBCube const* c1){
	if(c0 == NULL)
		return c1 != NULL;
	if(c1 == NULL)
		return false;

	// Compare first by change in the next_lb if merged with base
	int d_nxt0 = std::max(0, int(c0->lb) - max_lb);
	int d_nxt1 = std::max(0, int(c1->lb) - max_lb);
	if(d_nxt0 != d_nxt1){
		return d_nxt0 > d_nxt1;
	}
	return c0->activity < c1->activity;
	// int overlap0 = regress_intersect_size(op_lt.prob, base->state_ub, op, c0->state_ub);
	// int overlap1 = regress_intersect_size(op_lt.prob, base->state_ub, op, c1->state_ub);

	// if(overlap0 == overlap1){
	// 	return c0->activity < c1->activity;
	// }
	// return overlap0 < overlap1;
}

LearningPDB::LearningPDB(STRIPS_Problem& prob, bool trivial_expls, bool use_uip, bool strengthen)
	: op_lt({prob: prob}), cube_lt({op_lt:op_lt}), trivial_expls(trivial_expls), use_uip(use_uip), strengthen(strengthen), prob(prob), activity_delta(1.0)
{
	all_explanations.reserve(1<<16);
}


LearningPDB::CubeBuilder LearningPDB::builder() {
	return CubeBuilder(LBCube({
            least_succ_op: ordered_actions[0],
            lb: (unsigned) 0,
            next_lb: BigM,
            min_g: std::make_pair((unsigned)0, (unsigned)0),
	    state_ub: Bit_Set(prob.num_fluents()),
            num_vars: 0,
            activity: activity_delta,
            watching: prob.num_fluents(),
            dominated: 0
        }));
}

LearningPDB::LBCube* LearningPDB::addCube(const LBCube& e, LBCube* base) {
	assert(e.lb >= 0);
	assert(e.lb <= BigM);
	assert(e.lb >= BigM || e.next_lb > e.lb);

 	activity_delta /= 0.999;
	if(activity_delta > 1e30)
		reset_activity();

	LBCube* expl = NULL;

	if(base && dominates(&e, base)) {
		#ifdef DEBUG
		bool in_any = false;
		for(auto& arena_ptr : expl_arenas){
			if(arena_ptr.get() <= base
			   && base < arena_ptr.get() + ARENA_SIZE) {
				in_any = true;
				break;
			}
		}
		assert(in_any);
		#endif
		expl = base;
		if(base->watching < prob.num_fluents()){
			auto& wl = watchers.at(base->watching);
			auto it = std::find(wl.begin(), wl.end(), base);
			assert(it != wl.end());
			*it = std::move(wl.back());
			wl.pop_back();
		} else if (base->state_ub.size() == vars.size()){
			auto f = fixed_watchers.equal_range(compress_state(base->state_ub));
			for(auto i = f.first; i != f.second; ++i)
				if(i->second == base){
					fixed_watchers.erase(i);
					break;
				}
		}
	} else {
		if(expl_pool.empty()){
			expl_arenas.push_back(std::unique_ptr<LBCube[]>(new LBCube[ARENA_SIZE]));
			for(int i = 0; i < 1024; i++)
				expl_pool.push_back(&(expl_arenas.back().get()[i]));
		}
		expl = expl_pool.back();
		expl_pool.pop_back();
		all_explanations.push_back(expl);
	}

	*expl = e;
	auto& s = expl->state_ub;
	expl->watching = prob.num_fluents();
	expl->num_vars = vars.size();

	bool fixed = expl->state_ub.size() == vars.size();
	for(auto& v : vars){
		assert(!fixed || expl->state_ub.intersection_size(v.mask) == 1);
		expl->num_vars -= int(s.contains(v.mask));
	}
	if (fixed){
		fixed_watchers.insert(std::make_pair(compress_state(expl->state_ub), expl));
	}
	for(unsigned f = s.min_missing(max_state, 0); f < prob.num_fluents(); f = s.min_missing(max_state, f+1)){
		assert(!s.isset(f));
		if(!fixed && expl->watching == prob.num_fluents()) {
			expl->watching = f;
			watchers[f].push_back(expl);
		}
		fluent_activity[f] += activity_delta;
		vars[fluent_var[f]].activity += activity_delta / vars.size();
	}
	// if(true || expl->lb >= BigM)
	// 	dump_cube(prob, expl);
	return expl;
}

LearningPDB::LBCube* LearningPDB::closeState(const Bit_Set& s, PathInfo bounds) {
	//bounds.g.second += 1;
	auto e = best_expl(s, bounds);
	if(e->lb < BigM){
		auto b = builder();
		b.state_ub(s).lb(BigM).min_g(bounds.g);
		e = addCube(b);
	}
	return e;
}

void LearningPDB::setup(){

	_compressed_vars = 0;

	fluent_var.resize(prob.num_fluents(), -1);
	fluent_activity.resize(prob.num_fluents(), 1.0);
	watchers.resize(prob.num_fluents());
	for(auto& wl : watchers)
		wl.reserve(2048);
	max_state.resize(prob.num_fluents());
	for(auto f : prob.fluents())
		max_state.set(f->index());

	int var_idx = 0;
	for(auto v_it = prob.mutexes().begin_vars() ; v_it != prob.mutexes().end_vars(); ++v_it) {
		
		vars.push_back(VarInfo({
				id: var_idx,
				mask: Bit_Set(prob.num_fluents()),
				inv_mask: max_state,
				activity: 1.0,
				begin_f: prob.num_fluents(),
				end_f: prob.num_fluents()
			       }));
		auto& var = vars.back();
		auto& mask = var.mask;
		for(auto f : *v_it) {
			mask.set(f);
			fluent_var[f] = var_idx;
			var.fluents.push_back(f);
		}
		var.inv_mask.remove(mask);
		var.begin_f = mask.min_elem();
		var.end_f = var.begin_f + mask.size();
		var_heap.push_back(var_idx);
		var_idx++;
	}
	assert(*std::min_element(fluent_var.begin(), fluent_var.end()) == 0);

	for(auto act : prob.actions()){
		ordered_actions.push_back(act->index());
	}
	std::sort(ordered_actions.begin(), ordered_actions.end(), op_lt);
	action_order_index.resize(prob.num_actions());
	action_order_next.resize(prob.num_actions(), prob.num_actions());

	unsigned idx = 0;
	for(auto op : ordered_actions){
		if(idx > 0) {
			auto prev_op = ordered_actions[idx - 1];
			action_order_next[prev_op] = op;
		}
		action_order_index[op] = idx++;
	}
	action_order_index.push_back(prob.num_actions());

	Bit_Set all_bitset(prob.num_fluents());
	all_bitset.set_all();
	for(auto& f : prob.goal()){
		all_bitset.unset(f);
		int cost = BigM;
		for(auto act : prob.actions_adding(f)){
			if(act->cost() < cost) {
				cost = act->cost();
			}
		}
		auto b = builder();
		b.state_ub(all_bitset).lb(cost);
		addCube(b);
		all_bitset.set(f);
	}


	current_level = 0;
	prop_queue.reserve(prob.num_fluents());
	trail.reserve(prob.num_fluents());

	decision_level.resize(prob.num_fluents(), -1);
	impl_graph.resize(prob.num_fluents(), NULL);
	uip_lb = 0;
	uip_lso = ordered_actions[0];
	uip_state.resize(prob.num_fluents());
	for(auto& v : vars){
		uip_state.set_union(v.mask);
	}
}

void LearningPDB::update_watcher(const Bit_Set& s, std::vector<LBCube*>& wl, unsigned i) {
	auto e = wl[i];
	
	if(!e->dominated){
		unsigned new_f = next_watcher(s, e);
		if(new_f == prob.num_fluents())
			return;
		e->watching = new_f;
		watchers[new_f].emplace_back(e);
	} else {
		e->watching = prob.num_fluents();
	}
	wl[i] = std::move(wl.back());
	wl.pop_back();
}

LearningPDB::LBCube* LearningPDB::best_expl(const Bit_Set& s, const PathInfo& bounds){
	//TODO: see if we can add a cache here.
	LBCube *ret = best_expl(s, true,
				[&](LBCube* e){
					return bounds.g > e->min_g;
				});
	
	return ret;
}

LearningPDB::LBCube* LearningPDB::incr_expl(const Bit_Set& s, const PathInfo& bounds, LBCube* base, Action_Idx op){
	assert(op >= 0);
	auto act = prob.actions()[op];
	if(applies_after(s, bounds, op, base) && act->cost() + base->lb >= base->next_lb)
		return base;
	int next_lb = base->next_lb - int(act->cost());
	auto lt = cube_similar(base, op, next_lb);
	int min_lb = base->lb - int(act->cost());

	if(next_lb < 0){
		return NULL;
	}
	LBCube *ret = NULL;
	//TODO: add cache here

	auto& adds = act->add_set();
	auto& edels = act->edel_set();
	for(unsigned f = edels.min_elem(); f < prob.num_fluents(); f = edels.min_elem(f+1)){
		map_watchers(s, f,
			     [&](LBCube* e){
				     if((int) e->lb > min_lb && lt(ret, e) && applies_after(s, bounds, op, e))
					     ret = e;
				     return false;
			     });
	}
	if(ret != NULL)
		return ret;

	for(unsigned f = s.min_missing(max_state); f < prob.num_fluents(); f = s.min_missing(max_state, f+1)) {
		if(!adds.isset(f) && !edels.isset(f))
			map_watchers(s, f,
				     [&](LBCube* e){
					     if((int) e->lb > min_lb && lt(ret, e) && applies_after(s, bounds, op, e))
						     ret = e;
					     return false;
				     });
	}
	if(ret != NULL)
		return ret;
	for(auto r = fixed_watchers.equal_range(compress_succ_state(s, op)); r.first != r.second; r.first++){
		auto e = r.first->second;
		if((int) e->lb > min_lb && lt(ret, e) && applies_after(s, bounds, op, e))
			ret = e;
	}
	return ret;
}

LearningPDB::LBCube* LearningPDB::explain_lb(LBCube* expl){
	if(!expl || expl->least_succ_op < (int) prob.num_actions())
		return NULL;
	expl->lb = expl->next_lb;
	expl->next_lb = BigM;
	expl->least_succ_op = ordered_actions[0];
	if(strengthen){
		for(int i = var_heap.size() - 1; i >= 0; i--){
			auto& var = vars[var_heap[i]];
			bool fixed = expl->state_ub.intersection_size(var.inv_mask) == vars.size() - 1;
			if(fixed){
				var.map_false(expl->state_ub,
					      [&](int f){
						      auto r = fixed_watchers.equal_range(compress_delta_state(expl->state_ub, f));
						      std::for_each(r.first, r.second, [&](std::pair<uint64_t, LBCube*> p){
								      auto e = p.second;
								      if(e->lb >= expl->lb && expl->min_g >= e->min_g && e->state_ub.masked_contains(expl->state_ub, var.inv_mask)){
									      expl->state_ub.masked_union(e->state_ub, var.mask);
								      }
							      });
					      });
			}
			bool total = expl->state_ub.intersection_size(var.mask) == var.size();
			if(!total){
				var.map_true(expl->state_ub, [&](int f){
						map_watchers(expl->state_ub, f,
							     [&](LBCube* e){
								     if(!total && e->lb >= expl->lb &&
									expl->min_g >= e->min_g &&
									e->state_ub.masked_contains(expl->state_ub, var.inv_mask)){
									     expl->state_ub.masked_union(e->state_ub, var.mask);
									     total = expl->state_ub.intersection_size(var.mask) == var.size();
								     }
								     return false;
							     });
					});
			}
		}
	}
	return expl;
}

LearningPDB::LBCube* LearningPDB::explain_impossible_succ(const Bit_Set& s, const PathInfo& bounds, LBCube* expl){
	if(!expl)
		return NULL;
	Bit_Set s_ub = trivial_expls?s:expl->state_ub;
	Action_Idx op = expl->least_succ_op;
	auto act = prob.actions()[op];
	if(s.contains(act->prec_set())) {
		return NULL;
	}
	if(s_ub.contains(act->prec_set())){
		unsigned blocker = argmin(act->prec_set(),
					  [&](unsigned f){
						  if(s.isset(f))
							  return 1e99;
						  return -fluent_activity[f];
					  });
		assert(s_ub.isset(blocker));
		assert(!s.isset(blocker));
		s_ub.unset(blocker);
		fluent_activity[blocker] += activity_delta;
	}
	assert(!s_ub.contains(prob.actions()[op]->prec_set()));
	for(;
	    (unsigned) op < prob.num_actions()
	    && !s_ub.contains(prob.actions()[op]->prec_set());
	    op = action_order_next[op]){
		// pass
	}
	auto b = builder();
	b.state_ub(s_ub).lb(expl->lb).lso(op, expl->next_lb).min_g(expl->min_g);
	return addCube(b, expl);
}

LearningPDB::LBCube* LearningPDB::explain_unhelpful_succ(const Bit_Set& s, const PathInfo& bounds, LBCube* expl){
	if(!expl)
		return NULL;
	Action_Idx op = expl->least_succ_op;
	Bit_Set s_ub = trivial_expls?s:expl->state_ub;
	int next_lb = expl->next_lb;

	auto act = prob.actions()[op];
	LBCube* expl_after_op = incr_expl(s, bounds, expl, op);
	int next = (expl_after_op?expl_after_op->lb:0) + act->cost();
	if(next <= (int) expl->lb)
		return NULL;
	
	next_lb = std::min(next, next_lb);
	int max_cost = next_lb - (expl_after_op?expl_after_op->lb:0);
	if(expl_after_op){
		regress_intersect(s_ub, op, expl_after_op->state_ub);
	}
	for(;
	    (unsigned) op < prob.num_actions()
		    && prob.actions()[op]->cost() >= max_cost
		    && (!s_ub.contains(prob.actions()[op]->prec_set())
			||
			applies_after(s_ub, bounds, op, expl_after_op));
		    op = action_order_next[op]){
			// pass
	}

	auto b = builder();

	b.state_ub(s_ub).lb(expl->lb).lso(op, next_lb).min_g(expl->min_g);

	if(expl_after_op && expl_after_op->min_g > expl->min_g){
		auto g = expl_after_op->min_g;
		b.min_g(std::max(std::pair<unsigned, unsigned>(std::max(int(g.first - act->cost()),
									0),
							       std::max(int(g.second - 1),
									0)),
				 expl->min_g));
	}

	return addCube(b, expl);
}

int LearningPDB::eval(const Bit_Set& s, const PathInfo& bounds){
	if(use_uip){
		auto e = compute_uip(s, bounds);
		return std::max(e?e->lb:0, uip_lb);
	}
	unsigned ub = bounds.max_h >= BigM? BigM -1 : bounds.max_h + 1;

	LBCube *expl = best_expl(s, true, [&](LBCube* e){
			return bounds.g > e->min_g;
		});
	if(!expl)
		return 0; // found goal.
	LBCube *e2 = expl;

	PathInfo uip_bounds = bounds;
	uip_bounds.max_h = expl->lb;

	// if(use_uip){
	// 	e2 = compute_uip(s, uip_bounds);
	// 	//std::cerr << "uip" << std::endl;
	// 	if(!e2 || !(e2->lb > expl->lb || e2->least_succ_op >= expl->least_succ_op)){
	// 		e2 = expl;
	// 	}
	// }
	while(e2 != NULL && expl->lb <= ub){
		expl = e2;
		if((e2 = explain_lb(expl)))
			continue;
		if(!trivial_expls) {
			if((e2 = explain_impossible_succ(expl->state_ub, bounds, expl)))
				continue;
			if((e2 = explain_unhelpful_succ(expl->state_ub, bounds, expl)))
				continue;
		}
		if((e2 = explain_impossible_succ(s, bounds, expl)))
			continue;
		if((e2 = explain_unhelpful_succ(s, bounds, expl)))
			continue;
	}
	if(use_uip && expl->lb >= ub){
		uip_bounds.max_h = ub;
		e2 = compute_uip(s, uip_bounds);
		if(e2)
			expl = e2;
	}
	return expl->lb;
}

int LearningPDB::incr_eval(const Bit_Set& s, const PathInfo& bounds, Action_Idx op){
	auto base = best_expl(s, bounds);
	auto expl = incr_expl(s, bounds, base, op);
	return (expl == NULL ? 0 : expl->lb) + prob.actions()[op]->cost();
}

std::pair<int, Action_Idx> LearningPDB::best_action(const Bit_Set& s, const PathInfo& bounds) {
	typedef std::pair<int, Action_Idx> ret;
	LBCube *expl = NULL;
	if(bounds.min_h > bounds.max_h)
		return ret(bounds.min_h, -1);
	if(use_uip) {
		expl = compute_uip(s, bounds);
	} else {
		eval(s, bounds);
		expl = best_expl(s, bounds);
	}
	if(!expl)
		return ret(0, -1);
	if((int) expl->lb > bounds.max_h)
		return ret(expl->lb, -1);
	auto op = expl->least_succ_op;
	if(s.contains(prob.actions()[op]->prec_set()))
		return ret(expl->lb, op);
	assert(false);
	return ret(expl->lb, expl->least_succ_op);
}


bool LearningPDB::regress_intersect(Bit_Set& s, Action_Idx op, const Bit_Set& s1){
	if(&s == &s1)
		return false;
	auto act = prob.actions()[op];

	auto sps = s.bits().packs();
	auto s1ps = s1.bits().packs();
	auto del_ps = act->edel_set().bits().packs();
	bool ret = false;
	for(int i = 0; i < (int) s.bits().npacks() ; i++){
		auto x = sps[i];
		sps[i] &= (s1ps[i] | del_ps[i]);
		ret = ret || (x != sps[i]);
	}
	return ret;
}


bool LearningPDB::applies_after(const Bit_Set& s, const PathInfo& bounds, Action_Idx op, LearningPDB::LBCube const * e){
	if(!e)
		return true;
	auto g = bounds.g;
	auto act = prob.actions()[op];
	g.first += act->cost();
	g.second += 1;
	if(e->min_g >= g){
		return false;
	}
	auto sps = s.bits().packs();
	auto eps = e->state_ub.bits().packs();
	auto add_ps = act->add_set().bits().packs();
	auto del_ps = act->edel_set().bits().packs();

	for(int i = 0; i < (int) s.bits().npacks() ; i++){
		auto prog_p = (sps[i] & ~del_ps[i]) | add_ps[i];
		if( prog_p & ~eps[i] )
			return false;
	}
	return true;
}

void LearningPDB::remove_expls(decltype(all_explanations)::iterator it0){
	for(auto it = it0; it != all_explanations.end(); it++){
		auto e = *it;
		e->dominated = true;
		if(e->state_ub.size() == vars.size()){
			for(auto r = fixed_watchers.equal_range(compress_state(e->state_ub)); r.first != r.second; r.first++){
				if(r.first->second == e){
					fixed_watchers.erase(r.first);
					break;
				}
			}
		}
	}
	for(auto& wl : watchers){
		auto undominated = std::partition(wl.begin(), wl.end(), [&](LBCube* e){
					return e->locked || !e->dominated;
			});
		wl.erase(undominated, wl.end());
	}
	//std::cerr << "Removed " << all_explanations.end() - it0 << " clauses." << std::endl;
	for(auto it = it0; it != all_explanations.end(); it++)
		expl_pool.push_back(*it);
	all_explanations.erase(it0, all_explanations.end());
}

void LearningPDB::reset_activity(){
	for(auto& fa : fluent_activity)
		fa /= activity_delta;
	for(auto& v : vars)
		v.activity /= activity_delta;
	// remove_expls(std::partition(all_explanations.begin(),
	// 			    all_explanations.end(),
	// 			    [&](std::unique_ptr<LBCube>& e){
	// 				    e->activity /= activity_delta;
	// 				    return !e->dominated;
	// 			    }));
	activity_delta = 1.0;
}

void LearningPDB::gc(const std::vector<State*>& root, unsigned max_clauses, int cost_ub){
	int probably_dominated = 0;
	remove_expls(std::partition(all_explanations.begin(),
				    all_explanations.end(),
				    [&](LBCube* e){
					    if(e->next_lb > (unsigned)cost_ub){
					    	    e->next_lb = BigM;
					    }
					    if(e->lb > (unsigned)cost_ub){
					    	    e->lb = BigM;
					    }
					    if(e->state_ub.size() >= prob.num_fluents() - 1)
						    e->locked = true;
					    if(e->activity < 1e-40){
						    probably_dominated++;
						    return e->locked;
					    }
					    return !e->dominated;
				    }));
	reset_activity();
	PathInfo bounds = {
	    min_h: 0,
	    max_h: BigM,
	    g: std::make_pair(unsigned(0), unsigned(1))
	};
	for(auto s : root){
		auto e = best_expl(s->fluent_set(), bounds);
		if(e)
			e->locked = true;
		bounds.g.second += 1;

		e = best_expl(s->fluent_set(), bounds);
		if(e)
			e->locked = true;
		if(e){
			auto act = prob.actions()[e->least_succ_op];
			bounds.g.first += act->cost();
		}
	}
	// if(probably_dominated > (size() - max_clauses))
	// 	std::cerr << "Removed " << probably_dominated
	// 		  << " / " << (size() - max_clauses)
	// 		  << " probably dominated clauses" << std::endl;

	// Protect what are effectively binary clauses, and the blocking clauses for the roots
	auto protected_expls = std::partition(all_explanations.begin(),
					      all_explanations.end(),
					      [&](LBCube* e){
						      if(e->locked){
							      e->locked = false;
							      return true;
						      }
						      return false;
					      });
	if(protected_expls - all_explanations.begin() >= max_clauses){
		remove_expls(protected_expls);
		return;
	}
	auto end = all_explanations.end();
	while(end - all_explanations.begin() > max_clauses){
		auto pivot = all_explanations[max_clauses];
		auto p_it = std::partition(protected_expls,
					   end,
					   [&](LBCube* e){
						   // if(e->num_vars == pivot->num_vars)
						   // 	   return e->activity + e->lb * 1e6 > pivot->activity + pivot->lb * 1e6;
						   // return e->num_vars < pivot->num_vars;
						   if(e->min_g == pivot->min_g)
							   return e->activity*1e-8 + e->lb * 1e3 - 1e9*e->num_vars > pivot->activity*1e-8 + pivot->lb * 1e3 - 1e9*pivot->num_vars;
						   return e->min_g < pivot->min_g;
					   	   // if(e->lb == pivot->lb){
						   // 	   return e->activity > pivot->activity;
					   	   // }
					   	   // return e->lb > pivot->lb;
					   });
		//std::cerr << "Pivot (" << pivot << ") activity: " << pivot->activity << std::endl;
		
		if(p_it - all_explanations.begin() >= max_clauses){
			end = p_it;
		} else {
			protected_expls = p_it + 1;
			// std::cerr << "?" << p_it - all_explanations.begin() << " " << max_clauses << std::endl;
		}
	}
	remove_expls(end);
}

void LearningPDB::enqueue(unsigned p, LBCube* reason){
	if(uip_state.isset(p)){
		assert(!reason || reason->state_ub.isset(p));
		assert(!reason || next_watcher_var(uip_state, reason, p) == p);
		uip_state.unset(p);
		impl_graph[p] = reason;
		//if(reason) reason->impl = true;
		prop_queue.push_back(p);
		trail.push_back(p);
		
		current_level += int(reason == NULL);
		decision_level[p] = current_level;
		// std::cerr << "en c: " << current_level << std::endl;
		// std::cerr << "en t: " << trail.size() << std::endl;
	}
}

void LearningPDB::untrail(){
	assert(!trail.empty());
	assert(current_level <= trail.size());
	auto p = trail.back();
	bool decision = impl_graph[p] == NULL;
	decision_level[p] = -1;
	current_level -= int(decision);
	trail.pop_back();
	//if (impl_graph[p]) impl_graph[p]->impl = false;
	impl_graph[p] = NULL;
	uip_state.set(p);
	prop_queue.clear();
	while(!lso_impl_stack.empty() && !lso_impl_stack.back()->state_ub.contains(uip_state)){
		//lso_impl_stack.back()->impl = false;
		lso_impl_stack.pop_back();
	}
	if(!lso_impl_stack.empty())
		uip_lso = lso_impl_stack.back()->least_succ_op;
	else
		uip_lso = ordered_actions[0];

	while(!lb_impl_stack.empty() && !lb_impl_stack.back()->state_ub.contains(uip_state)){
		//lb_impl_stack.back()->impl = false;
		lb_impl_stack.pop_back();
	}
	if(!lb_impl_stack.empty())
		uip_lb = lb_impl_stack.back()->lb;
	else
		uip_lb = 0;
}

void LearningPDB::backtrack(int to_level){
	while(current_level > to_level){
		untrail();
	}
}

LearningPDB::LBCube* LearningPDB::propagate(const PathInfo& bounds){
	unsigned max_h = bounds.max_h == BigM ? BigM - 1: bounds.max_h;
	while(!prop_queue.empty()){
		unsigned f = prop_queue.back();
		assert(!uip_state.isset(f));
		prop_queue.pop_back();
		auto& var = vars[fluent_var[f]];

		for(int i = watchers[f].size() - 1; i >= 0; i--){
			auto e = watchers[f][i];
			// Only remove dominated clauses before they may
			// possibly have been used in the impl graph
			// if(!lb_impl_stack.empty()){
			// 	e->dominated = e->dominated || dominates(lb_impl_stack.back(), e);
			// }
			// if(!lso_impl_stack.empty()){
			// 	e->dominated = e->dominated || dominates(lso_impl_stack.back(), e);
			// }	
			// if(e->dominated){
			// 	watchers[f][i] = watchers[f].back();
			// 	watchers[f].pop_back();
			// 	e->watching = prob.num_fluents();
			// 	continue; 
			// }

			if(e->min_g >= bounds.g)
				continue;
			// if(e->next_lb < max_h && e->lb < uip_lb)
			// 	continue;

			bool applies = e->state_ub.contains(uip_state);
			if(e->lb >= uip_lb && applies && (lb_impl_stack.empty() || cube_lt(lb_impl_stack.back(), e))){
				uip_lb = e->lb;
				if(!lb_impl_stack.empty() && dominates(e, lb_impl_stack.back())) {
					lb_impl_stack.back()->dominated = true;
					lb_impl_stack.back() = e;
				} else {
					lb_impl_stack.push_back(e);
					//e->impl = true;
				}
			}
			if(e->next_lb > max_h && applies && op_lt(uip_lso, e->least_succ_op)){
				uip_lso = e->least_succ_op;
				if(!lso_impl_stack.empty() && dominates(e, lso_impl_stack.back())){
					lso_impl_stack.back()->dominated = true;
					lso_impl_stack.back() = e;
				} else {
					lso_impl_stack.push_back(e);
					//e->impl = true;
				}
			}

			// if(e->lb <= bounds.max_h)
			// 	continue;
			
			unsigned f1, f2;
			std::tie(f1, f2) = next_watchers(uip_state, e, f);
			if(f1 == prob.num_fluents() && (e->lb == BigM || e->lb > max_h)){ 
				prop_queue.clear();
				return analyze_conflict(e, bounds);
			}
			if(f1 == f2 && (e->lb == BigM || e->lb > max_h)) {
				for(auto p : vars[fluent_var[f1]]){
					if (impl_graph[p] && dominates(impl_graph[p], e)) {
						e->dominated = true;
						break;
						// dump_cube(prob, impl_graph[p]);
						// std::cerr << "Dominates " << std::endl;
						// dump_cube(prob, e);
					}
					if(e->state_ub.isset(p)){
						enqueue(p, e);
					}
				}
			} else if(f1 != prob.num_fluents()){
				watchers[f][i] = watchers[f].back();
				watchers[f].pop_back();
				watchers.at(f1).push_back(e);
				e->watching = f1;
			}
		}
		if(uip_state.intersection_size(var.mask) == 0) {
			auto b = builder();
			b.state_ub(var.inv_mask).lb(BigM);
			LBCube* e = addCube(b);
			return analyze_conflict(e, bounds);
		}
	}

	if(trail.size() == vars.size()){
		//Check fixed watchers!
		bool perfect_hash = (unsigned)compressed_vars() == vars.size();
		auto f = fixed_watchers.equal_range(compress_state(uip_state));
		auto dominated = f.second;
		for(auto i = f.first; i != f.second; ++i){
			auto e = i->second;
			if(!e->dominated){
				if(e->min_g >= bounds.g)
					continue;
				bool applies = perfect_hash || e->state_ub.contains(uip_state);
				if(e->lb > max_h && applies) {
					return analyze_conflict(e, bounds);
				}
				if(e->lb > uip_lb && applies)
					lb_impl_stack.push_back(e);
				if(e->least_succ_op > (int)uip_lso && applies && e->next_lb > max_h)
					lso_impl_stack.push_back(e);
			} else {
				dominated = i;
			}
		}
		if(dominated != f.second)
			fixed_watchers.erase(dominated);
	}

	return NULL;
}

bool LearningPDB::var_fixed(int var_id){
	return vars[var_id].mask.intersection_size(uip_state) == 1;
}

LearningPDB::LBCube* LearningPDB::compute_uip(const Bit_Set& base, const PathInfo& bounds) {

	backtrack(0);
	assert(uip_lb == 0);

	auto flt = [&](unsigned f0, unsigned f1){
		return fluent_activity[f0] > fluent_activity[f1];
	};
	unsigned max_confs = 100;
	assert(prop_queue.empty());
	std::sort(var_heap.begin(), var_heap.end(), [&](int v0, int v1){
			return vars[v0].activity > vars[v1].activity;
		});
	for(unsigned var_heap_idx = 0; var_heap_idx < vars.size(); var_heap_idx++){
		auto& var = vars[var_heap[var_heap_idx]];
		std::sort(var.begin(), var.end(), flt);
		auto i_df = var.begin();
		while(!prop_queue.empty() || i_df != var.end()) {
			if(max_confs == 0) {
				backtrack(0);
				return NULL;
			}
			auto conflict = propagate(bounds);
			if(conflict && !conflict->state_ub.contains(base)) {
				conflict = analyze_final(conflict, base, bounds);
			}
			assert(!conflict || conflict->state_ub.contains(base));
			if(conflict && conflict->state_ub.contains(base)) {
				// std::cerr << "Conflict" << std::endl;
				// dump_state(prob, base);
				// dump_cube(prob, conflict);
				return conflict;
			} else if(conflict) {
				// std::cerr << "Partial Conflict?? " << max_confs << std::endl;
				// dump_cube(prob, conflict);
				// dump_state(prob, base);
				max_confs--;
				int backtrack_to = 0;
				for(unsigned cf = 0; cf < prob.num_fluents(); cf++){
					if(conflict->state_ub.isset(cf))
						continue;
					if(decision_level[cf] >= backtrack_to && decision_level[cf] < current_level) {
						backtrack_to = decision_level[cf];
					}
				}
				backtrack(backtrack_to);
				var_heap_idx = -1;
				unsigned f0, f1;
				std::tie(f0, f1) = next_watchers(uip_state, conflict, conflict->watching);
				assert(f0 == f1);
				// Propagate the new clause
				prop_queue.clear();
				for(auto& p : vars[fluent_var[f0]])
					if(conflict->state_ub.isset(p))
						enqueue(p, conflict);
			}

			if(i_df != var.end() && prop_queue.empty()) {
				if(!base.isset(*i_df))
					enqueue(*i_df);
				i_df++;
			}
		}
	}

	LBCube* ret = NULL;
	if(!lb_impl_stack.empty()) {
		if(true
		   && !lso_impl_stack.empty()
		   && op_lt(lb_impl_stack.back()->least_succ_op,
			    lso_impl_stack.back()->least_succ_op)){
			// std::cerr << "-----------" << std::endl;
			// std::cerr << "compute uip" << std::endl;
			// dump_state(prob, base);

			lso_impl_stack.back()->activity += activity_delta;
			assert(lso_impl_stack.back()->next_lb > uip_lb);
			auto b = builder();
			auto lb_expl = lb_impl_stack.back();
			auto lso_expl = lso_impl_stack.back();
			b.state_ub(lb_expl->state_ub);
			b.e.state_ub.set_intersection(lso_expl->state_ub);
			b.lb(lb_expl->lb);
			b.lso(lso_expl->least_succ_op, lso_expl->next_lb);
			b.min_g(std::max(b.e.min_g,
					 lb_impl_stack.back()->min_g));

			lso_expl->activity += activity_delta;
			lb_expl->activity += activity_delta;

			// std::cerr << std::endl;
			// std::cerr << "h in [" << bounds.min_h << ", " << bounds.max_h << "]" << std::endl;
			// std::cerr << "g = (" << bounds.g.first << ", " << bounds.g.second << ")" << std::endl;
			// dump_cube(prob, lb_impl_stack.back());
			// dump_cube(prob, lso_impl_stack.back());
			// std::cerr << "-------------------" << std::endl;
			// dump_cube(prob, &b.e);
			// std::cerr << std::endl;

			ret = addCube(b); //, lb_impl_stack.back());
		} else {
			// auto b = builder();
			// b.e = *(lb_impl_stack.back());
			// ret = addCube(b);
			ret = lb_impl_stack.back();
		}
		PathInfo impr_bounds = {min_h: (int)uip_lb, max_h: (int)uip_lb, g: bounds.g};
		LBCube* e2 = ret;
		while(e2 != NULL && (int)ret->lb <= bounds.max_h){
			ret = e2;
			if((e2 = explain_lb(ret))){
				impr_bounds = {min_h: (int)e2->lb, max_h: (int)e2->lb, g: bounds.g};
				continue;
			}
			if((e2 = explain_impossible_succ(ret->state_ub, impr_bounds, ret)))
				continue;
			if((e2 = explain_unhelpful_succ(ret->state_ub, impr_bounds, ret)))
				continue;
			if((e2 = explain_impossible_succ(base, impr_bounds, ret)))
				continue;
			if((e2 = explain_unhelpful_succ(base, impr_bounds, ret)))
				continue;
		}
		// std::cerr << "-----" << std::endl;
		// dump_cube(prob, ret);
		ret = analyze_conflict(ret, bounds);
		ret = analyze_final(ret, base, bounds);
		// dump_cube(prob, ret);
	}
	assert(prop_queue.empty());
	// UIPWatcher bw = {f0: next_watcher(uip_state, base), f1:0, e: base};
	// bw.f1 = next_watcher_var(uip_state, base, bw.f0);
	if(!uip_state.contains(base))
		std::cerr << "WTF? No conflict? " << std::endl;
	if(!ret && !base.contains(uip_state) && max_confs > 0)
		std::cerr << "WTF2? No conflict? " << std::endl;
	// 	  << base->state_ub.contains(uip_state)
	// 	  << " "
	// 	  << uip_state.contains(base->state_ub)
	// 	  << " prop: " << bw.propagates(ub)
	// 	  << " || " << (bw.f1 == prob.num_fluents())
	// 	  << std::endl;
	// assert(ret->lb > bounds.max_h || base.contains(prob.actions()[ret->least_succ_op]->prec_set()));
	// ret = ret ? analyze_final(ret, base, bounds) : NULL;
	assert(ret->lb > bounds.max_h || base.contains(prob.actions()[ret->least_succ_op]->prec_set()));
	return ret;
}

LearningPDB::LBCube* LearningPDB::analyze_conflict(LBCube* base, const PathInfo& bounds){
	if(!base)
		return NULL;

	LBCube expl = *base;
	expl.watching = prob.num_fluents();

	if(trail.empty())
		std::cerr << "WTF Trail?" << std::endl;

	int conf_level = 0;
	for(unsigned p = 0; p < prob.num_fluents(); p++){
		if(expl.state_ub.isset(p))
			continue;
		conf_level = std::max(decision_level[p], conf_level);
	}
	assert(conf_level > 0);
	backtrack(conf_level);
	assert(current_level == conf_level);
	Bit_Set prev_levels(prob.num_fluents());
	for(auto p : expl.state_ub){
		if(decision_level[p] > conf_level)
			prev_levels.set(p);
	}

	unsigned w0, w1;
	std::tie(w0, w1) = next_watchers(prev_levels, &expl, 0);
	while(w0 != w1){
		auto f = trail.back();
		assert(expl.state_ub.contains(uip_state));
		assert(!trail.empty());
		assert(impl_graph[f] != NULL);
		assert(trail.size() > current_level);
		if(!expl.state_ub.isset(f)) {
			LBCube* r = impl_graph[f];
			auto& mask = vars[fluent_var[f]].mask;
			if(r) {
				for(unsigned p = r->state_ub.min_missing(expl.state_ub, 0);
				    p < prob.num_fluents();
				    p = r->state_ub.min_missing(expl.state_ub, p+1)){
					assert(expl.state_ub.isset(p) && !r->state_ub.isset(p));
					if(!mask.isset(p)){
						r->activity += activity_delta;
						assert(decision_level[p] >= 0);
						if(decision_level[p] < conf_level)
							prev_levels.set(p);
					}
				}
				resolve(&expl, bounds, r, fluent_var[f]);
			}
		}
		untrail();
		std::tie(w0, w1) = next_watchers(prev_levels, &expl, 0);
	}

	for(int i = trail.size() - 1; i >= 0; i--){
		auto f = trail[i];
		auto r = impl_graph[f];
		if(expl.state_ub.isset(f) || r == NULL)
			continue;
		// Self subsumption
		r->activity += activity_delta;
		if(next_watcher_var(expl.state_ub, r, f) == f) {
			resolve(&expl, bounds, r, fluent_var[f]);
		}
	}
	auto ret = addCube(expl, base);
	// dump_cube(prob, base);
	// dump_cube(prob, ret);
	return ret;
}

LearningPDB::LBCube* LearningPDB::analyze_final(LBCube* expl, const Bit_Set& assume, const PathInfo& bounds){
	assert(!trail.empty());
	// expl = explain_impossible_succ(uip_state, expl) || expl;
	auto b = builder();
	b.state_ub(expl->state_ub).lb(expl->lb).lso(expl->least_succ_op, expl->next_lb);
	for(int i = trail.size() - 1; i >= 0; i--){
		auto f = trail[i];
		assert(b.e.state_ub.contains(uip_state));
		auto r = impl_graph[f];
		if(r != NULL && !b.e.state_ub.isset(f)){
			if(r) {
				r->activity += activity_delta;
				resolve(&b.e, bounds, r, fluent_var[f]);
			} else {
				assert(!assume.isset(f));
			}
		}
	}

	assert(b.e.state_ub.contains(assume));
	for(int i = trail.size() - 1; i >= 0; i--){
		auto f = trail[i];
		auto r = impl_graph[f];
		if(b.e.state_ub.isset(f) || r == NULL)
			continue;
		// Self subsumption
		r->activity += activity_delta;
		if(next_watcher_var(b.e.state_ub, r, f) == f) {
			resolve(&b.e, bounds, r, fluent_var[f]);
		}
	}
	auto ret = addCube(b, expl);
	return ret;
}

int LearningPDB::compressed_vars() {
	if(_compressed_vars > 0)
		return _compressed_vars;

	uint64_t max_ret = 0;
	for(unsigned v = 0; v < vars.size(); v++){
		auto& mask = vars[v].mask;
		uint64_t sz = mask.size();
		if(UINT64_MAX / sz <= max_ret) {
			std::cerr << "Can compress " << v << "/" << vars.size()
				  << std::endl;
			return (_compressed_vars = v);
		}
		max_ret *= sz;
		max_ret += sz - 1;
	}
	return (_compressed_vars = vars.size());
}

uint64_t LearningPDB::compress_state(const Bit_Set& s){
	uint64_t ret = 0;
	for(int v = 0; v < compressed_vars(); v++){
		//auto& mask = vars[v].mask;
		uint64_t sz = vars[v].size();
		uint64_t min = vars[v].begin_f;
		ret *= sz;
		ret += s.min_elem(min) - min;
	}
	return ret;
}


uint64_t LearningPDB::compress_succ_state(const Bit_Set& s, Action_Idx op, int delta_f){
	uint64_t ret = 0;
	auto act = prob.actions()[op];
	auto& add = act->add_set();
	for(int v = 0; v < compressed_vars(); v++){
		//auto& mask = vars[v].mask;
		uint64_t sz = vars[v].size();
		uint64_t min = vars[v].begin_f;
		ret *= sz;
		uint64_t val = add.min_elem(min);
		if(val >= vars[v].end_f)
			val = s.min_elem(min);
		if((int)vars[v].begin_f <= delta_f && delta_f < (int)vars[v].end_f)
			val = delta_f;
		ret += val - min;
	}
	return ret;
}


uint64_t LearningPDB::compress_delta_state(const Bit_Set& s, unsigned f){
	uint64_t ret = 0;
	for(int v = 0; v < compressed_vars(); v++){
		//auto& mask = vars[v].mask;
		uint64_t sz = vars[v].size();
		uint64_t min = vars[v].begin_f;
		ret *= sz;
		uint64_t val = s.min_elem(min);
		if(fluent_var[f] == v)
			val = f;
		ret += val - min;
	}
	return ret;
}


void LearningPDB::explain_compressed_state(const Bit_Set& s, uint64_t compressed_lb, Bit_Set& expl){
	assert(compress_state(s) > compressed_lb);
	uncompress_state(compressed_lb, expl);
	int v;
	for(v = 0; v < compressed_vars(); v++){
		auto& mask = vars[v].mask;
		unsigned min_f = mask.min_elem();
		unsigned max_f = expl.min_elem(min_f);
		assert(max_f < min_f + mask.size());
		for(unsigned f = min_f; f < max_f; f++){
			expl.set(f);
		}

		if(s.min_elem(min_f) == max_f) {
			expl.set(max_f);
		} else {
			break;
		}
	}
	for(; v < compressed_vars(); v++){
		auto& mask = vars[v].mask;
		expl.set_union(mask);
	}
}


void LearningPDB::uncompress_state(uint64_t compressed, Bit_Set& s){
	uint64_t offset = prob.num_fluents();
	for(int v = compressed_vars() - 1; v >= 0; v--){
		auto& mask = vars[v].mask;
		uint64_t sz = mask.size();
		offset -= sz;
		s.set(offset + (compressed % sz));
		compressed /= sz;
	}
	for(unsigned v = compressed_vars(); v < vars.size(); v++){
		s.set_union(vars[v].mask);
	}
}

bool LearningPDB::dominates(const LBCube* e, const LBCube* dominated){
	auto ret = 
		e->lb >= dominated->lb
		&&
		e->min_g <= dominated->min_g
		&&
		e->state_ub.contains(dominated->state_ub)
		&&
		!op_lt(e->least_succ_op, dominated->least_succ_op)
		&&
		e->next_lb >= dominated->next_lb;
	return ret;
}

void LearningPDB::resolve(LBCube* expl, const PathInfo& bounds, const LBCube* resolvant, unsigned var_idx){
	if(expl == resolvant)
		return;
	auto& var = vars[var_idx];
	expl->state_ub.masked_intersection(resolvant->state_ub, var.mask);
	expl->state_ub.masked_union(resolvant->state_ub, var.mask);
	expl->lb = std::min(expl->lb,
			    resolvant->lb);
	if(resolvant->min_g > expl->min_g)
		expl->min_g = resolvant->min_g;
}
};

