/*
Lightweight Automated Planning Toolkit
Copyright (C) 2014
Toby Davies <tobyodavies@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* 
 * 
 */

#ifndef __DEPTH_FIRST_SEARCH__
#define __DEPTH_FIRST_SEARCH__

#define NOOP_LAYER_ASSUMPTIONS true
#define RELEVANCE_CLAUSES true
#define MUTEXES true
#undef NOSIMPLIFY

#include <ctime>       /* clock_t, clock, CLOCKS_PER_SEC */
#include <string>
#include <memory>
#include <vector>
#include <queue>
#include <utility>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <iostream>

#include <strips_prob.hxx>
#include <strips_state.hxx>
#include <h_2.hxx>
#include <aptk/search_prob.hxx>
#include <aptk/bit_set.hxx>

#include <minisat/core/SolverTypes.h>
//#include <minisat/core/Solver.h>
#include <minisat/simp/SimpSolver.h>


namespace opseq {

using namespace aptk;
using std::vector;
using Minisat::Lit;
using Minisat::mkLit;
using aptk::agnostic::H2_Heuristic;

#ifndef NOSIMPLIFY
typedef Minisat::SimpSolver Sat;
#else
typedef Minisat::Solver Sat;
#endif

typedef std::unordered_map<Action_Idx, unsigned> OpSet;
typedef STRIPS_Problem STRIPS_Prob;

struct Timer {
	std::string msg;
	clock_t t0;
	Timer(std::string msg_):msg(msg_), t0(clock()){
		std::cerr << "Start " << msg << std::endl;
	}
	~Timer(){
		std::cerr << msg << " took " << 1.0*(clock() - t0)/CLOCKS_PER_SEC << " seconds"  << std::endl;
	}
};

template<class OpSet>
class BaseLayer {
	typedef Minisat::Var Var;
	vector<Var>           ops;
	vector<Var>           facts;
	vector< vector<Var> > counts;
	Sat&                  cnf;
	const STRIPS_Prob&    prob;
	BaseLayer*            prev;
	Lit                   any_act;
	Var                   goal_satisfied;
	Var                   var_false;
	Lit                   lit_true;
	bool                  relaxed;
	int                   depth;

public:

	BaseLayer(const STRIPS_Prob& prob_, Sat& cnf_, BaseLayer* prev_, bool relaxed_=false):
			cnf(cnf_), prob(prob_), prev(prev_),
			any_act(mkLit(cnf.newVar())),
			var_false(prev!=NULL?prev->var_false:cnf.newVar()),
			lit_true(~mkLit(var_false)),
			relaxed(relaxed_),
			depth(prev?prev->depth+1:0)
	{
		if (!prev) {
			cnf.addClause(lit_true);
		}

		// std::cerr<< "Create new layer" <<std::endl;
		for(unsigned op = 0; op < prob.num_actions(); op++){
			ops.push_back(newOpVar(op));
		}
		// std::cerr<< "Created " << ops.size() << " op literals" <<std::endl;
		for(unsigned f = 0; f < prob.num_fluents(); f++){
			facts.push_back(newFactVar(f));
		}
		// std::cerr<< "Created " << facts.size() << " fact literals" <<std::endl;
		counts.resize(prob.num_actions());
		// std::cerr<< "Created " << counts.size() << " count lists" <<std::endl;

		// act_prev is True if an action with a lower index than op has occured in this layer
		Lit act_prev = ~lit_true;

		// If a fact is true at least one of its causes must be true
		for(unsigned f = 0 ; f < prob.num_fluents(); f++){
			Minisat::vec<Lit> clause;
			auto addedLit = mkLit(facts[f]);
			clause.push(~addedLit);
			cause_lits(f, clause);
			cnf.addClause(clause);
		}
		if (RELEVANCE_CLAUSES && prev) {
			// If a fact was true, it must be relevant
			for(unsigned f = 0 ; f < prob.num_fluents(); f++){
				Minisat::vec<Lit> prec_clause;
				// prev->f => f \/ prec for the op
				prec_clause.push(~mkLit(prev->facts[f]));
				prec_clause.push(mkLit(facts[f]));
				for(auto consumer : prob.actions_requiring(f)){
					prec_clause.push(mkLit(ops[consumer->index()]));
				}
				cnf.addClause(prec_clause);
				// prev->f => ~Added
				for(auto adder : prob.actions_adding(f)){
					if (!adder->requires(f)){
						cnf.addClause(~mkLit(prev->facts[f]),
							      ~mkLit(ops[adder->index()]));
					}
				}
			}
		}

		for(unsigned op = 0; op < prob.num_actions(); op++){
			assert(cnf.okay());
			auto action = prob.actions()[op];
			auto opLit = mkLit(ops[op]);

			// Preconditions
			auto precs = action->prec_vec();
			for(auto iprec = std::begin(precs); iprec != std::end(precs); iprec++){
				assert(cnf.okay());
				if (relaxed){
					if (!prob.is_in_init(*iprec)){
						// Relax ordering constraint
						cnf.addClause(mkLit(facts[*iprec]),
							      ~opLit);
					}
				} else if(prev && !relaxed){
					// ~prec => ~opLit
					cnf.addClause(mkLit(prev->facts[*iprec]),
						      ~opLit);
					assert(cnf.okay());
				} else {
					if (!prob.is_in_init(*iprec)){
						cnf.addClause(~opLit);
						assert(cnf.okay());
						break;
					}
				}
				assert(cnf.okay());
			}
			if(!relaxed){	
				// If an op occurs, all its deletes must hold
				// Del
				auto dels = action->del_vec();
				for(auto it = std::begin(dels); it != std::end(dels); it++){
					auto delLit = mkLit(facts[*it]);
					// opLit => ~delLit
					cnf.addClause(~opLit, ~delLit);
					assert(cnf.okay());
				}
			}
			if(RELEVANCE_CLAUSES){
				// Relevance: oplit => one effect added
				auto adds = action->add_vec();
				Minisat::vec<Lit> rel_clause;
				rel_clause.push(~opLit);
				for(auto it = std::begin(adds); it != std::end(adds); it++){
					rel_clause.push(mkLit(facts[*it]));
				}
				cnf.addClause(rel_clause);
			}

			countGE(op, 1);
			if(prev){
				countGE(op, prev->counts[op].size()-1);
			}

			// Ensure only one action occurs in a layer.
			if(!relaxed){
				// act_prev => ~opLit (alternatively opLit => ~act_prev)
				cnf.addClause(~act_prev, ~opLit);
				assert(cnf.okay());
			}
			// opLit => act_prev'
			auto act_prev2 = mkLit(cnf.newVar());
			cnf.addClause(~opLit, act_prev2);
			assert(cnf.okay());
			// act_prev => act_prev'
			cnf.addClause(~act_prev, act_prev2);
			assert(cnf.okay());
			// act_prev' => act_prev \/ opLit
			cnf.addClause(~act_prev2, act_prev, opLit);
			act_prev = act_prev2;
		}
		if(MUTEXES && !relaxed){
			for(auto& mutex : prob.mutexes()){
				add_mutex(mutex);
			}
		}

		// any_act <=> act_prev
		cnf.addClause(~any_act, act_prev);
		cnf.addClause(any_act, ~act_prev);
		
		// Force no-op layers to the end:
		// prev->any_act => any_act
		// ~any_act => ~prev.any_act
		if(prev){
			cnf.addClause(~prev->any_act, any_act);
			cnf.addClause(~mkLit(prev->goal_satisfied));
			assert(cnf.okay());
		}

		Minisat::vec<Lit> goal_clause;
		goal_satisfied = cnf.newVar();
		goal_clause.push(mkLit(goal_satisfied));
		for(unsigned f = 0; f < prob.num_fluents(); f++){
			auto factLit = mkLit(facts[f]);
			if (prob.is_in_goal(f)){
				goal_clause.push(~factLit);
				cnf.addClause(~mkLit(goal_satisfied), factLit);
			} else {
				cnf.addClause(~mkLit(goal_satisfied), ~factLit);
			}
		}
		cnf.addClause(goal_clause);
	}

	bool solve(OpSet& opset){
		Minisat::vec<Lit> assume;
		goal_assume(assume);
		get_assumptions(opset, assume);
		return solve(assume);
	}

	bool solve(Minisat::vec<Lit>& assume){
		// Timer t("post-copy solve");
		#ifndef NOSIMPLIFY
		cnf.thaw();
		cnf.freezeVar(goal_satisfied);
		for(auto v : facts)
			cnf.freezeVar(v);
		cnf.freezeVar(Minisat::var(lit_true));
		cnf.freezeVar(Minisat::var(any_act));
		freeze_counts();
                #endif
		return cnf.solve(assume);
	}

	bool solveLimited(OpSet& opset, int conflicts){
		Minisat::vec<Lit> assume;
		goal_assume(assume);
		get_assumptions(opset, assume);
		return solveLimited(assume, conflicts);
	}

	bool solveLimited(Minisat::vec<Lit>& assume, int conflicts){
		// Timer t("post-copy solve");
		#ifndef NOSIMPLIFY
		cnf.thaw();
		cnf.freezeVar(goal_satisfied);
		for(auto v : facts)
			cnf.freezeVar(v);
		cnf.freezeVar(Minisat::var(lit_true));
		cnf.freezeVar(Minisat::var(any_act));
		freeze_counts();
                #endif
		cnf.setConfBudget(conflicts);
		return cnf.solveLimited(assume) != Minisat::l_False;
	}

	void extract_nogood(const OpSet& opset, Bit_Set& cut){
		int actions = 0;
		for(Action_Idx op = 0; op < int(prob.num_actions()); op++){
			unsigned count = opset.count(op)?opset.at(op):0;
			actions += count;
			auto ub_lit = countGE(op, count+1);
			if(cnf.conflict.has(ub_lit)||cnf.conflict.has(~ub_lit)){
				//std::cerr << "Cut contains: " << prob.actions()[op]->signature() << std::endl;
				cut.set(op);
			}
		}
		BaseLayer* l = noop_prefix(actions);
		if (l && l != this){
			//auto old_elems = cut.count_elements();
			l->extract_nogood(OpSet(), cut);
			//std::cerr << "(new elems = " << cut.count_elements() - old_elems << ")" << std::endl;
		}
	}

	void strengthened_nogood(OpSet& opset, Bit_Set& cut){
		int iters = 1;
		extract_nogood(opset, cut);
		//return;

		Minisat::vec<Lit> assume;
		goal_assume(assume);
		get_assumptions(opset, assume);
		shrink_assumptions(assume);

		int orig_size = cut.bits().count_elements();
		for(int confs : {100}){
			std::unordered_set<Minisat::Var> reqd;
			for(int i = assume.size() - 1; i >= 0; i--){
				cnf.setConfBudget(confs);

				if(iters >= 1000)
					break;
				auto last = assume[i];
				if (reqd.count(Minisat::var(last)))
					break;

				// put ith elem last and then pop it.
				if (i != assume.size() - 1)
					std::swap(assume[i], assume[assume.size()-1]);
				assume.shrink_(1);
				
				if(cnf.solveLimited(assume) == Minisat::l_False){
					cut.reset();
					extract_nogood(opset, cut);
					shrink_assumptions(assume);
					i = assume.size()-1;
				} else {
					// last is now a required elem
					assume.push(last);
					std::swap(assume[reqd.size()], assume[assume.size()-1]);
					reqd.insert(Minisat::var(last));
					i++;
				}
				iters++;
			}
			std::cerr << "Reduced cut to " << cut.bits().count_elements() << " (-" << orig_size - cut.bits().count_elements() << ") in " << iters << " iterations. " << confs << std::endl;
		}

		cnf.budgetOff();
	}

	void extract_plan(vector<Action_Idx>& plan){
		if(prev){
			prev->extract_plan(plan);
		}
		int n_actions = prob.num_actions();
		for(int op = 0 ; op < n_actions ; op++){
			if(cnf.modelValue(ops.at(op))==Minisat::l_True){
				// std::cerr << "BaseLayer #" << i << " " << prob.actions()[op]->signature() << std::endl;
				plan.push_back(op);
				if (relaxed){
					std::cerr << "Warning delete relaxed action (a) " <<  std::endl;
				}
			}
		}

	}

	int extract_plan(OpSet opset, vector<Action_Idx>& plan){
		int layer_no = 0;
		if(prev){
			layer_no = prev->extract_plan(opset, plan) + 1;
		}
		if(cnf.modelValue(any_act)!=Minisat::l_True){
			std::cerr << "No action in layer " 
					  << layer_no << "." 
					  << std::endl;
			return layer_no;
		}
		bool any_op = false;
		for(auto iop = std::begin(opset); iop != std::end(opset); iop++){
			auto op = iop->first;
			if(cnf.modelValue(ops.at(op))==Minisat::l_True){
				any_op = true;
				// std::cerr << "BaseLayer #" << i << " " << prob.actions()[op]->signature() << std::endl;
				plan.push_back(op);
				if (relaxed){
					std::cerr << "Warning delete relaxed action (b) "
							  << prob.actions()[op]->signature() 
							  << "(layer " << layer_no << ")" 
							  << std::endl;
				}
			}
		}
		if(!any_op){
			std::cerr << "No action in layer "
				  << layer_no
				  << ", any_act is "
				  << (cnf.modelValue(any_act) == Minisat::l_False ? "False.":"Undef.")
				  << std::endl;
		}
		return layer_no;
	}

protected:

	Lit prev_val(unsigned f){
		if (prev) {
			return mkLit(prev->facts[f]);
		}
		if (prob.is_in_init(f)){
			return lit_true;
		}
		return ~lit_true;
	}

	Lit prevCountGE(unsigned op, int n){
		if (n <= 0)
			return lit_true;
		if (!prev)
			return ~lit_true;
		return prev->countGE(op, n);
	}

	Lit countGE(unsigned op, int n){
		//std::cerr << "layers[" << depth << "]->countGE(" << op << ", " << n << ")" << std::endl;
		if (n <= 0){
			if(counts[op].size() == 0)
				counts[op].push_back(Minisat::var(lit_true));
			return lit_true;
		}
		if(n > depth + 1){
			return ~lit_true;
		}
		if(int(counts[op].size()) <= n){
			auto iVar = cnf.newVar();
			auto iLit = mkLit(iVar);
			// iVar => any_act
			cnf.addClause(~iLit, any_act);

			// iVar => countGE(op, n-1)
			cnf.addClause(~iLit, countGE(op, n-1));

			assert(int(counts[op].size()) == n);
			counts[op].push_back(iVar);

			// prevCountGE(op, n) => iVar
			cnf.addClause(~prevCountGE(op, n), iLit);
			assert(cnf.okay());
			
			// prevCountGE(op, n-1) /\ ops[op] => iVar
			cnf.addClause(~prevCountGE(op, n-1),
						  ~mkLit(ops[op]),
						  iLit);
			assert(cnf.okay());

			// iVar => prevCountGE(op, n-1)
			cnf.addClause(~iLit,
						  prevCountGE(op, n-1));
			assert(cnf.okay());

			// iVar /\ ~ops[op] => prevCountGE(op, n)
			cnf.addClause(~iLit,
						  mkLit(ops[op]),
						  prevCountGE(op, n));
			assert(cnf.okay());
		}
		return mkLit(counts[op][n]);
	}

	void cause_lits(unsigned f, Minisat::vec<Lit>& clause){
		clause.push(prev_val(f));
		auto causes = prob.actions_adding(f);
		for(auto it = std::begin(causes); it != std::end(causes); it++){
			auto op = (*it)->index();
			auto actLit = mkLit(ops[op]);
			clause.push(actLit);
		}
	}
	
	void cause_bounds_lits(unsigned f, Minisat::vec<Lit>& clause, OpSet& opset){
		if(prob.is_in_init(f)){
			clause.push(lit_true);
			return;
		}
		auto causes = prob.actions_adding(f);
		for(auto it = std::begin(causes); it != std::end(causes); it++){
			auto op = (*it)->index();
			auto actLit = countGE(op, 1);
			clause.push(actLit);
		}
	}


	void add_mutex(const Fluent_Vec& mutex){
		auto f_prev = ~lit_true;
		for(auto f = std::begin(mutex); f != std::end(mutex); f++){
			// f_prev => ~facts[*f]
			cnf.addClause(~f_prev, ~mkLit(facts[*f]));
			assert(cnf.okay());
			auto f_prev2 = mkLit(auxVar());
			// f_prev => f_prev2
			cnf.addClause(~f_prev, f_prev2);
			assert(cnf.okay());
			f_prev = f_prev2;
		}

	}

	// void init_assume(Minisat::vec<Lit>& assume){
	// 	if(prev)
	// 		return prev->init_assume(assume);
	// 	for(int f = 0; f < init_facts->size(); f++){
	// 		if(!prob.is_in_init(f))
	// 			assume.push(~mkLit(init_facts->at(f)));
	// 	}
	// }

	void goal_assume(Minisat::vec<Lit>& assume){
		assume.push(mkLit(goal_satisfied));
	}

	void get_assumptions(const OpSet& opset, Minisat::vec<Lit>& assume){
		int actions = 0;
		for(auto opc : opset) {
			unsigned count = opc.second;
			actions += count;
		}
		//std::cerr<< "#actions = " << actions << std::endl;
		if (actions){
			BaseLayer* l = noop_prefix(actions);
			if (l && l != this){
				//std::cerr << "Adding noop prefix assumptions" << std::endl;
				l->get_assumptions(OpSet(), assume);
			}
		}
		for(Action_Idx op = 0; op < int(prob.num_actions()); op++){
			int count = opset.count(op)?opset.at(op):0;
			auto lit = ~countGE(op, count+1);
			assume.push(lit);
		}
	}


	bool shrink_assumptions(Minisat::vec<Lit>& assume){
		// std::cerr << "Initial assumption size:" << assume.size() << std::endl;
		unsigned i = 1;
		for(int j=i; j < assume.size(); j++){
			assume[i] = assume[j];
			if (cnf.conflict.has(~assume[i]) || cnf.conflict.has(assume[i])){
				i++;
			}
		}
		if(i == 1) {
			// std::cerr << "No assumptions" <<std::endl;
			return false;
		}
		assume.shrink_(assume.size() - i);
		// std::cerr << "Assumption size:" << assume.size() << " should = " << i << std::endl;
		return true;
	}

	void freeze_counts(bool final=true){
		#ifndef NOSIMPLIFY
		cnf.freezeVar(Minisat::var(any_act));
		for(Action_Idx op = 0; unsigned(op) < prob.num_actions(); op++){
			if(NOOP_LAYER_ASSUMPTIONS){
				cnf.freezeVar(counts[op].at(0));
				cnf.freezeVar(counts[op].at(1));
			}
			unsigned start_idx = final ? 0 : counts[op].size()-2;
			for(unsigned i = start_idx; i < counts[op].size(); i++){
				cnf.freezeVar(counts[op].at(i));
			}
		}
		if(prev){
			prev->freeze_counts(false);
		}
		#endif
	}

	Var auxVar(){
		return cnf.newVar();
	}

	Var newOpVar(Action_Idx op){
		return cnf.newVar();
	}
	
	Var newFactVar(int f){
		return cnf.newVar();
	}

	BaseLayer* noop_prefix(int required_layers){
		if(!NOOP_LAYER_ASSUMPTIONS)
			return NULL;

		if (required_layers == 0)
			return this;
		// if(relaxed)
		// 	std::cerr << "Hit relaxed layer for noop assumptions with " << required_layers << " layers to go!" << std::endl;
		if (prev)
			return prev->noop_prefix(required_layers - 1);
		return NULL;
	}

};

template<typename SearchProb, class OpSet=OpSet>
class OpSetEngine {
public:

	OpSetEngine(SearchProb& prob_):prob(prob_), rpg_len(-1), prefix_len(0) {
		//cnf.verbosity = 1;
	}
	OpSetEngine(const OpSetEngine& other): prob(other.prob), rpg_len(other.rpg_len), prefix_len(0){
		if (other.prefix_len){
			add_relaxation();
		}
	}
	virtual ~OpSetEngine(){
	}

	unsigned num_layers() const {
		return layers.size();
	}

	bool search_nogrow(OpSet& opset, Bit_Set& cut, std::vector<Action_Idx>* plan=NULL, int conflicts = -1){
		auto& goalLayer = layers.back();
		bool result = conflicts < 0 ? goalLayer->solve(opset) : goalLayer->solveLimited(opset, conflicts);
		if(!result){
			if (conflicts >= 0)
				goalLayer->extract_nogood(opset, cut);
			else
				goalLayer->strengthened_nogood(opset, cut);
			//std::cerr << "Cut Size = " << cut.bits().count_elements() << std::endl;
			assert(cnf.okay());
			return false;
		}
		if(conflicts < 0 && plan){
			goalLayer->extract_plan(opset, *plan);
		}
		return true;
	}

	bool search(OpSet& opset, std::vector<Action_Idx>& plan, Bit_Set& cut, int conflicts = -1){
		Timer t("SAT Solve");
		int required = required_layers(opset);
		if(conflicts < 0){
			for(int i = layers.size(); i < required; i+=5){
				ensure_layers(i);
				if(!search_nogrow(opset, cut, NULL, 0))
					return false;
			}
			ensure_layers(required);
		}
		return search_nogrow(opset, cut, &plan, conflicts);
	}

	bool add_relaxation(){
		if (layers.size() == 0){
			bool relaxed=true;
			Layer* prev = NULL;
			setup_watchlists();
			layers.emplace_back(new Layer(prob, cnf, prev, relaxed));
			prefix_len = 1;
			return true;
		}
		return false;
	}

	bool has_relaxation(){
		return prefix_len > 0;
	}

	void setup_watchlists(){
		if(!watchers.empty())
			return;
		Timer t("Setup watchlists");
		watchers.resize(prob.num_fluents());
		for(Action_Idx op = 0; op < prob.num_actions(); op++){
			auto act = prob.actions()[op];
			watchers[act->prec_vec()[0]].reserve(8);
			watchers[act->prec_vec()[0]].push_back(op);
		}
	}

	int RPGLen(){
		if(rpg_len >= 0)
			return rpg_len;

		setup_watchlists();

		clock_t t0 = clock();


		reachable.push_back(State(prob));
		reachable.back().set(prob.init());

		std::cerr << "RPGLen()" << std::endl;

		std::vector< std::pair<int, int> > q;
		q.reserve(prob.num_fluents());
		for(int f : prob.init()){
			q.emplace_back(std::make_pair(0, f));
			std::cerr << f << " in init" << std::endl;
		}

		while(q.size() > 0){
			int nlevel, f;
			pop_heap(q.begin(), q.end());
			std::tie(nlevel, f) = q.back();
			q.pop_back();
			auto level = -nlevel;
			std::cerr << level << " " << q.size() << std::endl;
			while (level + 1 >= reachable.size()){
				reachable.emplace_back(State(prob));
				reachable.back().set(reachable[reachable.size() - 2].fluent_vec());
			}
			auto& r = reachable[level];
			auto& next = reachable[level + 1];

			for(auto iAct = 0; iAct < watchers[f].size(); iAct++){
				auto op = watchers[f][iAct];
				auto act = prob.actions()[op];
				bool applies = true;
				for(auto p : act->prec_vec()){
					if (p != f && !r.entails(p)){
						std::swap(watchers[f][iAct], watchers[f].back());
						iAct--;
						watchers[f].pop_back();
						watchers[p].push_back(op);
						applies = false;
						break;
					}
				}

				if(applies){
					for (int f: act->add_vec()){
						if(!next.entails(f)){
							q.push_back(std::make_pair(nlevel - 1, f));
							std::push_heap(q.begin(), q.end());
						}
					}
					next.set(act->add_vec());
				}
			}
		}
		rpg_len = reachable.size();		
		reachable.shrink_to_fit();

		std::cerr << "RPGLen() = " << rpg_len << " (took " << 1.0*(clock() - t0)/CLOCKS_PER_SEC << "s)" << std::endl;

		return rpg_len;
	}

	const State& reachable_state(){
		return reachable.back();
	}

	void ComputeFactLMs(std::vector<Bit_Set> cuts) {
		OpSet ops;
		for(auto op = 0; op < prob.num_actions(); op ++)
			ops[op] = 1;

		for(unsigned f = 0; f < prob.num_fluents(); f++){

			if(prob.is_in_init(f))
				continue;
			std::vector<int> adding;
			for(auto act : prob.actions_adding(f))
				ops[act->index()] = 0;
			Bit_Set cut(prob.num_actions()-1);
			if(!StrengthenedHittingSet(ops, cut)){
				std::cerr << "Fact lm: " << prob.fluents()[f]->signature() << " elems: ";
				std::cerr << cut.bits().count_elements() << std::endl;
				cuts.push_back(cut);
			}
			for(auto act : prob.actions_adding(f))
				ops[act->index()] = 1;
		}
		std::cerr<<"Unit Fact LMs: "<< cuts.size() <<"/"<< prob.num_fluents() <<std::endl;
	}

	bool StrengthenedHittingSet(OpSet ops, Bit_Set& cut){
		return  StrengthenedHittingSet(ops, cut, prob.goal());
	}

	bool StrengthenedHittingSet(OpSet ops, Bit_Set& cut, const Fluent_Vec& goal){
		Bit_Set tmp_cut(cut);
		setup_watchlists();
		// std::cerr << "StrengthenedHittingSet" << std::endl;
		if(TestHittingSet(ops, tmp_cut, goal)){
			// std::cerr << "Found hitting set" << std::endl;
			return true;
		}
		// std::cerr << "Found cut" << std::endl;
		for(auto op = 0; op < prob.num_actions(); op ++)
			if(!tmp_cut.isset(op))
				ops[op] = 1;
		for(auto i = tmp_cut.begin(); i != tmp_cut.end(); ++i){
			ops[*i] = 1;
			cut.reset();
			if(TestHittingSet(ops, cut, goal)){
				ops[*i] = 0;
			}
			// std::cerr << "i = " << *i << std::endl;
		}
		TestHittingSet(ops, cut, goal);
		return false;
	}

	bool TestHittingSet(OpSet& ops, Bit_Set& cut, bool print_expl_size=false){
		return TestHittingSet(ops, cut, prob.goal(), print_expl_size);
	}

	bool TestHittingSet(OpSet& ops, Bit_Set& cut, const Fluent_Vec& goal, bool print_expl_size=false){
		// auto& wl = prob.watch_list();
		State s0(prob);
		s0.set(prob.init());
		// cut.reset()
		// auto s = wl.compute_fixpoint(s0, [&](unsigned op, const State& s2){
		// 		if(!(ops.count(op) && ops[count] > 0)){
		// 			cut.set(op);
		// 			return false;
		// 		}
		// 		return true;
		// 	});
		// return s.entails(goal);
		Bit_Set goal_set(prob.num_fluents());
		for(auto g: goal)
			goal_set.set(g);


		aptk::Fluent_Vec q = prob.init();
		q.reserve(prob.num_fluents());

		//std::cerr << "Testing hitting set" << std::endl;

		cut.reset();
		while(q.size() > 0){
			//std::cerr << "Testing hitting set q.size() = " << q.size() << std::endl;
			int f = q.back();
			q.pop_back();
			if (goal_set.isset(f) && s0.entails(goal)){
				return true;
			}
			// std::cerr << "Testing hitting set num watchers = " << watchers.at(f).size() << std::endl;
			for(auto iAct = 0; iAct < watchers[f].size(); iAct++){
				auto op = watchers[f][iAct];
				auto act = prob.actions()[op];
				bool applies = true;
				for(auto p : act->prec_vec()){
					if (p != f && !s0.entails(p)){
						// std::cerr << "Moving watchers... " << watchers[f].size() << std::endl;
						std::swap(watchers[f][iAct], watchers[f].back());
						iAct--;
						watchers[f].pop_back();
						watchers[p].push_back(op);
						applies = false;
						break;
					}
				}
				if(applies){
					if (!ops.count(op) || ops[op] == 0){
						// std::cerr << "Setcut" << std::endl;
						cut.set(op);
					} else {
						// std::cerr << "noset" << std::endl;
						for (int f: act->add_vec()){
							if(!s0.entails(f)){
								q.push_back(f);
							}
						}
						s0.set(act->add_vec());
					}
				}
			}
		}

		for(int op = 0; op < prob.num_actions(); op++){
			if(!cut.isset(op))
				continue;
			assert(ops.count(op) == 0 || ops.at(op) == 0);
			auto act = prob.actions()[op];
			// std::cerr << "shrink " << act->signature() << std::endl;
			if (s0.entails(act->add_vec())){
				cut.unset(op);
			}
		}
		if(print_expl_size){
			std::cerr << "LM (" << cut.bits().count_elements() 
				  << " elems), disabled by "
				  << prob.num_fluents() - s0.fluent_vec().size()
				  << " facts." << std::endl;
		}
				
		return false;
	}

	int ensure_layers(int min_layers){
		// std::cerr << "Ensure layers >= "<< min_layers << std::endl;
		for(int l = num_layers(); l < min_layers; l++){
			add_layer();
			assert(cnf.okay());
		}
		return min_layers;
	}
	
	int ensure_layers(OpSet& opset){
		return ensure_layers(required_layers(opset));
	}

private:
	typedef BaseLayer<OpSet> Layer;
	SearchProb& prob;

	Sat cnf;
	vector< std::unique_ptr< Layer > > layers;

	int rpg_len;
	int prefix_len;

	vector<State> reachable;
	vector< vector<Action_Idx> > watchers;
	

	int conflict_size(Minisat::vec<Lit>& assume){
		int ret = 0;
		for (int i = 0; i < assume.size(); i++) {
			if (cnf.conflict.has(assume[i])){
				ret++;
			}
		}
		return ret;
	}

	void add_layer(){
		bool relaxed = false;
		if (num_layers() == 0){
			// std::cerr << "Add first layer" << std::endl;
			layers.emplace_back(new Layer(prob, cnf, NULL, relaxed));
		} else {
			// std::cerr << "Add layer #"<< num_layers()<< std::endl;
			layers.emplace_back(new Layer(prob, cnf, layers.back().get(), relaxed));
		}
	}

	int required_layers(OpSet& opset){
		int min_layers = prefix_len + 1;
		for(auto it = std::begin(opset); it != std::end(opset); it++){
			min_layers += it->second;
		}
		return min_layers;
	}

};
}
#endif
