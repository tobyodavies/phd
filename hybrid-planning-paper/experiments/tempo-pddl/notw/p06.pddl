(define (problem bfrs_128)
(:domain bfrs)
(:objects
    p2 - location
    p1 - location
    d - location
    m1 - location
    m2 - location
    y - location
    x - location
    o0 - order
    o1 - order
    o2 - order
    o3 - order
    o4 - order
    o5 - order
    o6 - order
    o7 - order
    o8 - order
    o9 - order
    o10 - order
    o11 - order
    o12 - order
    o13 - order
    o14 - order
    o15 - order
    o16 - order
    o17 - order
    o18 - order
    o19 - order
    o20 - order
    o21 - order
    o22 - order
    o23 - order
    o24 - order
    o25 - order
    o26 - order
    o27 - order
    o28 - order
    o29 - order
    o30 - order
    o31 - order
    o32 - order
    o33 - order
    o34 - order
    o35 - order
    o36 - order
    o37 - order
    o38 - order
    o39 - order
    o40 - order
    o41 - order
    o42 - order
    o43 - order
    o44 - order
    o45 - order
    o46 - order
    o47 - order
    o48 - order
    o49 - order
    o50 - order
    o51 - order
    o52 - order
    o53 - order
    o54 - order
    o55 - order
    o56 - order
    o57 - order
    o58 - order
    o59 - order
    o60 - order
    o61 - order
    o62 - order
    o63 - order
    o64 - order
    o65 - order
    o66 - order
    o67 - order
    o68 - order
    o69 - order
    o70 - order
    o71 - order
    o72 - order
    o73 - order
    o74 - order
    o75 - order
    o76 - order
    o77 - order
    o78 - order
    o79 - order
    o80 - order
    o81 - order
    o82 - order
    o83 - order
    o84 - order
    o85 - order
    o86 - order
    o87 - order
    o88 - order
    o89 - order
    o90 - order
    o91 - order
    o92 - order
    o93 - order
    o94 - order
    o95 - order
    o96 - order
    o97 - order
    o98 - order
    o99 - order
    o100 - order
    o101 - order
    o102 - order
    o103 - order
    o104 - order
    o105 - order
    o106 - order
    o107 - order
    o108 - order
    o109 - order
    o110 - order
    o111 - order
    o112 - order
    o113 - order
    o114 - order
    o115 - order
    o116 - order
    o117 - order
    o118 - order
    o119 - order
    o120 - order
    o121 - order
    o122 - order
    o123 - order
    o124 - order
    o125 - order
    o126 - order
    o127 - order
    c0 - consist
    c1 - consist
    c2 - consist
    c3 - consist
    c4 - consist
    c5 - consist
    c6 - consist
    c7 - consist
    c8 - consist
    c9 - consist
    c10 - consist
    c11 - consist
    c12 - consist
    c13 - consist
    c14 - consist
    c15 - consist
    c16 - consist
    c17 - consist
    c18 - consist
    c19 - consist
    c20 - consist
    c21 - consist
    c22 - consist
    c23 - consist
    c24 - consist
    c25 - consist
    c26 - consist
    c27 - consist
    c28 - consist
    c29 - consist
    c30 - consist
    c31 - consist
    c32 - consist
    c33 - consist
    c34 - consist
    c35 - consist
    c36 - consist
    c37 - consist
    c38 - consist
    c39 - consist
    c40 - consist
    c41 - consist
    c42 - consist
    c43 - consist
    c44 - consist
    c45 - consist
    c46 - consist
    c47 - consist
    c48 - consist
    c49 - consist
    c50 - consist
    c51 - consist
    c52 - consist
    c53 - consist
    c54 - consist
    c55 - consist
    c56 - consist
    c57 - consist
    c58 - consist
    c59 - consist
    c60 - consist
    c61 - consist
    c62 - consist
    c63 - consist
    c64 - consist
    c65 - consist
    c66 - consist
    c67 - consist
    c68 - consist
    c69 - consist
    c70 - consist
    c71 - consist
    c72 - consist
    c73 - consist
    c74 - consist
    c75 - consist
    c76 - consist
    c77 - consist
    c78 - consist
    c79 - consist
    c80 - consist
    c81 - consist
    c82 - consist
    c83 - consist
    c84 - consist
    c85 - consist
    c86 - consist
    c87 - consist
    c88 - consist
    c89 - consist
    c90 - consist
    c91 - consist
    c92 - consist
    c93 - consist
    c94 - consist
    c95 - consist
    c96 - consist
    c97 - consist
    c98 - consist
    c99 - consist
    c100 - consist
    c101 - consist
    c102 - consist
    c103 - consist
    c104 - consist
    c105 - consist
    c106 - consist
    c107 - consist
    c108 - consist
    c109 - consist
    c110 - consist
    c111 - consist
    c112 - consist
    c113 - consist
    c114 - consist
    c115 - consist
    c116 - consist
    c117 - consist
    c118 - consist
    c119 - consist
    c120 - consist
    c121 - consist
    c122 - consist
    c123 - consist
    c124 - consist
    c125 - consist
    c126 - consist
    c127 - consist
)
(:init
  (is_yard y)
    (clear p2)
    (clear p1)
    (clear d)
    (clear m1)
    (clear m2)
    (clear y)
    (clear x)

    (conn p2 y)
    (conn y p2)
    (conn p1 y)
    (conn y p1)
    (conn d y)
    (conn y d)
    (conn d x)
    (conn x d)
    (conn d m1)
    (conn m1 d)
    (conn d m2)
    (conn m2 d)
    (conn y x)
    (conn x y)

    ;; order o0 ('m1', 'p2', 0, 12)
    (order_mine o0 m1)
    (order_port o0 p2)
    (not-delivered o0)

    ;; consist c0
    (available c0)
    (empty c0)
    (at c0 y)

    (compat c0 o0)

    ;; order o1 ('m1', 'p2', 12, 24)
    (order_mine o1 m1)
    (order_port o1 p2)
    (not-delivered o1)

    ;; consist c1
    (available c1)
    (empty c1)
    (at c1 y)

    (compat c1 o1)

    ;; order o2 ('m1', 'p2', 24, 36)
    (order_mine o2 m1)
    (order_port o2 p2)
    (not-delivered o2)

    ;; consist c2
    (available c2)
    (empty c2)
    (at c2 y)

    (compat c2 o2)

    ;; order o3 ('m1', 'p2', 36, 48)
    (order_mine o3 m1)
    (order_port o3 p2)
    (not-delivered o3)

    ;; consist c3
    (available c3)
    (empty c3)
    (at c3 y)

    (compat c3 o3)

    ;; order o4 ('m1', 'p2', 48, 60)
    (order_mine o4 m1)
    (order_port o4 p2)
    (not-delivered o4)

    ;; consist c4
    (available c4)
    (empty c4)
    (at c4 y)

    (compat c4 o4)

    ;; order o5 ('m1', 'p2', 60, 72)
    (order_mine o5 m1)
    (order_port o5 p2)
    (not-delivered o5)

    ;; consist c5
    (available c5)
    (empty c5)
    (at c5 y)

    (compat c5 o5)

    ;; order o6 ('m1', 'p2', 72, 84)
    (order_mine o6 m1)
    (order_port o6 p2)
    (not-delivered o6)

    ;; consist c6
    (available c6)
    (empty c6)
    (at c6 y)

    (compat c6 o6)

    ;; order o7 ('m1', 'p2', 84, 96)
    (order_mine o7 m1)
    (order_port o7 p2)
    (not-delivered o7)

    ;; consist c7
    (available c7)
    (empty c7)
    (at c7 y)

    (compat c7 o7)

    ;; order o8 ('m1', 'p2', 96, 108)
    (order_mine o8 m1)
    (order_port o8 p2)
    (not-delivered o8)

    ;; consist c8
    (available c8)
    (empty c8)
    (at c8 y)

    (compat c8 o8)

    ;; order o9 ('m1', 'p2', 108, 120)
    (order_mine o9 m1)
    (order_port o9 p2)
    (not-delivered o9)

    ;; consist c9
    (available c9)
    (empty c9)
    (at c9 y)

    (compat c9 o9)

    ;; order o10 ('m1', 'p2', 120, 132)
    (order_mine o10 m1)
    (order_port o10 p2)
    (not-delivered o10)

    ;; consist c10
    (available c10)
    (empty c10)
    (at c10 y)

    (compat c10 o10)

    ;; order o11 ('m1', 'p2', 132, 144)
    (order_mine o11 m1)
    (order_port o11 p2)
    (not-delivered o11)

    ;; consist c11
    (available c11)
    (empty c11)
    (at c11 y)

    (compat c11 o11)

    ;; order o12 ('m1', 'p2', 144, 156)
    (order_mine o12 m1)
    (order_port o12 p2)
    (not-delivered o12)

    ;; consist c12
    (available c12)
    (empty c12)
    (at c12 y)

    (compat c12 o12)

    ;; order o13 ('m1', 'p2', 156, 168)
    (order_mine o13 m1)
    (order_port o13 p2)
    (not-delivered o13)

    ;; consist c13
    (available c13)
    (empty c13)
    (at c13 y)

    (compat c13 o13)

    ;; order o14 ('m1', 'p2', 168, 180)
    (order_mine o14 m1)
    (order_port o14 p2)
    (not-delivered o14)

    ;; consist c14
    (available c14)
    (empty c14)
    (at c14 y)

    (compat c14 o14)

    ;; order o15 ('m1', 'p2', 180, 192)
    (order_mine o15 m1)
    (order_port o15 p2)
    (not-delivered o15)

    ;; consist c15
    (available c15)
    (empty c15)
    (at c15 y)

    (compat c15 o15)

    ;; order o16 ('m1', 'p2', 192, 204)
    (order_mine o16 m1)
    (order_port o16 p2)
    (not-delivered o16)

    ;; consist c16
    (available c16)
    (empty c16)
    (at c16 y)

    (compat c16 o16)

    ;; order o17 ('m1', 'p2', 204, 216)
    (order_mine o17 m1)
    (order_port o17 p2)
    (not-delivered o17)

    ;; consist c17
    (available c17)
    (empty c17)
    (at c17 y)

    (compat c17 o17)

    ;; order o18 ('m1', 'p2', 216, 228)
    (order_mine o18 m1)
    (order_port o18 p2)
    (not-delivered o18)

    ;; consist c18
    (available c18)
    (empty c18)
    (at c18 y)

    (compat c18 o18)

    ;; order o19 ('m1', 'p2', 228, 240)
    (order_mine o19 m1)
    (order_port o19 p2)
    (not-delivered o19)

    ;; consist c19
    (available c19)
    (empty c19)
    (at c19 y)

    (compat c19 o19)

    ;; order o20 ('m1', 'p2', 240, 252)
    (order_mine o20 m1)
    (order_port o20 p2)
    (not-delivered o20)

    ;; consist c20
    (available c20)
    (empty c20)
    (at c20 y)

    (compat c20 o20)

    ;; order o21 ('m1', 'p2', 252, 264)
    (order_mine o21 m1)
    (order_port o21 p2)
    (not-delivered o21)

    ;; consist c21
    (available c21)
    (empty c21)
    (at c21 y)

    (compat c21 o21)

    ;; order o22 ('m1', 'p2', 264, 276)
    (order_mine o22 m1)
    (order_port o22 p2)
    (not-delivered o22)

    ;; consist c22
    (available c22)
    (empty c22)
    (at c22 y)

    (compat c22 o22)

    ;; order o23 ('m1', 'p2', 276, 288)
    (order_mine o23 m1)
    (order_port o23 p2)
    (not-delivered o23)

    ;; consist c23
    (available c23)
    (empty c23)
    (at c23 y)

    (compat c23 o23)

    ;; order o24 ('m1', 'p2', 288, 300)
    (order_mine o24 m1)
    (order_port o24 p2)
    (not-delivered o24)

    ;; consist c24
    (available c24)
    (empty c24)
    (at c24 y)

    (compat c24 o24)

    ;; order o25 ('m1', 'p2', 300, 312)
    (order_mine o25 m1)
    (order_port o25 p2)
    (not-delivered o25)

    ;; consist c25
    (available c25)
    (empty c25)
    (at c25 y)

    (compat c25 o25)

    ;; order o26 ('m1', 'p2', 312, 324)
    (order_mine o26 m1)
    (order_port o26 p2)
    (not-delivered o26)

    ;; consist c26
    (available c26)
    (empty c26)
    (at c26 y)

    (compat c26 o26)

    ;; order o27 ('m1', 'p2', 324, 336)
    (order_mine o27 m1)
    (order_port o27 p2)
    (not-delivered o27)

    ;; consist c27
    (available c27)
    (empty c27)
    (at c27 y)

    (compat c27 o27)

    ;; order o28 ('m1', 'p2', 336, 348)
    (order_mine o28 m1)
    (order_port o28 p2)
    (not-delivered o28)

    ;; consist c28
    (available c28)
    (empty c28)
    (at c28 y)

    (compat c28 o28)

    ;; order o29 ('m1', 'p2', 348, 360)
    (order_mine o29 m1)
    (order_port o29 p2)
    (not-delivered o29)

    ;; consist c29
    (available c29)
    (empty c29)
    (at c29 y)

    (compat c29 o29)

    ;; order o30 ('m1', 'p2', 360, 372)
    (order_mine o30 m1)
    (order_port o30 p2)
    (not-delivered o30)

    ;; consist c30
    (available c30)
    (empty c30)
    (at c30 y)

    (compat c30 o30)

    ;; order o31 ('m1', 'p2', 372, 384)
    (order_mine o31 m1)
    (order_port o31 p2)
    (not-delivered o31)

    ;; consist c31
    (available c31)
    (empty c31)
    (at c31 y)

    (compat c31 o31)

    ;; order o32 ('m1', 'p1', 0, 12)
    (order_mine o32 m1)
    (order_port o32 p1)
    (not-delivered o32)

    ;; consist c32
    (available c32)
    (empty c32)
    (at c32 y)

    (compat c32 o32)

    ;; order o33 ('m1', 'p1', 12, 24)
    (order_mine o33 m1)
    (order_port o33 p1)
    (not-delivered o33)

    ;; consist c33
    (available c33)
    (empty c33)
    (at c33 y)

    (compat c33 o33)

    ;; order o34 ('m1', 'p1', 24, 36)
    (order_mine o34 m1)
    (order_port o34 p1)
    (not-delivered o34)

    ;; consist c34
    (available c34)
    (empty c34)
    (at c34 y)

    (compat c34 o34)

    ;; order o35 ('m1', 'p1', 36, 48)
    (order_mine o35 m1)
    (order_port o35 p1)
    (not-delivered o35)

    ;; consist c35
    (available c35)
    (empty c35)
    (at c35 y)

    (compat c35 o35)

    ;; order o36 ('m1', 'p1', 48, 60)
    (order_mine o36 m1)
    (order_port o36 p1)
    (not-delivered o36)

    ;; consist c36
    (available c36)
    (empty c36)
    (at c36 y)

    (compat c36 o36)

    ;; order o37 ('m1', 'p1', 60, 72)
    (order_mine o37 m1)
    (order_port o37 p1)
    (not-delivered o37)

    ;; consist c37
    (available c37)
    (empty c37)
    (at c37 y)

    (compat c37 o37)

    ;; order o38 ('m1', 'p1', 72, 84)
    (order_mine o38 m1)
    (order_port o38 p1)
    (not-delivered o38)

    ;; consist c38
    (available c38)
    (empty c38)
    (at c38 y)

    (compat c38 o38)

    ;; order o39 ('m1', 'p1', 84, 96)
    (order_mine o39 m1)
    (order_port o39 p1)
    (not-delivered o39)

    ;; consist c39
    (available c39)
    (empty c39)
    (at c39 y)

    (compat c39 o39)

    ;; order o40 ('m1', 'p1', 96, 108)
    (order_mine o40 m1)
    (order_port o40 p1)
    (not-delivered o40)

    ;; consist c40
    (available c40)
    (empty c40)
    (at c40 y)

    (compat c40 o40)

    ;; order o41 ('m1', 'p1', 108, 120)
    (order_mine o41 m1)
    (order_port o41 p1)
    (not-delivered o41)

    ;; consist c41
    (available c41)
    (empty c41)
    (at c41 y)

    (compat c41 o41)

    ;; order o42 ('m1', 'p1', 120, 132)
    (order_mine o42 m1)
    (order_port o42 p1)
    (not-delivered o42)

    ;; consist c42
    (available c42)
    (empty c42)
    (at c42 y)

    (compat c42 o42)

    ;; order o43 ('m1', 'p1', 132, 144)
    (order_mine o43 m1)
    (order_port o43 p1)
    (not-delivered o43)

    ;; consist c43
    (available c43)
    (empty c43)
    (at c43 y)

    (compat c43 o43)

    ;; order o44 ('m1', 'p1', 144, 156)
    (order_mine o44 m1)
    (order_port o44 p1)
    (not-delivered o44)

    ;; consist c44
    (available c44)
    (empty c44)
    (at c44 y)

    (compat c44 o44)

    ;; order o45 ('m1', 'p1', 156, 168)
    (order_mine o45 m1)
    (order_port o45 p1)
    (not-delivered o45)

    ;; consist c45
    (available c45)
    (empty c45)
    (at c45 y)

    (compat c45 o45)

    ;; order o46 ('m1', 'p1', 168, 180)
    (order_mine o46 m1)
    (order_port o46 p1)
    (not-delivered o46)

    ;; consist c46
    (available c46)
    (empty c46)
    (at c46 y)

    (compat c46 o46)

    ;; order o47 ('m1', 'p1', 180, 192)
    (order_mine o47 m1)
    (order_port o47 p1)
    (not-delivered o47)

    ;; consist c47
    (available c47)
    (empty c47)
    (at c47 y)

    (compat c47 o47)

    ;; order o48 ('m1', 'p1', 192, 204)
    (order_mine o48 m1)
    (order_port o48 p1)
    (not-delivered o48)

    ;; consist c48
    (available c48)
    (empty c48)
    (at c48 y)

    (compat c48 o48)

    ;; order o49 ('m1', 'p1', 204, 216)
    (order_mine o49 m1)
    (order_port o49 p1)
    (not-delivered o49)

    ;; consist c49
    (available c49)
    (empty c49)
    (at c49 y)

    (compat c49 o49)

    ;; order o50 ('m1', 'p1', 216, 228)
    (order_mine o50 m1)
    (order_port o50 p1)
    (not-delivered o50)

    ;; consist c50
    (available c50)
    (empty c50)
    (at c50 y)

    (compat c50 o50)

    ;; order o51 ('m1', 'p1', 228, 240)
    (order_mine o51 m1)
    (order_port o51 p1)
    (not-delivered o51)

    ;; consist c51
    (available c51)
    (empty c51)
    (at c51 y)

    (compat c51 o51)

    ;; order o52 ('m1', 'p1', 240, 252)
    (order_mine o52 m1)
    (order_port o52 p1)
    (not-delivered o52)

    ;; consist c52
    (available c52)
    (empty c52)
    (at c52 y)

    (compat c52 o52)

    ;; order o53 ('m1', 'p1', 252, 264)
    (order_mine o53 m1)
    (order_port o53 p1)
    (not-delivered o53)

    ;; consist c53
    (available c53)
    (empty c53)
    (at c53 y)

    (compat c53 o53)

    ;; order o54 ('m1', 'p1', 264, 276)
    (order_mine o54 m1)
    (order_port o54 p1)
    (not-delivered o54)

    ;; consist c54
    (available c54)
    (empty c54)
    (at c54 y)

    (compat c54 o54)

    ;; order o55 ('m1', 'p1', 276, 288)
    (order_mine o55 m1)
    (order_port o55 p1)
    (not-delivered o55)

    ;; consist c55
    (available c55)
    (empty c55)
    (at c55 y)

    (compat c55 o55)

    ;; order o56 ('m1', 'p1', 288, 300)
    (order_mine o56 m1)
    (order_port o56 p1)
    (not-delivered o56)

    ;; consist c56
    (available c56)
    (empty c56)
    (at c56 y)

    (compat c56 o56)

    ;; order o57 ('m1', 'p1', 300, 312)
    (order_mine o57 m1)
    (order_port o57 p1)
    (not-delivered o57)

    ;; consist c57
    (available c57)
    (empty c57)
    (at c57 y)

    (compat c57 o57)

    ;; order o58 ('m1', 'p1', 312, 324)
    (order_mine o58 m1)
    (order_port o58 p1)
    (not-delivered o58)

    ;; consist c58
    (available c58)
    (empty c58)
    (at c58 y)

    (compat c58 o58)

    ;; order o59 ('m1', 'p1', 324, 336)
    (order_mine o59 m1)
    (order_port o59 p1)
    (not-delivered o59)

    ;; consist c59
    (available c59)
    (empty c59)
    (at c59 y)

    (compat c59 o59)

    ;; order o60 ('m1', 'p1', 336, 348)
    (order_mine o60 m1)
    (order_port o60 p1)
    (not-delivered o60)

    ;; consist c60
    (available c60)
    (empty c60)
    (at c60 y)

    (compat c60 o60)

    ;; order o61 ('m1', 'p1', 348, 360)
    (order_mine o61 m1)
    (order_port o61 p1)
    (not-delivered o61)

    ;; consist c61
    (available c61)
    (empty c61)
    (at c61 y)

    (compat c61 o61)

    ;; order o62 ('m1', 'p1', 360, 372)
    (order_mine o62 m1)
    (order_port o62 p1)
    (not-delivered o62)

    ;; consist c62
    (available c62)
    (empty c62)
    (at c62 y)

    (compat c62 o62)

    ;; order o63 ('m1', 'p1', 372, 384)
    (order_mine o63 m1)
    (order_port o63 p1)
    (not-delivered o63)

    ;; consist c63
    (available c63)
    (empty c63)
    (at c63 y)

    (compat c63 o63)

    ;; order o64 ('m2', 'p2', 0, 12)
    (order_mine o64 m2)
    (order_port o64 p2)
    (not-delivered o64)

    ;; consist c64
    (available c64)
    (empty c64)
    (at c64 y)

    (compat c64 o64)

    ;; order o65 ('m2', 'p2', 12, 24)
    (order_mine o65 m2)
    (order_port o65 p2)
    (not-delivered o65)

    ;; consist c65
    (available c65)
    (empty c65)
    (at c65 y)

    (compat c65 o65)

    ;; order o66 ('m2', 'p2', 24, 36)
    (order_mine o66 m2)
    (order_port o66 p2)
    (not-delivered o66)

    ;; consist c66
    (available c66)
    (empty c66)
    (at c66 y)

    (compat c66 o66)

    ;; order o67 ('m2', 'p2', 36, 48)
    (order_mine o67 m2)
    (order_port o67 p2)
    (not-delivered o67)

    ;; consist c67
    (available c67)
    (empty c67)
    (at c67 y)

    (compat c67 o67)

    ;; order o68 ('m2', 'p2', 48, 60)
    (order_mine o68 m2)
    (order_port o68 p2)
    (not-delivered o68)

    ;; consist c68
    (available c68)
    (empty c68)
    (at c68 y)

    (compat c68 o68)

    ;; order o69 ('m2', 'p2', 60, 72)
    (order_mine o69 m2)
    (order_port o69 p2)
    (not-delivered o69)

    ;; consist c69
    (available c69)
    (empty c69)
    (at c69 y)

    (compat c69 o69)

    ;; order o70 ('m2', 'p2', 72, 84)
    (order_mine o70 m2)
    (order_port o70 p2)
    (not-delivered o70)

    ;; consist c70
    (available c70)
    (empty c70)
    (at c70 y)

    (compat c70 o70)

    ;; order o71 ('m2', 'p2', 84, 96)
    (order_mine o71 m2)
    (order_port o71 p2)
    (not-delivered o71)

    ;; consist c71
    (available c71)
    (empty c71)
    (at c71 y)

    (compat c71 o71)

    ;; order o72 ('m2', 'p2', 96, 108)
    (order_mine o72 m2)
    (order_port o72 p2)
    (not-delivered o72)

    ;; consist c72
    (available c72)
    (empty c72)
    (at c72 y)

    (compat c72 o72)

    ;; order o73 ('m2', 'p2', 108, 120)
    (order_mine o73 m2)
    (order_port o73 p2)
    (not-delivered o73)

    ;; consist c73
    (available c73)
    (empty c73)
    (at c73 y)

    (compat c73 o73)

    ;; order o74 ('m2', 'p2', 120, 132)
    (order_mine o74 m2)
    (order_port o74 p2)
    (not-delivered o74)

    ;; consist c74
    (available c74)
    (empty c74)
    (at c74 y)

    (compat c74 o74)

    ;; order o75 ('m2', 'p2', 132, 144)
    (order_mine o75 m2)
    (order_port o75 p2)
    (not-delivered o75)

    ;; consist c75
    (available c75)
    (empty c75)
    (at c75 y)

    (compat c75 o75)

    ;; order o76 ('m2', 'p2', 144, 156)
    (order_mine o76 m2)
    (order_port o76 p2)
    (not-delivered o76)

    ;; consist c76
    (available c76)
    (empty c76)
    (at c76 y)

    (compat c76 o76)

    ;; order o77 ('m2', 'p2', 156, 168)
    (order_mine o77 m2)
    (order_port o77 p2)
    (not-delivered o77)

    ;; consist c77
    (available c77)
    (empty c77)
    (at c77 y)

    (compat c77 o77)

    ;; order o78 ('m2', 'p2', 168, 180)
    (order_mine o78 m2)
    (order_port o78 p2)
    (not-delivered o78)

    ;; consist c78
    (available c78)
    (empty c78)
    (at c78 y)

    (compat c78 o78)

    ;; order o79 ('m2', 'p2', 180, 192)
    (order_mine o79 m2)
    (order_port o79 p2)
    (not-delivered o79)

    ;; consist c79
    (available c79)
    (empty c79)
    (at c79 y)

    (compat c79 o79)

    ;; order o80 ('m2', 'p2', 192, 204)
    (order_mine o80 m2)
    (order_port o80 p2)
    (not-delivered o80)

    ;; consist c80
    (available c80)
    (empty c80)
    (at c80 y)

    (compat c80 o80)

    ;; order o81 ('m2', 'p2', 204, 216)
    (order_mine o81 m2)
    (order_port o81 p2)
    (not-delivered o81)

    ;; consist c81
    (available c81)
    (empty c81)
    (at c81 y)

    (compat c81 o81)

    ;; order o82 ('m2', 'p2', 216, 228)
    (order_mine o82 m2)
    (order_port o82 p2)
    (not-delivered o82)

    ;; consist c82
    (available c82)
    (empty c82)
    (at c82 y)

    (compat c82 o82)

    ;; order o83 ('m2', 'p2', 228, 240)
    (order_mine o83 m2)
    (order_port o83 p2)
    (not-delivered o83)

    ;; consist c83
    (available c83)
    (empty c83)
    (at c83 y)

    (compat c83 o83)

    ;; order o84 ('m2', 'p2', 240, 252)
    (order_mine o84 m2)
    (order_port o84 p2)
    (not-delivered o84)

    ;; consist c84
    (available c84)
    (empty c84)
    (at c84 y)

    (compat c84 o84)

    ;; order o85 ('m2', 'p2', 252, 264)
    (order_mine o85 m2)
    (order_port o85 p2)
    (not-delivered o85)

    ;; consist c85
    (available c85)
    (empty c85)
    (at c85 y)

    (compat c85 o85)

    ;; order o86 ('m2', 'p2', 264, 276)
    (order_mine o86 m2)
    (order_port o86 p2)
    (not-delivered o86)

    ;; consist c86
    (available c86)
    (empty c86)
    (at c86 y)

    (compat c86 o86)

    ;; order o87 ('m2', 'p2', 276, 288)
    (order_mine o87 m2)
    (order_port o87 p2)
    (not-delivered o87)

    ;; consist c87
    (available c87)
    (empty c87)
    (at c87 y)

    (compat c87 o87)

    ;; order o88 ('m2', 'p2', 288, 300)
    (order_mine o88 m2)
    (order_port o88 p2)
    (not-delivered o88)

    ;; consist c88
    (available c88)
    (empty c88)
    (at c88 y)

    (compat c88 o88)

    ;; order o89 ('m2', 'p2', 300, 312)
    (order_mine o89 m2)
    (order_port o89 p2)
    (not-delivered o89)

    ;; consist c89
    (available c89)
    (empty c89)
    (at c89 y)

    (compat c89 o89)

    ;; order o90 ('m2', 'p2', 312, 324)
    (order_mine o90 m2)
    (order_port o90 p2)
    (not-delivered o90)

    ;; consist c90
    (available c90)
    (empty c90)
    (at c90 y)

    (compat c90 o90)

    ;; order o91 ('m2', 'p2', 324, 336)
    (order_mine o91 m2)
    (order_port o91 p2)
    (not-delivered o91)

    ;; consist c91
    (available c91)
    (empty c91)
    (at c91 y)

    (compat c91 o91)

    ;; order o92 ('m2', 'p2', 336, 348)
    (order_mine o92 m2)
    (order_port o92 p2)
    (not-delivered o92)

    ;; consist c92
    (available c92)
    (empty c92)
    (at c92 y)

    (compat c92 o92)

    ;; order o93 ('m2', 'p2', 348, 360)
    (order_mine o93 m2)
    (order_port o93 p2)
    (not-delivered o93)

    ;; consist c93
    (available c93)
    (empty c93)
    (at c93 y)

    (compat c93 o93)

    ;; order o94 ('m2', 'p2', 360, 372)
    (order_mine o94 m2)
    (order_port o94 p2)
    (not-delivered o94)

    ;; consist c94
    (available c94)
    (empty c94)
    (at c94 y)

    (compat c94 o94)

    ;; order o95 ('m2', 'p2', 372, 384)
    (order_mine o95 m2)
    (order_port o95 p2)
    (not-delivered o95)

    ;; consist c95
    (available c95)
    (empty c95)
    (at c95 y)

    (compat c95 o95)

    ;; order o96 ('m2', 'p1', 0, 12)
    (order_mine o96 m2)
    (order_port o96 p1)
    (not-delivered o96)

    ;; consist c96
    (available c96)
    (empty c96)
    (at c96 y)

    (compat c96 o96)

    ;; order o97 ('m2', 'p1', 12, 24)
    (order_mine o97 m2)
    (order_port o97 p1)
    (not-delivered o97)

    ;; consist c97
    (available c97)
    (empty c97)
    (at c97 y)

    (compat c97 o97)

    ;; order o98 ('m2', 'p1', 24, 36)
    (order_mine o98 m2)
    (order_port o98 p1)
    (not-delivered o98)

    ;; consist c98
    (available c98)
    (empty c98)
    (at c98 y)

    (compat c98 o98)

    ;; order o99 ('m2', 'p1', 36, 48)
    (order_mine o99 m2)
    (order_port o99 p1)
    (not-delivered o99)

    ;; consist c99
    (available c99)
    (empty c99)
    (at c99 y)

    (compat c99 o99)

    ;; order o100 ('m2', 'p1', 48, 60)
    (order_mine o100 m2)
    (order_port o100 p1)
    (not-delivered o100)

    ;; consist c100
    (available c100)
    (empty c100)
    (at c100 y)

    (compat c100 o100)

    ;; order o101 ('m2', 'p1', 60, 72)
    (order_mine o101 m2)
    (order_port o101 p1)
    (not-delivered o101)

    ;; consist c101
    (available c101)
    (empty c101)
    (at c101 y)

    (compat c101 o101)

    ;; order o102 ('m2', 'p1', 72, 84)
    (order_mine o102 m2)
    (order_port o102 p1)
    (not-delivered o102)

    ;; consist c102
    (available c102)
    (empty c102)
    (at c102 y)

    (compat c102 o102)

    ;; order o103 ('m2', 'p1', 84, 96)
    (order_mine o103 m2)
    (order_port o103 p1)
    (not-delivered o103)

    ;; consist c103
    (available c103)
    (empty c103)
    (at c103 y)

    (compat c103 o103)

    ;; order o104 ('m2', 'p1', 96, 108)
    (order_mine o104 m2)
    (order_port o104 p1)
    (not-delivered o104)

    ;; consist c104
    (available c104)
    (empty c104)
    (at c104 y)

    (compat c104 o104)

    ;; order o105 ('m2', 'p1', 108, 120)
    (order_mine o105 m2)
    (order_port o105 p1)
    (not-delivered o105)

    ;; consist c105
    (available c105)
    (empty c105)
    (at c105 y)

    (compat c105 o105)

    ;; order o106 ('m2', 'p1', 120, 132)
    (order_mine o106 m2)
    (order_port o106 p1)
    (not-delivered o106)

    ;; consist c106
    (available c106)
    (empty c106)
    (at c106 y)

    (compat c106 o106)

    ;; order o107 ('m2', 'p1', 132, 144)
    (order_mine o107 m2)
    (order_port o107 p1)
    (not-delivered o107)

    ;; consist c107
    (available c107)
    (empty c107)
    (at c107 y)

    (compat c107 o107)

    ;; order o108 ('m2', 'p1', 144, 156)
    (order_mine o108 m2)
    (order_port o108 p1)
    (not-delivered o108)

    ;; consist c108
    (available c108)
    (empty c108)
    (at c108 y)

    (compat c108 o108)

    ;; order o109 ('m2', 'p1', 156, 168)
    (order_mine o109 m2)
    (order_port o109 p1)
    (not-delivered o109)

    ;; consist c109
    (available c109)
    (empty c109)
    (at c109 y)

    (compat c109 o109)

    ;; order o110 ('m2', 'p1', 168, 180)
    (order_mine o110 m2)
    (order_port o110 p1)
    (not-delivered o110)

    ;; consist c110
    (available c110)
    (empty c110)
    (at c110 y)

    (compat c110 o110)

    ;; order o111 ('m2', 'p1', 180, 192)
    (order_mine o111 m2)
    (order_port o111 p1)
    (not-delivered o111)

    ;; consist c111
    (available c111)
    (empty c111)
    (at c111 y)

    (compat c111 o111)

    ;; order o112 ('m2', 'p1', 192, 204)
    (order_mine o112 m2)
    (order_port o112 p1)
    (not-delivered o112)

    ;; consist c112
    (available c112)
    (empty c112)
    (at c112 y)

    (compat c112 o112)

    ;; order o113 ('m2', 'p1', 204, 216)
    (order_mine o113 m2)
    (order_port o113 p1)
    (not-delivered o113)

    ;; consist c113
    (available c113)
    (empty c113)
    (at c113 y)

    (compat c113 o113)

    ;; order o114 ('m2', 'p1', 216, 228)
    (order_mine o114 m2)
    (order_port o114 p1)
    (not-delivered o114)

    ;; consist c114
    (available c114)
    (empty c114)
    (at c114 y)

    (compat c114 o114)

    ;; order o115 ('m2', 'p1', 228, 240)
    (order_mine o115 m2)
    (order_port o115 p1)
    (not-delivered o115)

    ;; consist c115
    (available c115)
    (empty c115)
    (at c115 y)

    (compat c115 o115)

    ;; order o116 ('m2', 'p1', 240, 252)
    (order_mine o116 m2)
    (order_port o116 p1)
    (not-delivered o116)

    ;; consist c116
    (available c116)
    (empty c116)
    (at c116 y)

    (compat c116 o116)

    ;; order o117 ('m2', 'p1', 252, 264)
    (order_mine o117 m2)
    (order_port o117 p1)
    (not-delivered o117)

    ;; consist c117
    (available c117)
    (empty c117)
    (at c117 y)

    (compat c117 o117)

    ;; order o118 ('m2', 'p1', 264, 276)
    (order_mine o118 m2)
    (order_port o118 p1)
    (not-delivered o118)

    ;; consist c118
    (available c118)
    (empty c118)
    (at c118 y)

    (compat c118 o118)

    ;; order o119 ('m2', 'p1', 276, 288)
    (order_mine o119 m2)
    (order_port o119 p1)
    (not-delivered o119)

    ;; consist c119
    (available c119)
    (empty c119)
    (at c119 y)

    (compat c119 o119)

    ;; order o120 ('m2', 'p1', 288, 300)
    (order_mine o120 m2)
    (order_port o120 p1)
    (not-delivered o120)

    ;; consist c120
    (available c120)
    (empty c120)
    (at c120 y)

    (compat c120 o120)

    ;; order o121 ('m2', 'p1', 300, 312)
    (order_mine o121 m2)
    (order_port o121 p1)
    (not-delivered o121)

    ;; consist c121
    (available c121)
    (empty c121)
    (at c121 y)

    (compat c121 o121)

    ;; order o122 ('m2', 'p1', 312, 324)
    (order_mine o122 m2)
    (order_port o122 p1)
    (not-delivered o122)

    ;; consist c122
    (available c122)
    (empty c122)
    (at c122 y)

    (compat c122 o122)

    ;; order o123 ('m2', 'p1', 324, 336)
    (order_mine o123 m2)
    (order_port o123 p1)
    (not-delivered o123)

    ;; consist c123
    (available c123)
    (empty c123)
    (at c123 y)

    (compat c123 o123)

    ;; order o124 ('m2', 'p1', 336, 348)
    (order_mine o124 m2)
    (order_port o124 p1)
    (not-delivered o124)

    ;; consist c124
    (available c124)
    (empty c124)
    (at c124 y)

    (compat c124 o124)

    ;; order o125 ('m2', 'p1', 348, 360)
    (order_mine o125 m2)
    (order_port o125 p1)
    (not-delivered o125)

    ;; consist c125
    (available c125)
    (empty c125)
    (at c125 y)

    (compat c125 o125)

    ;; order o126 ('m2', 'p1', 360, 372)
    (order_mine o126 m2)
    (order_port o126 p1)
    (not-delivered o126)

    ;; consist c126
    (available c126)
    (empty c126)
    (at c126 y)

    (compat c126 o126)

    ;; order o127 ('m2', 'p1', 372, 384)
    (order_mine o127 m2)
    (order_port o127 p1)
    (not-delivered o127)

    ;; consist c127
    (available c127)
    (empty c127)
    (at c127 y)

    (compat c127 o127)

)
(:goal
(and
  (delivered o0)
  (at c0 y)
  (delivered o1)
  (at c1 y)
  (delivered o2)
  (at c2 y)
  (delivered o3)
  (at c3 y)
  (delivered o4)
  (at c4 y)
  (delivered o5)
  (at c5 y)
  (delivered o6)
  (at c6 y)
  (delivered o7)
  (at c7 y)
  (delivered o8)
  (at c8 y)
  (delivered o9)
  (at c9 y)
  (delivered o10)
  (at c10 y)
  (delivered o11)
  (at c11 y)
  (delivered o12)
  (at c12 y)
  (delivered o13)
  (at c13 y)
  (delivered o14)
  (at c14 y)
  (delivered o15)
  (at c15 y)
  (delivered o16)
  (at c16 y)
  (delivered o17)
  (at c17 y)
  (delivered o18)
  (at c18 y)
  (delivered o19)
  (at c19 y)
  (delivered o20)
  (at c20 y)
  (delivered o21)
  (at c21 y)
  (delivered o22)
  (at c22 y)
  (delivered o23)
  (at c23 y)
  (delivered o24)
  (at c24 y)
  (delivered o25)
  (at c25 y)
  (delivered o26)
  (at c26 y)
  (delivered o27)
  (at c27 y)
  (delivered o28)
  (at c28 y)
  (delivered o29)
  (at c29 y)
  (delivered o30)
  (at c30 y)
  (delivered o31)
  (at c31 y)
  (delivered o32)
  (at c32 y)
  (delivered o33)
  (at c33 y)
  (delivered o34)
  (at c34 y)
  (delivered o35)
  (at c35 y)
  (delivered o36)
  (at c36 y)
  (delivered o37)
  (at c37 y)
  (delivered o38)
  (at c38 y)
  (delivered o39)
  (at c39 y)
  (delivered o40)
  (at c40 y)
  (delivered o41)
  (at c41 y)
  (delivered o42)
  (at c42 y)
  (delivered o43)
  (at c43 y)
  (delivered o44)
  (at c44 y)
  (delivered o45)
  (at c45 y)
  (delivered o46)
  (at c46 y)
  (delivered o47)
  (at c47 y)
  (delivered o48)
  (at c48 y)
  (delivered o49)
  (at c49 y)
  (delivered o50)
  (at c50 y)
  (delivered o51)
  (at c51 y)
  (delivered o52)
  (at c52 y)
  (delivered o53)
  (at c53 y)
  (delivered o54)
  (at c54 y)
  (delivered o55)
  (at c55 y)
  (delivered o56)
  (at c56 y)
  (delivered o57)
  (at c57 y)
  (delivered o58)
  (at c58 y)
  (delivered o59)
  (at c59 y)
  (delivered o60)
  (at c60 y)
  (delivered o61)
  (at c61 y)
  (delivered o62)
  (at c62 y)
  (delivered o63)
  (at c63 y)
  (delivered o64)
  (at c64 y)
  (delivered o65)
  (at c65 y)
  (delivered o66)
  (at c66 y)
  (delivered o67)
  (at c67 y)
  (delivered o68)
  (at c68 y)
  (delivered o69)
  (at c69 y)
  (delivered o70)
  (at c70 y)
  (delivered o71)
  (at c71 y)
  (delivered o72)
  (at c72 y)
  (delivered o73)
  (at c73 y)
  (delivered o74)
  (at c74 y)
  (delivered o75)
  (at c75 y)
  (delivered o76)
  (at c76 y)
  (delivered o77)
  (at c77 y)
  (delivered o78)
  (at c78 y)
  (delivered o79)
  (at c79 y)
  (delivered o80)
  (at c80 y)
  (delivered o81)
  (at c81 y)
  (delivered o82)
  (at c82 y)
  (delivered o83)
  (at c83 y)
  (delivered o84)
  (at c84 y)
  (delivered o85)
  (at c85 y)
  (delivered o86)
  (at c86 y)
  (delivered o87)
  (at c87 y)
  (delivered o88)
  (at c88 y)
  (delivered o89)
  (at c89 y)
  (delivered o90)
  (at c90 y)
  (delivered o91)
  (at c91 y)
  (delivered o92)
  (at c92 y)
  (delivered o93)
  (at c93 y)
  (delivered o94)
  (at c94 y)
  (delivered o95)
  (at c95 y)
  (delivered o96)
  (at c96 y)
  (delivered o97)
  (at c97 y)
  (delivered o98)
  (at c98 y)
  (delivered o99)
  (at c99 y)
  (delivered o100)
  (at c100 y)
  (delivered o101)
  (at c101 y)
  (delivered o102)
  (at c102 y)
  (delivered o103)
  (at c103 y)
  (delivered o104)
  (at c104 y)
  (delivered o105)
  (at c105 y)
  (delivered o106)
  (at c106 y)
  (delivered o107)
  (at c107 y)
  (delivered o108)
  (at c108 y)
  (delivered o109)
  (at c109 y)
  (delivered o110)
  (at c110 y)
  (delivered o111)
  (at c111 y)
  (delivered o112)
  (at c112 y)
  (delivered o113)
  (at c113 y)
  (delivered o114)
  (at c114 y)
  (delivered o115)
  (at c115 y)
  (delivered o116)
  (at c116 y)
  (delivered o117)
  (at c117 y)
  (delivered o118)
  (at c118 y)
  (delivered o119)
  (at c119 y)
  (delivered o120)
  (at c120 y)
  (delivered o121)
  (at c121 y)
  (delivered o122)
  (at c122 y)
  (delivered o123)
  (at c123 y)
  (delivered o124)
  (at c124 y)
  (delivered o125)
  (at c125 y)
  (delivered o126)
  (at c126 y)
  (delivered o127)
  (at c127 y)
))
(:metric minimize (total-time)))

