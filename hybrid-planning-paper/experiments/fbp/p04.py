import math
import networkx as nx
import itertools as it
import time

import pymas
import pymas.test.bulkfreight as bfrs

bfrs.BulkFreight.orders = [('m1', 'p2', 0, 12), ('m1', 'p2', 12, 24), ('m1', 'p2', 24, 36), ('m1', 'p2', 36, 48), ('m1', 'p2', 48, 60), ('m1', 'p2', 60, 72), ('m1', 'p2', 72, 84), ('m1', 'p2', 84, 96), ('m1', 'p1', 0, 12), ('m1', 'p1', 12, 24), ('m1', 'p1', 24, 36), ('m1', 'p1', 36, 48), ('m1', 'p1', 48, 60), ('m1', 'p1', 60, 72), ('m1', 'p1', 72, 84), ('m1', 'p1', 84, 96), ('m2', 'p2', 0, 12), ('m2', 'p2', 12, 24), ('m2', 'p2', 24, 36), ('m2', 'p2', 36, 48), ('m2', 'p2', 48, 60), ('m2', 'p2', 60, 72), ('m2', 'p2', 72, 84), ('m2', 'p2', 84, 96), ('m2', 'p1', 0, 12), ('m2', 'p1', 12, 24), ('m2', 'p1', 24, 36), ('m2', 'p1', 36, 48), ('m2', 'p1', 48, 60), ('m2', 'p1', 60, 72), ('m2', 'p1', 72, 84), ('m2', 'p1', 84, 96)]
bfrs.BulkFreight.graph = nx.from_edgelist([('p2', 'y'), ('p1', 'y'), ('d', 'y'), ('d', 'x'), ('d', 'm1'), ('d', 'm2'), ('y', 'x')])

start_t = time.time()
init = list(bfrs.solve(nfrags=int(2*math.log(32)+2)))
selector = pymas.FragSelector(init, bfrs.solve)
print "#frags:", len(selector.fragments)
initial_cost, selection = selector.heuristic_select()
print "Delivered:", len(selection)
print "Time:", time.time() - start_t
exes = [{sit.time(): act
         for (act, sit) in frag.sit.history}
        for frag in selection]
jexe = [(t, [exes[i].get(t) for i in xrange(len(selection))])
        for t in xrange(max(max(ex) for ex in exes))]
print "\n".join(str(e) for e in jexe)

