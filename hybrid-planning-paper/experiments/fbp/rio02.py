import math
import networkx as nx
import itertools as it
import time

import pymas
import pymas.test.bulkfreight as bfrs

bfrs.BulkFreight.orders = [('m_tp', 'p_dampier', 0, 45), ('m_tp', 'p_dampier', 45, 90), ('m_tp', 'p_dampier', 90, 135), ('m_tp', 'p_dampier', 135, 180), ('m_tp', 'p_capelambert', 0, 45), ('m_tp', 'p_capelambert', 45, 90), ('m_tp', 'p_capelambert', 90, 135), ('m_tp', 'p_capelambert', 135, 180), ('m_mj', 'p_dampier', 0, 45), ('m_mj', 'p_dampier', 45, 90), ('m_mj', 'p_dampier', 90, 135), ('m_mj', 'p_dampier', 135, 180), ('m_mj', 'p_capelambert', 0, 45), ('m_mj', 'p_capelambert', 45, 90), ('m_mj', 'p_capelambert', 90, 135), ('m_mj', 'p_capelambert', 135, 180), ('m_mnd', 'p_dampier', 0, 45), ('m_mnd', 'p_dampier', 45, 90), ('m_mnd', 'p_dampier', 90, 135), ('m_mnd', 'p_dampier', 135, 180), ('m_mnd', 'p_capelambert', 0, 45), ('m_mnd', 'p_capelambert', 45, 90), ('m_mnd', 'p_capelambert', 90, 135), ('m_mnd', 'p_capelambert', 135, 180), ('m_ya', 'p_dampier', 0, 45), ('m_ya', 'p_dampier', 45, 90), ('m_ya', 'p_dampier', 90, 135), ('m_ya', 'p_dampier', 135, 180), ('m_ya', 'p_capelambert', 0, 45), ('m_ya', 'p_capelambert', 45, 90), ('m_ya', 'p_capelambert', 90, 135), ('m_ya', 'p_capelambert', 135, 180), ('m_b2', 'p_dampier', 0, 45), ('m_b2', 'p_dampier', 45, 90), ('m_b2', 'p_dampier', 90, 135), ('m_b2', 'p_dampier', 135, 180), ('m_b2', 'p_capelambert', 0, 45), ('m_b2', 'p_capelambert', 45, 90), ('m_b2', 'p_capelambert', 90, 135), ('m_b2', 'p_capelambert', 135, 180), ('m_ma', 'p_dampier', 0, 45), ('m_ma', 'p_dampier', 45, 90), ('m_ma', 'p_dampier', 90, 135), ('m_ma', 'p_dampier', 135, 180), ('m_ma', 'p_capelambert', 0, 45), ('m_ma', 'p_capelambert', 45, 90), ('m_ma', 'p_capelambert', 90, 135), ('m_ma', 'p_capelambert', 135, 180), ('m_b4', 'p_dampier', 0, 45), ('m_b4', 'p_dampier', 45, 90), ('m_b4', 'p_dampier', 90, 135), ('m_b4', 'p_dampier', 135, 180), ('m_b4', 'p_capelambert', 0, 45), ('m_b4', 'p_capelambert', 45, 90), ('m_b4', 'p_capelambert', 90, 135), ('m_b4', 'p_capelambert', 135, 180), ('m_yac', 'p_dampier', 0, 45), ('m_yac', 'p_dampier', 45, 90), ('m_yac', 'p_dampier', 90, 135), ('m_yac', 'p_dampier', 135, 180), ('m_yac', 'p_capelambert', 0, 45), ('m_yac', 'p_capelambert', 45, 90), ('m_yac', 'p_capelambert', 90, 135), ('m_yac', 'p_capelambert', 135, 180), ('m_wa', 'p_dampier', 0, 45), ('m_wa', 'p_dampier', 45, 90), ('m_wa', 'p_dampier', 90, 135), ('m_wa', 'p_dampier', 135, 180), ('m_wa', 'p_capelambert', 0, 45), ('m_wa', 'p_capelambert', 45, 90), ('m_wa', 'p_capelambert', 90, 135), ('m_wa', 'p_capelambert', 135, 180), ('m_hd', 'p_dampier', 0, 45), ('m_hd', 'p_dampier', 45, 90), ('m_hd', 'p_dampier', 90, 135), ('m_hd', 'p_dampier', 135, 180), ('m_hd', 'p_capelambert', 0, 45), ('m_hd', 'p_capelambert', 45, 90), ('m_hd', 'p_capelambert', 90, 135), ('m_hd', 'p_capelambert', 135, 180), ('m_pbd', 'p_dampier', 0, 45), ('m_pbd', 'p_dampier', 45, 90), ('m_pbd', 'p_dampier', 90, 135), ('m_pbd', 'p_dampier', 135, 180), ('m_pbd', 'p_capelambert', 0, 45), ('m_pbd', 'p_capelambert', 45, 90), ('m_pbd', 'p_capelambert', 90, 135), ('m_pbd', 'p_capelambert', 135, 180)]
bfrs.BulkFreight.graph = nx.from_edgelist([('m_tp', 'j_b'), ('m_tp', 'm_pbd'), ('m_mj', 'y'), ('m_mj', 'm_ma'), ('m_mnd', 'j_b'), ('m_mnd', 'm_ya'), ('m_ya', 'm_yac'), ('m_b2', 'j_b'), ('m_b2', 'm_b4'), ('j_b', 'y'), ('j_b', 'j_wa'), ('m_wa', 'j_wa'), ('p_dampier', 'y'), ('m_hd', 'j_wa'), ('y', 'p_capelambert')])

start_t = time.time()
init = list(bfrs.solve(nfrags=88/2+1))
selector = pymas.FragSelector(init, bfrs.solve)
print "#frags:", len(selector.fragments)
initial_cost, selection = selector.heuristic_select()
print "Delivered:", len(selection)
print "Time:", time.time() - start_t
exes = [{sit.time(): act
         for (act, sit) in frag.sit.history}
        for frag in selection]
jexe = [(t, [exes[i].get(t) for i in xrange(len(selection))])
        for t in xrange(max(max(ex) for ex in exes))]
print "\n".join(str(e) for e in jexe)

