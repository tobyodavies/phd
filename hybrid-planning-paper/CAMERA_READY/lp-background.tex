\paragraph{Linear and Integer Programming.}

An Integer Program (IP) consists of a vector $\vect{x}$ of binary decision
variables,\footnote{In general decision variables can be integer but binary
  decision variables suffice for our purposes.} 
an objective linear expression and a set of linear inequalities.  
We will refer to these inequalities
as ``resources'' throughout this paper, and each decision represented
by the variable $x_i$ can be thought of as using $u_{r,i}$ units of
some resources $r \in R$, each of which has an availability of $a_r$.

A general form, assuming a set $R$ of inequalities, where $c_i$,
$u_{r,i}$ and $a_r$ are constants, is:
%
%\begin{center}
%\begin{tabular}{lll}
%   Minimize:~~~~~~
%&  $\sum c_i \cdot x_i$
%\\ Subject To: 
%&  $\sum_{x_i \in \vect{x}} u_{r,i} \cdot x_i \quad \le a_r$ & $\forall r \in R$
%\end{tabular}
%\end{center}
%
% 
\begin{align*}
\text{Minimize:}&\qquad \sum c_i \cdot x_i                & \\
\text{Subject To:}&\qquad \sum u_{r,i} \cdot x_i \quad \le a_r & \forall r \in R 
\end{align*}
%
Finding the optimal solution to an integer program is NP-hard, however
the linear program (LP), constructed by replacing the integrality
constraints $x_i \in \{0, 1\}$ with a continuous equivalent $x_i
\in [0,1]$, can be optimised in polynomial time.
This LP is known as the linear relaxation of the IP.

A model of this form where some variables are continuous and some are
integral is called a Mixed Integer Program (MIP). To limit confusion,
we will denote binary variables $x_i$ and continuous variables $v_i$,
and assume all $x_i \in \{0,1\}$ and $v_i \in [0,1]$ throughout this 
paper.

The (M)IP is solved using a ``branch-and-bound'' search strategy
where some $x_i$ which is fractional in the LP optimum is selected at
each node and two children are enqueued with additional constraints
$x_i = 1$ in one branch and $x_i = 0$ in the other.  Heuristically
constructed integer solutions provide an upper bound, whereas the
relaxations provide a lower bound.

Solving the linear relaxation implicitly also optimises the so-called
dual problem.  Intuitively the dual problem is another linear program
that seeks to minimize the unit price of each resource in $R$, subject
to the constraint that it must under-estimate the optimal objective of
the primal.  
We use $\lpdual_r$ to denote this so-called ``dual-price'' of resource $r$.
An estimate of the impact of consuming $u$ additional units of
resource $r$ on the current objective can then be computed by
multiplying usage by the dual price: $u \cdot \lpdual_r$.

Dual prices allow us to quickly identify bottlenecks in a
system. They give us an upper bound on just how far out of the way we
should consider going to avoid them.  This leads to the important
notion of reduced cost: an estimate of how much a decision can
improve an incumbent solution. 
The reduced cost is the
decision's intrinsic cost $c_i$, less the total dual price of
the resources required to make this decision.

Given an incumbent dual solution $\vect{\lpdual}$,
a decision variable $x_i$ has a reduced-cost $\gamma(i, \vect{\lpdual})$,
defined as:

\[ \gamma(i, \vect{\lpdual}) = c_i - \sum\limits_{r\in R} \lpdual_r \cdot u_{r,i} \]
%
This is guaranteed to be a locally optimistic estimate, 
so that, in order to improve an incumbent solution, we only need to
consider decisions $x_i$ with $\gamma(i, \vect{\lpdual}) < 0$.
Due to the convexity of
linear programs, repeatedly improving an incumbent solution is
sufficient to eventually reach the global optimum, and the
non-existence of any $x_i$ with negative reduced cost is 
sufficient to prove global optimality.

\paragraph{Column Generation and Branch-and-Price.}

Most real-world integer programs have a very small number of non-zero $x_i$.
This property, combined with the need only to consider
negative reduced-cost decision variables, allows us to solve problems with
otherwise intractably large decision vectors using a process known as
``column generation'' \cite{Desaulniers2005}.
The name reflects the fact that the new decision variable
is an additional column in the matrix representation of the
constraints.
%\todo[inline]{+more original citation}

Column generation starts with a restricted set of decision variables
obtained by some problem-dependent method to yield a linear feasible
initial solution.
With such a solution we can compute duals for this restricted master
problem (RMP) and use reduced-cost reasoning to prune huge areas of the 
column space.

%\pjs{Not clear purpose of next para!}
Incomplete and suboptimal methods of constructing integer feasible
solutions are referred to in the Operations Research
literature simply as ``Heuristics''. To avoid ambiguity we 
refer to them as ``MIP heuristics''.
These are essential for both finding a feasible initial solution, and
providing a worst bound on the solution during the branch-and-bound
search process.
They are analogues to fast but incomplete search algorithms in
planning such as enforced hill climbing.

Column generation then proceeds by repeatedly solving one or more
``pricing problems'' to generate new decision variables with negative
reduced cost and re-solving the RMP to generate new dual
prices.  Iterating this process until no more negative reduced cost
columns exist is guaranteed to reach a fixed point with the same
objective value as the much larger original linear program.  We can
then use a similar ``branch-and-bound'' approach as in integer
programming to reach an integer optimum by performing column
generation at each node in the search tree. This process is known as
``branch-and-price''.

%% CAMERA READY EDIT
%%
%% A technical point: the authors describe column generation well but then are ambiguous as to branch-and-price, describing it as a branch-and-bound-like procedure. A critical point that must be made clear: is the pricing problem re-solved at each node in the tree search? If not, then this is not branch-and-price and more importantly an optimal solution cannot be guaranteed. The technique of generating columns in the root node and then running B&B without generation of further columns is something that is done in mathematical programming to find high quality feasible solution for very large, hard problems. So this distinction exists, is important, and should be made clear.


Branching rules used in practical branch-and-price solvers are often
more complex than in IP branch-and-bound and are sometimes problem
dependent.  The branch $x_i = 0$ does not often partition the
search-space evenly: there are usually exponentially many ways to use
the resources consumed by $x_i$. Additionally, disallowing the
re-generation of the same specific solution $x_i$ by the pricing
problem is not possible with an IP-based pricing-problem without
fundamentally changing the structure of the pricing-problem.

Consequently, effectiveness of a branching strategy must be evaluated
in terms of how effectively the dual-price of the branching constraint
can be integrated into the pricing problem.
This is another motivation for our hybrid IP/Planning approach, as
using a planning-based pricing problem allows us to disallow specific
solutions and handle non-linear, time-dependent costs and constraints.

A concrete example of branch-and-price is presented in the \nameref{fbp}
section below.


\paragraph{Big-M Penalty Methods.}

We noted earlier that to start column generation, an initial (linear)
feasible solution is required. There is no guarantee that
finding such a feasible solution is trivial.
Indeed for classical planning problems, finding a feasible solution is
PSPACE-complete in general. No work we are aware of determines the
complexity of computing a linear relaxation of a plan.

We avoid this problem by transforming our IP into an MIP where for any
constraint having $a_r < 0$
%\pjs{What:are we assuming all $a_r < 0$, or are we transforming the IP. The result is not an IP but a MIP?}
 we add a new continuous variable
$v_r \in [0, 1]$, representing the degree to which the constraint is
violated, and replace the constraint with:
$\sum u_{r,i} \cdot x_i \le \abs{a_r} \cdot v_r - \abs{a_r}$ and $c_v = M$
where $M$ is a large number.  This guarantees the feasibility of the
trivial solution $x_i = 0$ for all $i$ and all $v_r = 1$.
%
It represents a relaxation of the original IP with the property
that, given sufficiently large $M$, the optimal solution is a feasible
solution to the original problem, iff such a solution exists.  This is
known as a ``penalty method'' or ``soft constraint''.  The process is
similar to the first phase of the simplex algorithm for finding an
initial feasible solution to a linear program when all decision
variables are known in advance.

