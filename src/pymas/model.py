"""
Modeling tools for multi-agent domains and non-deterministic programs
"""


import functools
import operator
import sys
import weakref


from pymas.utils import LinkedList
from pymas.fraggen import ProgramComposerMixin, PerformActions


class Resource(object):
    """
    The interface that the :py:class:`pymas.selector.FragSelector` expects of a `Resource`

    - :py:attr:`Resource.avail` a numeric availability.
      The sum of selected fragments usage of this resource must not exceed this number.
    """
    def __init__(self, avail=1, label=None):
        self.avail = avail
        self.label = label or id(self)

    def __repr__(self):
        return "<{self.__class__.__name__}{self.label}>".format(self=self)


class Fragment(object):
    """
    The interface that the :py:class:`pymas.selector.FragSelector` expects of a `Fragment`

    - :py:attr:`Fragment.resources` a dict-like mapping of :py:class:`Resource` to numeric usage/consumption
    - :py:attr:`Fragment.cost` a numeric cost of executing the fragment
    """

    def __init__(self, cost, res_usage=None):
        self.resources = res_usage if res_usage is not None else {}
        self.cost = cost
        self._hash = reduce(operator.xor,
                            (hash((r, u))
                             for (r, u) in self.resources.iteritems()),
                            int(self.cost))

    def __hash__(self):
        return self._hash

    def __eq__(self, other):
        return (self.cost == other.cost and
                hash(self) == hash(other) and
                self.resources == other.resources)

    def usage(self, res):
        """
        Convenience method for getting resource usage of a fragment
        """
        return self.resources.get(res, 0)


class Var(object):
    """
    An unground variable to be assigned by a :py:class:`VarChoice`
    """
    def __init__(self, vals=(), index=0):
        self.domain = vals
        self.index=index

    def label(self):
        return (self.domain[self.index],
                self.__class__(self.domain, self.index + 1))

    def __nonzero__(self):
        return self.index < len(self.domain)

class Formula(object):
    """
    A regressible formula
    """

    def __init__(self, func, *args, **kwargs):
        self.func = func
        self.args = args
        self.kwargs = kwargs
        self.cache = {} #weakref.WeakKeyDictionary()
        self.calls = 0
        self.hits = 0

    def __call__(self, sit):
        self.calls += 1
        if sit not in self.cache:
            self.cache[sit] = self.func(sit, *self.args, **self.kwargs)
        else:
            self.hits += 1
        return self.cache[sit]

    def __and__(self, other):
        return Formula(lambda s: self(s) and other(s))

    def __or__(self, other):
        return Formula(lambda s: self(s) or other(s))

    def __invert__(self):
        f = Formula(lambda s: not(self(s)))
        return f

    def implies(self, other):
        """
        Construct a formula equivalent to logical implication
        """
        return Formula(lambda s: not(self(s)) or other(s))

    def iff(self, other):
        """
        Construct a formula equivalent to bi-directional logical implication
        """
        return self.implies(other) and other.implies(self)

    def __repr__(self):
        return "%s(%s)" %(self.func.func_name, ', '.join("%r"%a for a in self.args))


class Fluent(object):
    """
    A class of formula

    >>> class ToySit(Situation):
    ...    _resources = {}
    ...    @fluent
    ...    def includes_action(self, act):
    ...        return any(a == act for a in self)
    >>> inc_x = ToySit.includes_action('x')
    >>> s0 = ToySit.nil()
    >>> inc_x(s0)
    False
    >>> s0.do(Action('x'), Action('y')).includes_action(Action('x'))
    True
    """

    def __init__(self, func):
        self.func = func
        self.cache = {}#weakref.WeakValueDictionary()

    def formula(self, *args, **kwargs):
        key = (args, frozenset(kwargs.iteritems()))
        if key not in self.cache:
            formula = Formula(self.func, *args, **kwargs)
            self.cache[key] = formula
        return self.cache[key]

    def __get__(self, sit, kls=None):
        if kls is None or sit is None or isinstance(sit, type):
            return functools.partial(Formula, self.func)

        #@functools.wraps(self.func)
        def wrapped(*args, **kwargs):
            """
            Call `self.func` using dp
            """
            return self.formula(*args, **kwargs)(sit)
        return wrapped

    def __call__(self, sit, *args, **kwargs):
        return self.formula(*args, **kwargs)(sit)


def fluent(func):
    "Decorator for fluent methods"
    return Fluent(func)


class Situation(LinkedList):
    """
    A sitcalc situation
    """

    _resources = None
    equiv = set()

    def __init__(self, head, tail=None):
        super(Situation, self).__init__(head, tail)
        self._usage = None
        self._cost = 0
        self.nexts = {}

    @classmethod
    def s0(cls):
        return cls.nil()

    def do(self, *actions):
        """
        Return the situation resulting from performing `actions`
        if that sequence is possible.
        """
        ret = self
        for act in actions:
            if ret.poss(act):
                if act not in ret.nexts:
                    ret.nexts[act] = ret.cons(act)
                ret = ret.nexts[act]
            else:
                return
        return ret

    @fluent
    def last_action(self, pred=None):
        """
        Return the most recent action satisfying `pred`
        """
        if not pred:
            pred = lambda _: True
        return next((act for act in self if pred(act)), None)

    @fluent
    def poss(self, act):
        """
        Use the actions' precond func to see if
        it thinks it is legal in the current situation
        """
        return act.precond(self)

    @fluent
    def legal(self):
        """
        Holds if all actions since s0 have been `poss`
        in the situation they were executed in
        """
        for act, sit in self.history:
            if not sit.poss(act):
                return False
        return True

    @fluent
    def resource_usage(self):
        """
        Compute all resources used so far in this situation
        """
        if self._usage is None:
            self._cost = 0
            self._usage = {}
            for act, sit in self.history:
                self._cost += act.cost(sit)
                for res, req in act.resources(sit).iteritems():
                    self._usage.setdefault(res, 0)
                    self._usage[res] += req
        return self._usage

    @classmethod
    def actions(cls):
        """
        Return all the actions defined on the class
        """
        for prop in dir(cls):
            attr = getattr(cls, prop)
            if isinstance(attr, type) and issubclass(attr, Action):
                yield attr

    def cost(self, duals=None):
        duals = duals if duals is not None else {}
        res = self.resource_usage()
        return self._cost - sum(u*duals.get(r, 0)
                                for (r, u) in res.iteritems())

    @fluent
    def landmarks(self, duals=None):
        if self:
            act = self.head
            return (.5*int(act.cost(self) < 0) +
                    .5*int(act.cost(self, duals) < 0) +
                    self.tail.landmarks(duals))
        return 0

    def usage(self, res):
        return self.resource_usage().get(res, 0)

    def time(self):
        return len(self)

    def shared(self, *args, **kwargs):
        """
        Create or lookup a resource indexed by the positional arguments
        """
        if args not in self._resources:
            ret = self._resources.get(args, Resource(label=args, **kwargs))
            self._resources[args] = ret
        return self._resources[args]

    def to_fragment(self):
        """
        Convert a situation into a :py:class:`Fragment`
        """
        cost = sum(a.cost(sit) for a, sit in self.history)
        resources = self.resource_usage()
        return Fragment(cost, resources)


class Action(ProgramComposerMixin):
    """
    Base action class
    """
    mincost = 0
    basecost = 1e-6
    natural = False

    def __init__(self, *args):
        self.args = args

    def fraggen(self):
        """
        Convert to a fragment generator,
        for the ProgramComposerMixin
        """
        return PerformActions(self)

    @property
    def actor(self):
        """
        The agent performing the action, if there is one
        """
        if not self.natural:
            return self.args[0] if len(self.args) else None

    def sub(self, var, val):
        """
        Substitute value for var in the arguments of the action
        """
        cls = self.__class__
        args = tuple(val if arg == var else arg
                     for arg in self.args)
        return cls(*args)

    def cost(self, sit, duals=None):
        """
        Compute the cost of executing this (possibly unground) action
        """
        duals = duals or {}
        return (self.basecost -
                sum(req * duals.get(res, 0)
                    for (res, req) in self.resources(sit).iteritems()))

    def precond(self, sit):
        """
        Is it legal to execute this action in `sit`?
        """
        return all(sit.usage(res) + req <= res.avail
                   for res, req in self.resources(sit).iteritems())

    def resources(self, sit):
        """
        Subclasses should implement this
        """
        return {sit.shared(self.actor, sit.time()): 1}

    def ground(self):
        """
        Return true if no argument to this Action is a `Var`
        """
        return not any(isinstance(arg, Var)
                       for arg in self.args)

    def __hash__(self):
        return reduce(operator.xor,
                      (hash(a) for a in self.args),
                      id(self.__class__))

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__,
                           ', '.join(repr(a)
                                     for a in self.args))

    def __eq__(self, other):
        return self.__class__ == other.__class__ and self.args == other.args


def action(func):
    """
    Return an action class built from a resource usage function

    """

    class Act(Action):
        """
        A specialised action, this docstring will be overwritten
        """
        __doc__ = func.__doc__
        MINCOST = 0
        _min_res = None

        def __init__(self, *args):
            super(Act, self).__init__(*args)
            self._legal, self.basecost, self._res = None, 1e-6, None

        def _func(self, sit):
            """
            call the underlying resource usage function
            """
            ret = func(sit, *self.args)
            if ret:
                legal = True
                basecost, res = ret
            else:
                legal = False
                basecost = sys.maxint
                res = {}
            return legal, basecost, res

        def resources(self, sit=None):
            _, __, res = self._func(sit)
            return res

        def precond(self, sit):
            legal, _, resources = self._func(sit)
            return legal
            ## return (legal and
            ##         all(sit.usage(res) + req <= res.avail
            ##             for res, req in resources.iteritems()))

        def cost(self, sit, duals=None):
            duals = duals or {}
            _, basecost, res = self._func(sit)
            return basecost - sum(u * duals.get(r, 0)
                                  for (r, u) in res.iteritems())

        @classmethod
        def min_res_func(cls, func=None):
            cls._min_res = (func,)

        def min_resources(self, sit):
            if self._min_res:
                mr = self.__class__._min_res[0]
                #print "Min resources known", mr, sit, self.args
                return mr(sit, *self.args)
            return {}

        def min_cost(self, sit, duals=None):
            #print "calc min cost"
            duals = duals or {}
            return (self.MINCOST -
                    sum(u * duals.get(r, 0)
                        for (r, u) in self.min_resources(sit).iteritems()))

    Act.__name__ = func.__name__

    return Act


def situation(name):
    """
    Construct a new Situation class
    """
    class Sit(Situation):
        """
        A Situation
        """
        __name__ = name
        _resources = {}

    return Sit


def mincost(cost):
    def decorate(thing):
        thing.MINCOST = cost
        return thing
    return decorate
