#ifndef __OpSetPLANNER__
#define __OpSetPLANNER__

#include <py_strips_prob.hxx>
#include <fwd_search_prob.hxx>
#include <landmark_graph.hxx>

#include <vector>
#include <opsetsat.hxx>


class	OpSetPlanner : public STRIPS_Problem
{
public:

	OpSetPlanner( );
	OpSetPlanner( std::string, std::string );
	virtual ~OpSetPlanner();

	
	virtual void setup();
	bool	solve(int n_layers=0);
	int num_layers(){ return opengine.num_layers(); }
	bool add_relaxation() { return opengine.add_relaxation(); }
	bool has_relaxation() { return opengine.has_relaxation(); }

	bool plan_found;
	aptk::search::opset::OpSet opset;
	std::vector<aptk::Action_Idx> plan;
	aptk::Bit_Array cut;
	double formulation_time;

	std::string	m_log_filename;
	std::string	m_plan_filename;
protected:
	aptk::search::opset::OpSetEngine<aptk::STRIPS_Problem> opengine;
};

#endif
