/*
Lightweight Automated Planning Toolkit
Copyright (C) 2012
Miquel Ramirez <miquel.ramirez@rmit.edu.au>
Nir Lipovetzky <nirlipo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <strips_prob.hxx>
#include <action.hxx>
#include <strips_state.hxx>
#include <fd_to_aptk.hxx>
#include <aptk/time.hxx>
#include <watched_lit_succ_gen.hxx>

#include <iostream>
#include <iterator>
#include <boost/program_options.hpp>
#include <vector>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <utility>
#include <string>
#include <random>

#include "pdb.hxx"
#include "potentials.hxx"

namespace po = boost::program_options;

using aptk::STRIPS_Problem;
using aptk::State;
using aptk::WatchedLitSuccGen;
using cdhl::LearningPDB;

struct Timer {
	std::string msg;
	clock_t t0;
	Timer(std::string msg_):msg(msg_), t0(clock()){
		std::cerr << "Start " << msg << std::endl;
	}

	template<class T>
	std::ostream& operator<<(T t){
		return std::cerr << "[" << msg << " " << seconds() << " s]: " << t;
	}

	double seconds() const {
		return 1.0*(clock() - t0)/CLOCKS_PER_SEC;
	}

	~Timer(){
		std::cerr << msg << " took " << 1.0*(clock() - t0)/CLOCKS_PER_SEC << " seconds"  << std::endl;	}
};


void process_command_line_options( int ac, char** av, po::variables_map& vars ) {
	po::options_description desc( "Options:" );
	
	desc.add_options()
		( "help", "Show help message" )
		( "sas-file",
		  po::value<std::string>()->default_value("output"),
		  "FD preprocessor output file" )
		( "use-potentials,p",
		  po::value<bool>()->default_value(true),
		  "Use a potential heuristic as an initial heuristic" )
		( "potential-sample-rate",
		  po::value<double>()->default_value(0.0),
		  "Compute new potential heuristics during search, sampling states with this probability" )
		( "clauses",
		  po::value<int>()->default_value(1024),
		  "Initial number of clauses" )
		( "clause-growth",
		  po::value<double>()->default_value(1.01),
		  "exponential growth rate of clauses" )
		( "clause-incr",
		  po::value<int>()->default_value(0),
		  "linear growth rate of clauses" )
		( "trivial-expls",
		   po::value<bool>()->default_value(false),
		  "Use trivial explanations for lower bounds (don't attempt to generalize)" )
		( "use-uip,u",
		  po::value<bool>()->default_value(false),
		  "Use Unique-implication-point reasoning when explaining lower bounds" )
		( "strengthen,s",
		  po::value<bool>()->default_value(true),
		  "Use iterative clause strengthening when new lower bounds are found." )
		( "close-states",
		  po::value<bool>()->default_value(false),
		  "Add clauses to close states." )
		( "max-search-time",
		  po::value<int>()->default_value(24*60*60),
		  "Maximum seconds of search time" )
		( "exists",
		  po::value<bool>()->default_value(false),
		  "Attempt to prove solvability" )
		( "anytime-w,W",
		  po::value< std::vector<double> >()->default_value(std::vector<double>(1, 1.0), "1.0"),
		  "Hill-climb to (repeatedly) find a solution" )
		( "restart-conflicts",
		  po::value<int>()->default_value(1024),
		  "Number of backtracks before moving on to the next w-value" )
		( "restart-growth",
		  po::value<double>()->default_value(1.005),
		  "Growth rate between restarts" )
	;
	
	try {
		po::store( po::parse_command_line( ac, av, desc ), vars );
		po::notify( vars );
	}
	catch ( std::exception& e ) {
		std::cerr << "Error: " << e.what() << std::endl;
		std::exit(1);
	}

	if ( vars.count("help") ) {
		std::cout << desc << std::endl;
		std::exit(0);
	}

}

int ncbt(STRIPS_Problem& prob, LearningPDB& pdb, int f_max, std::vector<State*>& states, std::vector<int>& plan){
	return plan.size();
	// int g = 0;
	// for(unsigned i = 0; i < plan.size(); i++){
	// 	auto& s = states[i];
	// 	auto op = plan[i];
	// 	auto act = prob.actions()[op];
	// 	int h = pdb.eval(s, f_max - g + 1);
	// 	int f = g + h;
	// 	if(f > f_max){
	// 		std::cerr << "NCBT backtracking to step " << i 
	// 			  << "(-" << plan.size() - i << " levels)"
	// 			  << std::endl;
	// 		// states.erase(states.begin() + i, states.end())
	// 		// plan.erase(plan.begin() + i, plan.end())
	// 		return i;
	// 	} else if (f == f_max && op != pdb.best_action(s, h)) {
	// 		std::cerr << "NCBT backtracking (2) to step " << i 
	// 			  << "(-" << plan.size() - i << " levels)"
	// 			  << std::endl;
	// 		return i;
	// 	}
	// 	//std::cerr << "g = " << g << " h = " << h << " f = " << f <<" f_max = " << f_max << std::endl;

	// 	g += act->cost();
	// }
	// //std::cerr << "NCBT backtracking failed" << std::endl;
	// return plan.size();
}

int main( int argc, char** argv ) {
	po::variables_map vm;

	process_command_line_options( argc, argv, vm );

	STRIPS_Problem	prob;
	std::vector<aptk::FD_Parser::DTG> dtgs;
	aptk::FD_Parser::get_problem_description( vm["sas-file"].as<std::string>(), prob, dtgs );
	
	std::cout << "PDDL problem description loaded: " << std::endl;
	std::cout << "\t#Actions: " << prob.num_actions() << std::endl;
	std::cout << "\t#Fluents: " << prob.num_fluents() << std::endl;
	std::cout << "\t#Mutexes: " << prob.mutexes().num_groups() << std::endl;
	std::cout << "\t#DTGs: " << dtgs.size() << std::endl;
	prob.compute_edeletes();


	if(vm["exists"].as<bool>()){
		for(auto act : prob.actions()){
			act->set_cost(0.0);
		}
	}

	potential::PotentialHeuristicCollection ph;
	potential::PotentialHeuristicBuilder* phb = NULL;
	bool use_potentials = vm["use-potentials"].as<bool>();
	if(use_potentials){
		phb = new potential::PotentialHeuristicBuilder(prob);
		std::cerr << "Using potential heuristic" << std::endl;
		phb->addSample(prob.init(), 1);
		ph.add(phb->build());
		phb->addSyntacticStates(1.0);
		phb->addSample(prob.init(), -1.0);
		ph.add(phb->build());
	}
	bool trivial_expls = vm["trivial-expls"].as<bool>();
	std::vector<double> Ws = vm["anytime-w"].as< std::vector<double> >();
	unsigned w_idx = 0;
	double W = Ws.at(w_idx % Ws.size());
	bool use_uip = vm["use-uip"].as<bool>();
	bool strengthen = vm["strengthen"].as<bool>();
	bool close_states = vm["close-states"].as<bool>();
	LearningPDB pdb(prob, trivial_expls, use_uip, strengthen);
	pdb.setup();
	auto builder = pdb.builder();

	Timer search_timer("Search");

	unsigned cost_ub = vm["exists"].as<bool>() ? 0 : cdhl::BigM;
	unsigned cost = 0;
	int max_conflicts = vm["restart-conflicts"].as<int>();
	int conflicts = 0;
	std::vector<int> plan;
	std::vector<State*> states;
	std::vector<State*> state_pool;

	states.push_back(new State(prob));
	states.back()->set(prob.init());

	LearningPDB::PathInfo bounds = {min_h: 0,
					max_h: cdhl::BigM,
					g: std::make_pair((unsigned)0, (unsigned) 1)
	};

	int lb0 = pdb.eval(*states.back(), bounds);
	bounds.min_h = lb0;
	bounds.max_h = W * lb0;
	unsigned nodes = 1;
	unsigned max_depth = 1;
	unsigned clauses = vm["clauses"].as<int>();

	double sample_rate = vm["potential-sample-rate"].as<double>();
	auto do_sample = [&](){
		if(sample_rate > 0 && rand() >= sample_rate){
			sample_rate *= 0.9;
			return true;
		}
		return false;
		
	};

	int max_time = vm["max-search-time"].as<int>();
	search_timer << "Initial lb: " << lb0 << std::endl;
	while(true){
		if(search_timer.seconds() > max_time){
			std::cerr << "Time out" <<std::endl;
			exit(2);
		}

		max_depth = std::max(max_depth, (unsigned)states.size());

		auto& s = *states.back();

		if(s.entails(prob.goal()) && cost <= cost_ub){
			search_timer << "Plan found! " << std::endl
				     << "      cost = " << cost << std::endl
				     << "    length = " << plan.size() << std::endl
				     << "        lb = " << cost + lb0 << std::endl
				     << "     nodes = " << nodes << std::endl
				     << "   clauses = " << pdb.size() << "/" << clauses << std::endl
				     << " conflicts = " << conflicts << std::endl
				     << std::endl;
			cost_ub = cost - 1;
			for(auto op : plan){
				std::cout << prob.actions()[op]->signature() << std::endl;
			}
			bounds.max_h = -1;
			if(cost_ub < cost + bounds.min_h)
				break;
			continue;
		}

		if(Ws.size() > 1 && conflicts >= max_conflicts){
			auto Wx = Ws.at(++w_idx % Ws.size());
			LearningPDB::PathInfo s0_bounds = {
			    min_h: 0,
			    max_h: cost_ub,
			    g: std::make_pair((unsigned)0, (unsigned) 1)
			};
			auto s0lb = pdb.eval(*states.at(0), s0_bounds);
			if(Wx*s0lb < cost_ub || Ws.size() == 2) {
				lb0 = s0lb;
				W = std::min(Wx, (double)cost_ub / s0lb);
				bounds = s0_bounds;
				bounds.max_h = W*lb0;
				plan.resize(0);
				while(states.size() > 1) states.pop_back();
				cost = 0;
				bounds.min_h = lb0;

				max_conflicts *= vm["restart-growth"].as<double>();
				search_timer << "W = " << W << std::endl
					     << "  conflicts = " << conflicts << std::endl
					     << "  clauses = " << pdb.size() << std::endl
					     << "  lb = " << s0lb << std::endl;
				conflicts=0;

				continue;
			} else if(Ws.size() > 2){
				search_timer << "W = " << Wx << " erased: " 
					     << Wx << "*" << s0lb << " >= " << cost_ub << std::endl
					     << "  conflicts = " << conflicts << std::endl
					     << "  clauses = " << pdb.size() << std::endl
					     << "  lb = " << s0lb << std::endl;
				Ws.erase(Ws.begin() + w_idx % Ws.size());
			}
		}

		int lb = cdhl::BigM;
		int op = -1;
		if(bounds.min_h > cost_ub) {
			lb = cdhl::BigM;
			op = -1;
		} else {
			std::tie(lb, op) = pdb.best_action(s, bounds);
		}
		if(lb < bounds.min_h)
			lb = bounds.min_h;
		if(use_potentials) {
			int h = ph(s.fluent_set());
			if(h > lb){
				Bit_Set expl = s.fluent_set();
				if(!trivial_expls && clauses > 0){
					ph.explain_lb(prob, expl, h);
				}
				builder.state_ub(expl).lb(h);
				pdb.addCube(builder);
				lb = pdb.eval(s, bounds);
			} else if (h < lb && do_sample()) {
				phb->discountSamples(0.999);
				phb->addSample(s, 0.001);
			}
		}

		// std::cerr << "g: " << cost << std::endl;
		// std::cerr << "d: " << states.size() << std::endl;
		// std::cerr << "h: " << lb << " (" << lb0 << ")" << std::endl;
		// std::cerr << "f: " << cost + lb << std::endl;
		// std::cerr << "op: " << op << std::endl;
		// if(op != -1)
		// 	std::cerr << prob.actions()[op]->signature() << std::endl;
		assert(op != -1 || lb > lb0 || lb > W*lb0);
		if (plan.empty() && lb > cost_ub){
			search_timer << "No more plans " << lb << " > " << cost_ub << std::endl;
			break;
		}
		if(cost + lb > cost_ub || bounds.max_h < 0 || lb > bounds.max_h || lb >= cdhl::BigM || op == -1){
			conflicts += 1;
			if(pdb.size() > clauses){
				//Timer t("gc");
				int sz0 = pdb.size();
				pdb.gc(states, 0.8*clauses, cost_ub);
				clauses *= vm["clause-growth"].as<double>();
				clauses += vm["clause-incr"].as<int>();
				//std::cerr << "Collected " << sz0 - pdb.size() << "/" << clauses <<" clauses" << std::endl;
			}

			if(plan.empty() && lb <= cost_ub){
				if(lb0 == cdhl::BigM)
					return 1;

				bounds.max_h = cost_ub;
				lb0 = pdb.eval(s, bounds);
				bounds.min_h = std::max(lb0, 0);
				bounds.max_h = std::min(unsigned(W * bounds.min_h), cost_ub);
				if(bounds.min_h > bounds.max_h) {
					search_timer << "No more plans (2) " << lb0 << " > " << cost_ub << std::endl;
					search_timer << "No more plans (2) " << bounds.min_h << " > " << bounds.max_h << std::endl;
					break;
				}
				search_timer << "Major iter" << std::endl;
				std::cerr << "             lb = " << lb0 << std::endl;
				std::cerr << "             ub = " << cost_ub << std::endl;
				std::cerr << "          nodes = " << nodes << std::endl;
				std::cerr << "        clauses = " << pdb.size() << "/" << clauses << std::endl;
				std::cerr << "      max_depth = " << max_depth << std::endl;
				std::cerr << "      conflicts = " << conflicts << std::endl;
				max_depth = 1;
				if(sample_rate > 0){
					ph.gc(9);
					ph.add(phb->build());
				}
			} else if(!plan.empty()) {
				int lvl = ncbt(prob, pdb, cost + lb0, states, plan);
				while(plan.size() >= (unsigned)lvl){
					auto act = prob.actions()[plan.back()];
					lb0 += act->cost();
					cost -= act->cost();
					bounds.min_h += act->cost();
					bounds.max_h += act->cost();
					bounds.g.first -= act->cost();
					bounds.g.second -= 1;

					op = plan.back();
					plan.pop_back();
					state_pool.push_back(states.back());
					states.pop_back();
				}
			} else if(plan.empty() && lb == cdhl::BigM) {
				std::cerr << "No plan exists" << std::endl;
				return 1;
			}
		} else {
			auto act = prob.actions()[op];
			if(close_states || W>1.0 || act->cost() == 0)
				pdb.closeState(s.fluent_set(), bounds);

			if(state_pool.empty())
				state_pool.push_back(new State(prob));
			State* next_state = state_pool.back();\
			state_pool.pop_back();
			next_state->reset();
			next_state->set(s.fluent_vec());

			plan.push_back(op);
			states.push_back(next_state);
			states.back()->unset(act->del_vec());
			states.back()->set(act->add_vec());

			lb0 -= act->cost();
			cost += act->cost();
			bounds.min_h -= act->cost();
			bounds.max_h -= act->cost();
			bounds.g.first += act->cost();
			bounds.g.second += 1;

			nodes += 1;
		}
		
	}
	std::cerr << "Proved optimality" << std::endl
		  << "     nodes = " << nodes << std::endl
		  << "   clauses = " << pdb.size() << std::endl
		  << " conflicts = " << conflicts << std::endl;
	for(auto op : plan){
		std::cout << prob.actions()[op]->signature() << std::endl;
	}

	return 0;
}
