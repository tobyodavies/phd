import os

debug = ARGUMENTS.get('debug', 0)
prof =  ARGUMENTS.get('profile', 0)
common_env = Environment( ENV = os.environ )


lwaptk_base_dir = '../LAPKT/'
include_paths = [
    '../LAPKT/include',
    '../LAPKT/interfaces/agnostic',
    '../LAPKT/interfaces/fd-output',
    '/usr/local/include',
    os.path.join(os.getenv('HOME'), 'boost/include'),
    os.path.join(os.getenv('GUROBI_HOME'), 'include'),
]
lib_paths = [
    os.path.join(os.getenv('HOME'), 'boost/lib'),
    os.path.join(os.getenv('GUROBI_HOME'), 'lib'),
]

libs = [ 'gurobi_c++', 'boost_program_options']
if prof:
    libs.append('profiler')
    libs.append('tcmalloc')

if 'gurobi65' in os.getenv('GUROBI_HOME'):
    libs.append('gurobi65')
elif 'gurobi6' in os.getenv('GUROBI_HOME'):
    libs.append('gurobi60')
else:
    libs.append('gurobi56')

common_env.Append( CPPPATH = [ os.path.abspath(p) for p in include_paths ] )
common_env.Append( CCFLAGS = ['-D' '__STDC_LIMIT_MACROS', '-D', '__STDC_FORMAT_MACROS'] )
common_env.Append( CCFLAGS = ['-g','-Wall', '-std=c++11', '-march=native', '-ftree-vectorize', '-ffast-math'] )



if ARGUMENTS.get('profile'):
    common_env.Append( CCFLAGS = ['-pg' ] )
    common_env.Append( LINKFLAGS = ['-pg' ] )

cxx_sources = Glob('*.cxx')
c_sources = Glob('*.c')

common_env.Append( LIBS=libs)
common_env.Append( LIBPATH=[ os.path.abspath(p) for p in lib_paths ] )

test_env = common_env.Clone()
test_env.Append( CPPPATH = ['/usr/include/gtest/', '/usr/include/gtest/internal/', '/usr/src/gtest/'] )
test_env.Append( LIBS = ['pthread'] )
test_env.Append( CCFLAGS = ['-DDEBUG', '-O3', '-ftree-vectorize', '-ffast-math' ] )
if int(debug) == 1 :
	common_env.Append( CCFLAGS = ['-DDEBUG', '-O0' ] )
else:
	common_env.Append( CCFLAGS = ['-O3', '-DNDEBUG'] )

Export( 'common_env' )

src_objs = ([ common_env.Object(s)
              for s in cxx_sources
              if (not s.path.endswith('_test.cxx')
                 and not s.path.endswith('main.cxx')) ]
            +
            [ common_env.Object(s) for s in c_sources ])
main = common_env.Object('main.cxx')
generic_objs = SConscript(os.path.join( lwaptk_base_dir,'src/SConscript.aptk'))
agnostic_objs = SConscript(os.path.join( lwaptk_base_dir,'interfaces/agnostic/SConscript.agnostic'))
fd_objs = SConscript(os.path.join( lwaptk_base_dir,'interfaces/fd-output/SConscript.fd'))


common_env.Program( 'bin/cdhl', [main] + src_objs  + generic_objs + agnostic_objs + fd_objs)

print ARGUMENTS

if int(ARGUMENTS.get('test', 1)):
    test_sources = Glob('*_test.cxx')
    gtest_all = test_env.Object('./gtest.o', '/usr/src/gtest/src/gtest-all.cc')
    gtest_main = test_env.Object('./gtest_main.o', '/usr/src/gtest/src/gtest_main.cc')
    test_objs = [ test_env.Object(s) for s in test_sources]
    test_prog = test_env.Program( 'bin/cdhl_tests', [gtest_main] + gtest_all + test_objs + generic_objs + agnostic_objs + fd_objs + src_objs)
    test = test_env.Command(
        target = "test.log",
        source = test_prog,
        action = "(date ; ./bin/cdhl_tests) 2>&1 | tee $TARGET" )
    Depends(test, 'bin/cdhl')
    AlwaysBuild( test )

