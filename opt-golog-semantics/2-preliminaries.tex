
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preliminaries}

\newcommand{\vars}[1]{\bar{#1}}
\newcommand{\Reg}{\mathcal{R}}
\newcommand{\Dt}{\mathcal{D}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{The Situation Calculus and Basic Action Theories}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 The \textit{situation calculus} is a logical language specifically
designed for representing and reasoning about dynamically changing
% worlds \cite{McCarthy:Sit,mccarthy69sitcalc,Reiter2001}.
worlds \cite{McCarthy:Sit,Reiter2001}.
%
Changes to the world result from \textit{actions},
which are terms in the language. We denote action variables by
lower case letters $a$, action types by capital letters $A$, and
action terms by $\alpha$, possibly with subscripts.
%
A possible world history is represented by a term called a
\emph{situation}. The constant $S_0$ is used to denote the initial
situation where no actions have yet been performed. Sequences of
actions are built using the function symbol $do$, such that $do(a,s)$
denotes the successor situation resulting from performing action $a$
in situation $s$.
%
Predicates and functions whose value varies from situation to situation are
called \textit{fluents}, and are denoted by symbols taking a
situation term as their last argument (e.g., $\id{Holding}(x,s)$).

Within the language, one can formulate action theories that describe
how the world changes as the result of the available actions.  Here,
we concentrate on \emph{basic action theories} as proposed in
\cite{Reiter2001}.  We also assume that there are a \emph{finite
  number of action types} in the domains that we consider. As a result
a basic action theory $\D$ is as set of situation calculus sentences describing a particular dynamic world. It consists of the union of the following disjoint
sets: the foundational domain-independent axioms of the situation
calculus ($\Sigma$); precondition axioms stating when actions can be
legally performed ($\D_{poss}$); \emph{successor state axioms} describing how
fluents change between situations ($\D_{ssa}$); unique name axioms for
actions and domain closure on action types ($\D_{ca}$); and axioms
describing the initial configuration of the world ($\D_{S_0}$).
% %%
A special predicate $\Poss(a,s)$ is used to state that action $a$ is executable
in situation $s$; precondition axioms in $\D_{poss}$ characterize this predicate. 

%\[\D=\Sigma\cup\D_{poss}\cup\D_{ssa}\cup\D_{S_{0}}\cup\D_{ca}\]

%\textbf{[We need a new running example in this section,
%  perhaps one based on the tic-tac-toe game spec from the general game
%  playing competition.]}

% Tic-tac-toe example
% leave for later
%
% To illustrate, we use tic-tac-toe as a running example.
% Players place either a nought, $p=O$, or cross, $p=X$, at position row $r=\{1,2,3\}$, column $r=\{1,2,3\}$ defined by action $Play(r,c,p)$. The state of the game is captured by the fluent, $Cell(r,c,p)$, which becomes true after performing action $Play(r,c,p)$, captured by effect axiom $Play(r,c,p) \supset Cell(r,c,p)$. Possible moves are subsequently defined as:

% \[Poss(Play(r,c,p),s) \equiv \neg Cell(r,c,O) \land \neg Cell(r,c,X) \]

% Once the dynamical system is modeled as a basic action theory, one can
% pose queries about its behavior or evolution as logical entailment
% queries relative to the theory.

Successor state axioms encode the causal laws of the world being
modeled; taking place of the effect axioms they provide a monotonic
solution to the frame problem. They have the following general form,
asserting the truth of a fluent $f$ in the successor situation
$do(\alpha,s)$ based on the current situation $s$ and the action $\alpha$ that
was performed:
$ f(do(\alpha,s))\equiv\Phi_f[do(\alpha,s)]$.
In general, $f$ may have an arbitrary argument vector, but we omit arguments for brevity.
We assume in places that $\Phi_f$ is transformed from effect axioms as in \cite{Reiter2001},
i.e.,
\begin{gather*}
\Phi_f[do(\alpha, s)] =\, \phi^+_f[do(\alpha, s)] \\
                     \lor \lnot (\phi^-_f[do(\alpha, s)] \lor \phi^+_f[do(\alpha, s)] \land \Phi_f[s])
\end{gather*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Regression}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Planning relies on effective answering of projection queries, $\phi$,
which determine the state of the world after possible ($\Legal$)
sequences of actions.
This is facilitated by the regression meta-operator $\Reg_{\Dt}$
\cite{pirri99contributions_sitcalc}, a syntactic manipulation whose
behaviour can be summarised as follows: it transforms a formula $\phi$
uniform in $do(c,s)$ into a formula $\Reg_{\Dt}(\phi)$ that is uniform
in $s$ and is equivalent to $\phi$ under the theory of action $\D$:
$\Dt\,\models\,\phi\equiv\Reg_{\Dt}(\phi)$.
% \[\Dt\,\models\,\phi\equiv\Reg_{\Dt}(\phi)\]

Regression replaces primitive fluents $f(\vars{x},do(c,s))$
with the body of the matching successor state axiom. It also replaces
non-primitive fluents such as $Poss$ with their corresponding definitions.
We will omit the subscript since only one $\Dt$ is relevant at any
time throughout the paper.
%% , and will simplify the presentation of
%% situation-suppressed
%% formulae by using $\Reg[\phi,c]$ to denote $\Reg[\phi(do(c,s))]^{-1}$.
If a formula $\phi$ refers to a situation that is rooted at $S_{0}$, repeated
applications of the regression operator (normally denoted by $\Reg^{*}$) can
transform it into an equivalent formula uniform in the initial situation.
The successor state and action precondition axioms are `compiled in'
to the formula and so are not required for answering the regressed
query:\begin{gather*}
\Dt\models\phi[do(c_{n},do(c_{n-1},\dots,do(c_{1},S_{0})))]\\
\mathrm{iff}\ \ \Dt_{ca}\cup\Dt_{S_{0}}\models\Reg^{*}(\phi)[S_{0}]\end{gather*}
We use this repeated regression exclusively in this paper, and any
uses of $\R$ can be read as $\R^*$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{High-Level Programs}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

To represent and reason about complex actions or processes obtained by suitably
executing atomic actions, various so-called \emph{high-level programming
languages} have been defined.
%
% , such as \Golog\ \cite{Levesque:JLP97-Golog}, which
% includes usual structured constructs and constructs for nondeterministic choices,
% \ConGolog\ \cite{DeGiacomoLL:AIJ00-ConGolog}, which extends \Golog\ to
% accommodate concurrency, and \IndiGolog\ \cite{SardinaDGLL:AMAI04}, which
% provides means for interleaving planning and execution.
%
Here we concentrate on a fragment of \ConGolog, which includes most constructs of
the language, except for (recursive) procedures.
Hereafter we will simply refer to \Golog{}, \ConGolog{} and its extensions simply as \Golog{}:
% \small
\begin{quote}\small
$\alpha$           \hfill  atomic action\\
$\varphi?$            \hfill test for a condition\\
$\delta_1;\delta_2$        \hfill  sequence\\
$\mif\ \varphi\ \mthen\ \delta_1\ \melse\ \delta_2$
                \hfill  conditional\\
$\mwhile\ \varphi\ \mdo\ \delta$
                \hfill  while loop\\
$\delta_1 \ndet \delta_2$   \hfill  nondeterministic branch\\
$\pi x.\delta$     \hfill  nondeterministic choice of argument\\
$\delta^*$             \hfill  nondeterministic iteration\\
$\delta_1 \conc \delta_2$  \hfill concurrency
\end{quote}
%%
In the above, $\alpha$ is an action term, possibly with parameters, and $\varphi$ is
situation-suppressed formula, that is, a formula in the language with all
situation arguments in fluents suppressed. We denote by $\varphi[s]$ the
situation calculus formula obtained from $\varphi$ by restoring the situation
argument $s$ into all fluents in $\varphi$.
%
% Note the presence of nondeterministic constructs, which allow the loose
% specification of programs by leaving ``\textit{gaps}'' that ought to be resolved
% by the executor.
% % %%
Program $\delta_1 \ndet \delta_2$ allows for the nondeterministic choice
between programs $\delta_1$ and $\delta_2$, while $\pi x.\delta$ executes program
$\delta$ for \textit{some} nondeterministic choice of a legal binding for
variable $x$.  $\delta^*$
performs $\delta$ zero or more times.
% %%
Program $\delta_1 \conc \delta_2$ expresses the concurrent execution
(interpreted as interleaving) of programs $\delta_1$ and $\delta_2$.

Formally, the semantics of \Golog{} is specified in terms of
single-step transitions, using the following two predicates
\cite{DeGiacomoLL:AIJ00-ConGolog}: 
\emph{(i)} $\Trans(\delta,s,\delta',s')$, which holds if one step
of program $\delta$ in situation $s$ may lead to situation $s'$ with
$\delta'$ remaining to be executed; and \emph{(ii)}  $\Final(\delta,s)$, which
holds if program $\delta$ may legally terminate in situation $s$.
%%
%
%% The definitions of $\Trans$ and $\Final$ for the constructs used in
%% this paper are shown below:
%% % \footnote{Note that since $\Trans$ and
%% %  $\Final$ take programs (that include test of formulas) as arguments,
%% %   this requires encoding formulas and programs as terms; see
%% %   \cite{DeGiacomoLL:AIJ00-ConGolog} for details.}
%% \[\small
%%  \begin{array}{l}
%% \Trans(\alpha,s,\delta',s') \equiv {}\\
%% \qquad
%% 	s'=do(\alpha,s) \land \Poss(\alpha,s) \land \delta'=True? \\[0.5ex]
%% \Trans(\varphi?,s,\delta',s') \equiv False \\[0.5ex]
%% \Trans(\delta_1; \delta_2,s,\delta',s') \equiv{} \\[0.5ex]
%% \qquad
%% 	\Trans(\delta_1,s,\delta_1',s') \land \delta'=\delta_1';\delta_2 \lor{}\\
%% \qquad
%% 		\Final(\delta_1,s) \land \Trans(\delta_2,s,\delta',s') \\[0.5ex]
%% \Trans(\mif\ \varphi\ \mthen\ \delta_1\ \melse\ \delta_2,s,\delta',s') \equiv{}\\
%% \qquad   
%%         \varphi[s] \land \Trans(\delta_1,s,\delta',s') \lor {}\\
%% \qquad   
%%         \lnot\varphi[s] \land \Trans(\delta_2,s,\delta',s') \\[0.5ex]
%% \Trans(\mwhile\ \varphi\ \mdo\ \delta ,s,\delta',s') \equiv{}\\
%% \qquad   
%%         \varphi[s] \land \Trans(\delta,s,\delta'',s') \land \delta'=\delta''; (\mwhile\ \varphi\ \mdo\ \delta)\\[0.5ex]
%% \Trans(\delta_1 \ndet \delta_2,s,\delta',s') \equiv{} \\
%% \qquad
%% 	\Trans(\delta_1,s,\delta',s') \lor \Trans(\delta_2,s,\delta',s') \\[0.5ex]
%% \Trans(\pi x.\delta, s, \delta',s') \equiv 
%% 	\exists x. \Trans(\delta,s,\delta',s')\\[0.5ex]
%% \Trans(\delta^*, s, \delta',s') \equiv
%% 	\Trans(\delta,s,\delta'',s')\land \delta'=\delta'';\delta^*\\
%% \Trans(\delta_1 \conc \delta_2,s,\delta',s') \equiv{} \\ 
%% \qquad
%% 	\Trans(\delta_1,s,\delta_1',s')  \land \delta'=\delta_1'\conc\delta_2 \lor {}\\
%% \qquad
%% 		\Trans(\delta_2,s,\delta_2',s')  \land \delta'=\delta_1\conc\delta_2' 
%% \\ \\
%%  \end{array}
%% \]
%% %
%% \[\small
%%  \begin{array}{l}
%% \Final(\alpha,s) \equiv False \\
%% \Final(\varphi?,s) \equiv \varphi[s] \\
%% \Final(\delta_1; \delta_2,s) \equiv 
%%         \Final(\delta_1,s) \land \Final(\delta_2,s)\\
%% \Final(\mif\ \varphi\ \mthen\ \delta_1\ \melse\ \delta_2,s) \equiv{}\\
%% \qquad
%%         \varphi[s] \land \Final(\delta_1,s) \lor %{}\\
%% %\qquad\qquad   
%%         \lnot\varphi[s] \land \Final(\delta_2) \\%[0.5ex]
%% \Final(\mwhile\ \varphi\ \mdo\ \delta ,s) \equiv
%%         \varphi[s] \land \Final(\delta,s') \lor \lnot\varphi[s]\\%[0.5ex]
%% \Final(\delta_1 | \delta_2,s) \equiv 
%% 	\Final(\delta_1,s) \lor \Final(\delta_2,s) \\
%% \Final(\pi x.\delta, s) \equiv
%% 	\exists x. \Final(\delta,s)\\
%% \Final(\delta^*, s) \equiv True\\
%% \Final(\delta_1 \conc \delta_2,s) \equiv 
%%        \Final(\delta_1,s) \land \Final(\delta_2,s)
%%  \end{array}
%% \]
%%
The definitions of $\Trans$ and $\Final$ we use are as in
\cite{Sardina2009}; these are standard
\cite{DeGiacomoLL:AIJ00-ConGolog}, except that, following
\cite{Classen:KR08}, the test construct $\varphi?$ does not yield
any transition, but is final when satisfied.  
Thus, it is a \emph{synchronous} version of the original test construct
(it does not allow interleaving).
Note that the definition of $\Trans(\delta, s, \delta', s')$ has
only one successful non-recursive case, where $s'$ is exactly one
action longer than $s$;
any successful transition adds exactly one action to the current situation.

% %% GGG: I added if and while explicitly to avoid confusion in GameGolog.
%%
% With this version of tests, the \ConGolog\ constructs for conditional
% and while-loop, which involve synchronous tests, are then immediately
% definable in terms of the other constructs: $(\mif\ \varphi\ \mthen\
% \delta_1\ \melse\ \delta_2) = \varphi?; \delta_1 |
% \lnot\varphi?; \delta_2$ and $(\mwhile\ \varphi\ \mdo\ \delta)  = (\varphi?; \delta)^*;\lnot \varphi?$. Notice that, in
% spite of the use of nondeterministic constructs in the above
% definitions, both conditional and while-loop are deterministic
% constructs.
%%
%%
%% Also, as in \cite{SardinaG09}, we require that in programs of the form $\pi
%% x.\delta$, the variable $x$ occurs in some non-variable action term in $\delta$;
%% we disallow cases where $x$ occurs \emph{only} in tests or as an action itself.
%% In this way, $\pi x.\delta$ acts as a construct for making nondeterministic choices
%% of action parameters (possibly constrained by tests).
% %%%
Finally, we assume without loss of generality that each occurrence of
the construct $\pi x.\delta$ in a program uses a unique fresh variable
$x$---no two occurrences of such a construct use the same variable.

% Observe that, since we are not considering recursive procedures here, we do not
% need to resort to a second-order definition of $\Trans$ and $\Final$, although we
% still need to use an encoding of programs as terms, cf.\ \cite{DeGiacomoLL:AIJ00-ConGolog}.

These two predicates define the single-step semantics of \Golog{};
they are used to look ahead, to solve the projection problem.
We define $\Trans^*$ to be the transitive closure of $\Trans$.
%% , i.e.
%% \begin{align*}
%% \Trans^*(\delta, s, \delta', s') \equiv& (\delta = \delta' \land s = s') \\
%%                                        & \lor (\Trans(\delta, s, \delta'', s'') \land \\
%%                                        & \qquad \Trans*(\delta'', s'', \delta', s'))
%% \end{align*}
%
$\Trans^*$ is used to define the reachable states, to solve the
projection problem, we wish to limit the search to those that are both
reachable, and any residual program is $\Final$.
$\Do(\delta, s, s')$ is used to solve this problem, defined as:
\begin{align*}
\Do(\delta, s, s') \equiv& \, \Trans^*(\delta, s, \delta', s') \land \Final(\delta') 
\end{align*}

