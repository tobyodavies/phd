

(define (problem BW-rand-4)
(:domain blocksworld)
(:objects b1 b2 b3 b4  - block
	  h1 h2 h3 h4  - hand)
(:init
(handempty h1)(handempty h2)(handempty h3)(handempty h4)
(on b1 b4)
(on b2 b1)
(ontable b3)
(ontable b4)
(clear b2)
(clear b3)
)
(:goal
(and
(on b1 b3)
(on b3 b4))
))



