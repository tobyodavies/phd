import json
import os
import subprocess
import tempfile

MODEL = os.path.join(os.path.dirname(__file__), "vartimeline.mzn")
INV_EPSILON = 100
HORIZON = 10000


class Operator(object):
    def __init__(self, idx, name, effects):
        self.idx = idx
        self.name = name
        self.prec = {var: prec for (var, prec, post) in effects if prec != -1}
        self.post = {var: post for (var, prec, post) in effects if prec != post and post != -1}
        self.pair = (self, 0)
        self.invariants = {}
        self.start_op = False
        self.end_op = False
        self.resources = dict()

    def set_starter(self, op, min_dur, max_dur, invariants, resources=None):
        self.pair = (op, max(1, int(INV_EPSILON * min_dur)))
        self.invariants.update(invariants)
        self.prec.update(invariants)
        op.pair = (self, -int(INV_EPSILON * max_dur))
        op.start_op = True
        self.end_op = True
        self.resources = resources if resources is not None else dict()


class Instance(object):
    def __init__(self, var_sizes, res_caps, init, goal):
        self.var_sizes = var_sizes
        self.res_caps = res_caps
        self.ops = {}
        self.starter = {}  # op: (prev_op, min_dt, max_dt)
        self.mkop("INIT", [(var, -1, val) for (var, val) in enumerate(init)])
        self.mkop("GOAL", [(var, val, -1) for (var, val) in dict(goal).items()])

    def mkop(self, name, effects):
        op = Operator(len(self.ops)+1, name, effects)
        self.ops[op.idx] = op
        return op

    def dumps(self):
        lines = []
        lines.extend([
            "num_ops = {};\n".format(len(self.ops)),
            "num_sasvars = {};\n".format(len(self.var_sizes)),
            "sasvar_sizes = {};\n".format(self.var_sizes),
            "num_resources = {};\n".format(len(self.res_caps)),
            "resource_caps = {};\n".format(self.res_caps),
        ])

        vars = range(len(self.var_sizes))
        resources = range(len(self.res_caps))
        precs = [op.prec.get(v, -1) + 1
                 for v in vars
                 for op in self.ops.values()]
        posts = [op.post.get(v, -1) + 1
                 for v in vars
                 for op in self.ops.values()]
        usage = [op.resources.get(r, 0)
                 for r in resources
                 for op in self.ops.values()]

        start_k = [op.pair[0].idx for op in self.ops.values()]
        min_dur = [op.pair[1] for op in self.ops.values()]

        lines.extend(
            [
                "op_prec = array2d(SASVARS, OPS, {});\n".format(precs),
                "op_post = array2d(SASVARS, OPS, {});\n".format(posts),
                "op_usage = array2d(RESOURCES, OPS, {});\n".format(usage),
                "op_pair_kind = {};\n".format(start_k),
                "min_dur = {};\n".format(min_dur),
            ])

        lines.append("invariant = array2d(SASVARS, OPS, [{}]);\n".format(
            ', '.join(
                str(v in op.invariants).lower()
                for v in vars
                for op in self.ops.values()
            )))

        return ''.join(lines)

    def loads(self, string):
        print string
        if '===UNSAT' in string:
            return []
        solns = string.split('----------')
        if solns[-1].strip() == '==========':
            solns = solns[:-1]
        print solns
        return [json.loads(s) for s in solns]

    def init_counts(self, count=None):
        return [4 for v in self.var_sizes]

    def update_counts(self, counts, soln):
        return [
            max(val, soln['next_counts'][var])
            for (var, val) in enumerate(counts)
        ]

    def solve(self):
        counts = self.init_counts()
        lb, ub = 1, 100000
        best = None
        dirname = tempfile.mkdtemp()
        print "Generating intermediate files in ", dirname
        i = 0
        dzn = self.dumps()
        while lb < ub:
            i += 1
            filename = os.path.join(dirname, "planspace-{}.dzn".format(i))
            with open(filename, "w") as fh:
                fh.write(dzn)
                fh.writelines(
                    [
                        "sasvar_counts = {};\n".format(counts),
                        "makespan_lb = {};\n".format(lb),
                        "makespan_ub = {};\n".format(ub),
                    ]
                )
            out = subprocess.check_output(["mzn-chuffed", "-a", "-k", MODEL, filename])
            solns = self.loads(out)
            if not solns:
                break
            lb = min(lb, solns[-1]['makespan'])
            if any(not s['relaxed'] for s in solns):
                ub, best = min((s['makespan'], s) for s in solns if not s['relaxed'])
                print best
            counts = self.update_counts(counts, solns[-1])

        return best

    @classmethod
    def from_sas_task(cls, sas_task):
        indexes = [i for (i, r) in enumerate(sas_task.variables.ranges) if r > 0]

        assert indexes == list(range(len(indexes))), "Duration vars must be last"

        var_sizes = [sas_task.variables.ranges[i] for i in indexes]
        init = [sas_task.init.values[i] for i in indexes]
        goal = {i: v for (i, v) in sas_task.goal.pairs if i in indexes}
        
        inst = cls(var_sizes, init, goal)
        for op in sas_task.operators:
            #TODO: Handle non-temporal case
            pass
        for op in sas_task.temp_operators:
            assert op.assign_effects == [[], []], "Numerics not yet supported"
            start_pre_post = op.pre_post[0]
            end_pre_post = op.pre_post[1]
            start_prevail = op.prevail[0]
            overall_prevail = op.prevail[1]
            end_prevail = op.prevail[2]
            min_d, max_d = -HORIZON, HORIZON
            for dur in op.duration[0]:
                val = sas_task.init.values[dur.var]
                if dur.op in ('=', '<='):
                    min_d = max(min_d, val)
                if dur.op in ('=', '>='):
                    max_d = min(max_d, val)
            s = inst.mkop(op.name, [(var, v0, v1) for (var, v0, v1, _c) in start_pre_post] + [(var, val, -1) for (var, val) in start_prevail])
            e = inst.mkop(op.name + " - end", [(var, v0, v1) for (var, v0, v1, _c) in end_pre_post] + [(var, val, -1) for (var, val) in end_prevail])
            e.set_starter(s, min_d, max_d, overall_prevail)
        return inst


if __name__ == '__main__':
    import sys
    sys.path.append(os.path.join(os.path.dirname(__file__), "tfd"))
    import pddl
    import translate
    pddl = pddl.open()
    sas_task = translate.pddl_to_sas(pddl)
    inst = Instance.from_sas_task(sas_task)
    inst.solve()
