
----------------------- REVIEW 1 ---------------------
PAPER: 115
TITLE: Fragment-Based Planning Using Column Generation
AUTHORS: Toby O. Davies, Adrian R. Pearce, Peter J. Stuckey and Harald Søndergaard

OVERALL EVALUATION: 0 (borderline paper)
Novelty of problem addressed and/or solution proposed: 4 (good)
Technical and/or experimental quality: 4 (good)
Breadth of interest to academics and/or practitioners: 4 (good)
Suitable for the system demonstration session?: 1 (No)

----------- REVIEW -----------
This paper addresses the bulk freight rail scheduling problem (as
defined by the authors).  The approach is to encode a Golog program
that encodes a set of "fragments".  The optimal set of fragments is
chosen by the solution to an IP problem.  The IP is solved via a
branch and price algorithm.  In addition to the rail scheduling
problem, the authors demonstrate the algorithm on a variant of blocks
world.  The authors also compare with several contemporary temporal
planners and show that their approach is effective (mainly because it
incorporates domain knowledge in the Golog programs).

Overall, the paper is well-written and represents a novel approach.
Aside from a few small issues (noted below), the main deficiency is
that a few pieces of related work are missing from the discussion.
Fragment-based planning was a term first coined by Kurien and Smith in
conformant planning.  The motivation was fairly similar there, but the
problem was obviously different.  This work is also very similar to
Brafman and Domshlak's factored planning, and Chen's SGPlan.

Minor comments:

- Introduction, last paragraph: there are three citations that can be
  concatenated with semicolons in the same parenthetical citation.
  Also, it should be "van den Briel", not "Briel".
- Introduction: If modifying 10K lines of C++ code took 9 months,
  then how long will it take to modify your approach?
- "Big-M Penalty Methods" section: "Planning" is capitalized, but
  should not be.


Author response:

The author response did not significantly change my opinion.  However, 
in reading the other reviewer comments, I too feel that the discussion 
of Golog could be improved and make the paper much stronger.


----------------------- REVIEW 2 ---------------------
PAPER: 115
TITLE: Fragment-Based Planning Using Column Generation
AUTHORS: Toby O. Davies, Adrian R. Pearce, Peter J. Stuckey and Harald Søndergaard

OVERALL EVALUATION: 0 (borderline paper)
Novelty of problem addressed and/or solution proposed: 4 (good)
Technical and/or experimental quality: 2 (poor)
Breadth of interest to academics and/or practitioners: 4 (good)
Suitable for the system demonstration session?: 1 (No)

----------- REVIEW -----------
I like the core idea of the paper: perform mixed integer programming to
achieve optimal planning (albeit in a somewhat domain-dependent way). The
results are also promising and indicate that the approach has potential.
The main reason my score is not higher is that I find the description to
be lacking. Almost the entire description of fragment-based planning
makes use of Golog predicates, but the paper does not explain how these
Golog predicates are constructed nor what they mean. In my case, that
makes it near impossible to understand what the algorithm does.

In the text you sometimes denote the initial state in uppercase,
sometimes in lowercase.

c(v) appears to denote the maximum number of *consists* that may occupy a
waypoint, not services.

If you are to use Golog predicates and actions to illustrate the
algorithm, you first have to introduce these concepts to the reader. The
definitions appear convoluted to me: my instinct would be to separate the
predicates "move(...)" and "At(...)", but the definition of "At" appears
to include "move" inline.

What is the intutition behind the rules that you list? Without a
description of the intuition many of the formulas are unreadable to me.

On page 6, what is the notation "++" for fragments?

In the inequality at the top of page 6, what is M?

What do you mean when you say that F_post fragments "immediately
invalidate" the preconditions?

Why does the resource "r" not appear as a parameter in the predicate
"usage"?

Exactly what domain knowledge can be encoded in Golog but not in PDDL?


----------------------- REVIEW 3 ---------------------
PAPER: 115
TITLE: Fragment-Based Planning Using Column Generation
AUTHORS: Toby O. Davies, Adrian R. Pearce, Peter J. Stuckey and Harald Søndergaard

OVERALL EVALUATION: 2 (accept)
Novelty of problem addressed and/or solution proposed: 4 (good)
Technical and/or experimental quality: 4 (good)
Breadth of interest to academics and/or practitioners: 4 (good)
Suitable for the system demonstration session?: 2 (Yes or maybe)

----------- REVIEW -----------
The authors describe a novel adaptation of linear programming to 
address problems in a (near)real-world scheduling (temporal planning) 
domain that exploits both search to find action sequences and the 
global perspective of LP.  Of particular interest, solving the LP 
relaxation 'dual' is used to provide an estimate of the impact of 
consuming additional units of key resources. The planning representation
is modeled in Golog, providing greater flexibility but challenging the 
conventional PDDL intuition of someone steeped in traditional planning.

Much of the front end of the paper is divided between providing some 
background in IP / LP/ MIP and describing the LP & Golog structure to 
permit casting a train scheduling problem as a hybrid linear 
programming/planning problem. My sense is that this is done reasonably 
well given the need to summarize a rich & complex field for an audience
with various degrees of familiarity. However, with only casual knowledge
of LP methods, I'm probably not qualified to identify  shortcomings or 
flaws if they're present.

It's a laborious trip through the section on modeling the railroad 
scheduling problem, followed by 'fragment-based planning' methodology.  
But it's fairly straightforward parsing the Golog routines and the 
pseudo code for the main FBP routines help keep that section from 
getting long-winded. The multi-agent version of blocksworld provides 
some context for comparison with more conventional planning domains; 
here representational downsides of Golog wrt PDDL seem more obvious.

= Results =
The FBP performance is fairly compelling based on the 3 tables of 
results. However at least some of this data would be easier to 
visualize and compare if cast in a suitable graphical form.  
As a minimum, the tables could be clarified by moving more information
into (broadened) headers [e.g. add the word 'vertices', 'orders' 
under the |V|, etc. Clarify Table 4 by adding sub-column labels 
for "1st / Opt" in the header]  At least some of these tables 
should be moved onto the page where they are discussed.

WA* in rh column is undefined.

  =Rebuttal Question=
You present results for 2 (2.5) very different domains. What, if 
anything, can you predict about the effectiveness of FBP in any 
of the numerous other benchmark planning domains?


----------------------- REVIEW 4 ---------------------
PAPER: 115
TITLE: Fragment-Based Planning Using Column Generation
AUTHORS: Toby O. Davies, Adrian R. Pearce, Peter J. Stuckey and Harald Søndergaard

OVERALL EVALUATION: 0 (borderline paper)
Novelty of problem addressed and/or solution proposed: 4 (good)
Technical and/or experimental quality: 3 (fair)
Breadth of interest to academics and/or practitioners: 3 (fair)
Suitable for the system demonstration session?: 1 (No)

----------- REVIEW -----------
This paper looks at using Golog to encode planning problems with 
resource constraints.  Golog generates plan fragments based on 
fragment programs; these are then used with a branch-and-price 
approach.  To support this, the authors describing a planning-specific
pricing problem.  Results compare favorably to domain independent 
planning approaches from the 2011 IPC, and where demonstrated, a 
constraint programming solver (CPX).

Overall, the work in this paper is interesting, from the point of view
of 'what is a planning problem?'; or more specifically, what should 
be the responsibility of state-space search, and what should be the 
responsibility of a MIP.  I particularly appreciated the authors' 
insights in the evaluation, discussing the division of labor between 
master and subproblems (e.g. the negative result in blocksworld).  
The paper also makes a decent effort at explaining the background to
'planning' researchers who would be drawn to the paper by its title, 
but are less familiar with column generation etc.

The evaluation is to me a mixed bag.  The comparison with cpx is 
useful, looking at a 'scheduling' solver, to solve what is in essence 
a scheduling problem.  (Though, I'd be interested to see what happens 
if the memory limit is raised from 4GB - this really isn't very much...).
Lazy clause generation seems a decent choice of benchmark, so the 
apparent value of the presented approach (when compared to that) is 
sound enough.

Comparison to planning was less persuasive.  On the two domains:

-- Blocksworld

I agree a comparison to domain-independent approaches is hardly fair: 
a fairer comparison would be something like TLPlan.  TLPlan can certainly
solve blocksworld problems, though as you note so can YAHSP2, with first
solution in 0.0 seconds (a tough time to beat...).  It would be 
interesting to see some larger blocksworld problems where the time to 
first solution is a few seconds, rather than sub-second.  Even if that 
means popf/CPT don't solve as many problems.  Similarly, to compare to 
optimal planners, as you are concerned about time to find optimal 
solutions.

-- BFRSP

This domain particularly exercises the temporal capabilities of the 
presented approach.  Coming back to the state-space--MIP separation, 
there was a paper at ICAPS 2012 (Tierney et al.) looking at another 
scheduling-type problem, that identified that POPF's treatment of TILs 
was poor, introducing a dummy step of applying the 'next' TIL.  Using 
the language of your paper, it leaves to state-space search the choice 
of when to coordinate TIL steps with the rest of the plan.  This
relates to your note on p7 that:

'popf2 performs very poorly when there is more than one
order to any mine or port. We presume this is because standard
planning heuristics cannot effectively detect the bottleneck
in the track network...'

This presumption might not be the case, given the number of TILs is 
proportional to the number of orders: adding TILs increases (at least)
exponentially the size of POPF's state-space.  Tierney noted that in 
the case where TILs imply time windows (an action can only occur during
an interval in which a TIL is true), it is better to recognize this as 
a special case, remove the TILs from state-space search, and use a MIP 
to solve the problem of finding timestamps for actions that obey the 
time window constraints.  A modified version of POPF doing this was, 
unsurprisingly, much better.

As the TILs in your work are also time window constraints, I would 
expect similar improvements.  As the planners are already disadvantaged 
by not having the domain knowledge captured in Golog, it would be 
slightly less unfair to at least use a planner that tries to do well 
in domains with TIL time windows.

The other option from a planning point of view would be to depart from
state-space search, and consider LPG-TD.  It handles TILs, and did well
on the TIL domains in the 2004 IPC (where TILs were introduced).


Thus, my recommendation comes down to how essential to the paper is the 
comparison to domain-independent planning approaches.  Given the paper
claims to be presenting a planning-with-domain-knowledge approach, I 
think this comparison is important; and at present, the choice of 
comparators in the evaluation doesn't convince me that this is the 
case, with a sufficient degree of confidence.

Misc:

p3: 'and c_{v} = M' - should be c_{v_r}?
p4: missing ) on line 4 of proc travelto


-------------------------  METAREVIEW  ------------------------
PAPER: 115
TITLE: Fragment-Based Planning Using Column Generation

The discussions surrounding this paper focused on the lack of clarity 
in presentation of Golog and, to a lesser extent, the MIP material. 
It was felt that, especially for the former, the authors were depending
on knowledge of Golog that the ICAPS audience didn't have and consequently
the comprehension of the approach was limited. Note that the complaint 
was not that Golog was used, but that the background/notation wasn't 
sufficiently explained to make sense to someone not already steeped 
in Golog. A second, just as important, issue was the fairness of the 
comparisons (as argued in detail by one of the reviewers).

My opinion based on reading the reviews, participating in the discussion 
and skimming the paper is that this is an innovative work that brings a
lot of non-ICAPS-standard tools to bear on a planning/scheduling problem.
While the concerns of the reviewers are valid (and the authors should do
their best to address them), in my judgment, the novelty and 
interestingness of the paper makes up for the imperfections correctly 
identified by the reviewers.

A technical point: the authors describe column generation well but 
then are ambiguous as to branch-and-price, describing it as a 
branch-and-bound-like procedure. A critical point that must be made 
clear: is the pricing problem re-solved at each node in the tree search?
If not, then this is not branch-and-price and more importantly an optimal
solution cannot be guaranteed. The technique of generating columns in 
the root node and then running B&B without generation of further columns
is something that is done in mathematical programming to find high 
quality feasible solution for very large, hard problems. So this 
distinction exists, is important, and should be made clear.

