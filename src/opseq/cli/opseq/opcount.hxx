#include <strips_prob.hxx>
#include <action.hxx>
#include <map>
#include <unordered_map>
#include <sstream>
#include <cmath>
#include "gurobi_c++.h"

#include <action.hxx>
#include <fluent.hxx>


namespace opseq {

#define MAX_OPCOUNT 10000
using aptk::Action;
using aptk::Fluent;

class OpCounter;

 class OpCounter {
public:
	OpCounter(std::vector<Action*>& actions, std::vector<Fluent*>& facts);

	GRBVar tnf_count(unsigned op_or_f) { return count(op_or_f); }
	GRBVar count(unsigned op) { return model.getVar(op); }
	const GRBVar count(unsigned op) const { return model.getVar(op); }

	GRBVar countGE(unsigned op, unsigned lb);

        bool safeCount(unsigned op, unsigned lb) const;

	double countVal(unsigned op) const {
		return count(op).get(GRB_DoubleAttr_X);
	}

	void addConstr(const GRBTempConstr& c, string name = "") {
		delayed.emplace_back( std::make_pair(c, name) );
	}
	void addLM(const std::vector<unsigned>& ops);
	void addLM(const std::vector<const Action*>& ops);
	bool optimize();
	void addDelayed();

	GRBModel& getModel() { return model; }

	unsigned num_ops() { return bounds.size(); }
	unsigned num_facts(){ return n_facts; }

	double getLB() const { return lb; }
	void setLB(double lb_){
		if(lb_ < (double)lb) return;
		unsigned lb_cand = unsigned(lb_ + .95);
		if(lb < lb_cand){
			lb = lb_cand;
			std::cerr << "New LB: " << lb << std::endl;
			lb_constr.set(GRB_DoubleAttr_RHS, lb);
		}
	}


protected:
	GRBEnv env;
	GRBModel model;
	GRBVar total_ops;

	GRBConstr lb_constr;
	unsigned lb;

	std::vector< std::vector<GRBVar> > bounds;
	std::vector<GRBConstr> bounds_constrs;

	std::vector< std::pair<unsigned, unsigned> > delayed_dom;
	std::vector< std::pair<GRBTempConstr, std::string> > delayed;

	int n_facts;

	

};

class LinRelaxSentry {
public:
	LinRelaxSentry(GRBModel& m): m(m){
		int numVars = m.get(GRB_IntAttr_NumVars);
		var_types.reserve(numVars);
		for(int i = 0; i < numVars; i++){
			auto v = m.getVar(i);
			var_types.push_back(v.get(GRB_CharAttr_VType));
			v.set(GRB_CharAttr_VType, GRB_CONTINUOUS);
		}
	}

	~LinRelaxSentry(){
		int i=0;
		for(char type : var_types){
			auto v = m.getVar(i++);
			v.set(GRB_CharAttr_VType, type);
		}
	}
private:
	GRBModel& m;
	std::vector<char> var_types;
};

};
