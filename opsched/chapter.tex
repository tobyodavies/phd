%% \chapter{Scheduling Operator Counts}
%% \label{chp:opsched}
%% \begin{ch-abstract}
%% TODO
%% \end{ch-abstract}
%\pagebreak
\section{Extension: Operator Scheduling}
\label{chp:opsched}

Temporal planning solves a similar class of problems to alternative
scheduling, but is typically modelled differently.  In this section we
will show how temporal planning problems modelled in a temporal SAS+
formalism can be transformed into a sequence of alternative scheduling
problems.

Temporal SAS+ is an extension of classical SAS+ where durative actions are
treated as a pair of start and end events, each having preconditions and
postconditions just like regular SAS+ \cite{eyerich2009}.
In addition to these instantaneous preconditions, durative actions can
have invariants: preconditions which must hold for the duration of the
action. 

Other planners use a Linear Program to solve a simple scheduling
problem as part of a forward search, most notably the related popf and
optic planners \cite{coles2010,benton2012}.

Of more immediate relevance are planners which compile to constraint
programming formulations, of particular interest is CPT, which tackles
temporal problems \cite{vidal2006} and the recently introduced PaP and
TCPP planners which treat each SAS+ variable as an independent
timeline which are explicitly synchronised
\cite{bartak2011,ghanbari2017}.

\subsection{A linear-size CP encoding of partial-order planning}

We present an encoding that combines aspects of these approaches in
a way that enables the addition of resource constraints that can
be tackled with existing scheduling constraints. We observe that each
variable must take exactly one value at any time. This might be
represented in a typical scheduling problem as a disjunctive
constraint.

Given this observation we can treat the planning problem given a fixed
set of actions as a scheduling problem, and we can use an
estimate of the number of required copies of an operator generated,
for example, by an operator-counting heuristic.
We use $\AC(o)$ to denote the number of copies in the current restricted problem.
We also slightly abuse notation to use $\AC(X)$ to denote the total number of actions
which change the value of variable $X$.

We then encode the scheduling model as a set of events: the start and
end of the application of each action.  We use $o_{i, \vdash}$ and
$o_{i, \dashv}$ to denote the events corresponding to the start and
end of the $i$-th application of operator $o$.

We use $prec(o_\vdash)$, $prec(o_\dashv)$ for the preconditions of the start and end of operators,
and $post(o_\vdash)$, $post(o_\dashv)$ for their postconditions.
We assume that any ``over-all'' precondition $X=x$ of an operator $o$ is also a precondition of the end event,
and satisfies the predicate $invar(o, X)$.
We will sometimes use events in place of operators in these functions as a shorthand,
so, for example, $prec(o_{i, \dashv})$ should be considered equivalent to $prec(o_\dashv)$.

These events define the starts and ends of various time-windows in which each variable takes a specific value.


We use the following variables in our model:
\begin{itemize}
\item $Seq(X, i)$, the $i$-th event effecting variable $X$.
\item $Val(X, i)$, the value of $X$ after the $i$-th event effecting it.
\item $S(X, i)$, the time the $i$-th value of $X$ is added.
\item $E(X, i)$, the time the $i$-th value of $X$ is deleted.
\item $InPlan(e)$, true iff $e$ is included in the plan.
\item $SI(X, e)$, the $i$ such that $Seq(X, i)$ is the supporter of precondition $X=Val(X,i)$ for event $e$.
\item $EI(X, e)$, the $i$ such that $Seq(X, i)$ is the effect of event $e$ on $X$.
\item $T(e)$, the time at which $e$ occurs.
\item $\Delta(o, i) = T(o_{i,\dashv}) - T(o_{i,\vdash})$, the duration of the $i$-th application of operator $o$.
\end{itemize}
All of these variables which are a function of an event are optional integers \cite{mears2014}, conditional on $InPlan(e)$. 

These variables then participate in the following constraints:
\begin{align}
\forall X \in V: &\quad increasing([S(X, i) | i \in \{1..\AC{X}\}]) \label{eq:timeline-inc-s}\\
\forall X \in V: &\quad increasing([T(o_{\vdash,i}) | i \in 1..\AC(o)]) \label{eq:op-start-inc}\\
\forall e \in E, X=x \in prec(e): &\quad InPlan(e) \Rightarrow S(X, SI(X, e)) < T(e) \le E(X,SI(X, e)) \label{eq:after-supports}\\
\forall e \in E, X=x \in post(e): &\quad InPlan(e) \Rightarrow S(X,EI(X, e)) = T(e) \label{eq:add-time}\\
\forall X \in V, i \in \{1..\AC(X)\}: &\quad E(X,i) \le S(X, i+1) \label{eq:del-time}\\
\forall o_{i_\dashv} \in E, X=x \in prec(e): &\quad invar(o, X) \Rightarrow T_S(X, o_{i,\dashv}) \le T(o_{i, \vdash}) \label{eq:invar} \\
\forall e \in E, X=x \in prec(e): &\quad InPlan(e) \Rightarrow Val(X, SI(X, e)) = x \label{eq:prec}\\
\forall e \in E, X=x \in post(e): &\quad InPlan(e) \Rightarrow Val(X, EI(X, e)) = x \label{eq:post}\\
\forall o \in O, i \in \{1..\AC(o)\}: & \quad InPlan(o_{\dashv,i}) \equiv InPlan(o_{\vdash,i}) \label{eq:start-and-end}\\
\forall X \in V, i \in \{1..\AC(X)\}: &\quad \lnot InPlan(Seq(X, i)) \Rightarrow Val(X, i) = Val(X, i-1) \label{eq:noop}\\
\forall X \in V, i \in \{1..\AC(X)-1\}: &\quad \lnot InPlan(Seq(X, i)) \Rightarrow \lnot InPlan(Seq(X, i+1)) \label{eq:noops-at-end}\\
\forall X=x \in s_0: &\quad Val(X, 0) = x \label{eq:init}\\
\forall X=x \in s_*: &\quad Val(X, \AC(X)) = x \label{eq:goal}
\end{align}

The objective is then to minimise $max(\{T(Seq(X, \AC(X))) | X \in V\})$.

Equation \ref{eq:timeline-inc-s} ensures
that the start times for each window in the timeline for variable X
are in order.
Equation \ref{eq:op-start-inc} breaks symmetries between the order in
which different copies of an action can occur.
Equation \ref{eq:after-supports} ensures that an event must occur
strictly after its preconditions are satisfied, and before they next
become unsatisfied.
Equations \ref{eq:add-time}, and \ref{eq:del-time} ensure
that the time-windows in which variables take a specific value are
equal to the event times of the causes of these changes.
Equation \ref{eq:invar} ensures that any ``over all'' preconditions of
actions are satisfied no later than the start of the action.
Equations \ref{eq:prec} and \ref{eq:post} ensure that preconditions
and postconditions of an event hold on all relevant variables before
and after each event.
Equation \ref{eq:start-and-end} ensures that any operator that starts must also finish, and vice-versa.
Equations \ref{eq:noop} and \ref{eq:noops-at-end} ensure respectively
that actions not occuring in the plan have no effect on the value of a
variable, and that such non-occuring actions occur at the end of the
sequence.
Equations \ref{eq:init} and \ref{eq:goal} ensure the initial and final
values of each timeline are consistent with the initial and goal
states respectively.


Note that the number of variables and constraints in this encoding
both grow linearly in $E$.  This is in contrast with CPT's similar
encoding which requires a constraint for every pair of potentially
conflicting actions \cite{vidal2006}.
This is not necessarily asymptotically faster, as the domain of the
variables in our encoding also increases linearly with $E$, but requires less
memory and is typically faster in practice, all else being equal.

The remaining challenge is to choose appropriate action counts.  In
this choice we have two options based on ideas developed in this
thesis: Use an operator counting heuristic and learn generalised
landmarks as discussed in chapter \ref{chp:opseq}; or, relax the
scheduling sub-problem in such a way that the relaxation may be refined
by restricting any relaxed operators chosen in the solution, and
adding more events.

The former approach is theoretically straightforward assuming the
reader is familiar with chapter \ref{chp:opseq}.
The alternative involves relaxing the precondition constraints for
events supported by the last event in a variable sequence.
This can be considered similar to the work of \citeauthor{robinson2010}
(\citeyear{robinson2010}) in optimal SAT planning which adds a
delete-relaxed suffix to a layer-based SAT formula.


The latter approach requires that we first transform our CP model from
a restriction of the full planning problem into a relaxation.
To this end we add a delete-relaxation to our model.



\subsection{Partial-order relaxation}

We can't simply apply a delete-relaxed ``suffix'' to our plan as there isn't necessarily
a single state where we can sensibly start using delete-relaxed operators.
Instead we allow delete-relaxed actions to be interleaved with regular actions.
To achieve this we add one delete relaxed copy of each operator to our
action counts, we refer to the set of these relaxed events as $E^+$,
and $o^+_\vdash$ and $o^+_\dashv$ to represent the start and end
events derived from relaxing operator $o$.

In order to accommodate relaxed actions in our partial-order formulation, we modify constraint \ref{eq:del-time} to read:
\begin{align}
\forall X \in V, i \in \{1..\AC(X)\}: &\quad (Seq(X,i) \not\in E^+) \Rightarrow E(X,i) \le S(X, i+1) \label{eq:del-time-ext}\\
\end{align}
This change allows the effect of an event to persist indefinitely, in-spite of $X$ taking other values at later times.
We must also modify the goal achievement constraint, equation \ref{eq:goal} to read
\begin{align}
\forall X=x \in s_*: \exists i. Val(X, i) = x \land (i = \AC(X) \lor Seq(X,i) \in E^+) \label{eq:del-goal}\\
\end{align}

This is slightly different to a traditional delete-relaxation where previous values remain true after the relaxed action,
but this variant makes it simpler to prove that this is in fact a relaxation of the full planning problem.
Nonetheless the resulting relaxation is sufficiently similar we do not feel it warrants a distinct name.

However this is not sufficient: the optimal solution to this program
would simply compute the optimal delete relaxed plan, regardless of
the other operators available.
This precludes the use of this model in a relaxation-refinement approach
as it will not converge to a true solution to the problem.

To address this we must add two new constraints:
\begin{align}
\forall o \in O: &\quad T(o_{\vdash,\AC(o)}) < T(o^+_\vdash) \\
\forall o \in O: &\quad \lnot InPlan(o_{\vdash,\AC(o)}) \Rightarrow \lnot InPlan(o^+_\vdash)
\end{align}
This requires the solution to use real operators before any delete-relaxed operators of the same type.

This model is a relaxation of the original planning problem regardless of the action count:
any plan for the original problem can be transformed into a plan for
this model by replacing the $\AC(o)+1$-th application of an operator $o$
in the original plan with $o^+$ in the relaxed plan, and omitting all subsequent applications of $o$.
All operators omitted this way are redundant,
as all of the positive effects will already be available earlier as a result of the relaxed action.

%% A solution to this partially delete-relaxed problem, will either
%% achieve the goal without using any delete-relaxed actions,
%% in which case the original problem is solved; or it will contain some
%% number of delete-relaxed actions which can be added to the next $\AC$,
%% and the problem can be re-solved.
%Since the solver must use 

This partially-relaxed model allows us to apply a relaxation-refinement approach:
First the optimal solution to the relaxed model is obtained.
If this contains any delete-relaxed actions, a new model is constructed with one more real copy of each relaxed operator used.
Otherwise the solution to the relaxed model is also a solution to the original problem, and thus must be optimal.

This approach has no fixed-points other than solutions: if an
operator's count is incremented, the previous solution is no-longer
valid in this or any future model because at least one more real copy of
the operator must be used strictly before a delete-relaxed version may be applied.


\subsection{Conclusions and Further Work}

We have introduced a relaxation-refinement approach to temporal planning that
solves the planning problem by solving a series of optional scheduling problems.
The results of these solves inform the way that the relaxation is refined.
Our model requires only a linear number of variables in the number of events being scheduled.

Unlike CPT, our approach does not rely on a specialised solver to carefully propagate
some constraints only in certain directions \cite{vidal2006}.
Having such a compilation of temporal planning to general CP allows us
to consider new ways to integrate existing scheduling constraints used
in resource-constrained scheduling problems.

One such approach is to add envelope-actions \cite{coles2009} which
could be annotated to indicate resource consumption, allowing us to add
constraints like:
\[
\forall R: cumulative([\tup{T(o_{i,\dashv}),\Delta(o, i), k} | \tup{o, k} \in requires(R), i \in \AC(o)], capacity(R))
\]
where $requires(R)$ is the set of \tup{o,k} pairs where operator $o$
that consumes $k$ units of a reusable resource with a total concurrent
usage limit of $capacity(R)$.
In our running gripper example, we could use an envelope action to represent ``holding(ball)'',
which could allow us to represent the capacity to hold several balls simultaneously.
This constraint could safely be applied to the delete-relaxed actions
in addition to real actions.  However care will need to be taken in
considering the interaction between such constraints and the delete
relaxation in the general case.

We had hoped that the linear-size encoding would counteract the
overhead of the MiniZinc modelling language, as compared to CPT's
highly specialised and optimised CP system.  However, since CPT only
supports basic concurrency with fixed action duration and no required
concurrency, our preliminary experiments suggest that the additional
overhead of supporting required concurrency largely eliminates this
benefit.  Additionally existing temporal planning benchmarks are not
ideally encoded for this approach, and we would need to investigate
specialised domains which truly exploit the potential for adding
scheduling constraints in order to demonstrate the true value of
this approach.
