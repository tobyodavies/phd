import time

import pymas
import math
from pymas.test.bulkfreight import solve, BulkFreight
import pymas.test.blocksworld as bw

def test_system():
    norders = len(BulkFreight.orders)
    init = list(solve(nfrags=int(math.log(norders)+2)))
    #init = list(solve(nfrags=10))
    #init = []
    selector = pymas.FragSelector(init, solve)
    print "#frags:", len(selector.fragments)
    initial_cost, selection = selector.heuristic_select()
    obj, _, __ = selector.packer.solve_lp()
    cost = selector.packer.cost(selection)
    print "Heuristic cost:", initial_cost, "(%d)"%len(selection)
    selector.optgap = .005

    try:
        obj, cost, selection = selector.solve(max_cols=int(math.log(norders)/2+1))
        #obj, cost, selection = selector.solve(max_cols=1)
    except KeyboardInterrupt:
        cost, selection = selector.heuristic_select()
        obj, _, __ = selector.packer.solve_lp()

    print "# resources:", len(set(r for f in selection for r in f.resources))
    print "makespan:", max(f.end_time for f in selection)
    print "endtimes:", sorted([f.end_time for f in selection])
    print "orders:", len(list(BulkFreight.orders))
    print "fulfiled:", (list(k
                             for k in BulkFreight.orders
                             for f in selection
                             for r in f.resources
                             if r.label == ('order_unload', k)))
    print "unfulfiled:", set(k
                             for k in BulkFreight.orders
                             for f in selection
                             for r in f.resources
                             if r.label == ('order_unload', k)) - set(BulkFreight.orders)
    print "len(soln):", len(selection)
    print "total actions:", sum(f.nactions for f in selection)
    print "cost:", cost
    print "lpcost:", obj
    print "#frags:", len(selector.fragments)
    assert len(selection) == len(BulkFreight.orders)


def test_heuristic():
    norders = len(BulkFreight.orders)
    init = list(solve(nfrags=norders))
    #init = list(solve(nfrags=10))
    #init = []
    selector = pymas.FragSelector(init, solve)
    print "#frags:", len(selector.fragments)
    initial_cost, selection = selector.heuristic_select()
    selector.solve_lp()
    initial_cost, selection = selector.heuristic_select()
    obj, _, __ = selector.packer.solve_lp()
    cost = selector.packer.cost(selection)
    print "Heuristic cost:", initial_cost, "(%d)" % len(selection)

    print "# resources:", len(set(r for f in selection for r in f.resources))
    print "makespan:", max(f.end_time for f in selection)
    print "endtimes:", sorted([f.end_time for f in selection])
    print "orders:", (list(BulkFreight.orders))
    print "fulfiled:", (list(k
                                for k in BulkFreight.orders
                                for f in selection
                                for r in f.resources
                                if r.label == ('order_unload', k)))
    print "unfulfiled:", (set(BulkFreight.orders) -
                          set(k
                              for k in BulkFreight.orders
                              for f in selection
                              for r in f.resources
                              if r.label == ('order_unload', k)))

    print "len(soln):", len(selection)
    print "total actions:", sum(f.nactions for f in selection)
    print "cost:", cost
    print "lpcost:", obj
    print "#frags:", len(selector.fragments)

    assert len(selection) == len(BulkFreight.orders)


def test_heuristic_bw():
    start_t = time.time()
    nblocks = len(bw.BlocksAgent.blocks)
    init = list(bw.solve(nfrags=nblocks))
    #init = list(solve(nfrags=10))
    #init = []
    selector = pymas.FragSelector(init, bw.solve)
    print "#frags:", len(selector.fragments)
    #selector.solve()
    for _ in xrange(5):
        satis = (lambda s: len(s) == nblocks)
        initial_cost, selection = selector.dive_heuristic_select(satisfices=satis)
        print "#frags:", len(selector.fragments)
        if satis(selection):
            break
    obj, _, __ = selector.packer.solve_lp()
    cost = selector.packer.cost(selection)
    print "Runtime:", time.time() - start_t
    print "Heuristic cost:", initial_cost, "(%d)" % len(selection)

    print "# resources:", len(set(r for f in selection for r in f.resources))
    print "makespan:", max(f.end_time for f in selection)
    print "endtimes:", sorted([f.end_time for f in selection])
    print "len(soln):", len(selection)
    print "total actions:", sum(f.nactions for f in selection)
    print "cost:", cost
    print "lpcost:", obj
    print "#frags:", len(selector.fragments)

    assert len(selection) == len(bw.BlocksAgent.blocks)

if __name__ == '__main__':
    test_heuristic_bw()
