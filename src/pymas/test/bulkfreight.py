"""
Simple domain for bulk freight
"""

import collections
from os import path
import random
import operator
import time

import pymas
from pymas import VarChoice, Var, NOOP, action, situation, fluent, proc, Assert
from pymas.utils import LinkedList, reduced_cost

### UTILS
def pypy_parse_dot(filename):
    """
    networkx's dot parser breaks horribly in pypy
    so we parse it with python2.7 and pickle to stdout
    """
    import subprocess
    import pickle
    dirname = path.dirname(__file__)
    script = path.join(dirname, "pickle_dot.sh")
    filename = path.join(dirname, filename)
    proc = subprocess.Popen([script, filename], stdout=subprocess.PIPE)
    proc.wait()
    return pickle.loads(proc.stdout.read())


def integrality(num):
    return abs(num - .5) / .5


class BulkFreight(situation('BulkFreight')):
    """
    Model class

    >>> sit0 = BulkFreight.s0()
    >>> sit0.enter('d')
    enter('d')
    >>> actions = set(BulkFreight.actions())
    >>> len(actions)
    4
    >>> assert sit0.enter in actions
    """

    graph = pypy_parse_dot("networks/test_trivial.dot")
    orders = [(m, p, t0, t0+10)
              for m in ('m1', 'm2')
              for p in ('p2', 'p1')
              for t0 in xrange(0, 24, 12)]
    _state = None

    @fluent
    def cur_section(self):
        if self is self.s0():
            return 'y'
        act = self.head
        if isinstance(act, BulkFreight.enter):
            return act.args[0]
        return self.tail.cur_section()

    @fluent
    def loaded(self):
        for act in self:
            if isinstance(act, self.load):
                return act.args[0]

    @fluent
    def at(self, loc):
        return self.cur_section() == loc

    @fluent
    def time(self):
        time = 0
        for act in self:
            if isinstance(act, self.wait):
                time += act.args[0]
            else:
                time += 1
        return time

    @action
    def enter(self, section):
        cur_section = self.cur_section()
        if section in self.graph[cur_section]:
            res = {self.shared('sec', section, self.time()): 1}
            if section == 'y':
                res = {}
            return 1, res

    @enter.min_res_func
    def _enter(self, section):
        if section == 'y':
            return {}
        return {self.shared('sec', section, self.time()): 1}

    @action
    def load(self, order):
        mine, _port, stime, etime = order
        if self.at(mine) and stime <= self.time() <= etime:
            return -5000, {self.shared('order_load', order): 1}
    load.MINCOST = -5000

    @load.min_res_func
    def _load(self, order, *args):
        #print self, order, args
        return {self.shared('order_load', order): 1}

    @action
    def unload(self, order):
        _mine, port, stime, etime = order
        if self.loaded() == order and self.at(port) and stime <= self.time() <= etime:
            return -5000, {self.shared('order_unload', order): 1}
    unload.MINCOST = -5000

    @unload.min_res_func
    def _unload(self, order):
        return {self.shared('order_unload', order): 1}

    @action
    def wait(self, time=1):
        if self.at('y'):
            return time, {}
        return time, {self.shared('sec', self.cur_section(), t): 1
                      for t in xrange(self.time(), self.time() + time)}
    wait.min_cost = (lambda a, sit, __: a.args[0])

    @proc
    def travel_to(self, loc, depth=0, visited=None):
        visited = (visited if visited is not None
                   else LinkedList.nil())
        nxt_loc = Var([n for n in self.graph[self.cur_section()]
                       if n not in visited])
        step = VarChoice(nxt_loc, self.enter(nxt_loc))
        repeat = BulkFreight.travel_to(loc, depth+1,
                                       visited.cons(self.cur_section()))
        arrived = BulkFreight.at(loc)
        return step + (Assert(arrived) |
                      (Assert(~arrived) + repeat))
        #return step.repeat_while(~arrived)

    @proc
    def maybe_wait(self, *args):
        waits = Var([t for t in xrange(*args) if t > 0])
        return (NOOP | VarChoice(waits, self.wait(waits)))


    def satisfy_order(self, order):
        mine, port, min_t, max_t = order
        return (self.maybe_wait(min_t-20, max_t-5) +
                BulkFreight.travel_to(mine) +
                self.maybe_wait(5) +
                self.load(order) +
                self.maybe_wait(5) +
                BulkFreight.travel_to(port) +
                self.maybe_wait(5) +
                self.unload(order) +
                self.maybe_wait(5) +
                BulkFreight.travel_to('y'))

    def subgoals(self, duals=None):
        duals = duals or collections.Counter()
        goals = []
        orders = list(self.orders)
        random.shuffle(orders)
        #print orders
        sgoals = []
        for order in orders:
            sgoals.append(self.satisfy_order(order))
        sortorder = sorted([i for i in xrange(len(orders))],
                           key=lambda i: sgoals[i].lowbound(self, duals))
        #print "orders: ",
        #print [(orders[i], sgoals[i].lowbound(self, duals)) for i in sortorder]
        ordervar = Var([orders[i] for i in xrange(len(orders))])
        return [sgoals[i] for i in sortorder]

    @fluent
    def has_unloaded(self):
        if self:
            return isinstance(self.head, self.unload) or self.tail.has_unloaded()


NFRAGS = 0
TOTAL_TIME = 0


def solve(duals=None, min_rc=0, existing_solutions=None, nfrags=10000):
    global NFRAGS
    global TOTAL_TIME

    s0 = BulkFreight.s0()
    duals = duals or collections.Counter()
    solution_seq = [prog.solutions(s0, duals, solution=s0, min_rc=min_rc,
                                   existing_solutions=existing_solutions)
                    for prog in s0.subgoals(duals)]
    i = 0
    solns = 0
    fails = 0
    tt0 = TOTAL_TIME
    t0 = time.time()
    while solution_seq:
        seq_idx = i % len(solution_seq)
        soln = next(solution_seq[seq_idx], None)
        if soln:
            frag = soln.to_fragment()
            frag.end_time = soln.time()
            if reduced_cost(duals, frag) < 0:
                #print soln.cost(), soln.cost(duals)
                #print soln
                solns += 1
                #print "Time to frag %d"%(solns,), time.time() - t0, "sec"
                frag.nactions = len(soln)
                frag.sit = soln

                NFRAGS += 1
                TOTAL_TIME += time.time() - t0
                yield frag
                t0 = time.time()
                if reduced_cost(duals, frag) < -1:
                    nfrags -= 1
                if nfrags <= fails:
                    break
        else:
            solution_seq.pop(seq_idx)
            fails += 1
        if solns and fails:
            break
        i += 1

    print "Time to fail", TOTAL_TIME - tt0, "sec"
    print "Avg time per frag:", TOTAL_TIME/(NFRAGS+.000001)
