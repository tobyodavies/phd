#include <memory>
#include <string>
#include <opcount.hxx>
#include <flow_abstraction.hxx>

namespace opseq {

#define BIN GRB_BINARY
#define INT GRB_INTEGER

OpCounter::OpCounter(std::vector<Action*>& actions, std::vector<Fluent*>& facts):
	env(GRBEnv()),
	model(GRBModel(env)),
	lb(0),
	bounds(actions.size()),
	n_facts(facts.size())
{
	//model.getEnv().set(GRB_IntParam_MIPFocus, 1); // Find Primal Solutions
	//model.getEnv().set(GRB_IntParam_MIPFocus, 2); // Prove Optimality
	model.getEnv().set(GRB_IntParam_MIPFocus, 3); // Improve LB
	model.getEnv().set(GRB_IntParam_Method, 1); // Dual Simplex
	model.getEnv().set(GRB_IntParam_Threads, 1);
	model.getEnv().set(GRB_IntParam_BranchDir, -1); // Down first
	model.getEnv().set(GRB_DoubleParam_MIPGap, 1e-8);
	model.getEnv().set(GRB_DoubleParam_MIPGapAbs, 0.95);

	std::unique_ptr<GRBVar> countvars(model.addVars(actions.size(),
							INT));
	std::unique_ptr<GRBVar> tnf_vars(model.addVars(facts.size(),
						       INT));
	std::unique_ptr<GRBConstr> boundsconstrs(model.addConstrs(actions.size()));

	total_ops = model.addVar(0, MAX_OPCOUNT, 0, INT, "total_ops");
	std::copy(boundsconstrs.get(),
		  boundsconstrs.get() + actions.size(),
		  std::back_inserter(bounds_constrs));
	model.update();
	total_ops.set(GRB_IntAttr_BranchPriority, 10);
	GRBLinExpr total_constr;
	GRBLinExpr obj_expr;
	for(unsigned op = 0 ; op < actions.size(); ++op) {
		auto& v = *(countvars.get()+op);
		total_constr += v;
		obj_expr += actions[op]->cost() * v;
		v.set(GRB_DoubleAttr_Obj, actions[op]->cost() + 1e-5);
		v.set(GRB_StringAttr_VarName, actions[op]->signature());
		model.chgCoeff(bounds_constrs[op],
			       v,
			       -1);
		v.set(GRB_IntAttr_BranchPriority, 5);
	}
	model.update();
	for(unsigned f = 0 ; f < facts.size(); ++f) {
		auto& v = *(tnf_vars.get()+f);
		v.set(GRB_StringAttr_VarName, "Forget_" + facts[f]->signature());
	}
	model.addConstr(total_constr == total_ops);
	lb_constr = model.addConstr(obj_expr >= 0);

	for(unsigned op = 0 ; op < actions.size(); ++op) {
		countGE(op, 5);
	}
	model.update();
	addDelayed();
}

GRBVar OpCounter::countGE(unsigned op, unsigned lb){
	assert(lb >= 1);
	auto& bvec = bounds[op];
	int new_vars = lb - bvec.size();
	const auto& varname = count(op).get(GRB_StringAttr_VarName);
	for (int i = 0, k = bvec.size() + 1 ; i < new_vars; i++, k++) {
		bvec.emplace_back(model.addVar(0, 1, -1e-6/k, BIN,
					       varname + " ge " + std::to_string(k)));
		delayed_dom.push_back( std::make_pair(op, k) );
	}
	return bounds[op][lb - 1];
}


bool OpCounter::safeCount(unsigned op, unsigned lb) const {
	return bounds[op].size() > lb;
}

void OpCounter::addDelayed(){
	model.update();
	for (auto& op_bound : delayed_dom){
		unsigned op, k;
		std::tie(op, k) = op_bound;
		auto& bvec = bounds[op];
		int bv_offset = k - 1;
		auto tmp_c = bvec[bv_offset] <= bvec[bv_offset - 1];
		if(bv_offset > 0){
			model.addConstr(bvec[bv_offset] <= bvec[bv_offset - 1]);
		}
		if(k == 1){
			bvec[bv_offset].set(GRB_IntAttr_BranchPriority, 100);
		} else {
			bvec[bv_offset].set(GRB_IntAttr_BranchPriority, 0);
		}
		// bvec[bv_offset].set(GRB_DoubleAttr_Obj, -1e-6/k);
		// bvec[bv_offset].set(GRB_DoubleAttr_UB, 1.0);
		
		model.chgCoeff(bounds_constrs[op], bvec[bv_offset], 1.0);
		model.addConstr(count(op) <= MAX_OPCOUNT*bvec[bv_offset] + k - 1);
	}
	delayed_dom.clear();
	model.update();
	for(auto& tc : delayed){
		model.addConstr(tc.first, tc.second);
	}
	model.update();
	delayed.clear();
}

bool OpCounter::optimize() {
	addDelayed();

	model.optimize();
	if(model.get(GRB_IntAttr_IsMIP)){
		setLB(model.get(GRB_DoubleAttr_ObjBound));
	}

	#ifdef DEBUG
	model.write("model.lp");
	bool infeasible = model.get(GRB_IntAttr_Status) == GRB_INFEASIBLE;
	if (infeasible){
		model.computeIIS();
		model.write("model.ilp");
	}
	#endif

	if(model.get(GRB_IntAttr_Status) == GRB_INTERRUPTED) {
		return optimize();
	} else {
		return model.get(GRB_IntAttr_Status) == GRB_OPTIMAL;
	}
}

void OpCounter::addLM(const std::vector<const Action*>& ops){
	GRBLinExpr lm;
	for( auto op : ops ){
		lm += countGE(op->index(), 1);
	}
	addConstr(lm >= 1);
}

void OpCounter::addLM(const std::vector<unsigned>& ops){
	GRBLinExpr lm;
	for( auto op : ops ){
		lm += countGE(op, 1);
	}
	addConstr(lm >= 1);
}

}
