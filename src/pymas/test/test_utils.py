"""
Tests for linked lists and search queues
"""


import collections
import sys


import pytest

from pymas.utils import LinkedList, SearchQ


def test_linkedlist():
    ll1 = LinkedList.nil()
    ll2 = LinkedList.nil()
    for i in xrange(10):
        assert len(ll1) == len(list(ll1))
        ll1 = ll1.cons(i)
        ll2 = ll2.cons(i)
        assert list(ll1) == list(reversed(range(i+1)))
        assert bool(ll2) == bool(range(i+1))
        assert ll1 == ll2


def test_searchq_push():
    q = SearchQ()

    assert not q
    for i in range(10):
        q.push(i+1, i, i)
    assert q
    assert len(q) == 10


def test_searchq_prune():
    q = SearchQ()

    assert not q
    for i in range(10):
        q.push(i+1, i, i)

    for i in xrange(9, -1, -1):
        q.add_solution('xyz', i)
        assert len(q) == i


def test_searchq_init():
    q = SearchQ([i for i in xrange(10)],
                heuristic=lambda x: (x, x))

    assert len(q) == 10


def test_searchq_search():
    Branch = collections.namedtuple('Branch', ['cost', 'curval'])

    def expand(b):
        est = b.cost + abs(10 - b.curval)
        return [(est, est-d, Branch(b.cost+1, m*b.curval+d))
                for m, d in [(-1, 0), (1, 1)]]

    def heuristic(b):
        if b.curval == 10:
            return b.cost + abs(10 - b.curval) + 1, 'xxx'
        return None, None

    q = SearchQ([Branch(0, 1)],
                expand=expand,
                heuristic=heuristic)
    assert len(list(q.solutions())) >= 1
