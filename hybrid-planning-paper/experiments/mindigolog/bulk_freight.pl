
%%%% A model for bulk-freight scheduling
file_search_path(mindigolog, 'mindigolog/').

:- use_module(mindigolog('clpqr/clpq')).

:- discontiguous(trans/4, final/2, prim_action/1, natural/1, poss/3,
                 conflicts/3, start/2,proc/2).

%%
%%  Provide Syntactic operators for MIndiGolog programs
%%
%%  These operators form the "syntactic sugar" for MIndiGolog programs
%%
:- op(660,xfy,/).  % Nondeterministic choice
:- op(650,xfy,:).  % Sequence
:- op(640,xfy,//). % Concurrent execution
:- op(640,xfy,>>). % Prioritised concurrency
:- op(620,fx,?).   % Test

%%
%%  Include the background domain
%%
:- include(mindigolog(mindigolog)).
:- include(mindigolog(sitcalc)).


%%% BEGIN DOMAIN

%% Track Network
%%
%% p1        m1
%%  \        /
%%   y ==== d
%%  /        \
%% p2        m2

track_node(X) :-
 member(X, [p1, p2, d, y, m1, m2]).

% track_section(?Section, ?From, ?To, ?MinDuration, ?MaxDuration)

%dual track
track_section(y_2_d, y, d, 1, 3).
track_section(d_2_y, y, d, 1, 3).

%load-loops
track_section(m1, d, d, 1, 2).
track_section(m2, d, d, 1, 2).
%unload-loops
track_section(p1, y, y, 1, 2).
track_section(p2, y, y, 1, 2).
%refuel loops
track_section(y, y, y, 1, 30).

track_section(S) :-
    track_section(S, _, _, _, _).


% loader(?Section)
loader(m1).
loader(m2).

% unloader(?Section)
unloader(p1).
unloader(p2).

connects(Section, Start, End) :-
    track_section(Section, Start, End, _, _)
    ;
    track_section(Section, End, Start, _, _).

adjacent(Sec1, Sec2, MidPoint):-
    connects(Sec1, _, MidPoint),
    connects(Sec2, MidPoint, _),
    Sec1 \= Sec2.

adjacent(S1, S2) :-
    adjacent(S1, S2, _).

duration(Section, DT) :-
    track_section(Section, _, _, DTMin, DTMax),
    between(DTMin, DTMax, DT).

%% Time Steps
time_step(X):-
    between(0, 20, X).

%% Orders
order(order(0, m1, p1, 0-12, 0-12)).
order(order(1, m1, p1, 12-24, 12-24)).
%order(order(2, m2, p2, 0-12, 0-12)).
%order(order(3, m2, p2, 12-24, 12-24)).

can_load_at(order(_, Mine, _, S-E, _), Mine, T):-
    between(S, E, T).
can_unload_at(order(_, _, Port, _, S-E), Port, T):-
    between(S, E, T).

%% Agents
train(t1).
train(t2).
train(t3).
train(t4).

% forward compatibility -- possibly more than one agent type in future
agent(X) :- train(X).

% Define available actions
prim_action(enter(Train, Section)) :-
    train(Train),
    track_section(Section).

prim_action(load(Train, Order)) :-
    train(Train),
    order(Order).

prim_action(unload(Train, Order)) :-
    train(Train),
    order(Order).

%% performance optimisation
prim_action(assign(Train, Order)):-
    train(Train),
    order(Order).


% Fluents
start(s0, 0). % this should really be in mindigolog.pl

% if Train is loaded with Order in S
loaded(Train, Order, do(C, _T, S)) :-
    member(load(Train, Order), C)
    ;
    (
       \+member(load(Train, _), C),
       \+member(unload(Train, _), C),
       loaded(Train, Order, S)
    ).

% if Train has unloaded Order in S
delivered(Train, Order, do(C, _T, S)) :-
    member(unload(Train, Order), C)
    ;
    delivered(Train, Order, S).

assigned(Train, Order, do(C, _T, S)) :-
    member(assign(Train, Order), C)
    ;
    assigned(Train, Order, S).

% succeeds if Train is on Section in the current situation
on_section(Train, Section, do(C, _T, S)) :-
    member(enter(Train, Section), C)
    ;
    (
     \+member(enter(Train, _), C),
     on_section(Train, Section, S)
    ).

% initial sections
on_section(t1, y, s0).
on_section(t2, y, s0).


entry_time(Train, Section, Entry, do(C, T, S)) :-
    member(enter(Train, Section), C) ->
        Entry = T
    ;
        entry_time(Train, Section, Entry, S).

entry_time(Train, Section, 0, s0) :-
    on_section(Train, Section, s0).

% objective is the number of delivered orders
objective(Val, S) :-
    findall(Ord, delivered(_, Ord, S), Deliveries),
    length(Deliveries, Val).


% Preconditions
poss(enter(Train, Section), T, S) :-
    (Section = y ; \+on_section(_, Section, S)),
    on_section(Train, PrevSection, S),
    adjacent(PrevSection, Section),
    entry_time(Train, PrevSection, EnterT, S),
    duration(PrevSection, Duration),
    time_step(T),
    Duration is T - EnterT.

poss(load(Train, Order), T, S) :-
    loader(Section),
    on_section(Train, Section, S),
    can_load_at(Order, Section, T),
    \+loaded(Train, _, S),
    \+loaded(_, Order, S),
    \+delivered(_, Order, S).

poss(unload(Train, Order), T, S) :-
    unloader(Section),
    on_section(Train, Section, S),
    can_unload_at(Order, Section, T),
    loaded(Train, Order, S),
    \+delivered(_, Order, S).

poss(assign(Train, Order), T, S) :-
    \+assigned(_, Order, S).

poss(noop, _, _).
poss(noop(A), _, _) :-
    agent(A).

%two trains can't be simultaneously assigned to the same order
conflicts(C, _, _) :-
    member(assign(T1, S), C),
    member(assign(T2, S), C),
    T1 \= T2.

%two trains can't simultaneously enter any track section
conflicts(C, _, _) :-
    member(enter(T1, S), C),
    member(enter(T2, S), C),
    T1 \= T2,
    S \= y. % except the yard

%one train can't simultaneously perform two different actions
conflicts(C,_,_) :-
    member(A1,C), actor(A1,Agt),
    member(A2,C), actor(A2,Agt),
    A2 \= A1.


visited(T, Sec, do(C, _, S)) :-
    member(enter(T, Sec), C)
    ;
    visited(T, Sec, S).

visited_since(T, S1, S2, do(C, _, S)) :-
    (member(enter(T, S1), C), visited(T, S2, S))
    ;
    visited_since(T, S1, S2, S).
    
next_section(T, Sec, Via, S) :-
    on_section(T, CurSec, S),
    adjacent(CurSec, Sec, Via).

%%% END DOMAIN

%%% BEGIN Program

proc(travel_to(Train, Section),
     travel_to(Train, Section, [], [])).

proc(travel_to(Train, Section, Visited, DontPass),
     if(on_section(Train, Section, now),
	noop,
	(enter(Train, Section)
	 /
	 pi(next,
	    pi(via,
	      ?(next_section(Train, next, via, now)):
	      ?(\+member(via, Visited)):
	      ?(\+member(via, DontPass)):
 	      enter(Train, next):
	      travel_to(Train, Section, [via | Visited], DontPass)))))).

proc(satisfy_order(Train, Order),
     ?(and(and(train(Train), order(Order)), \+assigned(_, Order, now))):
     assign(Train, Order):
     travel_to(Train, Mine):
     load(Train, Order):
     travel_to(Train, Port):
     unload(Train, Order):
     travel_to(Train, y)):-
    train(Train),
    order(Order),
    Order = order(_Id, Mine, Port, _, _).

% Stub to allow mindigolog to understand the fragment annotation
proc(fragment(Program), Program).

proc(control([]),
     noop).
proc(control([Order|Os]),
     pi(train,
        satisfy_order(train, Order))
     //
     control(Os)).
proc(control,
     control(Orders)) :-
    bagof(O, order(O), Orders).

%%% END PROGRAM

%%% BEGIN ANNOTATIONS
%%% These are wild guesses as to what we would would like to know to 
%%% generate a scheduling problem from a set of candidate fragments

% shared_resource(+resource, -Availability)
shared_resource(Section, 1) :-
    track_section(Section),
    Section \= y.

shared_resource(Train, 1) :-
    train(Train).

% uses_resource(+action, ?resource)
uses_resource(Action, Agent) :-
    actor(Action, Agent).
uses_resource(enter(_, S), S).

% acquires_resource(+action, ?resource, -usage)
% implicit cumulative contraint 
acquires_resource(enter(_, S), S, 1).

% releases_resource(+action, +resource)
releases_resource(enter(_, Next), Prev) :-
    Next \= Prev.

% symmetric_resource(+resource1, ?resource2)
% symmetric resources can be assigned by the fragment selector
% instead of frag generator.
symmetric_resource(Train1, Train2) :-
    train(Train1), train(Train2).

%%% END ANNOTATIONS

main(Args) :-
    ( length(Args,0) ->
        nl, do(control, s0, S), nl
    ;
        nl, display('ERROR: No arguments can be given'), nl
    ).
