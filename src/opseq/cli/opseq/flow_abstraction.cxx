#include <opcount.hxx>
#include <flow_abstraction.hxx>
namespace opseq {

Abstraction::Abstraction(OpCounter& oc, std::vector<unsigned> nodes, int init_idx, int goal_idx):
	oc(oc),
	inflow_ops(nodes.size()+1),
	outflow_ops(nodes.size()+1),
	init_idx(init_idx),
	goal_idx(goal_idx){
	unsigned i = 1;
	for(auto f : nodes){
		//std::cerr << "TNFx: " << f << " " << i << std::endl;
		addTransition(oc.num_ops() + f, i++, 0);
	}
};

Abstraction::Abstraction(Abstraction& a, Abstraction& b):
	oc(a.oc),
	inflow_ops(a.size() * b.size()),
	outflow_ops(a.size() * b.size()),
	init_idx(a.init_idx * b.size() + b.init_idx),
	goal_idx(a.goal_idx * b.size() + b.goal_idx){
	std::set<unsigned> ops;
	for(unsigned a_node = 0; a_node < a.size(); a_node++){
		for(auto op : a.outflow_ops[a_node]){
			unsigned src = a_node * b.size();
			unsigned dst = a.get_dst(op, a_node) * b.size();
			if(b.op_src.count(op)) {
				for(auto b_src : b.get_srcs(op)){
					//std::cerr << "Merge nodes = "<< a_node << "x" << b_src << std::endl;

					addTransition(op, src + b_src, dst + b.get_dst(op, b_src));
					if(b.get_dst(op, b_src) == b_src || src == dst) {
						prev_nodes.insert(src + b_src);
						prev_op_node[op] = src + b_src;
					}
				}
			} else {
				for(unsigned b_src = 0; b_src < b.size(); b_src++){
					//std::cerr << "Merge nodes (unsafe) = "<< a_node << "x" << b_src << std::endl;
					addUnsafeTransition(op, src + b_src, dst - src);
					op_copies[op][src + b_src] = b.get_var(op, b_src);
				}
			}
		}
	}
	// Add in ops not included in a
	for(unsigned b_node = 0; b_node < b.size(); b_node++){
		for(auto op : b.outflow_ops[b_node]){
			if(a.op_src.count(op) == 0) {
				for(unsigned a_src = 0; a_src < a.size(); a_src++){
					unsigned src = a_src * b.size() + b_node;
					unsigned offset = b.get_dst(op, b_node) - b_node;
					//std::cerr << "Merge nodes (unsafe2) = "<< a_src << "x" << b_node << std::endl;
					addUnsafeTransition(op, src, offset);
					op_copies[op][src] = a.get_var(op, a_src);
				}
			}
		}
	}

};

void Abstraction::addTransition(unsigned op, unsigned src, unsigned dst){
	//std::cerr << "TNF: " << op << " " << src << " " <<dst << " " << inflow_ops.size() << std::endl;
	inflow_ops.at(dst).insert(op);
	outflow_ops.at(src).insert(op);
	op_src[op].insert(src);
	op_dst_offset[op] = dst - src;
}

void Abstraction::addUnsafeTransition(unsigned op, unsigned src, int dst_offset){
	inflow_ops[src+dst_offset].insert(op);
	outflow_ops[src].insert(op);
	op_src[op].insert(src);
	op_dst_offset[op] = dst_offset;
}

bool Abstraction::safe(unsigned op) const {
	return op_src.count(op) == 1 && op_src.find(op)->second.size() == 1;
}

const std::set<unsigned>& Abstraction::get_srcs(unsigned op) const {
	return op_src.find(op)->second;
}

unsigned Abstraction::get_dst(unsigned op, unsigned src_node) const {
	if(op_dst_offset.count(op))
		return src_node + op_dst_offset.find(op)->second;
	return src_node;
}

GRBVar Abstraction::get_var(unsigned op, unsigned src_node) {
	if(op_src.count(op) && get_srcs(op).size() == 1) {
		return oc.tnf_count(op);
	}
	if(op_copies[op].count(src_node) == 0){
		const auto& vn = oc.tnf_count(op).get(GRB_StringAttr_VarName);
		// std::cerr << "Creating new var " << vn
		// 	  << " Given " << src_node
		// 	  << std::endl;
		op_copies[op][src_node] = oc.getModel().addVar(0, 1e9, 0, GRB_CONTINUOUS,
							       vn
							       + " given "
							       + std::to_string((unsigned long)this)
							       + "="
							       + std::to_string(src_node));
	}
	return op_copies[op][src_node];
}

GRBLinExpr Abstraction::netFlow(unsigned node) {
	GRBLinExpr net_flow;
	for(auto in_op : inflow_ops[node]){
		net_flow += get_var(in_op, node - op_dst_offset[in_op]);
	}
	for(auto out_op : outflow_ops[node]){
		net_flow -= get_var(out_op, node);
	}
	if(node == init_idx)
		net_flow += 1;
	if(node == goal_idx)
		net_flow -= 1;
	return net_flow;
}

GRBLinExpr Abstraction::prevFlow(unsigned op) {
	return netFlow(prev_op_node[op]);
}


void Abstraction::conserveFlow(unsigned node) {
	if(merged_nodes.count(node))
		return;
	merged_nodes.insert(node);
	oc.addConstr(netFlow(node) == 0);
}

void Abstraction::conservePrevFlow(unsigned op) {
	unsigned node = prev_op_node[op];
	conserveFlow(node);
}

void Abstraction::constrainOp(unsigned op) {
	if(prev_op_node.count(op) == 0)
		return;
	if(get_srcs(op).size() == 1)
		return;
	GRBLinExpr copies;
	for(auto& v : op_copies[op]){
		copies += v.second;
	}
	if(get_srcs(op).size() == op_copies[op].size())
		oc.addConstr(copies == oc.count(op));
	else
		oc.addConstr(copies <= oc.count(op));
}

void Abstraction::constrainOps(){
	for(auto op_it : op_copies)
		constrainOp(op_it.first);
}

void Abstraction::conserveAll(){
	for(unsigned i = 0; i < size(); i++)
		conserveFlow(i);
	for(auto op_node : op_src)
		constrainOp(op_node.first);
}

};
