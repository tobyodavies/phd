(define (domain bfrs)
(:requirements :typing
 :durative-actions
:numeric-fluents )
(:types		
	location
	order
	consist
)
(:predicates 


       	(at ?c - consist ?loc - location)
	(loaded ?c - consist ?o - order)
	(available ?c - consist)
	(empty ?c - consist)
	(clear ?loc - location)
	(delivered ?o - order)
	(not-delivered ?o - order)
	(conn ?from ?to - location)
	(compat ?c - consist ?o - order)
	(order_mine ?o - order ?loc - location)	
	(order_port ?o - order ?loc - location)	
	(is_yard ?l - location)
)

(:functions

	(current_time ?c - consist) - number
	(dummy_time) - number
	(load_after_time ?o - order) - number
	(unload_before_time ?o - order) - number
;	(cross_time ?from ?to - location) - number

)


(:durative-action enter_section 
:parameters (?c - consist ?curpos ?nextpos - location) 
:duration (= ?duration 1)
:condition (and 
		(at start (at ?c ?curpos)) 
		(at start (conn ?curpos ?nextpos)) 
		(at end (clear ?nextpos)) 
		(at start (available ?c) )
	   )
:effect (and  
         (at end   (at ?c ?nextpos))
	 (at end   (not (at ?c ?curpos)))  
         (at end (not (clear ?nextpos)))
	 (at start (clear ?curpos)) 
 	 (at start (not (available ?c)))
	 (at end   (available ?c))
	 (at end   (increase (current_time ?c) 1)))
)

(:durative-action enter_yard 
:parameters (?c - consist ?curpos ?nextpos - location) 
:duration (= ?duration 1)
:condition (and
	    (at start (is_yard ?nextpos))
 	    (at start (at ?c ?curpos))
            (at start (conn ?curpos ?nextpos))
            (at start (available ?c)))
:effect (and  
	     (at end (at ?c ?nextpos))
	     (at start  (not (at ?c ?curpos)))           
 	     (at start (not (available ?c)))
	     (at end (available ?c) )
	     (at end (increase (current_time ?c) 1) )
	 )
)

(:durative-action wait 
:parameters (?c - consist ?curpos - location ) 
:duration (= ?duration 1)
:condition (and  (over all (at ?c ?curpos)) (at start (available ?c) ) ) 
:effect (and  
	 (at end (increase (current_time ?c) 1) ) ;; REMEMBER CHANGE TO cross_time FUNCTION!!!
	 (at start (not (available ?c) ))
	 (at end (available ?c) )
;      (increase (dummy_time) 1)  ;; REMEMBER CHANGE TO cross_time FUNCTION!!!
	)
) 


(:durative-action load 
:parameters (?c - consist ?o - order ?curpos - location) 
:duration (= ?duration 1)
:condition (and 
	        (over all (compat ?c ?o))
	   	(at start (empty ?c))
		(at start (not-delivered ?o))
		(over all (at ?c ?curpos))
		(at start (order_mine ?o ?curpos))  
		(at start (>= (current_time ?c) (load_after_time ?o )))
	   ) 
:effect (and  
	     (at end (loaded ?c ?o))
	     (at start (not (empty ?c)))              
   	     (at end (increase (current_time ?c) 1))
	 ) 
) 

(:durative-action unload 
:parameters (?c - consist ?o - order ?curpos - location) 
:duration (= ?duration 1)
:condition (and 
		(at start (not-delivered ?o))
	   	(at start (loaded ?c ?o)) 
		(over all (at ?c ?curpos)) 
		(at start (order_port ?o ?curpos))  
		(at end (<= (current_time ?c) (unload_before_time ?o )) )
	   ) 
:effect (and
	     (at start (not (loaded ?c ?o)))
	     (at end  (empty ?c))
	     (at end  (delivered ?o))
             (at end (not (not-delivered ?o)))
	     (at end (increase (current_time ?c) 1) ) ;; REMEMBER CHANGE TO cross_time FUNCTION!!!
;	     (increase (dummy_time) 1)  ;; REMEMBER CHANGE TO cross_time FUNCTION!!!
	 ) 
)
)