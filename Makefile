ORG = emacs --batch --visit overview.org --load org --load babel-setup.el --funcall

all : overview.html src/doc overview.pdf test violations hybrid-planning-paper/index.pdf

.PHONY: src/doc test publish clean violations subdir

overview.pdf : overview.tex
	pdflatex -interaction nonstopmode overview || echo "WARN: pdf generation failed" 1>&2

overview.tex : overview.org
	$(ORG) org-export-as-latex-batch

overview.html : overview.org
	$(ORG) org-export-as-html-batch

src/doc:
	cd src ; tox -e docs

test:
	cd src; if (which detox); then detox -r -e py27; else tox -r -e py27; fi

publish: overview.html src/doc overview.pdf
	rsync -rv --exclude .tox ./ tobyodavies.com:/var/www/notes
	rsync -rv --exclude .tox ./ todavies@toaster.cs.mu.oz.au:public_html/phd

clean:
	find src -name '*.pyc' -print0 | xargs -0 rm `mktemp`
	find src/doc/ -name 'pymas*.rst' -o -name 'module.rst' | xargs rm `mktemp`

violations:
	cd src; pylint --rcfile=pylintrc -f parseable -r y pymas > pylint.txt || echo "Non-zero exit"

hybrid-planning-paper/index.pdf: subdir
	cd hybrid-planning-paper ; $(MAKE) index.pdf || echo "WARN: pdf generation failed" 1>&2

subdir:
	true
