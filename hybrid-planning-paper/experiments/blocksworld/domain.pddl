;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; 4 Op-blocks world
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain blocksworld)
  (:requirements :typing :durative-actions)
  (:types block hand)
  (:predicates (on ?x - block ?y - block)
	       (ontable ?x - block)
	       (clear ?x - block)
	       (handempty ?h - hand)
	       (holding ?x - block ?h - hand)
	       )
  (:durative-action pick-up
	     :parameters (?x - block ?h - hand)
	     :duration (= ?duration 1)
	     :condition (and
	                    (at start (clear ?x))
			    (at start (ontable ?x))
			    (at start (handempty ?h))
			   )
	     :effect
	     (and (at start (not (ontable ?x)))
                  (at start (not (clear ?x)))
		  (at start (not (handempty ?h)))
		  (at end (holding ?x ?h))))

  (:durative-action put-down
	     :parameters (?x - block ?h - hand)
	     :duration (= ?duration 1)
	     :condition (and
	                 (at start (holding ?x ?h))
			)
	     :effect
	     (and (at start (not (holding ?x ?h)))
		  (at end (clear ?x))
		  (at end (handempty ?h))
		  (at end (ontable ?x))))
  (:durative-action stack
	     :parameters (?x - block ?y - block ?h - hand)
	     :duration (= ?duration 1)
	     :condition (and
	                    (at start (holding ?x ?h))
			    (at start (clear ?y)))
	     :effect
	     (and (at start (not (holding ?x ?h)))
		  (at start (not (clear ?y)))
		  (at end (clear ?x))
		  (at end (handempty ?h))
		  (at end (on ?x ?y))))

  (:durative-action unstack
	     :parameters (?x - block ?y - block ?h - hand)
	     :duration (= ?duration 1)
	     :condition (and
	                     (at start (on ?x ?y))
			     (at start (clear ?x))
			     (at start (handempty ?h))
			)
	     :effect
	     (and (at end (holding ?x ?h))
		  (at end (clear ?y))
		  (at start (not (clear ?x)))
		  (at start (not (handempty ?h)))
		  (at start (not (on ?x ?y)))))
)
