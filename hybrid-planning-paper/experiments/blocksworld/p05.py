# Blocks-world: 7 blocks
import math
import networkx as nx
import itertools as it
import sys
import time

import pymas
import pymas.test.blocksworld as bw

H_ITERS = int(sys.argv[1]) if len(sys.argv)>=2 else 0

nblocks = 7
bw.BlocksAgent.blocks = [u'b4', u'b5', u'b6', u'b7', u'b1', u'b2', u'b3']
bw.BlocksAgent.nagents = 7
bw.BlocksAgent.init_on = {u'b4': u'b7', u'b5': u'b6', u'b6': None, u'b7': u'b5', u'b1': u'b2', u'b2': None, u'b3': None}
bw.BlocksAgent.goal_on = {u'b4': u'b6', u'b5': None, u'b6': None, u'b7': None, u'b1': u'b7', u'b2': u'b4', u'b3': u'b2'}

start_t = time.time()
init = list(bw.solve(nfrags=7))
selector = pymas.FragSelector(init, bw.solve)
print "#frags:", len(selector.fragments)

min_frags = sum(1
                for b in bw.BlocksAgent.blocks
	        if bw.BlocksAgent.init_on[b] != bw.BlocksAgent.goal_on[b])



def satis(s):
    print s
    hb = reduce(set.union, [f.sit.handled_blocks() for f in s], set())
    print "handled", hb
    return len(s) >= min_frags and selector.cost(s) <= acceptable_obj


obj, _, __ = selector.solve_lp()
acceptable_obj = .99*obj
initial_cost, selection = selector.heuristic_select()

#while obj < acceptable_obj:
#   print "WTF Mate???\n"*10
#   obj, _, __ = selector.solve_lp()

for _ in xrange(H_ITERS):
    if satis(selection):
        break
    initial_cost, selection = selector.heuristic_select(satisfices=satis)
    if satis(selection):
        break
    initial_cost, selection = selector.dive_heuristic_select(satisfices=satis)

    print "#frags:", len(selector.fragments)

    print "Blocks solved:", len(selection)
    print "Time:", time.time() - start_t

if H_ITERS == 0 or not satis(selection):
    print "*"*10
    print "Branch and price"
    print "*"*10
    selector.solve(satisfices=satis)
    initial_cost, selection = selector.heuristic_select()

print "Time:", time.time() - start_t

print "#frags:", len(selector.fragments)

import collections
exes = [collections.defaultdict(list)
        for _ in selection]
for i, frag in enumerate(selection):
    for act, sit in frag.sit.history:
        exes[i][sit.time()].append(act)

max_t = [max(e) for e in exes]
jexe = [(t, [exes[i].get(t) for i in xrange(len(selection))])
        for t in xrange(max(max_t)+1)]
print "\n".join(str(e) for e in jexe)

