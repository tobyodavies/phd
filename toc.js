jQuery.fn.tocify_one = function(more, less, minimised) {
    $header = $('>:first-child', this);
    $tocSections = $header.nextAll();
    console.log([$header, $tocSections]);
    $header.after('<a class="toc-toggle">'+less+'</a>');
    $toggle = $('.toc-toggle', this);
    $toggle.click(function(ev){
	$(this).nextAll().toggle();
	$(this).html($(this).html()==more?less:more)
    });
    if (minimised) {
	$toggle.click();
    }
};

jQuery.fn.tocify = function(more, less, minimised) {
    $(this).each(function(){
	$(this).tocify_one(more, less, minimised);
    });
};

$(function(){
    //$('#table-of-contents>ul').accordion();
    //return
    var more = '<b>+</b>'
    var less = '<b>-</b>'
    $('#table-of-contents>div>ul>li').tocify(more, less,true);
    //$('.outline-2').tocify("<b>more</b>", "<b>less</b>", true);
    $('#table-of-contents').append('<br /><a href="overview.pdf">pdf</a>');
})
