﻿;; Transport sequential
;;

(define (domain transport)
  (:requirements :typing :durative-actions)
  (:types
        location target locatable - object
        vehicle package - locatable
        capacity-number - object
  )

  (:predicates 
     (road ?l1 ?l2 - location)
     (at ?x - locatable ?v - location)
     (in ?x - package ?v - vehicle)
     (capacity ?v - vehicle ?s1 - capacity-number)
     (capacity-predecessor ?s1 ?s2 - capacity-number)
  )

  (:functions
     (road-length ?l1 ?l2 - location) - number
  )

  (:durative-action drive
      :parameters (?v - vehicle ?from ?to)
      :duration (= ?duration (road-length ?from ?to))
      :condition (and 
            (at start (at ?v ?from))
            (over all (road ?from ?to)))
      :effect (and
            (at start (not (at ?v ?from)))
            (at end (at ?v ?to)))
  )

 (:durative-action transport-package
    :parameters (?v - vehicle ?from ?to - location ?p - package ?s1 ?s2 ?s3 ?s4 - capacity-number)
    :duration (>= ?duration 1)
    :condition (and
        (at start (at ?v ?from))
        (at start (at ?p ?from))
        (at start (capacity ?v ?s2))
        (at end (at ?v ?to))
        (at end (capacity ?v ?s3))
        (over all (in ?p ?v))
        (over all (capacity-predecessor ?s1 ?s2))
        (over all (capacity-predecessor ?s3 ?s4))
      )
    :effect (and
        (at start (not (at ?p ?l)))
        (at start (in ?p ?v))
        (at start (capacity ?v ?s1))
        (at start (not (capacity ?v ?s2)))
        (at end (not (capacity ?v ?s3)))
        (at end (capacity ?v ?s4))
        (at end (not (in ?p ?v)))
        (at end (at ?p ?to))
      )
  )

)
