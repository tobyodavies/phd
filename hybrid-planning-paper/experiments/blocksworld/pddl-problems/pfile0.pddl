
(define (problem BW-rand-3)
(:domain blocksworld)
(:objects 
	  b1 b2 b3  - block
	  h1 h2 h3  - hand
)
(:init
(handempty h1)
(handempty h2)
(handempty h3)
(on b1 b3)
(ontable b2)
(on b3 b2)
(clear b1)
)
(:goal
(and
(on b1 b2)
(on b2 b3))))
