(define (problem bfrs_${len(orders)})
(:domain bfrs)
(:objects
  % for node in graph.nodes():
    ${node} - location
  % endfor
  % for i, _ in enumerate(orders):
    o${i} - order
  % endfor
  % for i, _ in enumerate(orders):
    c${i} - consist
  %endfor
)
(:init
  (is_yard y)
  % for node in graph.nodes():
    (clear ${node})
  % endfor

  % for from_block, to_block in graph.edges():
    (conn ${from_block} ${to_block})
    (conn ${to_block} ${from_block})
  % endfor

  % for i, (mine, port, load_after, unload_before) in enumerate(orders):
    ;; order o${i} ${(mine, port, load_after, unload_before)}
    (order_mine o${i} ${mine})
    (order_port o${i} ${port})
    (= (load_after_time o${i}) ${load_after})
    (= (unload_before_time o${i}) ${unload_before})
    (not-delivered o${i})

    ;; consist c${i}
    (= (current_time c${i}) 0)
    (available c${i})
    (empty c${i})
    (at c${i} y)

    (compat c${i} o${i})

  %endfor
)
(:goal
(and
% for i, _ in enumerate(orders):
  (delivered o${i})
  (at c${i} y)
% endfor
))
(:metric minimize (total-time)))
