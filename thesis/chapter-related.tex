\chapter{Knowledge and Conflict}
\begin{ch-abstract}
This chapter focuses on placing the thesis in a broader context,
specific technical background and notation will be introduced in chapters as needed. 
\end{ch-abstract}

This thesis assumes knowledge of deterministic planning to the level
of the textbook ``A Concise Introduction to Models and Methods for
Automated Planning'' \cite{geffner2013}.  Familiarity with
Satisfiability \cite{biere2009} and Mathematical Programming
\cite{papadimitriou1982} would also be an advantage.

\section{Preliminaries of combinatorial optimisation}

Combinatorial problems are characterised by the need to choose values
for a set or sequence of variables such that they satisfy some
conditions.
This means there are a finite, though possibly enormous, number of
possible solutions: every possible combination or sequence of values.
The most basic algorithm solving this class of problems is
brute-force, where every one of these potential solutions is generated
and tested to see if it satisfies the required conditions.

Problems in this class are often NP-hard, so algorithms must make a
trade-off if they want to guarantee they will find a solution if one
exists.  Algorithms guaranteed to find a solution are called complete
algorithms and, assuming $P \ne NP$, must have super-polynomial runtimes.
Incomplete algorithms in contrast make no such guarantee, but instead
typically guarantee polynomial runtime and/or bounded
memory usage.  This thesis focuses on complete algorithms.

The most common class of complete algorithm is search.  Searching for
a solution consists of choosing variables to assign one at a time and
guessing all possible values for that variable.  Obviously the
algorithm can only consider one of these guesses at a time, so the
other possibilities must be stored for later consideration.  The
structure which stores such a partial assignment is called a ``search
node''.  The set of search nodes that still need to be considered is
called an ``open set'' or ``open list''.

Search techniques can perform empirically better than brute force only when they
can either detect conflicts in these partial solutions and skip generating
many of the possible search nodes, or if they choose which partial
solutions to extend in such a way to find a solution early.
Search algorithms primarily differ in the data-structure used to
implement the open list, and the types of reasoning they use to detect
conflicts.

The basic outline of a search algorithm is:
  \begin{enumerate}
    \item Add a search node representing having made \textit{no} choices to the open set.
    \item Pick some search node from the open set.
    \item Reason about the consequences of the choices represented by that node.
    \item Optionally \textbf{discard the node} if there is a conflict between these choices.
    \item If all variables have been assigned, you have found a solution, terminate.
    \item Choose one variable whose value is not known and add a search node to the open list for each choice of value that variable can take.
    \item Repeat steps 2-6 while there are any nodes remaining in the open set.
    \item If there are no open nodes remaining, there is no solution.
  \end{enumerate}

Intelligently choosing the order to expand nodes, or discarding nodes,
either implicitly or explicitly, are the only ways for
such algorithms to be faster than brute-force.
It is hoped that the cost of reasoning is paid for by a significant
reduction in the number of nodes generated before a solution is found.
The reason that a node can be \textbf{safely} discarded is what we
refer to as a conflict.\footnote{Safely here means ``without discarding the last optimal potential solution''}
%
A number of combinatorial search pruning strategies are summarised in table \ref{intro:pruning}.
We hope the reader is familiar with one or more of these techniques.


\begin{table}
\def\arraystretch{1.5}
\begin{tabularx}{\textwidth}{c | X }
Technology                & Pruning method           \\
\hline
Constraint Satisfaction   & Propagation implicitly prunes nodes that would be in conflict within a single constraint.
                            Additionally, nodes containing a variable with no valid value after propagation are pruned.\\
Branch-and-bound          & A relaxation of the problem is used to prune nodes with objective lower-bounds no better than  each solution found. \\
IDA*                      & A relaxation of the problem is used to prune nodes with objective lower-bounds worse than a pre-determined lower bound. \\
(Weighted-)A*             & A relaxation of the problem is used to implicitly prune (i.e. never generate) nodes with objective lower-bounds no better than the best solution found.
                            Additionally, nodes representing paths to previously seen states are pruned.\\
\end{tabularx}
\caption{Pruning in combinatorial search}\label{intro:pruning}
\end{table}


\paragraph{Satisfiability and Constraint Programming}
 are two technologies which have made the greatest use of conflict-based reasoning. 
Constraint Programming (CP) is a generalisation of the Satisfiability
problem (SAT) where a fixed finite set of variables are each assigned an arbitrary value from
a finite domain (as opposed to just the values True or False for SAT).
Both are NP-complete problems \cite{cook1971}.

In these problems, the majority of nodes are pruned implicitly by
(unit-)propagation.  Additionally, a node may become ``failed'' when
a variable has no values left in its domain.  In this case, there
exists some propagation rule that could have pruned the failed node
earlier in the search tree.  This rule is a consequence of some subset
of the constraints, and is typically learned as a Boolean clause.
This type of learning was originally introduced in the Chaff solver
for SAT \cite{moskewicz2001}, and by Lazy Clause Generation for
general constraint programming \cite{lcg}.

These learned clauses are often referred to as conflicts in the SAT and
CP literature, but in this thesis we will show why it is interesting
to maintain the distinction, especially in the context of planning and
plan-space search.


\subsection{Planning problems}

Planning problems are characterised by the need to find a
\textbf{sequence} of decisions satisfying some properties, as opposed
to a set of assignments whose order is not significant.\footnote{Not significant for correctness, but often crucial for performance.}
The length of this sequence is typically exponential (or even unbounded) in
the size of the problem description.
%
A planning problem will have an initial state, a goal condition, and a set of available operators (equiv. actions).
The solution to a planning problem is a sequence of operators called a plan.
The operators in a plan will have preconditions and effects, such that
the first operator's preconditions hold in the initial state; each
subsequent action's preconditions hold in the state resulting from
applying the effects of all prior actions in sequence; and the state
resulting in applying the whole plan satisfies the goal condition.

There are many planning formalisms considered in the literature, in
particular the equivalent STRIPS \cite{strips} and SAS+
\cite{backstrom1992} classical formalisms.
We describe the technical details of these formalisms in Part II.
Both formalisms describe PSPACE-complete planning problems, requiring
at most an exponential number of operators in a plan.

More expressive formalisms exist which incorporate
programming-language constructs such as loops and recursion, and as
such are typically Turing-complete.  In particular we will describe
the \Golog{}-family of languages in Part I.  Another important
formalism of this kind is Hierarchical Task Networks \cite{sacerdoti1975,tate1977},
we expect the work described in Part I will have analogues in this
formalism, but exploring this is beyond the scope of this thesis.

A SAS+ planning problem can be visually represented by a set of Domain
Transition Graphs such as those seen in Figure \ref{intro:gripper-dtgs}.
Domain Transition Graphs can be very simply transformed into
Deterministic Finite Automata (DFAs) by adding a self-loop at every node in
each graph for each operator that does not appear anywhere else in
that graph.
A SAS+ planning problem can then be seen as the problem of finding a
sequence that is simultaneously accepted by all of the DFAs.

\begin{figure}[t]
\begin{center}
\begin{tikzpicture}%
  [<-,place/.style={circle,draw=black}]
  \node (VarR) at (0,4.5) {$R:$} ;
  \node (L) at (1,4.5) [place, double] {$l$} ;
  \node (R) at (5,4.5)   [place,double] {$r$} ;
  \path (VarR) edge[->] (L) ; 
  \path (L) edge[-triangle 45, bend left] node[below] {\small \Move{r}} (R) ;
  \path (R) edge[-triangle 45, bend left] node[above] {\small \Move{l}} (L) ;
  \path (L) edge[-triangle 45, loop below] node[below] {\small \Grip{*}{l}} (L) ;
  \path (L) edge[-triangle 45, loop above] node[above] {\small \Drop{*}{l}} (L) ;
  \path (R) edge[-triangle 45, loop below] node[below] {\small \Grip{*}{r}} (R) ;
  \path (R) edge[-triangle 45, loop above] node[above] {\small \Drop{*}{r}} (R) ;

  \node (VarB) at (0,2) {$B_i:$} ;
  \node (XL) at (1,2)   [place] {$l$} ;
  \node (XG) at (3,2)   [place] {$g$} ;
  \node (XR) at (5,2)   [place,double] {$r$} ;
  \path (VarB) edge[->] (XL) ; 
  \path (XL) edge[-triangle 45, bend left] node[above] {\small \Grip{i}{l}} (XG) ;
  \path (XG) edge[-triangle 45, bend left] node[above] {\small \Drop{i}{r}} (XR) ;
  \path (XR) edge[-triangle 45, bend left] node[below] {\small \Grip{i}{r}} (XG) ;
  \path (XG) edge[-triangle 45, bend left] node[below] {\small \Drop{i}{l}} (XL) ;

  \node (VarG) at (0,0) {$G:$} ;
  \node (E) at (1, 0) [place,double] {$e$} ;
  \node (N) at (5, 0) [place,double] {$n$} ;
  \path (VarG) edge[->] (E) ; 
  \path (E) edge[-triangle 45, bend left] node[below] {\small \Grip{*}{*}} (N) ;
  \path (N) edge[-triangle 45, bend left] node[above] {\small \Drop{*}{*}} (E) ;
\end{tikzpicture}
\end{center}
    \caption{Domain Transition Graphs in gripper\label{intro:gripper-dtgs}.}
\end{figure}


The simplified gripper domain shown in Figure \ref{intro:gripper-dtgs} with
2 balls, $B_1$ and $B_2$ is a simple example of a classical planning
problem.
The goal is to move both balls from the left room to the right,
using a robot with a single ``gripper'' which can hold only one ball
at a time.  The robot starts in the left room, and can only pick up
and drop balls in the room it is currently occupying.
For each automaton in Figure \ref{intro:gripper-dtgs}, the initial state is
marked by an incoming arrow, and the states consistent with the goal are double circled.
The variable $R$ represents the location of the robot (in the left or right room) ;
$B_i$ represents the location of ball $i$, (in the left or right room, or in the gripper);
$G$ represents the state of the gripper (empty or non-empty).


\paragraph{State-space search} has been the most widely-used and widely
studied approach to planning over the last decade.
It is characterised by searching in the state-space, starting with the
initial state in the open list, search proceeds directionally by
adding successors of the current state to the open list.\footnote{This process also works in reverse, starting from the goal, but is typically less effective.}
This effectively means that state-space search considers partial plans
only as prefixes, and inserts operators into the plan only at the end.

Heuristic search is a particularly common approach to state-based search,
it reasons about the cost from the current state to the goal,
this focuses on intelligently choosing plan prefixes to extend,
instead of pruning.
Heuristic functions, or just heuristics, map states to an estimate of
the remaining cost.  Heuristics may be ``admissible'', meaning that
their estimate is guaranteed to be a lower bound on the true minimum
cost to the goal.
Such admissible heuristics can be used as in a pruning method in
addition to guiding the order of expanding nodes in the open list.


\paragraph{Plan-space search} is, broadly, any planning
approach whose nodes do not map directly to a state in the state-space,
searching over the space of possible plans, rather than the state-space itself.
Partial-order planning is the most widely known plan-space search
algorithm, commonly found in AI textbooks \cite{penberthy1992,russell2003}.
In partial-order planning, operators are inserted as supporters of
unachieved goals and sub-goals, and ordering constraints are added only
between a subset of operators to resolve ``threats''.

\section{Types of conflict}

\begin{sidewaystable}
    \centering
    \small
\def\arraystretch{1.4}
\begin{tabular}{l | l | l | l | l}
  Algorithm  &  Search Node      & Open List &   Conflict    & Knowledge Learned              \\
\hline
 IDA* \cite{korf1985} & Prefix + f   & Stack   &  Insufficient f-budget    &  Increase $h(s_0)$          \\%
 Alpha-Pruning \cite{korf1990}  & Prefix    & Stack      &  $h(s)$ too low           &  Increase $h(s)$            \\%
 SAT-Plan \cite{kautz1992} & SAT Formula     & Number of layers & Insufficient layers      &  Add layer                  \\%
\hline
 $h^{++}$\cite{haslum2012}  & Relaxed-plan   & MIP &  Deleted goal/prec        &  Prevailing conjunction     \\
 CEGAR \cite{seipp2013} & Abstract plan   &  Abstraction Graph & Invalid Path    &  Split abstraction node     \\
 PDR \cite{suda2014} & Prefix + len.   & Stack or queue & Dead-end                 &  Blocked-cube               \\
 \textbf{FragPlan} (Ch. \ref{chp:fragplan}+\ref{chp:orsc}) & Agent-Plans     & Linear Program &  Resource limitations     &  Improving agent plan         \\
 \textbf{OpSeq} (Ch. \ref{chp:opseq}) & Operator Count  &  MIP & Unsequencable            &  Generalised Landmark       \\
 DFS-CL \cite{steinmetz2016}  & Prefix    & Stack      &  Dead-end                 &  Critical-path conjunction  \\
 \textbf{CDHL} (Ch. \ref{chp:cdhl}) & Prefix + f   & Stack   &  Suboptimality            &  Cost-Clause                \\
 \textbf{MznLBBD} (Ch. \ref{chp:autolbbd}) & Resource-alloc. & MIP &  Unschedulable            &  Benders Cut                \\
 \textbf{OpSched} (Sec. \ref{chp:opsched}) & Sched. + count & CP & Non-empty count         & Add Events  \\
\end{tabular}
\caption{Characterising learning planning algorithms.}\label{intro:learning}
\end{sidewaystable}


A conflict is a flaw that can be detected at a search node.  Obviously
the conflicts that can be detected are a function of the decisions
encoded in each search node, and the kind(s) of reasoning conducted at
each node.

In CP and SAT, each search node encodes a restricted domain of the
variables.
A conflict will occur iff some variable has no valid values after propagation \cite{marriott1998}.
This is the only kind of failure that can be detected given the representation of
search nodes.
Because of this limitation, it is common for constraint programming
models to introduce redundant variables and constraints which allow
certain domain-specific conflicts to be detected earlier in the
search \cite{cheng1999}.
Thus effectively delegating the problem of identifying the most useful
kind of conflict to the modeller.

In Branch-and-Bound, each node stores the optimal solution to a relaxed problem.
This allows nodes to be pruned when either the relaxation is infeasible, 
or the objective of the relaxed solution is larger than the incumbent.
Depending on the kind of relaxation, it may be strengthened by further inference,
such as cut generation in Mixed-Integer Programming.

In (forward) state-space search, a search node is a state in the state
space, together with the cheapest path reaching that state from the initial state.
Necessarily then, any conflict that can be detected in such a planning
algorithm must be a function of the state variables, plus possibly the
path cost (``g-value'') and/or heuristic estimate (``h-value''), as
this is the only information represented in the search node.


In contrast to SAT, CP, or state-space search, plan-space search does
not have a universally agreed-upon search-node representation.
As a consequence, algorithms can conceivably be designed that can detect
types of conflict as yet unconsidered.
We introduce a very general class of conflict for classical planning (``unsequenceability'')
in chapter \ref{chp:opseq}.


\section{Knowledge derived from conflict}

Conflict-directed algorithms learn something from each conflict that
occurs while solving a single instance.  This knowledge guarantees that
the same conflict will never be seen in any other search
node.\footnote{So long as that knowledge is kept: many algorithms ``garbage
  collect'' knowledge that is not significantly pruning search. This
  knowledge can be re-learned if it becomes relevant again later.}

Importantly this is  distinct from the relatively well studied problem of
learning from separate but related instances.
There have been several ``learning tracks'' of the international
planning competition dedicated to this latter problem,
and is typically solved by techniques such as algorithm configuration
and portfolio construction.

Effective conflict-derived knowledge must avoid an exponential amount
of future work.  In order to do so, it must generalise to prune an
exponential number of future nodes; and resolve with other learned
knowledge to prune whole sub-trees, not just leaves.  Ideally it will
also synergise with the branching strategy in some way, enabling the
search to focus on the aspects of the problem that are responsible
for failure.

The ubiquitous A* algorithm can definitely be described as learning
path dominance relationships.  However it does not do so based on
conflicts: it learns a closed list regardless of whether multiple
paths to a state exist. In this sense it learns from (partial)
success rather than conflict.

In table \ref{intro:learning}, we list several algorithms that have
some conflict-directed properties.
The earliest algorithms in the first part of table
\ref{intro:learning} learn only basic information from conflict,
specifically they do not attempt to generalise the learned knowledge.

Iterative Deepening A* learns only about the initial state. With the
addition of a transposition table it eventually learns the perfect
heuristic value for each node on the optimal path \cite{reinefeld1994},
however the learned knowledge applies only to states that have already been
seen, and is not generalised in any way.
Alpha-Pruning \cite{korf1990} is substantially similar to IDA* with a
transposition table, introduced in the same paper as (Learning)
Real-Time A*, as the offline improvement procedure for those
algorithms. It differs from IDA* with a transposition table only in
that it allows the specification of a finite look-ahead depth to use to
learn a better heuristic value.
Since this distinction is not particularly significant in the context
of this thesis, we will focus on IDA* with transposition tables over
these more complex algorithms.

DFS-CL \cite{steinmetz2016}, and our
``Conflict-Directed Heuristic Learning'' (CDHL, chapter 6) both
attempt to solve planning problems depth-first, similarly to IDA*.
They are differentiated by DFS-CL focusing on finding \textit{a}
solution, thus recognising only dead-ends as conflicts, whereas CDHL
attempts to find the \textit{optimal} solution.  It does this by
learning the perfect heuristic, thus treating particular kinds of
inaccuracy in the heuristic as a conflict.
In this sense CDHL generalises IDA* with a transposition table by
generalising the reason for the heuristic's value to states that have
not necessarily been visited yet.

Planning as Satisfiability \cite{kautz1992, rintanen2009} is a family of approaches
which compile the planning problem into a sequence of length-limited SAT problems.
These algorithms will test if the planning problem can be solved in $n$ ``steps'',\footnote{In the simplest case, a ``step'' corresponds to exactly 1
  action, but much more complex (and effective) definitions exist that
  allow many actions to occur in a single step.\cite{wehrle2007}} if
the answer is UNSAT, they will proceed to test if a plan of $n+k$
steps exists for some small constant $k$.
Variants exist which test several of these formula lengths concurrently.
%
In all cases, all that is necessarily learned from each conflict (UNSAT
formula) is that the plan must be longer than $n$.  Many SAT solvers
allow the use of assumptions which can be used to enable clauses
learned at shorter horizons to be reused.  We describe how this is
possible in our simple SAT encoding used in chapter \ref{chp:opseq},
however to our knowledge this approach has not been investigated in
traditional SAT-planners.


Other planning algorithms make use of SAT formulae.  Most notably
Property Directed Reachability (PDR), a verification algorithm adapted
to planning by \citeauthor{suda2014} (\citeyear{suda2014}).  PDR
maintains a set of CNF formulas, where the $i$-th formula represents
an over-approximation of the set of states which are at most $i$ steps
from a goal state.  What really separates PDR from traditional
SAT-planning techniques is its ability to detect unsolvability: if, at
certain points in the algorithm, the formula representing the set of
states at most $n$ steps from the goal is identical to the formula for
states at most $n+1$ steps from the goal, then all subsequent formulae
will be identical too, and the algorithm has reached a fixed-point. If
the initial state is not in the set of states $n$ steps from the goal
at fixed-point, it will also not be in any set of states $m>n$ steps
from the goal, and the planning problem is provably unsolvable.


Our ``Operator Sequencing'' approach (described in chapter
\ref{chp:opseq}) also falls into this SAT-enabled category.
This approach combines ideas from SAT-planning with one of the most
recent ideas from heuristic search: Operator Counting
\cite{pommerening2014}.
Operator Sequencing uses a state-of-the-art heuristic to estimate an
``operator count'' that may be sequenceable into a plan, then
constructs a SAT-formula to test the sequencability of this operator
count.
A linear inequality is learned and added to the heuristic, encoded as a
Mixed-Integer Program, to improve future operator counts.

``Counter-Example Guided Abstraction Refinement'' (CEGAR) is another
conflict-directed algorithm adapted from the verification community
\cite{clarke2003,seipp2013}.
CEGAR is both a complete algorithm, and a method for constructing
abstraction heuristics.  An abstraction maps every state in the state
space to a small number of abstract states.  It begins with the
simplest possible abstraction which distinguishes only goal states
from all other states.
An optimal path through the abstraction is tested in the real search
space and, if it is not a valid plan, the abstraction is refined by
splitting one or more abstract states on the optimal path in two.

In contrast to adapting ideas from the SAT and verification
communities, other learning algorithms have their roots in heuristic
search, in particular the $h^m$ and $h^C$ family of critical path
heuristics.  These heuristics include explicit conjunctions of facts
in their estimation of goal distance.  As the set of conjunctions
considered approaches the full state space, these heuristics approach
the perfect heuristic.  The $h^m$ heuristic considers all conjunctions
of up to $m$ facts, whereas $h^C$ considers an explicit set $C$ of
conjunctions of arbitrary size.  In addition to computing a critical
path with these conjunctions, the conjunctions can be compiled into a
new planning problem, denoted $\Pi^C$, such that $h^1$ in $P^C$ is
equivalent to $h^C$ in the original problem \cite{haslum2009,haslum2012}.
This compilation may exponentially increase the number of operators in
the planning problem,\footnote{unless conditional effects are added in
  the compilation, leading to a compiled problem denoted
  $\Pi^C_{ce}$} but allows other algorithms to be applied with
potentially improved results.
Heuristic methods for choosing $C$ are the primary focus in the
literature, however one conflict-directed approach has been covered:
$h^{++}$ computes an optimal delete-relaxed plan for $\Pi^C$,
initially with only unit conjunctions in $C$, and adds conjunctions to the plan which
invalidate the previous optimal plan \cite{haslum2012}.
By iterating this process, $h^{++}$ will eventually compute a
delete-relaxed plan which is also a plan in the real state-space.

In addition to these techniques, we have introduced a number of 
plan-space search algorithms whose inspiration comes from the
fields of Operations Research and Mathematical Programming.
These fields are concerned with many real-world resource allocation
and scheduling problems, and the distinction between scheduling and
planning in real-world domains is often blurry.

In particular we utilise incremental decompositions to Mixed-Integer
Programming problems.
These decompositions are not typically described as conflict-directed,
simply because that term is not widely used in the OR or MP
communities, but the widely used and supported technique of
cut-generation bears a strong resemblance to lazy clause generation.
These decompositions rely on the observation that while many variables
and constraints may be required to ensure the correctness of a MIP
model, relatively few of these are actually necessary to find a
solution and prove its optimality.

Logic-Based Benders Decomposition (LBBD) is a 
class of cut generation decomposition typically combining Mathematical
Programming with Constraint Programming or SAT.
Two approaches in this thesis are based primarily on LBBD: ``Operator
Sequencing'' in chapter \ref{chp:opseq} and ``Automatic LBBD'' in
chapter \ref{chp:autolbbd}.

Automatic LBBD is not strictly speaking a planning algorithm, but an
approach to solving general constraint programming problems specified
in the MiniZinc modelling language.
LBBD is particularly well suited to alternative scheduling problems,
which are a useful middle-ground between traditional scheduling problems
and temporal planning. 
This framework is then exploited by our ``Operator Scheduling''
approach in section \ref{chp:opsched} to try to bridge the gap between
CP-based scheduling and temporal planning, by defining a way to model
open-ended scheduling problems in CP so that the problem can be
sensibly extended incrementally and make use of important
resource-constrained scheduling constraints like ``cumulative'' and
``alldifferent''.

Another Mathematical Programming decomposition: ``Column-generation'',
in contrast does not have an obvious analogue in SAT or CP, as the
approach relies on the so-called ``dual solution'' of a linear
program.  The dual solution to a resource allocation problem assigns
prices to each resource in a way that encourages agents to avoid
resource bottlenecks, and thus to effectively cooperate.
We introduce a multiagent planning algorithm: ``Fragment-Based
planning'' in Part I of this thesis.
This approach is a hybrid that uses state-space search as a
sub-problem solver within a plan-space algorithm that commits to one
agent's plan at a time.
At each iteration, the dual solution is used to learn better plans for
some subset of the agents which avoid conflicts seen in previous
iterations and improve the current relaxed solution.



%% \subsection{A note on the separation of NP and PSPACE}

%% Several of the algorithms discussed are incremental compilations from
%% PSPACE-hard planning problems to NP-complete problems.  On the surface
%% this may seem like a counter-example to the separation of NP and
%% PSPACE,
%% however this is really no different to the fact that extracting the
%% final solution from a priority queue in $A^*$ is not PSPACE-hard: in
%% the worst case, an exponential amount of information must be added to
%% the open-list, regardless of if that open-list is explicit (e.g. a heap), or
%% a symbolic (e.g. a MIP).
%% In particular it is important to remember the distinction between
%% polynomial and pseudo-polynomial compilations. These issues will be
%% highlighted in the relevant chapters where they arise throughout this
%% thesis.

%% Nonetheless, this type of compilation exploits the observation made
%% above, that in many practical situations, an exponential amount of
%% information may not be necessary, and algorithms are designed in the
%% hope that some practical instances fall into this sub-exponential
%% category.


