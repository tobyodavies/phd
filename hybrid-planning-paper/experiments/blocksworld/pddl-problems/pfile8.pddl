

(define (problem BW-rand-11)
(:domain blocksworld)
(:objects b1 b2 b3 b4 b5 b6 b7 b8 b9 b10 b11  - block
	  h1 h2 h3 h4 h5 h6 h7 h8 h9 h10 h11  - hand)
(:init
(handempty h1)(handempty h2)(handempty h3)(handempty h4)(handempty h5)(handempty h6)(handempty h7)(handempty h8)(handempty h9)(handempty h10)(handempty h11)
(on b1 b8)
(on b2 b1)
(ontable b3)
(on b4 b3)
(on b5 b2)
(ontable b6)
(on b7 b5)
(on b8 b9)
(ontable b9)
(on b10 b4)
(on b11 b6)
(clear b7)
(clear b10)
(clear b11)
)
(:goal
(and
(on b3 b4)
(on b5 b2)
(on b6 b8)
(on b8 b11)
(on b10 b1)
(on b11 b10))
))



