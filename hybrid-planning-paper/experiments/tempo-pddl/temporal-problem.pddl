(define (problem Toby_test)
(:domain Toby)
(:objects
	m1 - location
	m2 - location
	m3 - location
	j - location
	d - location

	p1 - location
	p2 - location

	o1 - order
	o2 - order
	o3 - order

	c1 - consist
	c2 - consist
	c3 - consist

)
(:init
	(conn m1 d) (conn d m1)  (conn m2 j) 
	(conn j m2)  (conn m3 j) (conn j m3) 
	(conn j d) (conn d j) (conn d y) 
	(conn y d) (conn y p1) (conn p1 y) 
	(conn y p2) (conn p2 y)   

	(order_mine o1 m1) 	(order_mine o2 m2) 	(order_mine o3 m3)
	(order_port o1 p1) 	(order_port o2 p2) 	(order_port o3 p1)
	(not-delivered o1)      (not-delivered o2)      (not-delivered o3)	

	(clear m1)		(clear m2)		(clear m3)	
	(clear j)		(clear d)		(clear y)
	(clear p1)		(clear p2)

	(= (load_after_time o1) 0)
	(= (load_after_time o2) 0)
	(= (load_after_time o3) 0)

	(= (unload_before_time o1) 16)
	(= (unload_before_time o2) 16)
	(= (unload_before_time o3) 16)

	(at c1 y)	(at c2 y)	(at c3 y)
	(empty c1)	(empty c2)	(empty c3)
	(available c1)	(available c2)	(available c3)

	(= (current_time c1) 0)
	(= (current_time c2) 0)
	(= (current_time c3) 0)

	(= (dummy_time) 0)

)
(:goal
(and
;		(at c1 y)	(at c2 y)	(at c3 y)
		(delivered o1) (delivered o2) (delivered o3)
;		(loaded c1 o1)
;		(loaded c2 o2)
)
)
(:metric minimize (total-time))
)
