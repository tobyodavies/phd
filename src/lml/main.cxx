/*
Lightweight Automated Planning Toolkit
Copyright (C) 2012
Miquel Ramirez <miquel.ramirez@rmit.edu.au>
Nir Lipovetzky <nirlipo@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <strips_prob.hxx>
#include <action.hxx>
#include <strips_state.hxx>
#include <fd_to_aptk.hxx>
#include <aptk/time.hxx>

#include <iostream>
#include <iterator>
#include <boost/program_options.hpp>
#include <vector>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <utility>
#include <string>

#include "landmark.hxx"
#include "engine.hxx"


namespace po = boost::program_options;

using aptk::STRIPS_Problem;
using aptk::State;
using lml::LMDB;
using lml::SearchEngine;

struct Timer {
	std::string msg;
	clock_t t0;
	Timer(std::string msg_):msg(msg_), t0(clock()){
		std::cerr << "Start " << msg << std::endl;
	}
	~Timer(){
		std::cerr << msg << " took " << 1.0*(clock() - t0)/CLOCKS_PER_SEC << " seconds"  << std::endl;	}
};


void process_command_line_options( int ac, char** av, po::variables_map& vars ) {
	po::options_description desc( "Options:" );
	
	desc.add_options()
		( "help", "Show help message" )
		( "sas-file",
		  po::value<std::string>()->default_value("output"),
		  "FD preprocessor output file" )
	;
	
	try {
		po::store( po::parse_command_line( ac, av, desc ), vars );
		po::notify( vars );
	}
	catch ( std::exception& e ) {
		std::cerr << "Error: " << e.what() << std::endl;
		std::exit(1);
	}

	if ( vars.count("help") ) {
		std::cout << desc << std::endl;
		std::exit(0);
	}

}

int main( int argc, char** argv ) {
	po::variables_map vm;

	process_command_line_options( argc, argv, vm );

	STRIPS_Problem	prob;
	std::vector<aptk::FD_Parser::DTG> dtgs;
	aptk::FD_Parser::get_problem_description( vm["sas-file"].as<std::string>(), prob, dtgs );
	
	std::cout << "PDDL problem description loaded: " << std::endl;
	std::cout << "\t#Actions: " << prob.num_actions() << std::endl;
	std::cout << "\t#Fluents: " << prob.num_fluents() << std::endl;
	std::cout << "\t#Mutexes: " << prob.mutexes().num_groups() << std::endl;
	std::cout << "\t#DTGs: " << dtgs.size() << std::endl;
	prob.compute_edeletes();

	Timer search_timer("Search");

	SearchEngine engine(prob);
	engine.setup();

	std::vector<int> plan;
	do {
		std::cerr << "LMs: " << engine.get_lmdb().size() 
			  << " (" << engine.get_lmdb().num_active()
			  << " active)" << std::endl;
		std::cerr << "LB: " << engine.lb() << std::endl;
		if(engine.lb() >= 1e9)
			break;
	} while(engine.search(plan) != NULL);

	std::cout << "; Plan length: " << plan.size() << std::endl;
	unsigned cost = 0;
	for(auto op : plan){
		cost += prob.actions()[op]->cost();
		std::cout << prob.actions()[op]->signature() << std::endl;
	}
	std::cout << "; Plan cost: " << cost << std::endl;

	return 0;
}
