"""
Test the column generator with some artificial domains
"""

import glob
import itertools
import os
import operator
import sys
from os import path
import platform
import random


import networkx as nx
import pulp
import pytest

import pymas
from pymas.utils import reduced_cost

def pypy_parse_dot(filename):
    """
    networkx's dot parser breaks horribly in pypy
    so we parse it with python2.7 and pickle to stdout
    """
    import subprocess
    import pickle
    script = path.join(path.dirname(__file__), "pickle_dot.sh")
    proc = subprocess.Popen([script, filename], stdout=subprocess.PIPE)
    proc.wait()
    return pickle.loads(proc.stdout.read())


def rglob(root_dir, base, maxdepth=200):
    return (f
            for i in xrange(maxdepth)
            for f in glob.glob(path.join(root_dir, '/'.join('*' * i) + '/' + base)))


def time_expand(G, timesteps=100):
    G2 = nx.DiGraph()
    for n0, n1, data in G.edges(data=True):
        for t in xrange(timesteps):
            G2.add_edge((n0, t), (n1, t + float(data.get('weight', 1))), **data)
            G2.add_edge((n1, t), (n0, t + float(data.get('weight', 1))), **data)
    for n, data in G.nodes(data=True):
        for t in xrange(timesteps):
            G2.add_edge((n, t), (n, t + 1), **data)
    return G2


class Network(object):
    def __init__(self, filename, orders=None):
        self.G = time_expand(pypy_parse_dot(filename))
        space_nodes = set(n for (n, t) in self.G.nodes())
        mines = set(n for n in space_nodes if n.startswith('m'))
        ports = set(n for n in space_nodes if n.startswith('p'))
        orders = [((m, t0 + d - 30), (p, t0 + d))
                  for (m, t0) in zip(mines, xrange(20, 50, 30/len(mines)))
                  for (p, d) in zip(ports, xrange(30, 90, 5))
                  if t0 + d + 10 < 100]
        print mines, ports, orders
        #network = nx.Graph([(0, 2), (1, 2), (2, 3), (3, 4), (3, 5), (2, 6), (6, 3)])
        self.edge_resources = {(i, j): pymas.Resource(1 if (i[0], j[0]) != ('y', 'y') else 100)
                               for (i, j) in self.G.edges()
                               if i <= j}
        self.order_resources = {(src, snk): pymas.Resource(1)
                                for src, snk in orders}


DOTFILES = list(rglob(path.dirname(__file__), 'test_*.dot'))


@pytest.fixture(params=DOTFILES)
def network(request):
    return Network(request.param)


@pytest.fixture(params=pymas.utils.SUPPORTED_SOLVERS)
def solver_type(request):
    return request.param


def generator(network, resource_costs):
    M = 1.0
    graph = nx.DiGraph(network.G)
    rc = lambda f: reduced_cost(resource_costs, f)
    if resource_costs:
        M = max(abs(cost) for cost in resource_costs.itervalues()) + 1
    for src, snk in graph.edges():
        r = network.edge_resources.get((src, snk))
        graph[src][snk]['weight'] = M - float(resource_costs.get(r, 1))
    ## print "#orders:", len(network.order_resources)
    for (src, snk), order in network.order_resources.items():
        for start_t in xrange(max([0, src[1]-10]), src[1]-1):
            try:
                #print "Shortest path for %r -> %r"%(('y', start_t), src),
                path = nx.shortest_path(graph, ('y', start_t), src, weight='weight')
                #print len(path)
                #print "Shortest path for %r -> %r -> %r"%(('y', start_t), src, snk),
                path += nx.shortest_path(graph, src, snk, weight='weight')
                try:
                    for end_t in xrange(snk[1]+1, snk[1]+10):
                        p = list(path)
                        p += nx.shortest_path(graph, snk, ('y', end_t), weight='weight')
                        resources = {order: 1}
                        edges = zip(p[1:], p[:-1])
                        er = network.edge_resources
                        resources.update({er.get((n0, n1), er.get((n1, n0))): 1 for (n0, n1) in edges})
                        if None in resources:
                            del resources[None]
                            frag = pymas.Fragment(cost=-1000 - len(resources), res_usage=resources)
                            ## print "frag:", frag.cost, len(resources)
                        ##
                        if rc(frag) < 0:
                            ## print "Shortest path for %r -> %r -> %r ->%r"%(('y', start_t), src, snk, ('y', end_t)), rc(frag)
                            yield frag
                except nx.NetworkXNoPath:
                    pass
            except nx.NetworkXNoPath:
                pass


def mk_generator(network):
    def incr_generator(resource_costs, min_rc=0.0, _ignore=None):
        return generator(network, resource_costs)
    return incr_generator


def test_colgen(network, solver_type):
    if not solver_type().available():
        pytest.xfail()
    if platform.python_implementation() == 'PyPy' and solver_type == pulp.GUROBI:
        pytest.xfail()

    gen = mk_generator(network)

    s = pymas.FragSelector(list(gen({})), gen, solver_type=solver_type)
    print "#frags:", len(s.fragments)
    obj, cost, selection = s.solve()

    print selection
    print "orders:", list(network.order_resources)
    print "fulfiled:", list(k
                            for f in selection
                            for r in f.resources
                            for k in network.order_resources
                            if network.order_resources[k] == r)
    print "len(soln):", len(selection)
    print "cost:", cost
    print "lpcost:", obj
    print "#frags:", len(s.fragments)

    assert len(selection) == len(network.order_resources), "There exists an unsatisfied order"
    assert (len(set(r for f in selection for r in f.resources)) <=
            len(network.edge_resources) + len(network.order_resources)), "A Resource was used more than once"


if __name__ == '__main__':
    G = pypy_parse_dot(DOTFILES[0])
    print G, len(G)
    test_colgen(Network(DOTFILES[0]), pulp.COIN)
