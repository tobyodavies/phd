
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preliminaries}
\label{bg}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{The Situation Calculus and Basic Action Theories.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 The \textit{situation calculus} is a logical language specifically
designed for representing and reasoning about dynamically changing
worlds \cite{Reiter2001}.
%
All changes to the world are the result of \textit{actions},
which are terms in the language. We denote action variables by
lower case letters $a$, and
action terms by $\alpha$, possibly with subscripts.
%
A possible world history is represented by a term called a
\emph{situation}. The constant $S_0$ is used to denote the initial
situation where no actions have yet been performed. Sequences of
actions are built using the function symbol $do$, such that $do(a,s)$
denotes the successor situation resulting from performing action $a$
in situation $s$.
%
Predicates and functions whose value varies from situation to situation are
called \textit{fluents}, and are denoted by symbols taking a
situation term as their last argument (e.g., $\Holding(x,s)$).

Within the language, one can formulate \emph{basic action theories} that describe
how the world changes as the result of the available actions \cite{Reiter2001}.
These theories, combined with \Golog{}, are more expressive than STRIPS or ADL \cite{Roger2007}.
%% As a result
%% a basic action theory $\D$ is the union of the following disjoint
%% sets: the foundational, domain independent, axioms of the situation
%% calculus ($\Sigma$); precondition axioms stating when actions can be
%% legally performed ($\D_{poss}$); successor state axioms describing how
%% fluents change between situations ($\D_{ssa}$); unique name axioms for
%% actions and domain closure on action types ($\D_{ca}$); and axioms
%% describing the initial configuration of the world ($\D_{S_0}$).
% %%
Two special fluents are used to define a legal execution:
$\Poss(a,s)$ is used to state that action $a$ is executable in situation $s$;
and $\Conflict(\id{as}, s)$ is used to state that the set of actions 
$\id{as}$ may not be executed concurrently.
 %% precondition axioms in $\D_{poss}$ characterize this predicate.
% %%%
%% In turn, successor state axioms encode the causal laws of the world being
%% modeled; they take the place of the so-called effect axioms
%% and provide a solution to the frame problem.
% %%%
%% Similarly $\Conflict(as, s)$ is used to state that a set $as$ of actions
%% cannot be legally executed concurrently.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{High-Level Programs.}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% To represent and reason about complex actions or processes obtained by suitably
%% executing atomic actions, various so-called \emph{high-level programming
%% languages} have been defined.
%
% , such as \Golog\ \cite{Levesque:JLP97-Golog}, which
% includes usual structured constructs and constructs for nondeterministic choices,
% \ConGolog\ \cite{DeGiacomoLL:AIJ00-ConGolog}, which extends \Golog\ to
% accommodate concurrency, and \IndiGolog\ \cite{SardinaDGLL:AMAI04}, which
% provides means for interleaving planning and execution.
%

High-level non-deterministic programs can be used to specify complex goals:
the goal of a \Golog{} program is to find a sequence of actions
generated by some path through the program.
We use temporal semantics from \mindigolog{} \cite{Kelly2006} which
builds on \ConGolog{} \cite{De_Giacomo2000}, and refer to these
extensions simply as \Golog{}.
A \Golog{} program $\delta$ is defined in terms of the following
structures:
% the language, except for (recursive) procedures:
% \small
\begin{quote}\small
$\alpha$           \hfill  atomic action\\
$\varphi?$            \hfill test for a condition\\
$\delta_1;\delta_2$        \hfill  sequential composition\\
%% $\mif\ \varphi\ \mthen\ \delta_1\ \melse\ \delta_2$
%%                 \hfill  conditional\\
$\mwhile\ \varphi\ \mdo\ \delta$
                \hfill  while loop\\
$\delta_1 \ndet \delta_2$   \hfill  nondeterministic branch\\
$\pi x.\delta$     \hfill  nondeterministic choice of argument\\
$\delta^*$             \hfill  nondeterministic iteration\\
$\delta_1 \conc \delta_2$  \hfill concurrent composition
\end{quote}
%%
In the above, $\alpha$ is an action term, possibly with parameters,
$\delta$ is a \Golog{} program, and $\varphi$ a
situation-suppressed formula, that is, a formula in the language with
all situation arguments in fluents suppressed.
%We denote by $\varphi[s]$ the situation calculus formula obtained from
%$\varphi$ by restoring the situation argument $s$ into all fluents in
%$\varphi$.
% Note the presence of nondeterministic constructs, which allow the loose
% specification of programs by leaving ``\textit{gaps}'' that ought to be resolved
% by the executor.
% % %%
Program $\delta_1 \ndet \delta_2$ allows for the nondeterministic choice
between programs $\delta_1$ and $\delta_2$, while $\pi x.\delta$ executes program
$\delta$ for \textit{some} nondeterministic choice of a legal binding for
variable $x$.  $\delta^*$ performs $\delta$ zero or more times.
Program $\phi?$ succeeds only if $\phi$ holds in the current situation.
% %%
Program $\delta_1 \conc \delta_2$ expresses the concurrent execution
of $\delta_1$ and $\delta_2$.
For notational convenience we add:
\begin{quote}\small
$\pi(x \in X). \delta$ \hfill equivalent to $\pi x. (x \in X)? ; \delta$ \\
%$\texttt{for } \,\, x \,\, \texttt{in} \,\, vs \,\, \texttt{do} \,\, \delta$ \hfill equivalent to $\pi(v \in vs).\delta;(\texttt{for } x \in vs \texttt{\textbackslash} \{v\}\texttt{ do } \delta)$ \\
$\texttt{foreach} \,\, x \,\, \texttt{in} \,\, vs \,\, \texttt{do} \,\, \delta$ \hfill equivalent to $\delta_{[x/v_1]} ; \cdots ; \delta_{[x/v_n]}$ \\
$\texttt{forconc} \,\, x \,\, \texttt{in} \,\, vs \,\, \texttt{do} \,\, \delta$ \hfill equivalent to $\delta_{[x/v_1]} || \cdots || \delta_{[x/v_n]}$
\end{quote}

\noindent
Here $\delta_{[x/y]}$ denotes the program $\delta$ where
each occurrence of variable $x$ has been replaced with the value
$y$, and $v_i$ is the $i$th element of the sequence $vs$.

%Finally, as in \cite{Sardina2009}, we require that in programs of the form $\pi
%x.\delta$, the variable $x$ occurs in some non-variable action term in $\delta$;
%we disallow cases where $x$ occurs \emph{only} in tests or as an action itself.
%In this way, $\pi x.\delta$ acts as a construct for making nondeterministic choices
%of action parameters (possibly constrained by tests).

