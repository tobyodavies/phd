#ifndef ARGMIN_HXX

#define ARGMIN_HXX
#include <type_traits>

template <class C>
struct container_value {
	typedef decltype(*(std::declval<C>().begin())) value;
	typedef typename std::remove_reference<value>::type type;
};

template <class C, class CB, class V, class value_type=typename container_value<C>::type >
value_type argminbb(C container, V ub, CB cb) {
	auto best = *(container.begin());
	auto best_f = cb(best, ub);
	for(auto iel = container.begin(); iel != container.end(); ++iel){
		auto el = *iel;
		auto new_f = cb(el, best_f);
		if(new_f < best_f){
			best = el;
			best_f = new_f;
		}
	}
	return best;
}

template <class C, class CB, class value_type=typename container_value<C>::type >
value_type argmin(C container, CB cb) {
	auto best = *(container.begin());
	auto best_f = cb(best);
	return argminbb(container, best_f,
			[&](value_type v, decltype(best_f) ub){
				return cb(v);
			});
}

#endif
