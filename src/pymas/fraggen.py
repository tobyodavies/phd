"""
The classes in the fragment generator
"""


import copy
import collections
import sys

from decorator import decorator

from pymas.utils import LinkedList, SearchQ


class PlanSearchNode(collections.namedtuple('PlanSearchNode', ['sit', 'prog'])):
    """
    A situation, residual program pair
    """

    def expand(self, duals):
        """
        Expand all nodes, one transition at a time
        """
        if self.prog is not None:
            for sit, prog in self.prog.trans(self.sit):
                newnode = PlanSearchNode(sit, prog)
                rem = prog.lowbound(sit, duals) if prog else 0
                cost = sit.cost(duals)
                weighted_cost = (sit.landmarks()+1) * cost
                lowbound = rem + cost
                if lowbound > -1e-10:
                    continue
                yield (weighted_cost, prog.size(), lowbound), lowbound, newnode

    def heuristic(self, duals):
        """
        If the current situation is accepting, return the situation
        """
        if self.prog is None or self.prog.final(self.sit):
            return self.sit.cost(duals), self.sit
        return None, None


class ProgramComposerMixin(object):
    """
    The mechanics of composing programs
    """
    def fraggen(self):
        """
        Return a fragment generator (having trans/final methods)
        """
        raise NotImplementedError()

    def __or__(self, other):
        return Alternate(self.fraggen(), other.fraggen())

    def __add__(self, other):
        return Sequence(self.fraggen(), other.fraggen())

    def repeat(self):
        "Golog's `*` operator"
        return Iterate(self.fraggen())

    def repeat_while(self, fluent):
        "Syntactic sugar"
        return Iterate(Assert(fluent) + self.fraggen()) + Assert(~fluent)


class AbstractSearchStrategy(ProgramComposerMixin):
    """
    Composable search stategies for satisfying goals and subgoals
    """

    children = ()

    def __init__(self):
        self._subs = {}
        self._lowbound = None

    def fraggen(self):
        """
        Return a fragment generator (having trans/final methods)
        """
        return self

    def trans(self, _sit):
        """
        Returns an iterable of pairs of new situation, residual program pairs
        """
        return []

    def final(self, _sit):
        """
        Returns true if the program can legally terminate in `sit`

        Analagous to an accepting state in a ND automaton
        """
        return False

    def solutions(self, s0, duals=None, solution=None, min_rc=0, existing_solutions=None):
        """
        Find some solutions in order of decreasing (reduced) cost
        """
        def expand(plannode):
            return plannode.expand(duals)

        def heuristic(plannode):
            return plannode.heuristic(duals)

        existing_solutions = existing_solutions or {}
        duals = duals or {}
        q = SearchQ(expand=expand,
                    heuristic=heuristic,
                    soln_filter=lambda f: f not in existing_solutions)
        initial_node = PlanSearchNode(s0, self)
        q.addbranch(None, None, initial_node)
        q.add_solution(s0, min_rc)
        if self.final(s0):
            q.add_solution(s0, s0.cost(duals))
        if solution is not None:
            q.add_solution(solution, solution.cost(duals))
        return q.solutions()

    def lowbound(self, sit, duals=None):
        """
        Compute a lower-bound for executing this program in some future of `sit`
        """
        if self._lowbound is None:
            if hasattr(self, 'children'):
                self._lowbound = sum(c.lowbound(sit, duals)
                                     for c in self.children
                                     if c)
            else:
                self._lowbound = 0.
        return self._lowbound

    def sub(self, var, val):
        """
        Substitute `val` for `var` throughout this program
        """
        if (var, var) not in self._subs:
            subbed = self
            if hasattr(self, 'children'):
                subbed = copy.copy(self)
                subbed.children = [c.sub(var, val)
                                   for c in self.children]
            self._subs[var, val] = subbed
        return self._subs[var, val]

    def size(self):
        """
        An approximate measure of the amount of program left to execute
        """
        if hasattr(self, 'children'):
            return 1 + sum(c.size() for c in self.children if c)
        return 1

    def __repr__(self):
        return ("{self.__class__.__name__}{children!r}"
                .format(self=self,
                        children=getattr(self, 'children', ())))

    def __hash__(self):
        return hash(tuple(self.children)) ^ hash(self.__class__)

    def __eq__(self, other):
        return self is other or (self.__class__ == other.__class__ and
                                 tuple(self.children) == tuple(other.children))


class VarChoice(AbstractSearchStrategy):
    """
    Golog's `pi` operator
    """

    def __init__(self, var, scope, index=0):
        AbstractSearchStrategy.__init__(self)
        self.var = var
        self.choices = var.domain
        #print var, scope, index
        self.scope = scope.fraggen()
        self.index = index

    def sub(self, var, val):
        if (var, val) not in self._subs:
            if var != self.var:
                newscope = self.scope.sub(var, val)
            else:
                newscope = self.scope
            self._subs[var, val] = VarChoice(self.var,
                                             newscope)
        return self._subs[var, val]

    def domain_empty(self):
        return self.index >= len(self.choices)

    def label(self):
        return (self.scope.sub(self.var, self.choices[self.index]),
                self.__class__(self.var, self.scope, self.index + 1))

    def trans(self, sit):
        if self.var:
            prog, newchoice = self.label()
            #print "sub", sit.cost(), prog
            yield sit, prog
            if not newchoice.domain_empty():
                yield sit, newchoice

    def size(self):
        return 1 + len(self.choices)*self.scope.size()

    def lowbound(self, sit, duals=None):
        return min([self.scope.sub(self.var, c).lowbound(sit, duals)
                    for i in xrange(self.index, len(self.choices))
                    for c in [self.choices[i]]] + [sys.maxint])

    def __eq__(self, other):
        return self is other

    def __repr__(self):
        return "<{self.__class__.__name__}({self.var}, {self.scope}, {self.index}) {choices}>".format(self=self, choices=list(self.choices)[self.index:])

class Sequence(AbstractSearchStrategy):
    """
    execute some sequence of sub-programs sequentially

    Implements Golog's `;` operator
    """

    def __init__(self, *strategies):
        AbstractSearchStrategy.__init__(self)
        self.children = tuple(s for s in strategies if s is not None)

    def trans(self, sit):
        seq = self.children
        if seq:
            if seq[0].final(sit):
                yield sit, Sequence(*seq[1:])
            for nxt, prog in seq[0].trans(sit):
                yield nxt, Sequence(prog, *seq[1:])

    def final(self, sit):
        return all(p.final(sit) for p in self.children)

    def __add__(self, other):
        return Sequence(*(self.children + (other.fraggen(),)))


class Alternate(AbstractSearchStrategy):
    """
    Perform one of a set of sub-programs

    Implements Golog's `|` operator
    """

    def __init__(self, *strategies):
        AbstractSearchStrategy.__init__(self)
        self.children = strategies

    def trans(self, sit):
        for prog in self.children:
            yield sit, prog

    def final(self, sit):
        return any(p.final(sit) for p in self.children)

    def lowbound(self, sit, duals=None):
        return min(c.lowbound(sit, duals)
                   for c in self.children)

    def __or__(self, other):
        return Alternate(*(self.children + (other.fraggen(),)))


class Iterate(AbstractSearchStrategy):
    """
    Perform some sub-program zero-or-more times

    Golog's `*` operator
    """

    def __init__(self, scope):
        AbstractSearchStrategy.__init__(self)
        self.children = [scope]

    def trans(self, sit):
        yield sit, None
        subprog = self.children[0]
        for sit, prog in subprog.trans(sit):
            if prog is not None:
                yield sit, prog + self
            ## else:
            ##     yield sit, self

    def final(self, _sit):
        return True

    def lowbound(self, sit, duals=None):
        if self.children[0] and self.children[0].lowbound(sit, duals) < 0:
            return -sys.maxint
        return 0

    def size(self):
        """
        Since this can be executed 0 times, give it constant size
        """
        return 1


class Assert(AbstractSearchStrategy):
    """
    Assert that a formula holds
    """

    def __init__(self, func, *args, **kwargs):
        AbstractSearchStrategy.__init__(self)
        self.func = func
        self.args = args
        self.kwargs = kwargs

    def sub(self, var, val):
        args = [val if a == var else a
                for a in self.args]
        kwargs = {k: (val if a == var else a)
                  for k, a in self.kwargs.iteritems()}
        return Assert(self.func, *args, **kwargs)

    def trans(self, sit):
        if self.func(sit, *self.args, **self.kwargs):
            yield sit, None

    def __eq__(self, other):
        return all(sf == of
                   for (sf, of) in [(self.func, other.func),
                                    (self.args, other.args),
                                    (self.kwargs, other.kwargs)])


class PerformActions(AbstractSearchStrategy):
    """
    This is the most basic program, a sequence of primitive actions
    """

    def __init__(self, *actions):
        AbstractSearchStrategy.__init__(self)
        self.actions = actions

    def trans(self, sit):
        newsit = sit.do(*self.actions)
        if newsit:
            yield newsit, None

    def final(self, _sit):
        return len(self.actions) == 0

    def lowbound(self, sit, duals=None):
        return sum(act.min_cost(sit, duals)
                   for act in self.actions)

    def sub(self, var, val):
        return PerformActions(*(a.sub(var, val) for a in self.actions))

    def __add__(self, others):
        if isinstance(others, PerformActions):
            return PerformActions(self.actions + others.actions)
        return AbstractSearchStrategy.__add__(self, others)

    def size(self):
        """
        The size is 1 because actions must all be performed
        and thus add no more branching
        """
        return int(len(self.actions) > 0)

    def __repr__(self):
        return "<" + '; '.join("%s"%a for a in self.actions) + ">"

    def __hash__(self):
        return hash(tuple(self.actions))

    def __eq__(self, other):
        return hasattr(other, 'actions') and self.actions == other.actions

NOOP = PerformActions()


class ProcCall(AbstractSearchStrategy):
    """
    Call a procedure (A python function returning a possibly recursive plan)
    """
    def __init__(self, *args, **kwargs):
        AbstractSearchStrategy.__init__(self)
        #self.func = func
        self.args = args
        self.kwargs = kwargs

    def sub(self, var, val):
        args = tuple(a if a != var else val
                     for a in self.args)
        kwargs = dict(((k if k != var else val), (v if v != var else val))
                      for (k, v) in self.kwargs.iteritems())
        return self.__class__(*args, **kwargs)

    def trans(self, sit):
        if False:
            yield sit, None

    def final(self, _sit):
        return False


def proc(func):
    """
    A decorator for turning a python function/method into a ProcCall
    """
    class Proc(ProcCall):
        def __init__(self, *args, **kwargs):
            self.func = func
            super(Proc, self).__init__(*args, **kwargs)
        def trans(self, sit):
            ret = func(sit, *self.args, **self.kwargs)
            yield sit, ret

        @classmethod
        def mincost_func(cls, func):
            def lowbound(self, sit, duals=None):
                return func(sit, self, duals)
            cls.lowbound = lowbound
    return Proc
