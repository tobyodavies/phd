"""
Simple domain for blocksworld
"""

import collections
from os import path
import random
import operator
import time

import pymas
from pymas import VarChoice, Var, action, situation, fluent, proc, Assert, NOOP
from pymas.utils import reduced_cost

#NOOP = None

class BlocksAgent(situation('BlocksWorld')):
    """
    Agent class for blocksworld
    """
    blocks = 'ABC'
    init_on = {'C': 'A'}
    goal_on = {'A': 'B', 'B': 'C', 'C': None}
    nagents = 5
    _state = None

    def dest(self, block):
        return self.goal_on.get(block, False)

    def is_goal(self, block1, block2):
        return self.dest(block1) == block2

    @fluent
    def time(self):
        if self is self.s0():
            return 0
        if isinstance(self.head, BlocksAgent.wait):
            (dt,) = self.head.args
            return dt + self.tail.time()
        if isinstance(self.head, BlocksAgent.persist):
            return self.tail.time()
        return 1 + self.tail.time()

    @fluent
    def on(self, block):
        if self is self.s0():
            return self.init_on.get(block)
        if isinstance(self.head, BlocksAgent.grab):
            (dropped,) = self.head.args
            if dropped == block:
                return None
        if isinstance(self.head, BlocksAgent.drop):
            onto, = self.head.args
            dropped = self.tail.holding()
            if dropped == block:
                return onto
        return self.tail.on(block)

    @fluent
    def is_handled(self, block):
        return self.on(block) == self.dest(block)

    @fluent
    def is_dest(self, block, dest):
        return self.dest(block) == dest

    def above(self, block1, block2):
        if block2 is None:
            return not self.holding() == block1
        below1 = self.on(block1)
        if below1 is None:
            return False
        return below1 == block2 or self.above(below1, block2)

    def depth(self, block):
        top_block = next((b
                          for b in self.blocks
                          if self.on(b) == block),
                         None)
        if top_block is None:
            return 0
        return 1 + self.depth(top_block)

    def desired_height(self, block):
        on_block = self.goal_on.get(block)
        if on_block is None:
            return 0
        return 1 + self.desired_height(on_block)

    @fluent
    def holding(self):
        if self is self.s0():
            return None
        if isinstance(self.head, BlocksAgent.grab):
            return self.head.args[0]
        if isinstance(self.head, BlocksAgent.drop):
            return None
        return self.tail.holding()

    def handled_blocks(self):
        if self is self.s0():
            return set()
        if isinstance(self.head, BlocksAgent.drop):
            holding = self.tail.holding()
            onto, = self.head.args
            #print holding, onto
            #print self.head
            if self.is_goal(holding, onto):
                return set([holding]) | self.tail.handled_blocks()
        return self.tail.handled_blocks()

    def clear(self, block, time=None):
        if time is None:
            time = self.time()
        clear = (time == 0 and
                 not any(block == self.init_on[b] for b in self.init_on))
        avail = 1 if clear else 0
        return self.shared('clear', block, time, avail=avail)

    @action
    def grab(self, block):
        ## print "grab(%s)"%block
        ## print "holding(%s)"%self.holding()
        if self.holding() is None and block not in self.handled_blocks():
            was_on = self.on(block)
            was_goal = self.is_goal(block, was_on)
            cost = 1 if not was_goal else 99
            res = {self.clear(block, self.time()): 1}
            if was_on is not None:
                res.update({self.clear(was_on, self.time()+1): -1})
            ## print cost, res
            return cost, res
    grab.MINCOST = -1000

    @action
    def drop(self, onto=None):
        ## if self.holding() == 'A':
        ##     print "Drop(%s)"%onto, list(reversed(list(self))), self.cost()
        ##     print "holding(%s)"%self.holding()
        block = self.holding()
        if self.holding() is not None:
            ## print "drop(%s) should succeed"%self.holding()
            cost = 1
            res = {self.clear(block, self.time()+1): -1}
            if onto is not None:
                res.update({self.clear(onto, self.time()): 1})
            if self.is_goal(block, onto):
                ## print "Drop should be a goal"
                res.update({self.shared('finished', block): 1})
                cost = -100
            ## print cost, res
            return cost, res
    drop.MINCOST = -100

    @drop.min_res_func
    def _drop(self, onto=None):
        res = {}
        block = self.holding()
        if self.is_goal(block, onto):
            res.update({self.shared('finished', block): 1})
        return res

    @action
    def wait(self, dt=1):
        block = self.holding()
        res = {}
        ## if block:
        ##     res = {self.shared('holding', block, t, avail=1): 1
        ##            for t in xrange(self.time(), self.time() + dt)}
        ##     print res
        return 1.0 * dt + 1e-6, res

    @action
    def persist(self, resource, units=1, time_delta=1):
        args = resource.label
        t_idx = args[-1]
        newargs = args[:-1] + (t_idx + time_delta,)
        newres = self.shared(*newargs, avail=0)
        return 1e-6, {resource: units, newres: -units}

    @proc
    def trace(self, msg):
        print msg(self)
        return NOOP

    @proc
    def wait_until(self, earliest, latest=None, max_delta=None):
        latest = earliest if latest is None else latest
        now = self.time()
        if now <= earliest:
            if max_delta is None:
                max_delta = latest - self.time() + 1
            waits = Var(xrange(max(0, earliest - now), now + max_delta + 1))
            return VarChoice(waits, self.wait(waits))

    @proc
    def maybe_wait(self, *args):
        #        return NOOP
        waits = Var(range(*args))
        return (
    #            NOOP |
            VarChoice(waits, self.wait(waits)))

    @proc
    def delay_use(self, resource_fn, time_delta):
        res, units = resource_fn(self.time())
        return (self.persist(res, units, time_delta) +
                self.wait(time_delta))

    @proc
    def maybe_delay(self, resource_fn, *args):
        waits = Var(range(*args))
        return (
    #            NOOP |
            VarChoice(waits,
                      self.delay_use(resource_fn, waits)))

    def move_block_to(self, block, onto):
        min_wait = max(0, self.s0().depth(block) - self.time())
        #print "min_wait(%s)"%block, min_wait
    ##     return (self.maybe_wait(min_wait, len(self.blocks)) +
    ## #self.maybe_delay(lambda t: (self.clear(block, time=t), 1), 2) +
    ##             self.grab(block) +
    ##             self.maybe_wait(len(self.blocks)) +
    ## #self.maybe_delay(lambda t: (self.clear(onto, time=t), -1), 2) +
    ##             self.drop(onto))
        return (self.wait_until(self.s0().depth(block), len(self.blocks)) +
                self.grab(block) +
                self.wait_until(self.s0().desired_height(block), max_delta=len(self.blocks)) +
                self.drop(onto))

    @proc
    def handle_block(self, block):
        onto = self.dest(block)
        #print block, "->", onto
        #onto = Var(self.blocks)
        #onto_check = Assert(BlocksAgent.is_dest(block, onto))
        ## return (
        ##         (self.move_block_to(block, None) + VarChoice(onto, self.move_block_to(block, onto) + onto_check))
        ##         |
        ##         VarChoice(onto, self.move_block_to(block, onto) + onto_check)
        ##        )
        return ((self.move_block_to(block, None) + self.move_block_to(block, onto))
                |
                self.move_block_to(block, onto))

    @handle_block.mincost_func
    def _handle_block(self, proc, duals=None):
        block, = proc.args
        duals = duals or {}
        #print block, "->", self.dest(block), ">=", -100 - duals.get(block, 0)
        return -100 - duals.get(block, 0)

    @proc
    def handle_some_blocks(self):
        blocks = toposort([b
                           for b in self.blocks
                           if not any(self.s0().above(b, h)
                                      for h in self.handled_blocks())],
                          self.s0().above)
        print self
        first = Var(blocks)
        rep = BlocksAgent.handle_some_blocks()
        #print VarChoice(first, self.handle_block(first)).lowbound(self)
        return ((VarChoice(first, BlocksAgent.handle_block(first)).repeat() + rep)
                |
                (VarChoice(first, BlocksAgent.handle_block(first)).repeat())
                |
                rep)

    @handle_some_blocks.mincost_func
    def _handle_some_blocks(self, proc, duals=None):
        blocks = [b
                  for b in self.blocks
                    if not any(self.s0().above(b, h)
                               for h in self.handled_blocks())]
        duals = duals or {}
        #print blocks
        return sum(min(-100 - duals.get(self.shared('finished', block), 0), 0)
                   for block in blocks)

    def subgoals(self, duals=None):
        duals = duals or collections.Counter()
        blocks = list(self.blocks)
        random.shuffle(blocks)
        sgoals = [self.handle_block(block)
                  for block in blocks]
        sortorder = sorted([i for i in xrange(len(blocks))],
                           key=lambda i: sgoals[i].lowbound(self, duals))
        ## print "blocks: ",
        ## print [(blocks[i], sgoals[i], sgoals[i].lowbound(self, duals)) for i in sortorder]
        return [sgoals[i] for i in sortorder]

    def subgoals2(self, duals=None):
        duals = duals or collections.Counter()
        blocks = list(self.blocks)
        random.shuffle(blocks)
        sgoals = [BlocksAgent.handle_some_blocks()
                  for _ in xrange(self.nagents)]
        sortorder = sorted([i for i in xrange(self.nagents)],
                           key=lambda i: sgoals[i].lowbound(self, duals))
        ## print "blocks: ",
        ## print [(blocks[i], sgoals[i], sgoals[i].lowbound(self, duals)) for i in sortorder]
        return [sgoals[i] for i in sortorder]


def solve(duals=None, min_rc=0, existing_solutions=None, nfrags=10000):
    s0 = BlocksAgent.s0()
    duals = duals or collections.Counter()
    solution_seq = [prog.solutions(s0, duals, solution=s0, min_rc=min_rc,
                                   existing_solutions=existing_solutions)
                    for prog in s0.subgoals(duals)]
    i = 0
    solns = 0
    fails = 0
    t0 = time.time()
    # print duals
    while solution_seq:
        ## print len(solution_seq),
        seq_idx = i % len(solution_seq)
        soln = next(solution_seq[seq_idx], None)
        if soln:
            ## print "soln:", soln
            frag = soln.to_fragment()
            frag.end_time = soln.time()
            if reduced_cost(duals, frag) < 0:
                #print soln.cost(), soln.cost(duals)
                #print soln
                solns += 1
                print "Time to frag %d"%(solns,), time.time() - t0, "sec"
                ## print list(reversed([(a, a.resources(s)) for a, s in soln.history])), frag.resources
                frag.nactions = len(soln)
                frag.sit = soln
                yield frag
                if reduced_cost(duals, frag) < -1:
                    nfrags -= 1
                if nfrags <= fails:
                    break
        else:
            solution_seq.pop(seq_idx)
            fails += 1
        if solns and fails:
            break
        i += 1
    print "Time to fail", time.time() - t0, "sec"


def toposort(blocks, above):
    if not blocks:
        return []
    tops = [b
            for b in blocks
            if not any(above(b2, b)
                       for b2 in blocks)]
    random.shuffle(tops)
    return tops + toposort([b for b in blocks if b not in tops], above)

if __name__ == '__main__':
    s0 = BlocksAgent.s0()
    print s0.handle_block('A')
    print list(s0.handle_block('A').solutions(s0))
