/*
Lightweight Automated Planning Toolkit
Copyright (C) 2014
Toby Davies <tobyodavies@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* 
 * 
 */

#ifndef __DEPTH_FIRST_SEARCH__
#define __DEPTH_FIRST_SEARCH__

#include <ctime>       /* clock_t, clock, CLOCKS_PER_SEC */

#include <memory>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <iostream>
#include <strips_prob.hxx>
#include <strips_state.hxx>
#include <aptk/search_prob.hxx>
#include <aptk/bit_array.hxx>

#include <minisat/core/SolverTypes.h>
#include <minisat/core/Solver.h>
#include <minisat/simp/SimpSolver.h>


namespace aptk {

namespace search {

namespace opset {

using namespace aptk;
using std::vector;
using Minisat::Lit;
using Minisat::mkLit;

typedef Minisat::SimpSolver Sat;
typedef std::unordered_map<Action_Idx, unsigned> OpSet;
typedef STRIPS_Problem STRIPS_Prob;


struct Layer {
	typedef Minisat::Var Var;
	vector<Var>           ops;
	vector<Var>           facts;
	vector< vector<Var> > counts;
	Sat&                  cnf;
	const STRIPS_Prob&    prob;
	Layer*                prev;
	Lit                   any_act;
	Var                   var_false;
	Lit                   lit_true;
	bool                  relaxed;
	

	Layer(const STRIPS_Prob& prob_, Sat& cnf_, Layer* prev_, const State& reachable, const State& relevant, bool relaxed_=false):
			cnf(cnf_), prob(prob_), prev(prev_),
			var_false(prev!=NULL?prev->var_false:cnf.newVar()),
			lit_true(~mkLit(var_false)),
			relaxed(relaxed_){
		if (!prev) {
			cnf.addClause(lit_true);
		}

		// std::cerr<< "Create new layer" <<std::endl;
		for(unsigned op = 0; op < prob.num_actions(); op++){
			ops.push_back(newOpVar(op, reachable, relevant));
		}
		// std::cerr<< "Created " << ops.size() << " op literals" <<std::endl;
		for(unsigned f = 0; f < prob.num_fluents(); f++){
			facts.push_back(newFactVar(f, reachable, relevant));
		}
		// std::cerr<< "Created " << facts.size() << " fact literals" <<std::endl;
		counts.resize(prob.num_actions());
		// std::cerr<< "Created " << counts.size() << " count lists" <<std::endl;

		// act_prev is True if an action with a lower index than op has occured in this layer
		Lit act_prev = ~lit_true;

		for(unsigned op = 0; op < prob.num_actions(); op++){
			assert(cnf.okay());
			auto action = prob.actions()[op];
			auto opLit = mkLit(ops[op]);

			// Preconditions
			auto precs = action->prec_vec();
			for(auto iprec = std::begin(precs); iprec != std::end(precs); iprec++){
				assert(cnf.okay());
				if(prev){
					// ~prec => ~opLit
					cnf.addClause(mkLit(prev->facts[*iprec]),
								  ~opLit);
					assert(cnf.okay());
				} else {
					if (!prob.is_in_init(*iprec)){
						cnf.addClause(~opLit);
						assert(cnf.okay());
						break;
					}
				}
				assert(cnf.okay());
			}

			// If a fact is true at least one of its causes must be true
			for(unsigned f = 0 ; f < prob.num_fluents(); f++){
				Minisat::vec<Lit> clause;
				auto addedLit = mkLit(facts[f]);
				clause.push(~addedLit);
				cause_lits(f, clause);
				cnf.addClause(clause);
				// Minisat::vec<Lit> bounds_clause;
				// cause_bounds_lits(f, bounds_clause, opset);
				// cnf.addClause(clause);
			}

			// If an op occurs, all its postconditions must hold
			// Add
			auto adds = action->add_vec();
			for(auto it = std::begin(adds); it != std::end(adds); it++){
				if(!relevant.entails(*it))
					continue;
				auto addLit = mkLit(facts[*it]);
				// opLit => addLit
				cnf.addClause(~opLit, addLit);
				assert(cnf.okay());
			}
			if(!relaxed){
				// Del
				auto dels = action->del_vec();
				for(auto it = std::begin(dels); it != std::end(dels); it++){
					auto delLit = mkLit(facts[*it]);
					// opLit => ~delLit
					cnf.addClause(~opLit, ~delLit);
					assert(cnf.okay());
				}
			}

			if(prev){
				countGE(op, prev->counts[op].size()-1);
			}

			// Ensure only one action occurs in a layer.
			if(!relaxed){
				// act_prev => ~opLit (alternatively opLit => ~act_prev)
				cnf.addClause(~act_prev, ~opLit);
				assert(cnf.okay());
			}
			// opLit => act_prev'
			auto act_prev2 = mkLit(auxVar());
			cnf.addClause(~opLit, act_prev2);
			assert(cnf.okay());
			// act_prev => act_prev'
			cnf.addClause(~act_prev, act_prev2);
			assert(cnf.okay());
			// act_prev' => act_prev \/ opLit
			cnf.addClause(~act_prev2, act_prev, opLit);
			act_prev = act_prev2;
		}
		any_act = act_prev;
		
		// Force no-op layers to the end:
		// prev->any_act => any_act
		// ~any_act => ~prev.any_act
		if(prev){
			cnf.addClause(~prev->any_act, any_act);
			
			assert(cnf.okay());
		}
		if(!relaxed){
			for (auto imutex = prob.mutexes().begin(); imutex != prob.mutexes().end(); imutex++){
				add_mutex(*imutex);
			}
		}
	}

	Lit prev_val(unsigned f){
		if (prev) {
			return mkLit(prev->facts[f]);
		}
		if (prob.is_in_init(f)){
			return lit_true;
		}
		return ~lit_true;
	}

	Lit prevCountGE(unsigned op, int n){
		if (n <= 0)
			return lit_true;
		if (!prev)
			return ~lit_true;
		return prev->countGE(op, n);
	}

	Lit countGE(unsigned op, int n){
		if (n <= 0){
			if(counts[op].size() == 0)
				counts[op].push_back(Minisat::var(lit_true));
			return lit_true;
		}
		if(int(counts[op].size()) <= n){
			auto iVar = cnf.newVar();
			auto iLit = mkLit(iVar);

			// iVar => countGE(op, n-1)
			cnf.addClause(~iLit, countGE(op, n-1));

			assert(int(counts[op].size()) == n);
			counts[op].push_back(iVar);

			// prevCountGE(op, n) => iVar
			cnf.addClause(~prevCountGE(op, n), iLit);
			assert(cnf.okay());
			
			// prevCountGE(op, n-1) /\ ops[op] => iVar
			cnf.addClause(~prevCountGE(op, n-1),
						  ~mkLit(ops[op]),
						  iLit);
			assert(cnf.okay());

			// iVar => prevCountGE(op, n-1)
			cnf.addClause(~iLit,
						  prevCountGE(op, n-1));
			assert(cnf.okay());

			// iVar /\ ~ops[op] => prevCountGE(op, n)
			cnf.addClause(~iLit,
						  mkLit(ops[op]),
						  prevCountGE(op, n));
			assert(cnf.okay());
		}
		return mkLit(counts[op][n]);
	}

	void cause_lits(unsigned f, Minisat::vec<Lit>& clause){
		clause.push(prev_val(f));
		auto causes = prob.actions_adding(f);
		for(auto it = std::begin(causes); it != std::end(causes); it++){
			auto op = (*it)->index();
			auto actLit = mkLit(ops[op]);
			clause.push(actLit);
		}
	}
	
	void cause_bounds_lits(unsigned f, Minisat::vec<Lit>& clause, OpSet& opset){
		if(prob.is_in_init(f)){
			clause.push(lit_true);
			return;
		}
		auto causes = prob.actions_adding(f);
		for(auto it = std::begin(causes); it != std::end(causes); it++){
			auto op = (*it)->index();
			auto actLit = countGE(op, 1);
			clause.push(actLit);
		}
	}


	void add_mutex(const Fluent_Vec& mutex){
		auto f_prev = ~lit_true;
		for(auto f = std::begin(mutex); f != std::end(mutex); f++){
			// f_prev => ~facts[*f]
			cnf.addClause(~f_prev, mkLit(facts[*f], false));
			assert(cnf.okay());
			auto f_prev2 = mkLit(auxVar());
			// f_prev => f_prev2
			cnf.addClause(~f_prev, f_prev2);
			assert(cnf.okay());
			f_prev = f_prev2;
		}

	}

	bool solve(std::vector<Lit>& assume){
		Minisat::vec<Lit> mtl_assume;
		for(auto it = std::begin(assume); it != std::end(assume); it++)
			mtl_assume.push(*it);
		return solve(mtl_assume);
	}

	bool solve(Minisat::vec<Lit>& bounds_assume){
		Minisat::vec<Lit> assume;
		bounds_assume.copyTo(assume);
		cnf.thaw();
		cnf.freezeVar(Minisat::var(lit_true));
		cnf.freezeVar(Minisat::var(any_act));
		for(unsigned f = 0; f < prob.num_fluents(); f++){
			cnf.freezeVar(facts[f]);
			if (prob.is_in_goal(f)){
				auto factLit = mkLit(facts[f]);
				assume.push(factLit);
			}
		}
		freeze_counts();
		//cnf.toDimacs("prob.cnf", assume);
		return cnf.solve(assume);
	}

	void get_assumptions(OpSet& opset, Minisat::vec<Lit>& assume){
		for(Action_Idx op = 0; unsigned(op) < prob.num_actions(); op++){
			unsigned count = opset.count(op)?opset[op]:0;
			//std::cerr << "Assume #op="<<op<< " <= " << count << std::endl;
			assume.push(~countGE(op, count+1));
			assert(cnf.okay());
		}
	}

	bool solve(OpSet& opset){
		Minisat::vec<Lit> assume;
		get_assumptions(opset, assume);
		return solve(assume);
	}

	void extract_nogood(OpSet& opset, Bit_Array& cut){
		for(Action_Idx op = 0; op < int(prob.num_actions()); op++){
			unsigned count = opset.count(op)?opset[op]:0;
			auto ub_lit = countGE(op, count+1);
			if(cnf.conflict.has(ub_lit)||cnf.conflict.has(~ub_lit)){
				//std::cerr << "Cut contains: " << prob.actions()[op]->signature() << std::endl;
				cut.set(op);
			}
		}
	}

	bool shrink_assumptions(Minisat::vec<Lit>& assume){
		std::cerr << "Initial assumption size:" << assume.size() << std::endl;
		int i = 0;
		for(int j=0; j < assume.size(); j++){
			assume[i] = assume[j];
			if (cnf.conflict.has(~assume[i]) || cnf.conflict.has(assume[i])){
				i++;
			}
		}
		if(i == 0){
			std::cerr << "No assumptions" <<std::endl;
			return false;
		}
		assume.shrink(int(0.7*(assume.size() - i)));
		assume.pop();
		std::cerr << "Assumption size:" << assume.size() << " should = " << i - 1 << std::endl;
		return true;
	}

	void strengthened_nogood(OpSet& opset, Bit_Array& cut){
		int max_iter = 3;
		extract_nogood(opset, cut);
		Minisat::vec<Lit> assume;
		get_assumptions(opset, assume);

		shrink_assumptions(assume);

		std::cerr << "UNSAT Conflict Size = " << cnf.conflict.size() << std::endl;
		while(max_iter-- && !solve(assume)){
			std::cerr << "UNSAT Conflict Size = " << cnf.conflict.size() << std::endl;
			cut.reset();
			extract_nogood(opset, cut);
			if(!shrink_assumptions(assume)){
				break;
			}
			std::cerr << "Assumption size:" << assume.size() << std::endl;
		}
	}

	void extract_plan(vector<Action_Idx>& plan){
		if(prev){
			prev->extract_plan(plan);
		}
		if(cnf.modelValue(any_act)!=Minisat::l_True){
			return;
		}
		int n_actions = prob.num_actions();
		for(int op = 0 ; op < n_actions ; op++){
			if(cnf.modelValue(ops.at(op))==Minisat::l_True){
				// std::cerr << "Layer #" << i << " " << prob.actions()[op]->signature() << std::endl;
				plan.push_back(op);
				if (relaxed){
					std::cerr << "Warning delete relaxed action" <<  std::endl;
				}
			}
		}

	}

	int extract_plan(OpSet opset, vector<Action_Idx>& plan){
		int layer_no = 0;
		if(prev){
			layer_no = prev->extract_plan(opset, plan) + 1;
		}
		if(cnf.modelValue(any_act)!=Minisat::l_True){
			// std::cerr << "No action in layer " 
			// 		  << layer_no << "." 
			// 		  << std::endl;
			return layer_no;
		}
		for(auto iop = std::begin(opset); iop != std::end(opset); iop++){
			auto op = iop->first;
			if(cnf.modelValue(ops.at(op))==Minisat::l_True){
				// std::cerr << "Layer #" << i << " " << prob.actions()[op]->signature() << std::endl;
				plan.push_back(op);
				if (relaxed){
					std::cerr << "Warning delete relaxed action "
							  << prob.actions()[op]->signature() 
							  << "(layer " << layer_no << ")" 
							  << std::endl;
				}
			}
		}
		return layer_no;
	}

	void freeze_counts(bool final=true){
		for(Action_Idx op = 0; unsigned(op) < prob.num_actions(); op++){
			unsigned start_idx = final ? 0 : counts[op].size()-2;
			for(unsigned i = start_idx; i < counts[op].size(); i++){
				cnf.freezeVar(counts[op][i]);
			}
		}
		if(prev){
			prev->freeze_counts(false);
		}
	}

	Var auxVar(){
		return cnf.newVar(Minisat::l_Undef, false);
	}

	Var newOpVar(Action_Idx op, const State& reachable, const State& relevant){
		// return cnf.newVar();
		auto act = prob.actions()[op];
		if (act->can_be_applied_on(reachable) && act->is_relevant_to(relevant))
			return cnf.newVar();
		return var_false;
	}
	
	Var newFactVar(int f, const State& reachable, const State& relevant){
		// return cnf.newVar();
		if (reachable.entails(f) && relevant.entails(f))
			return cnf.newVar();
		return var_false;
	}

};

template<typename SearchProb>
class OpSetEngine {
public:

	OpSetEngine(SearchProb& prob_):prob(prob_), rpg_len(-1), prefix_len(0) {
		//cnf.verbosity = 1;
	}
	OpSetEngine(const OpSetEngine& other): prob(other.prob), rpg_len(other.rpg_len), prefix_len(0){
		if (other.prefix_len){
			add_relaxation();
		}
	}
	virtual ~OpSetEngine(){
	}

	unsigned num_layers() const {
		return layers.size();
	}

	bool search_nogrow(OpSet& opset, Bit_Array& cut, std::vector<Action_Idx>* plan=NULL){
		auto& goalLayer = layers.back();
		if(!goalLayer->solve(opset)){
			goalLayer->extract_nogood(opset, cut);
			std::cerr << "UNSAT Conflict Size = " << cnf.conflict.size() << std::endl;
			assert(cnf.okay());
			return false;
		}
		if(plan){
			layers.back()->extract_plan(opset, *plan);
		}
		return true;
	}

	bool search(OpSet& opset, std::vector<Action_Idx>& plan, Bit_Array& cut, unsigned n_layers = 0){
		for(int required = required_layers(opset); required >= num_layers(); ensure_layers(num_layers()+5)){
			if(!search_nogrow(opset, cut)){
				return false;
			}
			std::cerr << "Extending formula: search inconclusive (" 
					  << num_layers() << "/" << required
					  << ")" << std::endl;
		}
		return search_nogrow(opset, cut, &plan);
	}

	bool add_relaxation(){
		if (layers.size() == 0){
			for(int rpg = RPGLen(); prefix_len <= rpg; prefix_len++){
				Layer* prev = (layers.size()==0 ? NULL : layers.back().get());
				bool relaxed = true;
				layers.emplace_back(new Layer(prob, cnf, prev, reachable_state(), relevant_state(), relaxed));
				// std::cerr << "RPG Construction " << prefix_len << "/" << rpg << std::endl;
			}
			return true;
		}
		return false;
	}

	bool has_relaxation(){
		return prefix_len > 0;
	}

	int RPGLen(){
		if(rpg_len >= 0)
			return rpg_len;
		// clock_t t0 = clock();
		aptk::State next(prob);
		next.set(prob.init());
		while(true){
			reachable.emplace_back(State(prob));

			auto& r = reachable.back();
			r.set(next.fluent_vec());
			assert(next == r);
			// std::cerr << "RPGLen >= " << rpg_len << std::endl;
			// std::cerr << "Reachable >= " << r.fluent_vec().size() << "/" << prob.num_fluents() << std::endl;
			std::vector<int> actions;
			prob.applicable_actions_v2(r, actions);
			for(auto iAct = std::begin(actions); iAct != std::end(actions); iAct++){
				auto act = prob.actions()[*iAct];
				next.set(act->add_vec());
			}
			if(next == r)
				break;
		}
		
		// std::cerr << "RPGLen() = " << rpg_len << " (took " << 1.0*(clock() - t0)/CLOCKS_PER_SEC << "s)" << std::endl;
		relevant_state();
		rpg_len = relevant.size();
		return rpg_len;
	}

	const State& reachable_state(){
		RPGLen();
		return reachable.back();
	}

	State relevant_state() {
		int relevant_actions = 0;
		if (relevant.size() > 0)
			return relevant.back();
		clock_t t0 = clock();
		aptk::State prev(prob);
		prev.set(prob.goal());
		while(true) {
			relevant_actions = 0;
			relevant.emplace_back(State(prob));
			auto& r = relevant.back();
			r.set(prev.fluent_vec());
			for(unsigned iAct = 0; iAct < prob.num_actions(); iAct++){
				auto act = prob.actions()[iAct];
				if(act->is_relevant_to(r)){
					prev.set(act->prec_vec());
					relevant_actions++;
				}
			}

			if (prev == r)
				break;
		}
		std::cerr << "relevant_state() = " << relevant.back().fluent_vec().size() << "/" << prob.num_fluents() << " (took " << 1.0*(clock() - t0)/CLOCKS_PER_SEC << "s)" << std::endl;
		std::cerr << "relevant actions = " << relevant_actions << "/" << prob.num_actions() << std::endl;
		return relevant.back();
	}

private:
	SearchProb& prob;

	Sat cnf;
	vector< std::unique_ptr<Layer> > layers;

	int rpg_len;
	int prefix_len;

	vector<State> reachable;
	vector<State> relevant;

	int conflict_size(Minisat::vec<Lit>& assume){
		int ret = 0;
		for (int i = 0; i < assume.size(); i++) {
			if (cnf.conflict.has(assume[i])){
				ret++;
			}
		}
		return ret;
	}

	void add_layer(){
		bool relaxed = false;
		if (num_layers() == 0){
			// std::cerr << "Add first layer" << std::endl;
			layers.emplace_back(new Layer(prob, cnf, NULL, reachable_state(), relevant_state(), relaxed));
		} else {
			// std::cerr << "Add layer #"<< num_layers()<< std::endl;
			layers.emplace_back(new Layer(prob, cnf, layers.back().get(), reachable_state(), relevant_state(), relaxed));
		}
	}

	int ensure_layers(int min_layers){
		// std::cerr << "Ensure layers >= "<< min_layers << std::endl;
		for(int l = num_layers() - RPGLen(); l < min_layers; l++){
			add_layer();
			assert(cnf.okay());
		}
		return min_layers;
	}
	int ensure_layers(OpSet& opset){
		return ensure_layers(required_layers(opset));
	}

	int required_layers(OpSet& opset){
		int min_layers = prefix_len;
		for(auto it = std::begin(opset); it != std::end(opset); it++){
			min_layers += it->second;
		}
		return min_layers;
	}

};
}
}
}
#endif
