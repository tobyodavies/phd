import gurobipy as gpy


model = gpy.Model()


class Action(object):
    def __init__(self, task, idx, cost=None, name=None):
        if name is None:
            name = "act_%s" % task.action_signature(idx)
        if cost is None:
            cost = task.cost(idx)
        self.idx = idx
        self.cost = cost
        self.count_var = model.addVar(obj=cost,
                                      vtype=gpy.GRB.INTEGER,
                                      name=name)
        if 0 <= idx <= task.num_actions():
            self.precs = set(task.precs(idx))
            self.adds = set(task.adds(idx))
            self.dels = set(task.dels(idx))
            self.edels = set(task.edels(idx))
            self.sig = task.action_signature(idx)
        else:
            self.precs = set()
            self.adds = set()
            self.dels = set()
            self.edels = set()
            self.sig = None
        self.dels -= self.adds
        self.prevs = self.precs - self.adds - self.dels

        self.copy_vars = {}  # maps preconds tuple to a var
        self.lb_vars = [1]
        self.count_ge(2)

    def __repr__(self):
        return "<Action " + self.sig + ">"

    def _add_lb_var(self):
        num = len(self.lb_vars)
        var = model.addVar(
            name="act_bound_%s_ge_%d" % (self.sig, num),
            vtype=gpy.GRB.BINARY,
            ub=1.0)
        model.update()
        #model.addConstr(self.count_var >= num * var)
        model.addConstr(self.count_var - num <= 1e4*var - 1)
        if self.lb_vars:
            #model.addConstr(num * var <= gpy.quicksum(self.lb_vars))
            model.addConstr(var <= self.lb_vars[-1])
        self.lb_vars.append(var)
        model.addConstr(self.count_var >= gpy.quicksum(self.lb_vars[1:]))

    def count_ge(self, num):
        while len(self.lb_vars) <= num:
            self._add_lb_var()
        return self.lb_vars[num]

    def count_ge_safe(self, num):
        if len(self.lb_vars) > num:
            return self.lb_vars[num]
        return self.count_var / (1.0 * num)

    @property
    def count(self):
        return int(self.count_var.x + 0.9)

    def relax_count(self):
        self.count_var.setAttr(gpy.GRB.attr.VType, 'C')
        for lb_var in self.lb_vars[1:]:
            lb_var.setAttr(gpy.GRB.attr.VType, 'C')

    def unrelax_count(self):
        self.count_var.setAttr(gpy.GRB.attr.VType, 'I')
        for lb_var in self.lb_vars[1:]:
            lb_var.setAttr(gpy.GRB.attr.VType, 'B')

    def set_unitcost(self):
        self.cost = 1
        self.count_var.setAttr("Obj", 1.0)


class PartState(object):
    def __init__(self, task, all_of=(), none_of=()):
        self._demand = None
        self.task = task
        self.all_of = frozenset(all_of)
        self.none_of = frozenset(none_of)
        self.mutexes = [m
                        for m in get_mutexes(task)
                        if m & self.all_of]
        self.maybe_legal = (
            not(self.all_of & self.none_of)
            and
            not any(len(m & self.all_of) > 1
                    for m in self.mutexes))
        self.mutex_with = (self.none_of
                           |
                           frozenset(i
                                     for m in self.mutexes
                                     for i in (m - self.all_of)))

    def __repr__(self):
        return "PartState({all_of}, {none_of})".format(all_of=list(self.all_of), none_of=list(self.none_of))

    def __str__(self):
        return "PartState({all_of}, {none_of})".format(all_of=[self.task.get_atom_name(i)
                                                               for i in self.all_of],
                                                       none_of=[self.task.get_atom_name(i)
                                                                for i in self.none_of])

    def __hash__(self):
        return hash((self.all_of, self.mutex_with))

    def __eq__(self, other):
        return self.all_of == other.all_of and self.mutex_with == other.mutex_with

    def universal(self):
        return not(self.all_of) and not(self.none_of)

    def atomic(self):
        return len(self.all_of) == 1 and len(self.none_of) == 0

    def get_atoms(self):
        return [PartState(self.task, (i,))
                for i in self.all_of]

    def intersect(self, other):
        return PartState(self.task,
                         self.all_of & other.all_of,
                         self.none_of & other.none_of)

    def union(self, other):
        return PartState(self.task,
                         self.all_of | other.all_of,
                         self.none_of | other.none_of)

    def is_mutex_with(self, other):
        return (bool(self.all_of & other.mutex_with)
                or
                bool(self.mutex_with & other.all_of))

    def entails(self, other):
        """
        Return true if other must match every state that self matches
        """
        return (not self.is_mutex_with(other)
                and
                self.all_of.issuperset(other.all_of)
                and self.mutex_with.issuperset(other.none_of))

    def producer_prevails(self, action):
        return action.prevs | (self.all_of - action.adds)

    def produced_by(self, action):
        prevs = self.producer_prevails(action)
        return (not(prevs & self.mutex_with)  # No prevails can be mutex with this state
                and
                PartState(self.task, action.precs | prevs).maybe_legal
                and
                not(action.dels & self.all_of)  # Can't delete any part of the required state
                and
                not(action.adds & self.mutex_with)  # Can't add anything that must be false in this state
                and
                (action.adds & self.all_of or action.dels & self.none_of))

    def consumed_by(self, action):
        return (not(action.precs & self.mutex_with)  # No preconds can be mutex with this state
                and
                (action.dels & self.all_of or action.adds & self.none_of))  # must invalidate this state

    def producers(self, actions):
        """
        Returns a list of pairs of an action and the PartState the
        action must be applied in to produce this state
        """
        return [(a, state)
                for a in actions
                for state in [PartState(self.task, self.producer_prevails(a) | a.precs, self.mutex_with - a.dels)]
                if self.produced_by(a) and state.maybe_legal]

    def consumers(self, actions):
        """
        Returns a list of pairs of an action and the PartState the
        action will produce when applied in this state.
        """
        return [(a, state)
                for a in actions
                for state in [PartState(self.task, a.adds | (self.all_of - a.dels), a.dels | (self.mutex_with - a.adds))]
                if self.consumed_by(a) and state.maybe_legal]

    def actions_requiring(self, actions):
        """
        Returns a list of pairs of an action and the PartState of it's
        preconditions, where those actions can only be applied from
        this state.
        """
        return [(a, precs)
                for a in actions
                for precs in [PartState(self.task, a.precs)]
                if precs.entails(self)]

    def flow_delta_min_max(self, goal_var):
        init = PartState(self.task,
                         self.task.init(),
                         set(range(self.task.num_atoms())) - set(self.task.init()))
        goal = PartState(self.task, self.task.goal())
        poss_goal_state = not goal.is_mutex_with(self)
        init_state = init.entails(self)
        if goal_var:
            if poss_goal_state and init_state:
                return 0, 0
            elif poss_goal_state:
                return 1, 1
            elif init_state:
                return -1, -1
            else:
                return 0, 0
        else:
            if poss_goal_state and init_state:
                return -1, 0
            elif poss_goal_state:
                return 0, 1
            elif init_state:
                return -1, -1
            else:
                return 0, 0

    def add_lm(self, actions, init=None):
        # print self, "is a landmark"
        if init is None:
            init = PartState(self.task,
                             self.task.init(),
                             set(range(self.task.num_atoms())) - set(self.task.init()))
        if init.entails(self):
            # print self, "is initially true"
            return True

        producers = self.producers(actions)
        if not producers:
            return False

        model.addConstr(gpy.quicksum([a.count_var for a, _ in producers]) >= 1)
        necessary_precond = reduce(PartState.intersect, [p for _, p in producers])
        retval = necessary_precond.add_lm(actions, init)
        if not self.atomic():
            return retval and all(atom.add_lm(actions, init)
                                  for atom in self.get_atoms())
        return retval

    def specialize(self, part_states):
        ret = set()
        for p in part_states:
            p1 = self.union(p)
            if p1.maybe_legal:
                ret.add(p1)
        return ret

    def specialize_product(self, mutexes):
        mutexes = [self.specialize(m)
                   for m in mutexes]
        ret = set()
        if all(m for m in mutexes):
            for m0 in mutexes[0]:
                if len(mutexes) <= 1:
                    ret.add(m0)
                else:
                    for m1 in m0.specialize_product(mutexes[1:]):
                        ret.add(m1)
        return ret

    @classmethod
    def all_from_mutex(cls, task, mutex):
        return cls(task, [], []).specialize([cls(task, [i], [])
                                             for i in mutex]
                                            +
                                            [cls(task, none_of=mutex)])

    @classmethod
    def all_from_mutexes(cls, task, mutexes):
        return cls(task, [], []).specialize_product(
            [cls.all_from_mutex(task, m)
             for m in mutexes])


def atom(task, idx):
    return PartState(task, [idx])


def get_mutexes(task):
    return [set(task.get_mutex(i))
            for i in range(task.num_mutexes())]
