#include <unordered_map>
#include <queue>
#include <vector>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include "gurobi_c++.h"

namespace opseq {

class OpCounter;
struct Abstraction;

class Abstraction {
public:
	Abstraction(OpCounter& oc, std::vector<unsigned> nodes, int init_idx, int goal_idx);
	Abstraction(Abstraction& a, Abstraction& b);

	unsigned size() const { return inflow_ops.size(); }

	void addTransition(unsigned op, unsigned src, unsigned dst);
	void addUnsafeTransition(unsigned op, unsigned src, int dst_offset);

	void conserveAll();
	void conserveFlow(unsigned node);
	void conservePrevFlow(unsigned op);
	void constrainOp(unsigned op);
	void constrainOps();

	GRBLinExpr netFlow(unsigned node);
	GRBLinExpr prevFlow(unsigned op);

	void conservePrevails() {
		for(auto n : prev_nodes){
			conserveFlow(n);
		}
		for(auto op: op_copies)
			constrainOp(op.first);
	}


	bool safe(unsigned op) const;
	const std::set<unsigned>& get_srcs(unsigned op) const;
	unsigned get_dst(unsigned op, unsigned src_node) const;

	template<class F>
	double getPHO(GRBLinExpr& constr, F cost){
		std::priority_queue< std::pair<double,unsigned> > q;
		//q.c.reserve(nodes.size());
		std::vector<double> costs(inflow_ops.size(),
					  std::numeric_limits<double>::max());
		std::vector< std::map<unsigned, double> > tr_cost(inflow_ops.size());
		unsigned node = 0;
		for(auto& ops : inflow_ops){
			for(auto op: ops) {
				constr += cost(op) * oc.count(op);
				for(auto src : op_src[op]){
					bool init = tr_cost[src].insert(std::make_pair(node, cost(op))).second;
					if(init || tr_cost[src][node] > cost(op)){
						// std::cerr << src << "->" << node << " costs <= " << cost(op) << " (op=" << op << ") " << tr_cost[src][node] << std::endl;
						tr_cost[src][node] = cost(op);
					}
				}
			}
			node++;
		}

		q.push(std::make_pair(0.0, init_idx));
		costs[init_idx] = 0;
		while(!q.empty()){
			node = q.top().second;
			q.pop();
			for(auto& tr : tr_cost[node]){
				if(costs[tr.first] > costs[node] + tr.second){
					costs[tr.first] = costs[node] + tr.second;
					q.push(std::make_pair(-costs[tr.first],
							      tr.first));
					// std::cerr << node << "->" << tr.first << " " << costs[tr.first] << std::endl;
				}
			}
		}
		// std::cerr << "init: " << init_idx << " " << costs[init_idx] << std::endl;
		// std::cerr << "goal: " << goal_idx << " " << costs[goal_idx] << std::endl;
		return costs[goal_idx];
	}


	GRBVar get_var(unsigned op, unsigned src_node);

	OpCounter& oc;

	std::vector< std::set<unsigned> > inflow_ops;
	std::vector< std::set<unsigned> > outflow_ops;

	std::unordered_map<unsigned, std::set<unsigned> > op_src;
	std::unordered_map<unsigned, int> op_dst_offset;
	std::unordered_map<unsigned, std::unordered_map<unsigned, GRBVar> > op_copies;
	std::set<unsigned> prev_nodes;
	unsigned init_idx, goal_idx;
	std::unordered_map<unsigned, unsigned> prev_op_node;
	std::unordered_set<unsigned> merged_nodes;
};

}
