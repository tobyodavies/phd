import glob
import os
from os import path
import subprocess
import sys


here = path.join('.', path.dirname(__file__))
benchmarks = {r: fs for r, _, fs in os.walk(here)}
cwd = os.getcwd()


def probid(f):
    return path.basename(f).split(path.extsep, 1)[0]


def probs_in_dir(odir):
    ret = [path.join(cwd, d, f)
           for d, _, fs in os.walk(odir)
           for f in fs
           if f.endswith(".pddl") and 'domain' not in f]
    ret.sort()
    return ret


def probs_for_domain(ofile):
    ofiles = glob.glob(ofile)
    ofiles.sort()
    odir = path.dirname(ofile)
    pfiles = probs_in_dir(path.join(odir, '..', 'problems'))
    if not pfiles:
        pfiles = [p for p in probs_in_dir(path.join(odir))
                  if p not in ofiles]
    if len(ofiles) > 1:
        return {of: [f
                     for f in pfiles
                     if probid(f) in base]
                for of in ofiles
                for base in [path.basename(of)]}
    elif len(ofiles) == 1:
        return {ofiles[0]: pfiles}
    return {}


coverage = 0

plannerdir, domainfile = sys.argv[1:]
while plannerdir.endswith('/'):
    plannerdir = plannerdir[:-1]
label = path.basename(plannerdir)

for ops, fact_files in probs_for_domain(domainfile).items():
    for facts in fact_files:
        log = '.'.join([cwd, ops, label, "log"])
        subprocess.call(["mkdir", "-p", path.dirname(log)])
        cmd = ("(cd {plannerdir} ; timeout 1800 ./plan {ops} {facts} /dev/null > {log})"
               .format(plannerdir=plannerdir,
                       ops=path.join(cwd, ops),
                       facts=path.join(cwd, facts),
                       log=log))
        print cmd
        if subprocess.call(cmd, shell=True) == 0:
            coverage += 1

print plannerdir, domainfile, coverage
