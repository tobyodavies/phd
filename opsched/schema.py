from collections import namedtuple

import opsched

Truck = namedtuple("Truck", ["cap", "fuel", "start"])
Pkg = namedtuple("Pkg", ["size", "start", "dest"])
trucks = [Truck(10, 2, 1), Truck(5, 50, 1)]
packages = [Pkg(5, 0, 2), Pkg(3, 1, 2)]
graph_time = {(0,1): 1, (1,2): 10, (1,0): 1, (2,1): 10}
#graph_fuel = {(1,2): 10, (2,3): 1, (2,1): 10, (3,2): 1}

graph_nodes = 3

inst = opsched.Instance(
    (len(trucks) * [graph_nodes+1]) + (len(packages) * [graph_nodes + len(trucks) + 1]),
    [t.cap for t in trucks],
    [t.start for t in trucks] + [p.start for p in packages],
    {len(trucks) + i: p.dest for i, p in enumerate(packages)}
)

# Moves
for truck in range(len(trucks)):
    for (src, dst), dt in graph_time.items():
        s = inst.mkop(
            "Move-t{truck}-{src}-to-{dst}".format(**vars()),
            [(truck, src, graph_nodes)])
        e = inst.mkop(
            "end-move-t{truck}-{src}-to-{dst}".format(**vars()),
            [(truck, graph_nodes, dst)])
        e.set_starter(s, dt, dt, [(truck, 4)])
        for i,pkg in enumerate(packages):
            s = inst.mkop(
                "Transport-{i}-from-{src}-to-{dst}-on-{truck}".format(**vars()),
                [(len(trucks)+i, src, 4+truck), (truck, src, -1)])
            e = inst.mkop(
                "end-transport-{i}-to-{src}-to-{dst}-on-{truck}".format(**vars()),
                [(len(trucks)+i, 4+truck, dst), (truck, dst, -1)])
            e.set_starter(s, 2, 1000, [(len(trucks)+i, 4+truck)], {truck: pkg.size})

print inst.dumps()
