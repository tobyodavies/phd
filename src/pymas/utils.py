"""
Search Utils
============

A set of utilities shared between :py:mod:`pymas.selector` and :py:mod:`pymas.generator`

"""

import functools
import heapq
import itertools
import operator
import platform
import sys

import pulp as p

SUPPORTED_SOLVERS = (p.GUROBI, p.COIN, p.PYGLPK)


def get_solver(preferences=None, **kwargs):
    """
    Construct the first available solver
    """
    preferences = preferences or SUPPORTED_SOLVERS
    for solver_factory in preferences:
        if (platform.python_implementation() == 'PyPy' and
            solver_factory == p.GUROBI):
            continue
        solver = solver_factory(**kwargs)
        if solver.available():
            return solver
    raise RuntimeError("Cannot construct any solver from %r"%(preferences,))


class LinkedList(object):
    """
    A utility class allowing structure-sharing lists for use in branching search

    Usage:

    >>> x = LinkedList.nil().cons(1)
    >>> x
    LinkedList(1, LinkedList.nil())
    >>> y = x.cons(2)
    >>> assert y.tail is x
    >>> z = x.cons(3)
    >>> list(z)
    [3, 1]
    """
    equiv = set()
    def __init__(self, head, tail=None):
        self.head = head
        self.tail = tail if tail is not None else self.nil()
        self.length = getattr(self.tail, 'length', -1) + 1
        self._hash = None

    def __iter__(self):
        cur = self
        while cur != self.nil():
            yield cur.head
            cur = cur.tail

    def cons(self, data):
        """
        Construct a new :py:class:`LinkedList`
        with `data` at the head and `self` as the parent/tail

        >>> list(LinkedList(2).cons(1))
        [1, 2]
        """
        ret = self.__class__(data, tail=self)
        return ret

    def __len__(self):
        return self.length

    def __repr__(self):
        if self is self.nil():
            return "%s.nil()"%(self.__class__.__name__,)
        return "LinkedList(%r, %r)" % (self.head, self.tail)

    def __add__(self, iterable):
        ret = self.nil()
        for x in reversed(list(itertools.chain(self, iterable))):
            ret = ret.cons(x)
        return ret

    def __hash__(self):
        if self._hash is None:
            self._hash = id(self.__class__) ^ hash(self.head) ^ hash(self.tail)
        return self._hash

    def __eq__(self, other):
        if len(self) != len(other) or hash(self) != hash(other):
            return False
        if self is other:
            return True
        equiv = self.__class__.equiv
        jhist = itertools.izip(self.history, other.history)
        for (act1, hist1), (act2, hist2) in jhist:
            if act1 != act2:
                return False
            if hist1 is hist2:
                return True
            if (hist1, hist2) in equiv or (hist2, hist1) in equiv:
                equiv.add((hist1, hist2))
                return True
        return True

    @classmethod
    def nil(cls):
        if not hasattr(cls, '_NIL') or not isinstance(cls._NIL, cls):
            cls._NIL = cls(None, False)
        return cls._NIL

    @property
    def history(self):
        """
        An iterable over the action, situation pairs comprising this situation
        """
        node = self
        while node != self.nil():
            yield node.head, node.tail
            node = node.tail


def reduced_cost(duals, fragment):
    """
    Return the reduced cost for a given fragment
    """
    return (fragment.cost -
            sum(fragment.resources.get(r, 0) * duals.get(r, 0)
                for r in fragment.resources))


def reduced_coster(duals):
    """
    Returns a function :py:class:`pymas.selector.Fragment` -> reduced cost, given a set of dual values

    >>> import collections
    >>> FakeFrag = collections.namedtuple('FakeFrag', ['cost', 'resources'])
    >>> f = reduced_coster({'foo': 1})
    >>> f(FakeFrag(1, {'foo': 1})) == 0
    True
    """
    return functools.partial(reduced_cost, duals)


class SearchQ(object):
    """
    A best-first search queue with support for heuristics,
    lower bound pruning and incremental search

    """

    def __init__(self, branches=None, expand=None, heuristic=None, soln_filter=None):
        self.expansions = 0
        self.enqueued = {}
        self.branches = []
        if branches is None:
            branches = []
        self.best = None
        # These can be specified in init or by subclassing
        if expand:
            self.expand = expand
        if heuristic:
            self.heuristic = heuristic
        if soln_filter:
            self.soln_filter = soln_filter
        for branch in branches:
            self.addbranch(None, None, branch)

    @property
    def best_cost(self):
        return self.best[0]

    @property
    def best_soln(self):
        return self.best[1]

    def push(self, cost, lb, branch):
        """
        Add a new branch if its lower bound is better than the incumbent
        """
        if branch not in self.enqueued or self.enqueued[branch] > cost:
            self.enqueued[branch] = cost
            if self.best is None or lb < self.best_cost:
                heapq.heappush(self.branches, (cost, lb, branch))

    def pop(self):
        """
        Return the estimated best branch
        """
        _h, _lb, branch = heapq.heappop(self.branches)
        #self.enqueued.remove(branch)
        return branch

    def prune_branches(self, cost=None):
        """
        Remove all branches with non-`None` lower bounds >= `cost`

        :param cost: The upper bound of the optimal solution. Defaults to `self.best_cost`.
        """
        if cost is None:
            cost = self.best_cost
        self.branches[:] = ((c, lb, vs)
                            for c, lb, vs in self.branches
                            if lb is None or lb < cost)
        heapq.heapify(self.branches)

    def add_solution(self, solution, cost):
        """
        Add a new solution, replacing the existing best if this solution is better
        :returns: `True` if solution is the new best solution
        """
        if (self.soln_filter(solution) and
                (self.best is None or cost < self.best_cost)):
            self.best = (cost, solution)
            self.prune_branches()
            return True
        return False

    def __nonzero__(self):
        return bool(self.branches)

    def __len__(self):
        return len(self.branches)

    def expand(self, root):
        """
        Expand the search tree at `root`

        Subclasses should implement this method
        or, alternatively pass a function to the constructor

        :rtype: Iterable of (searchorder, lowbound, branch) tuples
        """
        raise NotImplementedError("This is an abstract method")

    def heuristic(self, branch):
        """
        Construct a heuristic solution at branch, used to bound the search
        """
        return (None, None)

    def soln_filter(self, soln):
        return True

    def addbranch(self, order, lb, branch):
        """
        Add a branch
        """
        self.push(order, lb, branch)
        cost, soln = self.heuristic(branch)
        if soln is not None:
            return self.add_solution(soln, cost)
        return False

    def solutions(self, iters=sys.maxint):
        for i in xrange(iters):
            if not self.branches:
                break
            branch = self.pop()
            self.expansions += 1
            for b in self.expand(branch):
                if len(b) != 3:
                    print b
                cost, lb, sub = b
                if self.addbranch(cost, lb, sub):
                    if self.soln_filter(self.best_soln):
                        yield self.best_soln

    def optimum(self, maxiter=sys.maxint):
        soln = None
        for soln in self.solutions(maxiter):
            pass
        return soln


def _memo(func, keynames=None):
    """
    apply dynamic programming to func
    """
    cache = {}
    code = func.func_code
    if not keynames:
        keynames = code.co_varnames

    def key(args, kwargs):
        """
        Construct a tuple of relevant arg values
        """
        named_args = dict(zip(code.co_varnames, args))
        named_args.update(kwargs)
        ndefaults = len(func.func_defaults or [])
        named_args.update(zip(code.co_varnames[code.co_argcount - ndefaults:],
                              (func.func_defaults or [])))
        return tuple(named_args[arg] for arg in keynames)

    @functools.wraps(func)
    def wrapped(*args, **kwargs):
        "lookup the args to see if an answer has been computed previously"
        call_key = key(args, kwargs)
        if call_key not in cache:
            cache[call_key] = func(*args, **kwargs)
        return cache[call_key]

    wrapped.cache = cache

    return wrapped


def memo(*varnames):
    """
    memoise a function using a subset of its named arguments

    >>> class obj(object):
    ...     @memo('self', 'x')
    ...     def foo(self, x, y):
    ...         return y
    >>> o = obj()
    >>> o.foo(1, 1)
    1
    >>> o.foo(1, 2)
    1
    >>> obj().foo(1, 2)
    2
    >>> (o, 1) in obj.foo.cache
    True
    """
    if not varnames:
        varnames = None
    return lambda f: _memo(f, varnames)
