(define (problem bfrs_88)
(:domain bfrs)
(:objects
    m_tp - location
    m_mj - location
    m_mnd - location
    m_ya - location
    m_b2 - location
    m_ma - location
    j_b - location
    m_b4 - location
    m_yac - location
    m_wa - location
    p_dampier - location
    m_hd - location
    y - location
    j_wa - location
    p_capelambert - location
    m_pbd - location
    o0 - order
    o1 - order
    o2 - order
    o3 - order
    o4 - order
    o5 - order
    o6 - order
    o7 - order
    o8 - order
    o9 - order
    o10 - order
    o11 - order
    o12 - order
    o13 - order
    o14 - order
    o15 - order
    o16 - order
    o17 - order
    o18 - order
    o19 - order
    o20 - order
    o21 - order
    o22 - order
    o23 - order
    o24 - order
    o25 - order
    o26 - order
    o27 - order
    o28 - order
    o29 - order
    o30 - order
    o31 - order
    o32 - order
    o33 - order
    o34 - order
    o35 - order
    o36 - order
    o37 - order
    o38 - order
    o39 - order
    o40 - order
    o41 - order
    o42 - order
    o43 - order
    o44 - order
    o45 - order
    o46 - order
    o47 - order
    o48 - order
    o49 - order
    o50 - order
    o51 - order
    o52 - order
    o53 - order
    o54 - order
    o55 - order
    o56 - order
    o57 - order
    o58 - order
    o59 - order
    o60 - order
    o61 - order
    o62 - order
    o63 - order
    o64 - order
    o65 - order
    o66 - order
    o67 - order
    o68 - order
    o69 - order
    o70 - order
    o71 - order
    o72 - order
    o73 - order
    o74 - order
    o75 - order
    o76 - order
    o77 - order
    o78 - order
    o79 - order
    o80 - order
    o81 - order
    o82 - order
    o83 - order
    o84 - order
    o85 - order
    o86 - order
    o87 - order
    c0 - consist
    c1 - consist
    c2 - consist
    c3 - consist
    c4 - consist
    c5 - consist
    c6 - consist
    c7 - consist
    c8 - consist
    c9 - consist
    c10 - consist
    c11 - consist
    c12 - consist
    c13 - consist
    c14 - consist
    c15 - consist
    c16 - consist
    c17 - consist
    c18 - consist
    c19 - consist
    c20 - consist
    c21 - consist
    c22 - consist
    c23 - consist
    c24 - consist
    c25 - consist
    c26 - consist
    c27 - consist
    c28 - consist
    c29 - consist
    c30 - consist
    c31 - consist
    c32 - consist
    c33 - consist
    c34 - consist
    c35 - consist
    c36 - consist
    c37 - consist
    c38 - consist
    c39 - consist
    c40 - consist
    c41 - consist
    c42 - consist
    c43 - consist
    c44 - consist
    c45 - consist
    c46 - consist
    c47 - consist
    c48 - consist
    c49 - consist
    c50 - consist
    c51 - consist
    c52 - consist
    c53 - consist
    c54 - consist
    c55 - consist
    c56 - consist
    c57 - consist
    c58 - consist
    c59 - consist
    c60 - consist
    c61 - consist
    c62 - consist
    c63 - consist
    c64 - consist
    c65 - consist
    c66 - consist
    c67 - consist
    c68 - consist
    c69 - consist
    c70 - consist
    c71 - consist
    c72 - consist
    c73 - consist
    c74 - consist
    c75 - consist
    c76 - consist
    c77 - consist
    c78 - consist
    c79 - consist
    c80 - consist
    c81 - consist
    c82 - consist
    c83 - consist
    c84 - consist
    c85 - consist
    c86 - consist
    c87 - consist
)
(:init
  (is_yard y)
    (clear m_tp)
    (clear m_mj)
    (clear m_mnd)
    (clear m_ya)
    (clear m_b2)
    (clear m_ma)
    (clear j_b)
    (clear m_b4)
    (clear m_yac)
    (clear m_wa)
    (clear p_dampier)
    (clear m_hd)
    (clear y)
    (clear j_wa)
    (clear p_capelambert)
    (clear m_pbd)

    (conn m_tp j_b)
    (conn j_b m_tp)
    (conn m_tp m_pbd)
    (conn m_pbd m_tp)
    (conn m_mj y)
    (conn y m_mj)
    (conn m_mj m_ma)
    (conn m_ma m_mj)
    (conn m_mnd j_b)
    (conn j_b m_mnd)
    (conn m_mnd m_ya)
    (conn m_ya m_mnd)
    (conn m_ya m_yac)
    (conn m_yac m_ya)
    (conn m_b2 j_b)
    (conn j_b m_b2)
    (conn m_b2 m_b4)
    (conn m_b4 m_b2)
    (conn j_b y)
    (conn y j_b)
    (conn j_b j_wa)
    (conn j_wa j_b)
    (conn m_wa j_wa)
    (conn j_wa m_wa)
    (conn p_dampier y)
    (conn y p_dampier)
    (conn m_hd j_wa)
    (conn j_wa m_hd)
    (conn y p_capelambert)
    (conn p_capelambert y)

    ;; order o0 ('m_tp', 'p_dampier', 0, 45)
    (order_mine o0 m_tp)
    (order_port o0 p_dampier)
    (= (load_after_time o0) 0)
    (= (unload_before_time o0) 45)
    (not-delivered o0)

    ;; consist c0
    (= (current_time c0) 0)
    (available c0)
    (empty c0)
    (at c0 y)

    (compat c0 o0)

    ;; order o1 ('m_tp', 'p_dampier', 45, 90)
    (order_mine o1 m_tp)
    (order_port o1 p_dampier)
    (= (load_after_time o1) 45)
    (= (unload_before_time o1) 90)
    (not-delivered o1)

    ;; consist c1
    (= (current_time c1) 0)
    (available c1)
    (empty c1)
    (at c1 y)

    (compat c1 o1)

    ;; order o2 ('m_tp', 'p_dampier', 90, 135)
    (order_mine o2 m_tp)
    (order_port o2 p_dampier)
    (= (load_after_time o2) 90)
    (= (unload_before_time o2) 135)
    (not-delivered o2)

    ;; consist c2
    (= (current_time c2) 0)
    (available c2)
    (empty c2)
    (at c2 y)

    (compat c2 o2)

    ;; order o3 ('m_tp', 'p_dampier', 135, 180)
    (order_mine o3 m_tp)
    (order_port o3 p_dampier)
    (= (load_after_time o3) 135)
    (= (unload_before_time o3) 180)
    (not-delivered o3)

    ;; consist c3
    (= (current_time c3) 0)
    (available c3)
    (empty c3)
    (at c3 y)

    (compat c3 o3)

    ;; order o4 ('m_tp', 'p_capelambert', 0, 45)
    (order_mine o4 m_tp)
    (order_port o4 p_capelambert)
    (= (load_after_time o4) 0)
    (= (unload_before_time o4) 45)
    (not-delivered o4)

    ;; consist c4
    (= (current_time c4) 0)
    (available c4)
    (empty c4)
    (at c4 y)

    (compat c4 o4)

    ;; order o5 ('m_tp', 'p_capelambert', 45, 90)
    (order_mine o5 m_tp)
    (order_port o5 p_capelambert)
    (= (load_after_time o5) 45)
    (= (unload_before_time o5) 90)
    (not-delivered o5)

    ;; consist c5
    (= (current_time c5) 0)
    (available c5)
    (empty c5)
    (at c5 y)

    (compat c5 o5)

    ;; order o6 ('m_tp', 'p_capelambert', 90, 135)
    (order_mine o6 m_tp)
    (order_port o6 p_capelambert)
    (= (load_after_time o6) 90)
    (= (unload_before_time o6) 135)
    (not-delivered o6)

    ;; consist c6
    (= (current_time c6) 0)
    (available c6)
    (empty c6)
    (at c6 y)

    (compat c6 o6)

    ;; order o7 ('m_tp', 'p_capelambert', 135, 180)
    (order_mine o7 m_tp)
    (order_port o7 p_capelambert)
    (= (load_after_time o7) 135)
    (= (unload_before_time o7) 180)
    (not-delivered o7)

    ;; consist c7
    (= (current_time c7) 0)
    (available c7)
    (empty c7)
    (at c7 y)

    (compat c7 o7)

    ;; order o8 ('m_mj', 'p_dampier', 0, 45)
    (order_mine o8 m_mj)
    (order_port o8 p_dampier)
    (= (load_after_time o8) 0)
    (= (unload_before_time o8) 45)
    (not-delivered o8)

    ;; consist c8
    (= (current_time c8) 0)
    (available c8)
    (empty c8)
    (at c8 y)

    (compat c8 o8)

    ;; order o9 ('m_mj', 'p_dampier', 45, 90)
    (order_mine o9 m_mj)
    (order_port o9 p_dampier)
    (= (load_after_time o9) 45)
    (= (unload_before_time o9) 90)
    (not-delivered o9)

    ;; consist c9
    (= (current_time c9) 0)
    (available c9)
    (empty c9)
    (at c9 y)

    (compat c9 o9)

    ;; order o10 ('m_mj', 'p_dampier', 90, 135)
    (order_mine o10 m_mj)
    (order_port o10 p_dampier)
    (= (load_after_time o10) 90)
    (= (unload_before_time o10) 135)
    (not-delivered o10)

    ;; consist c10
    (= (current_time c10) 0)
    (available c10)
    (empty c10)
    (at c10 y)

    (compat c10 o10)

    ;; order o11 ('m_mj', 'p_dampier', 135, 180)
    (order_mine o11 m_mj)
    (order_port o11 p_dampier)
    (= (load_after_time o11) 135)
    (= (unload_before_time o11) 180)
    (not-delivered o11)

    ;; consist c11
    (= (current_time c11) 0)
    (available c11)
    (empty c11)
    (at c11 y)

    (compat c11 o11)

    ;; order o12 ('m_mj', 'p_capelambert', 0, 45)
    (order_mine o12 m_mj)
    (order_port o12 p_capelambert)
    (= (load_after_time o12) 0)
    (= (unload_before_time o12) 45)
    (not-delivered o12)

    ;; consist c12
    (= (current_time c12) 0)
    (available c12)
    (empty c12)
    (at c12 y)

    (compat c12 o12)

    ;; order o13 ('m_mj', 'p_capelambert', 45, 90)
    (order_mine o13 m_mj)
    (order_port o13 p_capelambert)
    (= (load_after_time o13) 45)
    (= (unload_before_time o13) 90)
    (not-delivered o13)

    ;; consist c13
    (= (current_time c13) 0)
    (available c13)
    (empty c13)
    (at c13 y)

    (compat c13 o13)

    ;; order o14 ('m_mj', 'p_capelambert', 90, 135)
    (order_mine o14 m_mj)
    (order_port o14 p_capelambert)
    (= (load_after_time o14) 90)
    (= (unload_before_time o14) 135)
    (not-delivered o14)

    ;; consist c14
    (= (current_time c14) 0)
    (available c14)
    (empty c14)
    (at c14 y)

    (compat c14 o14)

    ;; order o15 ('m_mj', 'p_capelambert', 135, 180)
    (order_mine o15 m_mj)
    (order_port o15 p_capelambert)
    (= (load_after_time o15) 135)
    (= (unload_before_time o15) 180)
    (not-delivered o15)

    ;; consist c15
    (= (current_time c15) 0)
    (available c15)
    (empty c15)
    (at c15 y)

    (compat c15 o15)

    ;; order o16 ('m_mnd', 'p_dampier', 0, 45)
    (order_mine o16 m_mnd)
    (order_port o16 p_dampier)
    (= (load_after_time o16) 0)
    (= (unload_before_time o16) 45)
    (not-delivered o16)

    ;; consist c16
    (= (current_time c16) 0)
    (available c16)
    (empty c16)
    (at c16 y)

    (compat c16 o16)

    ;; order o17 ('m_mnd', 'p_dampier', 45, 90)
    (order_mine o17 m_mnd)
    (order_port o17 p_dampier)
    (= (load_after_time o17) 45)
    (= (unload_before_time o17) 90)
    (not-delivered o17)

    ;; consist c17
    (= (current_time c17) 0)
    (available c17)
    (empty c17)
    (at c17 y)

    (compat c17 o17)

    ;; order o18 ('m_mnd', 'p_dampier', 90, 135)
    (order_mine o18 m_mnd)
    (order_port o18 p_dampier)
    (= (load_after_time o18) 90)
    (= (unload_before_time o18) 135)
    (not-delivered o18)

    ;; consist c18
    (= (current_time c18) 0)
    (available c18)
    (empty c18)
    (at c18 y)

    (compat c18 o18)

    ;; order o19 ('m_mnd', 'p_dampier', 135, 180)
    (order_mine o19 m_mnd)
    (order_port o19 p_dampier)
    (= (load_after_time o19) 135)
    (= (unload_before_time o19) 180)
    (not-delivered o19)

    ;; consist c19
    (= (current_time c19) 0)
    (available c19)
    (empty c19)
    (at c19 y)

    (compat c19 o19)

    ;; order o20 ('m_mnd', 'p_capelambert', 0, 45)
    (order_mine o20 m_mnd)
    (order_port o20 p_capelambert)
    (= (load_after_time o20) 0)
    (= (unload_before_time o20) 45)
    (not-delivered o20)

    ;; consist c20
    (= (current_time c20) 0)
    (available c20)
    (empty c20)
    (at c20 y)

    (compat c20 o20)

    ;; order o21 ('m_mnd', 'p_capelambert', 45, 90)
    (order_mine o21 m_mnd)
    (order_port o21 p_capelambert)
    (= (load_after_time o21) 45)
    (= (unload_before_time o21) 90)
    (not-delivered o21)

    ;; consist c21
    (= (current_time c21) 0)
    (available c21)
    (empty c21)
    (at c21 y)

    (compat c21 o21)

    ;; order o22 ('m_mnd', 'p_capelambert', 90, 135)
    (order_mine o22 m_mnd)
    (order_port o22 p_capelambert)
    (= (load_after_time o22) 90)
    (= (unload_before_time o22) 135)
    (not-delivered o22)

    ;; consist c22
    (= (current_time c22) 0)
    (available c22)
    (empty c22)
    (at c22 y)

    (compat c22 o22)

    ;; order o23 ('m_mnd', 'p_capelambert', 135, 180)
    (order_mine o23 m_mnd)
    (order_port o23 p_capelambert)
    (= (load_after_time o23) 135)
    (= (unload_before_time o23) 180)
    (not-delivered o23)

    ;; consist c23
    (= (current_time c23) 0)
    (available c23)
    (empty c23)
    (at c23 y)

    (compat c23 o23)

    ;; order o24 ('m_ya', 'p_dampier', 0, 45)
    (order_mine o24 m_ya)
    (order_port o24 p_dampier)
    (= (load_after_time o24) 0)
    (= (unload_before_time o24) 45)
    (not-delivered o24)

    ;; consist c24
    (= (current_time c24) 0)
    (available c24)
    (empty c24)
    (at c24 y)

    (compat c24 o24)

    ;; order o25 ('m_ya', 'p_dampier', 45, 90)
    (order_mine o25 m_ya)
    (order_port o25 p_dampier)
    (= (load_after_time o25) 45)
    (= (unload_before_time o25) 90)
    (not-delivered o25)

    ;; consist c25
    (= (current_time c25) 0)
    (available c25)
    (empty c25)
    (at c25 y)

    (compat c25 o25)

    ;; order o26 ('m_ya', 'p_dampier', 90, 135)
    (order_mine o26 m_ya)
    (order_port o26 p_dampier)
    (= (load_after_time o26) 90)
    (= (unload_before_time o26) 135)
    (not-delivered o26)

    ;; consist c26
    (= (current_time c26) 0)
    (available c26)
    (empty c26)
    (at c26 y)

    (compat c26 o26)

    ;; order o27 ('m_ya', 'p_dampier', 135, 180)
    (order_mine o27 m_ya)
    (order_port o27 p_dampier)
    (= (load_after_time o27) 135)
    (= (unload_before_time o27) 180)
    (not-delivered o27)

    ;; consist c27
    (= (current_time c27) 0)
    (available c27)
    (empty c27)
    (at c27 y)

    (compat c27 o27)

    ;; order o28 ('m_ya', 'p_capelambert', 0, 45)
    (order_mine o28 m_ya)
    (order_port o28 p_capelambert)
    (= (load_after_time o28) 0)
    (= (unload_before_time o28) 45)
    (not-delivered o28)

    ;; consist c28
    (= (current_time c28) 0)
    (available c28)
    (empty c28)
    (at c28 y)

    (compat c28 o28)

    ;; order o29 ('m_ya', 'p_capelambert', 45, 90)
    (order_mine o29 m_ya)
    (order_port o29 p_capelambert)
    (= (load_after_time o29) 45)
    (= (unload_before_time o29) 90)
    (not-delivered o29)

    ;; consist c29
    (= (current_time c29) 0)
    (available c29)
    (empty c29)
    (at c29 y)

    (compat c29 o29)

    ;; order o30 ('m_ya', 'p_capelambert', 90, 135)
    (order_mine o30 m_ya)
    (order_port o30 p_capelambert)
    (= (load_after_time o30) 90)
    (= (unload_before_time o30) 135)
    (not-delivered o30)

    ;; consist c30
    (= (current_time c30) 0)
    (available c30)
    (empty c30)
    (at c30 y)

    (compat c30 o30)

    ;; order o31 ('m_ya', 'p_capelambert', 135, 180)
    (order_mine o31 m_ya)
    (order_port o31 p_capelambert)
    (= (load_after_time o31) 135)
    (= (unload_before_time o31) 180)
    (not-delivered o31)

    ;; consist c31
    (= (current_time c31) 0)
    (available c31)
    (empty c31)
    (at c31 y)

    (compat c31 o31)

    ;; order o32 ('m_b2', 'p_dampier', 0, 45)
    (order_mine o32 m_b2)
    (order_port o32 p_dampier)
    (= (load_after_time o32) 0)
    (= (unload_before_time o32) 45)
    (not-delivered o32)

    ;; consist c32
    (= (current_time c32) 0)
    (available c32)
    (empty c32)
    (at c32 y)

    (compat c32 o32)

    ;; order o33 ('m_b2', 'p_dampier', 45, 90)
    (order_mine o33 m_b2)
    (order_port o33 p_dampier)
    (= (load_after_time o33) 45)
    (= (unload_before_time o33) 90)
    (not-delivered o33)

    ;; consist c33
    (= (current_time c33) 0)
    (available c33)
    (empty c33)
    (at c33 y)

    (compat c33 o33)

    ;; order o34 ('m_b2', 'p_dampier', 90, 135)
    (order_mine o34 m_b2)
    (order_port o34 p_dampier)
    (= (load_after_time o34) 90)
    (= (unload_before_time o34) 135)
    (not-delivered o34)

    ;; consist c34
    (= (current_time c34) 0)
    (available c34)
    (empty c34)
    (at c34 y)

    (compat c34 o34)

    ;; order o35 ('m_b2', 'p_dampier', 135, 180)
    (order_mine o35 m_b2)
    (order_port o35 p_dampier)
    (= (load_after_time o35) 135)
    (= (unload_before_time o35) 180)
    (not-delivered o35)

    ;; consist c35
    (= (current_time c35) 0)
    (available c35)
    (empty c35)
    (at c35 y)

    (compat c35 o35)

    ;; order o36 ('m_b2', 'p_capelambert', 0, 45)
    (order_mine o36 m_b2)
    (order_port o36 p_capelambert)
    (= (load_after_time o36) 0)
    (= (unload_before_time o36) 45)
    (not-delivered o36)

    ;; consist c36
    (= (current_time c36) 0)
    (available c36)
    (empty c36)
    (at c36 y)

    (compat c36 o36)

    ;; order o37 ('m_b2', 'p_capelambert', 45, 90)
    (order_mine o37 m_b2)
    (order_port o37 p_capelambert)
    (= (load_after_time o37) 45)
    (= (unload_before_time o37) 90)
    (not-delivered o37)

    ;; consist c37
    (= (current_time c37) 0)
    (available c37)
    (empty c37)
    (at c37 y)

    (compat c37 o37)

    ;; order o38 ('m_b2', 'p_capelambert', 90, 135)
    (order_mine o38 m_b2)
    (order_port o38 p_capelambert)
    (= (load_after_time o38) 90)
    (= (unload_before_time o38) 135)
    (not-delivered o38)

    ;; consist c38
    (= (current_time c38) 0)
    (available c38)
    (empty c38)
    (at c38 y)

    (compat c38 o38)

    ;; order o39 ('m_b2', 'p_capelambert', 135, 180)
    (order_mine o39 m_b2)
    (order_port o39 p_capelambert)
    (= (load_after_time o39) 135)
    (= (unload_before_time o39) 180)
    (not-delivered o39)

    ;; consist c39
    (= (current_time c39) 0)
    (available c39)
    (empty c39)
    (at c39 y)

    (compat c39 o39)

    ;; order o40 ('m_ma', 'p_dampier', 0, 45)
    (order_mine o40 m_ma)
    (order_port o40 p_dampier)
    (= (load_after_time o40) 0)
    (= (unload_before_time o40) 45)
    (not-delivered o40)

    ;; consist c40
    (= (current_time c40) 0)
    (available c40)
    (empty c40)
    (at c40 y)

    (compat c40 o40)

    ;; order o41 ('m_ma', 'p_dampier', 45, 90)
    (order_mine o41 m_ma)
    (order_port o41 p_dampier)
    (= (load_after_time o41) 45)
    (= (unload_before_time o41) 90)
    (not-delivered o41)

    ;; consist c41
    (= (current_time c41) 0)
    (available c41)
    (empty c41)
    (at c41 y)

    (compat c41 o41)

    ;; order o42 ('m_ma', 'p_dampier', 90, 135)
    (order_mine o42 m_ma)
    (order_port o42 p_dampier)
    (= (load_after_time o42) 90)
    (= (unload_before_time o42) 135)
    (not-delivered o42)

    ;; consist c42
    (= (current_time c42) 0)
    (available c42)
    (empty c42)
    (at c42 y)

    (compat c42 o42)

    ;; order o43 ('m_ma', 'p_dampier', 135, 180)
    (order_mine o43 m_ma)
    (order_port o43 p_dampier)
    (= (load_after_time o43) 135)
    (= (unload_before_time o43) 180)
    (not-delivered o43)

    ;; consist c43
    (= (current_time c43) 0)
    (available c43)
    (empty c43)
    (at c43 y)

    (compat c43 o43)

    ;; order o44 ('m_ma', 'p_capelambert', 0, 45)
    (order_mine o44 m_ma)
    (order_port o44 p_capelambert)
    (= (load_after_time o44) 0)
    (= (unload_before_time o44) 45)
    (not-delivered o44)

    ;; consist c44
    (= (current_time c44) 0)
    (available c44)
    (empty c44)
    (at c44 y)

    (compat c44 o44)

    ;; order o45 ('m_ma', 'p_capelambert', 45, 90)
    (order_mine o45 m_ma)
    (order_port o45 p_capelambert)
    (= (load_after_time o45) 45)
    (= (unload_before_time o45) 90)
    (not-delivered o45)

    ;; consist c45
    (= (current_time c45) 0)
    (available c45)
    (empty c45)
    (at c45 y)

    (compat c45 o45)

    ;; order o46 ('m_ma', 'p_capelambert', 90, 135)
    (order_mine o46 m_ma)
    (order_port o46 p_capelambert)
    (= (load_after_time o46) 90)
    (= (unload_before_time o46) 135)
    (not-delivered o46)

    ;; consist c46
    (= (current_time c46) 0)
    (available c46)
    (empty c46)
    (at c46 y)

    (compat c46 o46)

    ;; order o47 ('m_ma', 'p_capelambert', 135, 180)
    (order_mine o47 m_ma)
    (order_port o47 p_capelambert)
    (= (load_after_time o47) 135)
    (= (unload_before_time o47) 180)
    (not-delivered o47)

    ;; consist c47
    (= (current_time c47) 0)
    (available c47)
    (empty c47)
    (at c47 y)

    (compat c47 o47)

    ;; order o48 ('m_b4', 'p_dampier', 0, 45)
    (order_mine o48 m_b4)
    (order_port o48 p_dampier)
    (= (load_after_time o48) 0)
    (= (unload_before_time o48) 45)
    (not-delivered o48)

    ;; consist c48
    (= (current_time c48) 0)
    (available c48)
    (empty c48)
    (at c48 y)

    (compat c48 o48)

    ;; order o49 ('m_b4', 'p_dampier', 45, 90)
    (order_mine o49 m_b4)
    (order_port o49 p_dampier)
    (= (load_after_time o49) 45)
    (= (unload_before_time o49) 90)
    (not-delivered o49)

    ;; consist c49
    (= (current_time c49) 0)
    (available c49)
    (empty c49)
    (at c49 y)

    (compat c49 o49)

    ;; order o50 ('m_b4', 'p_dampier', 90, 135)
    (order_mine o50 m_b4)
    (order_port o50 p_dampier)
    (= (load_after_time o50) 90)
    (= (unload_before_time o50) 135)
    (not-delivered o50)

    ;; consist c50
    (= (current_time c50) 0)
    (available c50)
    (empty c50)
    (at c50 y)

    (compat c50 o50)

    ;; order o51 ('m_b4', 'p_dampier', 135, 180)
    (order_mine o51 m_b4)
    (order_port o51 p_dampier)
    (= (load_after_time o51) 135)
    (= (unload_before_time o51) 180)
    (not-delivered o51)

    ;; consist c51
    (= (current_time c51) 0)
    (available c51)
    (empty c51)
    (at c51 y)

    (compat c51 o51)

    ;; order o52 ('m_b4', 'p_capelambert', 0, 45)
    (order_mine o52 m_b4)
    (order_port o52 p_capelambert)
    (= (load_after_time o52) 0)
    (= (unload_before_time o52) 45)
    (not-delivered o52)

    ;; consist c52
    (= (current_time c52) 0)
    (available c52)
    (empty c52)
    (at c52 y)

    (compat c52 o52)

    ;; order o53 ('m_b4', 'p_capelambert', 45, 90)
    (order_mine o53 m_b4)
    (order_port o53 p_capelambert)
    (= (load_after_time o53) 45)
    (= (unload_before_time o53) 90)
    (not-delivered o53)

    ;; consist c53
    (= (current_time c53) 0)
    (available c53)
    (empty c53)
    (at c53 y)

    (compat c53 o53)

    ;; order o54 ('m_b4', 'p_capelambert', 90, 135)
    (order_mine o54 m_b4)
    (order_port o54 p_capelambert)
    (= (load_after_time o54) 90)
    (= (unload_before_time o54) 135)
    (not-delivered o54)

    ;; consist c54
    (= (current_time c54) 0)
    (available c54)
    (empty c54)
    (at c54 y)

    (compat c54 o54)

    ;; order o55 ('m_b4', 'p_capelambert', 135, 180)
    (order_mine o55 m_b4)
    (order_port o55 p_capelambert)
    (= (load_after_time o55) 135)
    (= (unload_before_time o55) 180)
    (not-delivered o55)

    ;; consist c55
    (= (current_time c55) 0)
    (available c55)
    (empty c55)
    (at c55 y)

    (compat c55 o55)

    ;; order o56 ('m_yac', 'p_dampier', 0, 45)
    (order_mine o56 m_yac)
    (order_port o56 p_dampier)
    (= (load_after_time o56) 0)
    (= (unload_before_time o56) 45)
    (not-delivered o56)

    ;; consist c56
    (= (current_time c56) 0)
    (available c56)
    (empty c56)
    (at c56 y)

    (compat c56 o56)

    ;; order o57 ('m_yac', 'p_dampier', 45, 90)
    (order_mine o57 m_yac)
    (order_port o57 p_dampier)
    (= (load_after_time o57) 45)
    (= (unload_before_time o57) 90)
    (not-delivered o57)

    ;; consist c57
    (= (current_time c57) 0)
    (available c57)
    (empty c57)
    (at c57 y)

    (compat c57 o57)

    ;; order o58 ('m_yac', 'p_dampier', 90, 135)
    (order_mine o58 m_yac)
    (order_port o58 p_dampier)
    (= (load_after_time o58) 90)
    (= (unload_before_time o58) 135)
    (not-delivered o58)

    ;; consist c58
    (= (current_time c58) 0)
    (available c58)
    (empty c58)
    (at c58 y)

    (compat c58 o58)

    ;; order o59 ('m_yac', 'p_dampier', 135, 180)
    (order_mine o59 m_yac)
    (order_port o59 p_dampier)
    (= (load_after_time o59) 135)
    (= (unload_before_time o59) 180)
    (not-delivered o59)

    ;; consist c59
    (= (current_time c59) 0)
    (available c59)
    (empty c59)
    (at c59 y)

    (compat c59 o59)

    ;; order o60 ('m_yac', 'p_capelambert', 0, 45)
    (order_mine o60 m_yac)
    (order_port o60 p_capelambert)
    (= (load_after_time o60) 0)
    (= (unload_before_time o60) 45)
    (not-delivered o60)

    ;; consist c60
    (= (current_time c60) 0)
    (available c60)
    (empty c60)
    (at c60 y)

    (compat c60 o60)

    ;; order o61 ('m_yac', 'p_capelambert', 45, 90)
    (order_mine o61 m_yac)
    (order_port o61 p_capelambert)
    (= (load_after_time o61) 45)
    (= (unload_before_time o61) 90)
    (not-delivered o61)

    ;; consist c61
    (= (current_time c61) 0)
    (available c61)
    (empty c61)
    (at c61 y)

    (compat c61 o61)

    ;; order o62 ('m_yac', 'p_capelambert', 90, 135)
    (order_mine o62 m_yac)
    (order_port o62 p_capelambert)
    (= (load_after_time o62) 90)
    (= (unload_before_time o62) 135)
    (not-delivered o62)

    ;; consist c62
    (= (current_time c62) 0)
    (available c62)
    (empty c62)
    (at c62 y)

    (compat c62 o62)

    ;; order o63 ('m_yac', 'p_capelambert', 135, 180)
    (order_mine o63 m_yac)
    (order_port o63 p_capelambert)
    (= (load_after_time o63) 135)
    (= (unload_before_time o63) 180)
    (not-delivered o63)

    ;; consist c63
    (= (current_time c63) 0)
    (available c63)
    (empty c63)
    (at c63 y)

    (compat c63 o63)

    ;; order o64 ('m_wa', 'p_dampier', 0, 45)
    (order_mine o64 m_wa)
    (order_port o64 p_dampier)
    (= (load_after_time o64) 0)
    (= (unload_before_time o64) 45)
    (not-delivered o64)

    ;; consist c64
    (= (current_time c64) 0)
    (available c64)
    (empty c64)
    (at c64 y)

    (compat c64 o64)

    ;; order o65 ('m_wa', 'p_dampier', 45, 90)
    (order_mine o65 m_wa)
    (order_port o65 p_dampier)
    (= (load_after_time o65) 45)
    (= (unload_before_time o65) 90)
    (not-delivered o65)

    ;; consist c65
    (= (current_time c65) 0)
    (available c65)
    (empty c65)
    (at c65 y)

    (compat c65 o65)

    ;; order o66 ('m_wa', 'p_dampier', 90, 135)
    (order_mine o66 m_wa)
    (order_port o66 p_dampier)
    (= (load_after_time o66) 90)
    (= (unload_before_time o66) 135)
    (not-delivered o66)

    ;; consist c66
    (= (current_time c66) 0)
    (available c66)
    (empty c66)
    (at c66 y)

    (compat c66 o66)

    ;; order o67 ('m_wa', 'p_dampier', 135, 180)
    (order_mine o67 m_wa)
    (order_port o67 p_dampier)
    (= (load_after_time o67) 135)
    (= (unload_before_time o67) 180)
    (not-delivered o67)

    ;; consist c67
    (= (current_time c67) 0)
    (available c67)
    (empty c67)
    (at c67 y)

    (compat c67 o67)

    ;; order o68 ('m_wa', 'p_capelambert', 0, 45)
    (order_mine o68 m_wa)
    (order_port o68 p_capelambert)
    (= (load_after_time o68) 0)
    (= (unload_before_time o68) 45)
    (not-delivered o68)

    ;; consist c68
    (= (current_time c68) 0)
    (available c68)
    (empty c68)
    (at c68 y)

    (compat c68 o68)

    ;; order o69 ('m_wa', 'p_capelambert', 45, 90)
    (order_mine o69 m_wa)
    (order_port o69 p_capelambert)
    (= (load_after_time o69) 45)
    (= (unload_before_time o69) 90)
    (not-delivered o69)

    ;; consist c69
    (= (current_time c69) 0)
    (available c69)
    (empty c69)
    (at c69 y)

    (compat c69 o69)

    ;; order o70 ('m_wa', 'p_capelambert', 90, 135)
    (order_mine o70 m_wa)
    (order_port o70 p_capelambert)
    (= (load_after_time o70) 90)
    (= (unload_before_time o70) 135)
    (not-delivered o70)

    ;; consist c70
    (= (current_time c70) 0)
    (available c70)
    (empty c70)
    (at c70 y)

    (compat c70 o70)

    ;; order o71 ('m_wa', 'p_capelambert', 135, 180)
    (order_mine o71 m_wa)
    (order_port o71 p_capelambert)
    (= (load_after_time o71) 135)
    (= (unload_before_time o71) 180)
    (not-delivered o71)

    ;; consist c71
    (= (current_time c71) 0)
    (available c71)
    (empty c71)
    (at c71 y)

    (compat c71 o71)

    ;; order o72 ('m_hd', 'p_dampier', 0, 45)
    (order_mine o72 m_hd)
    (order_port o72 p_dampier)
    (= (load_after_time o72) 0)
    (= (unload_before_time o72) 45)
    (not-delivered o72)

    ;; consist c72
    (= (current_time c72) 0)
    (available c72)
    (empty c72)
    (at c72 y)

    (compat c72 o72)

    ;; order o73 ('m_hd', 'p_dampier', 45, 90)
    (order_mine o73 m_hd)
    (order_port o73 p_dampier)
    (= (load_after_time o73) 45)
    (= (unload_before_time o73) 90)
    (not-delivered o73)

    ;; consist c73
    (= (current_time c73) 0)
    (available c73)
    (empty c73)
    (at c73 y)

    (compat c73 o73)

    ;; order o74 ('m_hd', 'p_dampier', 90, 135)
    (order_mine o74 m_hd)
    (order_port o74 p_dampier)
    (= (load_after_time o74) 90)
    (= (unload_before_time o74) 135)
    (not-delivered o74)

    ;; consist c74
    (= (current_time c74) 0)
    (available c74)
    (empty c74)
    (at c74 y)

    (compat c74 o74)

    ;; order o75 ('m_hd', 'p_dampier', 135, 180)
    (order_mine o75 m_hd)
    (order_port o75 p_dampier)
    (= (load_after_time o75) 135)
    (= (unload_before_time o75) 180)
    (not-delivered o75)

    ;; consist c75
    (= (current_time c75) 0)
    (available c75)
    (empty c75)
    (at c75 y)

    (compat c75 o75)

    ;; order o76 ('m_hd', 'p_capelambert', 0, 45)
    (order_mine o76 m_hd)
    (order_port o76 p_capelambert)
    (= (load_after_time o76) 0)
    (= (unload_before_time o76) 45)
    (not-delivered o76)

    ;; consist c76
    (= (current_time c76) 0)
    (available c76)
    (empty c76)
    (at c76 y)

    (compat c76 o76)

    ;; order o77 ('m_hd', 'p_capelambert', 45, 90)
    (order_mine o77 m_hd)
    (order_port o77 p_capelambert)
    (= (load_after_time o77) 45)
    (= (unload_before_time o77) 90)
    (not-delivered o77)

    ;; consist c77
    (= (current_time c77) 0)
    (available c77)
    (empty c77)
    (at c77 y)

    (compat c77 o77)

    ;; order o78 ('m_hd', 'p_capelambert', 90, 135)
    (order_mine o78 m_hd)
    (order_port o78 p_capelambert)
    (= (load_after_time o78) 90)
    (= (unload_before_time o78) 135)
    (not-delivered o78)

    ;; consist c78
    (= (current_time c78) 0)
    (available c78)
    (empty c78)
    (at c78 y)

    (compat c78 o78)

    ;; order o79 ('m_hd', 'p_capelambert', 135, 180)
    (order_mine o79 m_hd)
    (order_port o79 p_capelambert)
    (= (load_after_time o79) 135)
    (= (unload_before_time o79) 180)
    (not-delivered o79)

    ;; consist c79
    (= (current_time c79) 0)
    (available c79)
    (empty c79)
    (at c79 y)

    (compat c79 o79)

    ;; order o80 ('m_pbd', 'p_dampier', 0, 45)
    (order_mine o80 m_pbd)
    (order_port o80 p_dampier)
    (= (load_after_time o80) 0)
    (= (unload_before_time o80) 45)
    (not-delivered o80)

    ;; consist c80
    (= (current_time c80) 0)
    (available c80)
    (empty c80)
    (at c80 y)

    (compat c80 o80)

    ;; order o81 ('m_pbd', 'p_dampier', 45, 90)
    (order_mine o81 m_pbd)
    (order_port o81 p_dampier)
    (= (load_after_time o81) 45)
    (= (unload_before_time o81) 90)
    (not-delivered o81)

    ;; consist c81
    (= (current_time c81) 0)
    (available c81)
    (empty c81)
    (at c81 y)

    (compat c81 o81)

    ;; order o82 ('m_pbd', 'p_dampier', 90, 135)
    (order_mine o82 m_pbd)
    (order_port o82 p_dampier)
    (= (load_after_time o82) 90)
    (= (unload_before_time o82) 135)
    (not-delivered o82)

    ;; consist c82
    (= (current_time c82) 0)
    (available c82)
    (empty c82)
    (at c82 y)

    (compat c82 o82)

    ;; order o83 ('m_pbd', 'p_dampier', 135, 180)
    (order_mine o83 m_pbd)
    (order_port o83 p_dampier)
    (= (load_after_time o83) 135)
    (= (unload_before_time o83) 180)
    (not-delivered o83)

    ;; consist c83
    (= (current_time c83) 0)
    (available c83)
    (empty c83)
    (at c83 y)

    (compat c83 o83)

    ;; order o84 ('m_pbd', 'p_capelambert', 0, 45)
    (order_mine o84 m_pbd)
    (order_port o84 p_capelambert)
    (= (load_after_time o84) 0)
    (= (unload_before_time o84) 45)
    (not-delivered o84)

    ;; consist c84
    (= (current_time c84) 0)
    (available c84)
    (empty c84)
    (at c84 y)

    (compat c84 o84)

    ;; order o85 ('m_pbd', 'p_capelambert', 45, 90)
    (order_mine o85 m_pbd)
    (order_port o85 p_capelambert)
    (= (load_after_time o85) 45)
    (= (unload_before_time o85) 90)
    (not-delivered o85)

    ;; consist c85
    (= (current_time c85) 0)
    (available c85)
    (empty c85)
    (at c85 y)

    (compat c85 o85)

    ;; order o86 ('m_pbd', 'p_capelambert', 90, 135)
    (order_mine o86 m_pbd)
    (order_port o86 p_capelambert)
    (= (load_after_time o86) 90)
    (= (unload_before_time o86) 135)
    (not-delivered o86)

    ;; consist c86
    (= (current_time c86) 0)
    (available c86)
    (empty c86)
    (at c86 y)

    (compat c86 o86)

    ;; order o87 ('m_pbd', 'p_capelambert', 135, 180)
    (order_mine o87 m_pbd)
    (order_port o87 p_capelambert)
    (= (load_after_time o87) 135)
    (= (unload_before_time o87) 180)
    (not-delivered o87)

    ;; consist c87
    (= (current_time c87) 0)
    (available c87)
    (empty c87)
    (at c87 y)

    (compat c87 o87)

)
(:goal
(and
  (delivered o0)
  (at c0 y)
  (delivered o1)
  (at c1 y)
  (delivered o2)
  (at c2 y)
  (delivered o3)
  (at c3 y)
  (delivered o4)
  (at c4 y)
  (delivered o5)
  (at c5 y)
  (delivered o6)
  (at c6 y)
  (delivered o7)
  (at c7 y)
  (delivered o8)
  (at c8 y)
  (delivered o9)
  (at c9 y)
  (delivered o10)
  (at c10 y)
  (delivered o11)
  (at c11 y)
  (delivered o12)
  (at c12 y)
  (delivered o13)
  (at c13 y)
  (delivered o14)
  (at c14 y)
  (delivered o15)
  (at c15 y)
  (delivered o16)
  (at c16 y)
  (delivered o17)
  (at c17 y)
  (delivered o18)
  (at c18 y)
  (delivered o19)
  (at c19 y)
  (delivered o20)
  (at c20 y)
  (delivered o21)
  (at c21 y)
  (delivered o22)
  (at c22 y)
  (delivered o23)
  (at c23 y)
  (delivered o24)
  (at c24 y)
  (delivered o25)
  (at c25 y)
  (delivered o26)
  (at c26 y)
  (delivered o27)
  (at c27 y)
  (delivered o28)
  (at c28 y)
  (delivered o29)
  (at c29 y)
  (delivered o30)
  (at c30 y)
  (delivered o31)
  (at c31 y)
  (delivered o32)
  (at c32 y)
  (delivered o33)
  (at c33 y)
  (delivered o34)
  (at c34 y)
  (delivered o35)
  (at c35 y)
  (delivered o36)
  (at c36 y)
  (delivered o37)
  (at c37 y)
  (delivered o38)
  (at c38 y)
  (delivered o39)
  (at c39 y)
  (delivered o40)
  (at c40 y)
  (delivered o41)
  (at c41 y)
  (delivered o42)
  (at c42 y)
  (delivered o43)
  (at c43 y)
  (delivered o44)
  (at c44 y)
  (delivered o45)
  (at c45 y)
  (delivered o46)
  (at c46 y)
  (delivered o47)
  (at c47 y)
  (delivered o48)
  (at c48 y)
  (delivered o49)
  (at c49 y)
  (delivered o50)
  (at c50 y)
  (delivered o51)
  (at c51 y)
  (delivered o52)
  (at c52 y)
  (delivered o53)
  (at c53 y)
  (delivered o54)
  (at c54 y)
  (delivered o55)
  (at c55 y)
  (delivered o56)
  (at c56 y)
  (delivered o57)
  (at c57 y)
  (delivered o58)
  (at c58 y)
  (delivered o59)
  (at c59 y)
  (delivered o60)
  (at c60 y)
  (delivered o61)
  (at c61 y)
  (delivered o62)
  (at c62 y)
  (delivered o63)
  (at c63 y)
  (delivered o64)
  (at c64 y)
  (delivered o65)
  (at c65 y)
  (delivered o66)
  (at c66 y)
  (delivered o67)
  (at c67 y)
  (delivered o68)
  (at c68 y)
  (delivered o69)
  (at c69 y)
  (delivered o70)
  (at c70 y)
  (delivered o71)
  (at c71 y)
  (delivered o72)
  (at c72 y)
  (delivered o73)
  (at c73 y)
  (delivered o74)
  (at c74 y)
  (delivered o75)
  (at c75 y)
  (delivered o76)
  (at c76 y)
  (delivered o77)
  (at c77 y)
  (delivered o78)
  (at c78 y)
  (delivered o79)
  (at c79 y)
  (delivered o80)
  (at c80 y)
  (delivered o81)
  (at c81 y)
  (delivered o82)
  (at c82 y)
  (delivered o83)
  (at c83 y)
  (delivered o84)
  (at c84 y)
  (delivered o85)
  (at c85 y)
  (delivered o86)
  (at c86 y)
  (delivered o87)
  (at c87 y)
))
(:metric minimize (total-time)))

