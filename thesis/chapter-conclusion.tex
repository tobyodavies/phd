
\chapter{Conclusion}
\label{chapter:conclusion}%
\begin{ch-abstract}
Here we reiterate the contributions, conclusions and further work identified in this thesis.
\end{ch-abstract}

In this thesis we set out to explore the ways in which knowledge
derived from conflict can be exploited to find and prove optimal plans
in three key variants of the planning problem.

We introduced a number of algorithms, listed in table \ref{conc:contribs},
in the development of these algorithms, several new notions of
conflict and representations of knowledge were also introduced.
In many cases these conflicts and representations of knowledge have potential
applications in different algorithms, which we have not yet explored.

We also examined a number of existing algorithms through the lens of
conflict-directed learning.

\begin{table}
\begin{tabular}{l | l | l }
  Algorithm  &   Conflict    & Knowledge Learned              \\
\hline
 \textbf{FragPlan} (Ch. \ref{chp:fragplan}+\ref{chp:orsc})  &  Resource limitations     &  Improving agent plan         \\
 \textbf{OpSeq} (Ch. \ref{chp:opseq})  & Unsequencable            &  Generalised Landmark       \\
 \textbf{CDHL} (Ch. \ref{chp:cdhl})    &  Suboptimality            &  Clause                \\
 \textbf{MznLBBD} (Ch. \ref{chp:autolbbd})  &  Unschedulable            &  Benders Cut                \\
 \textbf{OpSched} (Sec. \ref{chp:opsched})  & Unschedulable          & Add Events  \\
\end{tabular}
\caption{Summary of introduced algorithms\label{conc:contribs}}
\end{table}



\section{Part I}
In Part I we examined multi-agent planning, introducing Fragment-Based
Planning (FragPlan in table \ref{conc:contribs}), certainly the most
empirically effective of the algorithms introduced in this thesis.
FragPlan is an anytime multi-agent plan-space planning algorithm for
real-world resource constrained problems which can solve problems
orders of magnitude larger than state-of-the-art planning or
scheduling approaches.

Fragment-Based planning relies on shared resources to model agents'
interactions, agents then plan independently and over-used resources
are conflicts, which are subsequently assigned prices.
Agents will then tend to avoid using these resources in subsequent
plans.
FragPlan can thus either be seen as learning the true cost of
resources, or as learning an optimal set of per-agent plans.
We prefer the latter view because there are domains in which there is
not \textbf{one} set of prices from which we can derive the optimal
joint plan, but a different set of prices for each agent.

In defining this algorithm we introduced the notion of
``bounded-benefit'' programs, a decidable subclass of \Golog{}
program. These have to potential to simplify modelling in traditional
\Golog{} dialects by exploiting a notion of cost to prune areas of the
search.

We also introduced ``precondition relaxations'', an application of Lagrangian
Relaxation to dynamic multi-agent systems. These have the potential to
enable a principled way to combine per-agent heuristics into an
admissible multi-agent heuristic, while preserving some degree of privacy.


\section{Part II}

In Part II we consider classical planning, and introduce two significant algorithms
Operator Sequencing (OpSeq), and Conflict-Directed Heuristic Learning (CDHL).
OpSeq is an application of Logic-Based Benders Decomposition to the
classical planning problem. It exploits the observation that the cost
of a plan is independent of the order in which the actions are
applied, in order to build an ever-improving lower-bound on plan cost.
At each iteration a Mixed-Integer Program is solved to generate an
estimate of an optimal set of actions (called a plan projection or
operator count). This projection is then either sequenced into a plan,
or refuted and a ``generalised landmark'' is learned and added to the
MIP to refine future operator counts.

We introduced a SAT-based sequencing/refutation sub-problem, but one
can easily imagine a state-based algorithm that attempts to explore
the reachable state-space restricted to using only the projected
actions. Such a sub-problem solver could allow some of the learned
landmarks to be re-used recursively in similar states. We believe this
kind of sub-problem solver may be more effective (if more complex)
than the SAT-based approach we present in this thesis.

More traditional applications of generalised landmarks remain
unexplored.  For example, one can imagine a fixed
heuristically-generated set of these landmarks could be computed at
the start of a search and exploited in a traditional heuristic similar
to LM-cut, operator counting, or potential heuristic.

In this part we also introduce Conflict-Directed Heuristic Learning
(CDHL), which generalises IDA* with a transposition table.
We first analyse the addition of a transposition table to IDA* as a
non-generalising, but nonetheless surprisingly effective learning
algorithm, using the notion of reduced-cost of operators as the root
of the type of conflict that IDA* detects and prunes.

Given this understanding, we exploit regression to examine which
properties of a state are actually responsible for it being further
from the goal than the heuristic initially estimated, and update the
heuristic estimate for a set of states, not just the one we have
actually seen.
Specifically we learn a clause in terms of facts that \textbf{do not
  hold} in a state, and integrate into this clausal database a
successor generator that is aware of reduced cost, allowing CDHL to
learn to prune sub-trees earlier.


\section{Part III}

Part III explores temporal planning and scheduling problems.
Learning from conflict in traditional scheduling problems is well
explored in the constraint programming literature, and we introduce an
Automatic Logic-Based Benders Decomposition (AutoLBBD), which
out-performs a state-of-the-art conflict-directed learning CP system,
at least on the expressive alternative scheduling problems we
investigate.

AutoLBBD is the first ``model-and-run'' LBBD solver, capable of
solving an arbitrary unannotated constraint program using this
highly-effective conflict-directed algorithm.
Our results show that this approach, while far from perfect,
outperforms both traditional CP and MIP formulations of several
alternative scheduling problems.
This chapter is, to our knowledge, the broadest comparison
of logic-based Benders with MIP and CP. Our automatic decomposition
enabled us to investigate many different domains without significant
additional engineering effort.

This work also highlighted the duality of Logic-Based Benders
Decomposition and the local search technique Large Neighbourhood
Search.

To exploit this AutoLBBD framework, we also described Operator
Scheduling (OpSched), a CP model of expressive temporal planning,
unfortunately, disappointing preliminary results suggest that the
overhead of the MiniZinc modelling language is significant.
Additionally, we suspect that the types of problems
considered in existing temporal planning benchmarks do not exploit the
potential strengths of this approach.

We are particularly interested in investigating domains which could
potentially exploit the rich resource-constrained scheduling
constraints that can be added to a temporal problem in this framework.
In developing this extension we also introduced a partial-order delete
relaxation which may have applications in more traditional
partial-order planners.

\section{Summary}

Our results show that conflict-directed reasoning is a highly
effective approach to cost-optimal planning in industrial domains when
appropriate decompositions and explanations are known.

We have introduced a number of novel notions of conflict, particularly
within plan-space search, and algorithms which learn and exploit
knowledge that can be learned from these conflicts.
Several of these algorithms derive decompositions and explanations
automatically from unannotated problem descriptions in the standard
modelling languages PDDL and MiniZinc.
We believe there is significant future work to be done in
investigating more efficient ways to derive and exploit these
decompositions and explanations.

We are excited about the directions for future research our
investigations into conflict in plan-space planning has opened.
It is our hope that new, more expressive modelling approaches for
planning can be built upon the algorithms introduced in this
thesis.

