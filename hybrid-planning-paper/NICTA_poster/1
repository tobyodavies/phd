
We tackle the problem of meeting orders by rail 'services' to satisfy as many 'orders' as possible within a minimum time duration - such as moving stock-piles of iron ore from multiple mines to ports through a complex rail network. 

Toby's industry experience taught him that modification of the code used to generate service plans took 9 months to amend 10,000 lines of C++ - hundreds of goals; thousands of time points. 

That's a problem for industry, they'd like to solve every day as new orders dynamically arise; execute 'just-in-time' to assign crew.

Such 'state-dependent', temporal, resource constraints are hard to apply to existing global optimisation techniques without getting huge models. 

Solution? Combine the strengths of two core technologies - automated planning and MIP.

Planning guides the search towards feasible action sequences based on a concise temporal program - while the global MIP master provides dual information from a linear program relaxation to predict costs - the dual facilitates the prediction of resource consumption which is fed back to the planner.

Our fragment-based planning solver works in a column generation setting - utilising branch-and-price - allowing the quick identification of bottlenecks and is guaranteed to reach a fixed point with the same objective value.

This enables scaling up to far larger planning problems and is easier to modify because it is more concise.

