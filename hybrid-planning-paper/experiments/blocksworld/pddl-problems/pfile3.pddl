

(define (problem BW-rand-6)
(:domain blocksworld)
(:objects b1 b2 b3 b4 b5 b6  - block
	  h1 h2 h3 h4 h5 h6  - hand)
(:init
(handempty h1)(handempty h2)(handempty h3)(handempty h4)(handempty h5)(handempty h6)
(on b1 b6)
(ontable b2)
(on b3 b1)
(on b4 b5)
(on b5 b3)
(on b6 b2)
(clear b4)
)
(:goal
(and
(on b1 b6)
(on b3 b5)
(on b4 b3)
(on b6 b4))
)
(:metric minimize (total-time)))



