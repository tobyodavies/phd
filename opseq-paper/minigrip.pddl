(define (domain minigripper)
  (:requirements :typing)
  (:types ball - object
  		  room - object)

(:predicates 
	(hand-at ?r - room)
	(ball-at ?b - ball ?r - room)
	(in-hand ?b)
	(hand-empty)
)

(:action pick-up
  :parameters (?b - ball ?r - room)
  :precondition (and
					(hand-empty)
					(hand-at ?r)
					(ball-at ?b ?r)
				)
  :effect (and
			(in-hand ?b)
			(not (ball-at ?b ?r))
			(not (hand-empty))))

(:action drop
  :parameters (?b - ball ?r - room)
  :precondition (and
  				  (in-hand ?b)
				  (hand-at ?r))
  :effect (and
			(not (in-hand ?b))
			(ball-at ?b ?r)
			(hand-empty)))

(:action move
  :parameters (?r0 ?r1 - room)
  :precondition (and (hand-at ?r0))
  :effect (and
			(not (hand-at ?r0))
  		  	(hand-at ?r1)))
)
