import random
import networkx as nx


M = 1e9


class JustificationGraph(object):
    def __init__(self, actions, opset, init):
        producers = {atom: frozenset(a.idx for a in actions if atom in a.adds)
                     for atom in reduce(set.union,
                                        [a.adds for a in actions])}
        produced = {atom: sum(opset.get(op, 0) for op in producers[atom]) + int(atom in init)
                    for atom in producers}
        self.produced = produced
        self.producers = producers
        self.init = init
        self.opset = opset
        self.actions = actions
        self.support = {}
        self.h1 = {atom: 1e9 for atom in producers}
        self.max_flow = {}
        self._cut = {}
        self.calc_supporters()

    def calc_supporters(self, randomize=False):
        if randomize:
            def rnd(x):
                return random.random() * x
        else:
            def rnd(x):
                return (x, random.random())
        self.calc_h1()
        for act in self.actions:
            op = act.idx
            _, prec = max((rnd(self.h1.get(f, 1e9)), f) for f in act.precs)
            self.support[op] = prec

    def count(self, op):
        return min(1, self.opset.get(op, 0))

    def rcost(self, op):
        return (1 - self.count(op)) * self.actions[op].cost

    def op_h1(self, op):
        return self.rcost(op) + max(self.h1.get(prec, 1e9)
                                    for prec in self.actions[op].precs)

    def step_h1(self):
        changed = False
        for atom in self.init:
            self.h1[atom] = 0
        for atom, ops in self.producers.items():
            self.h1.setdefault(atom, 1e9)
            reduced_cost = min(self.op_h1(op)
                               for op in ops
                               for a in [self.actions[op]])
            if reduced_cost < self.h1[atom]:
                changed = True
                self.h1[atom] = reduced_cost
        return changed

    def calc_h1(self):
        while self.step_h1():
            pass

    def _get_cuts(self, goals, cuts):
        any_cut = False
        G = self.to_graph(goals)
        flow = 0.0
        while flow < 1 - 1e-5:
            flow, (left, right) = nx.minimum_cut(G, 'init', 'goal')
            if flow > 1 - 1e-3:
                break
            cut = frozenset(op
                            for src in left
                            for (kind, op) in G[src]
                            if (kind, op) in right)
            # assert all(self.op_node(op) in right
            #            for op in cut)
            # assert all(self.fact_node(self.support[op]) in left
            #            for op in cut)
            if cut not in cuts:
                any_cut = True
                cuts.add(cut)
            _, op = min((self.rcost(op), op)
                        for op in cut)
            self.opset[op] = 1
            G['fact', self.support[op]]['op', op]['capacity'] = 1.0
        return any_cut

    def get_cuts(self, goals, opset=None, randomize=False):
        if opset is not None:
            self.opset = dict(opset)
            self.h1 = {atom: 1e9 for atom in self.producers}
            self._cut = {}
        cuts = set()
        any_cut = True
        while any_cut:
            any_cut = False
            self.calc_supporters(randomize=randomize)
            any_cut = self._get_cuts(goals, cuts)
        return cuts

    def get_randomized_cuts(self, goals, opset=None):
        return self.get_cuts(goals, opset, randomize=True)

    def op_node(self, op):
        return ('op', op)

    def fact_node(self, fact):
        return ('fact', fact)

    def to_graph(self, goals):
        G = nx.DiGraph()
        for op, prec in self.support.items():
            act = self.actions[op]
            G.add_edge(self.fact_node(prec), self.op_node(op),
                       capacity=self.opset.get(op, 0),
                       weight=act.cost)
            G.add_edges_from([(self.op_node(op), self.fact_node(add),
                               {'capacity': M, 'weight': M})
                              for add in act.adds])
        _, goal = max(((self.h1.get(atom), random.random()), atom) for atom in goals)
        G.add_edge(self.fact_node(goal), 'goal', capacity=M, weight=0)
        G.add_edges_from([('init', self.fact_node(atom),
                           {'capacity': M, 'weight': M})
                          for atom in self.init])
        return G
