running in simple mode
(define (problem bfrs_64)
(:domain bfrs)
(:objects
    p2 - location
    p1 - location
    d - location
    m1 - location
    m2 - location
    y - location
    x - location
    o0 - order
    o1 - order
    o2 - order
    o3 - order
    o4 - order
    o5 - order
    o6 - order
    o7 - order
    o8 - order
    o9 - order
    o10 - order
    o11 - order
    o12 - order
    o13 - order
    o14 - order
    o15 - order
    o16 - order
    o17 - order
    o18 - order
    o19 - order
    o20 - order
    o21 - order
    o22 - order
    o23 - order
    o24 - order
    o25 - order
    o26 - order
    o27 - order
    o28 - order
    o29 - order
    o30 - order
    o31 - order
    o32 - order
    o33 - order
    o34 - order
    o35 - order
    o36 - order
    o37 - order
    o38 - order
    o39 - order
    o40 - order
    o41 - order
    o42 - order
    o43 - order
    o44 - order
    o45 - order
    o46 - order
    o47 - order
    o48 - order
    o49 - order
    o50 - order
    o51 - order
    o52 - order
    o53 - order
    o54 - order
    o55 - order
    o56 - order
    o57 - order
    o58 - order
    o59 - order
    o60 - order
    o61 - order
    o62 - order
    o63 - order
    c0 - consist
    c1 - consist
    c2 - consist
    c3 - consist
    c4 - consist
    c5 - consist
    c6 - consist
    c7 - consist
    c8 - consist
    c9 - consist
    c10 - consist
    c11 - consist
    c12 - consist
    c13 - consist
    c14 - consist
    c15 - consist
    c16 - consist
    c17 - consist
    c18 - consist
    c19 - consist
    c20 - consist
    c21 - consist
    c22 - consist
    c23 - consist
    c24 - consist
    c25 - consist
    c26 - consist
    c27 - consist
    c28 - consist
    c29 - consist
    c30 - consist
    c31 - consist
    c32 - consist
    c33 - consist
    c34 - consist
    c35 - consist
    c36 - consist
    c37 - consist
    c38 - consist
    c39 - consist
    c40 - consist
    c41 - consist
    c42 - consist
    c43 - consist
    c44 - consist
    c45 - consist
    c46 - consist
    c47 - consist
    c48 - consist
    c49 - consist
    c50 - consist
    c51 - consist
    c52 - consist
    c53 - consist
    c54 - consist
    c55 - consist
    c56 - consist
    c57 - consist
    c58 - consist
    c59 - consist
    c60 - consist
    c61 - consist
    c62 - consist
    c63 - consist
)
(:init
  (is_yard y)
    (clear p2)
    (clear p1)
    (clear d)
    (clear m1)
    (clear m2)
    (clear y)
    (clear x)

    (conn p2 y)
    (conn y p2)
    (conn p1 y)
    (conn y p1)
    (conn d y)
    (conn y d)
    (conn d x)
    (conn x d)
    (conn d m1)
    (conn m1 d)
    (conn d m2)
    (conn m2 d)
    (conn y x)
    (conn x y)

    ;; order o0 ('m1', 'p2', 0, 12)
    (order_mine o0 m1)
    (order_port o0 p2)
    (= (load_after_time o0) 0)
    (= (unload_before_time o0) 12)
    (not-delivered o0)

    ;; consist c0
    (= (current_time c0) 0)
    (available c0)
    (empty c0)
    (at c0 y)

    (compat c0 o0)

    ;; order o1 ('m1', 'p2', 12, 24)
    (order_mine o1 m1)
    (order_port o1 p2)
    (= (load_after_time o1) 12)
    (= (unload_before_time o1) 24)
    (not-delivered o1)

    ;; consist c1
    (= (current_time c1) 0)
    (available c1)
    (empty c1)
    (at c1 y)

    (compat c1 o1)

    ;; order o2 ('m1', 'p2', 24, 36)
    (order_mine o2 m1)
    (order_port o2 p2)
    (= (load_after_time o2) 24)
    (= (unload_before_time o2) 36)
    (not-delivered o2)

    ;; consist c2
    (= (current_time c2) 0)
    (available c2)
    (empty c2)
    (at c2 y)

    (compat c2 o2)

    ;; order o3 ('m1', 'p2', 36, 48)
    (order_mine o3 m1)
    (order_port o3 p2)
    (= (load_after_time o3) 36)
    (= (unload_before_time o3) 48)
    (not-delivered o3)

    ;; consist c3
    (= (current_time c3) 0)
    (available c3)
    (empty c3)
    (at c3 y)

    (compat c3 o3)

    ;; order o4 ('m1', 'p2', 48, 60)
    (order_mine o4 m1)
    (order_port o4 p2)
    (= (load_after_time o4) 48)
    (= (unload_before_time o4) 60)
    (not-delivered o4)

    ;; consist c4
    (= (current_time c4) 0)
    (available c4)
    (empty c4)
    (at c4 y)

    (compat c4 o4)

    ;; order o5 ('m1', 'p2', 60, 72)
    (order_mine o5 m1)
    (order_port o5 p2)
    (= (load_after_time o5) 60)
    (= (unload_before_time o5) 72)
    (not-delivered o5)

    ;; consist c5
    (= (current_time c5) 0)
    (available c5)
    (empty c5)
    (at c5 y)

    (compat c5 o5)

    ;; order o6 ('m1', 'p2', 72, 84)
    (order_mine o6 m1)
    (order_port o6 p2)
    (= (load_after_time o6) 72)
    (= (unload_before_time o6) 84)
    (not-delivered o6)

    ;; consist c6
    (= (current_time c6) 0)
    (available c6)
    (empty c6)
    (at c6 y)

    (compat c6 o6)

    ;; order o7 ('m1', 'p2', 84, 96)
    (order_mine o7 m1)
    (order_port o7 p2)
    (= (load_after_time o7) 84)
    (= (unload_before_time o7) 96)
    (not-delivered o7)

    ;; consist c7
    (= (current_time c7) 0)
    (available c7)
    (empty c7)
    (at c7 y)

    (compat c7 o7)

    ;; order o8 ('m1', 'p2', 96, 108)
    (order_mine o8 m1)
    (order_port o8 p2)
    (= (load_after_time o8) 96)
    (= (unload_before_time o8) 108)
    (not-delivered o8)

    ;; consist c8
    (= (current_time c8) 0)
    (available c8)
    (empty c8)
    (at c8 y)

    (compat c8 o8)

    ;; order o9 ('m1', 'p2', 108, 120)
    (order_mine o9 m1)
    (order_port o9 p2)
    (= (load_after_time o9) 108)
    (= (unload_before_time o9) 120)
    (not-delivered o9)

    ;; consist c9
    (= (current_time c9) 0)
    (available c9)
    (empty c9)
    (at c9 y)

    (compat c9 o9)

    ;; order o10 ('m1', 'p2', 120, 132)
    (order_mine o10 m1)
    (order_port o10 p2)
    (= (load_after_time o10) 120)
    (= (unload_before_time o10) 132)
    (not-delivered o10)

    ;; consist c10
    (= (current_time c10) 0)
    (available c10)
    (empty c10)
    (at c10 y)

    (compat c10 o10)

    ;; order o11 ('m1', 'p2', 132, 144)
    (order_mine o11 m1)
    (order_port o11 p2)
    (= (load_after_time o11) 132)
    (= (unload_before_time o11) 144)
    (not-delivered o11)

    ;; consist c11
    (= (current_time c11) 0)
    (available c11)
    (empty c11)
    (at c11 y)

    (compat c11 o11)

    ;; order o12 ('m1', 'p2', 144, 156)
    (order_mine o12 m1)
    (order_port o12 p2)
    (= (load_after_time o12) 144)
    (= (unload_before_time o12) 156)
    (not-delivered o12)

    ;; consist c12
    (= (current_time c12) 0)
    (available c12)
    (empty c12)
    (at c12 y)

    (compat c12 o12)

    ;; order o13 ('m1', 'p2', 156, 168)
    (order_mine o13 m1)
    (order_port o13 p2)
    (= (load_after_time o13) 156)
    (= (unload_before_time o13) 168)
    (not-delivered o13)

    ;; consist c13
    (= (current_time c13) 0)
    (available c13)
    (empty c13)
    (at c13 y)

    (compat c13 o13)

    ;; order o14 ('m1', 'p2', 168, 180)
    (order_mine o14 m1)
    (order_port o14 p2)
    (= (load_after_time o14) 168)
    (= (unload_before_time o14) 180)
    (not-delivered o14)

    ;; consist c14
    (= (current_time c14) 0)
    (available c14)
    (empty c14)
    (at c14 y)

    (compat c14 o14)

    ;; order o15 ('m1', 'p2', 180, 192)
    (order_mine o15 m1)
    (order_port o15 p2)
    (= (load_after_time o15) 180)
    (= (unload_before_time o15) 192)
    (not-delivered o15)

    ;; consist c15
    (= (current_time c15) 0)
    (available c15)
    (empty c15)
    (at c15 y)

    (compat c15 o15)

    ;; order o16 ('m1', 'p2', 192, 204)
    (order_mine o16 m1)
    (order_port o16 p2)
    (= (load_after_time o16) 192)
    (= (unload_before_time o16) 204)
    (not-delivered o16)

    ;; consist c16
    (= (current_time c16) 0)
    (available c16)
    (empty c16)
    (at c16 y)

    (compat c16 o16)

    ;; order o17 ('m1', 'p2', 204, 216)
    (order_mine o17 m1)
    (order_port o17 p2)
    (= (load_after_time o17) 204)
    (= (unload_before_time o17) 216)
    (not-delivered o17)

    ;; consist c17
    (= (current_time c17) 0)
    (available c17)
    (empty c17)
    (at c17 y)

    (compat c17 o17)

    ;; order o18 ('m1', 'p2', 216, 228)
    (order_mine o18 m1)
    (order_port o18 p2)
    (= (load_after_time o18) 216)
    (= (unload_before_time o18) 228)
    (not-delivered o18)

    ;; consist c18
    (= (current_time c18) 0)
    (available c18)
    (empty c18)
    (at c18 y)

    (compat c18 o18)

    ;; order o19 ('m1', 'p2', 228, 240)
    (order_mine o19 m1)
    (order_port o19 p2)
    (= (load_after_time o19) 228)
    (= (unload_before_time o19) 240)
    (not-delivered o19)

    ;; consist c19
    (= (current_time c19) 0)
    (available c19)
    (empty c19)
    (at c19 y)

    (compat c19 o19)

    ;; order o20 ('m1', 'p2', 240, 252)
    (order_mine o20 m1)
    (order_port o20 p2)
    (= (load_after_time o20) 240)
    (= (unload_before_time o20) 252)
    (not-delivered o20)

    ;; consist c20
    (= (current_time c20) 0)
    (available c20)
    (empty c20)
    (at c20 y)

    (compat c20 o20)

    ;; order o21 ('m1', 'p2', 252, 264)
    (order_mine o21 m1)
    (order_port o21 p2)
    (= (load_after_time o21) 252)
    (= (unload_before_time o21) 264)
    (not-delivered o21)

    ;; consist c21
    (= (current_time c21) 0)
    (available c21)
    (empty c21)
    (at c21 y)

    (compat c21 o21)

    ;; order o22 ('m1', 'p2', 264, 276)
    (order_mine o22 m1)
    (order_port o22 p2)
    (= (load_after_time o22) 264)
    (= (unload_before_time o22) 276)
    (not-delivered o22)

    ;; consist c22
    (= (current_time c22) 0)
    (available c22)
    (empty c22)
    (at c22 y)

    (compat c22 o22)

    ;; order o23 ('m1', 'p2', 276, 288)
    (order_mine o23 m1)
    (order_port o23 p2)
    (= (load_after_time o23) 276)
    (= (unload_before_time o23) 288)
    (not-delivered o23)

    ;; consist c23
    (= (current_time c23) 0)
    (available c23)
    (empty c23)
    (at c23 y)

    (compat c23 o23)

    ;; order o24 ('m1', 'p2', 288, 300)
    (order_mine o24 m1)
    (order_port o24 p2)
    (= (load_after_time o24) 288)
    (= (unload_before_time o24) 300)
    (not-delivered o24)

    ;; consist c24
    (= (current_time c24) 0)
    (available c24)
    (empty c24)
    (at c24 y)

    (compat c24 o24)

    ;; order o25 ('m1', 'p2', 300, 312)
    (order_mine o25 m1)
    (order_port o25 p2)
    (= (load_after_time o25) 300)
    (= (unload_before_time o25) 312)
    (not-delivered o25)

    ;; consist c25
    (= (current_time c25) 0)
    (available c25)
    (empty c25)
    (at c25 y)

    (compat c25 o25)

    ;; order o26 ('m1', 'p2', 312, 324)
    (order_mine o26 m1)
    (order_port o26 p2)
    (= (load_after_time o26) 312)
    (= (unload_before_time o26) 324)
    (not-delivered o26)

    ;; consist c26
    (= (current_time c26) 0)
    (available c26)
    (empty c26)
    (at c26 y)

    (compat c26 o26)

    ;; order o27 ('m1', 'p2', 324, 336)
    (order_mine o27 m1)
    (order_port o27 p2)
    (= (load_after_time o27) 324)
    (= (unload_before_time o27) 336)
    (not-delivered o27)

    ;; consist c27
    (= (current_time c27) 0)
    (available c27)
    (empty c27)
    (at c27 y)

    (compat c27 o27)

    ;; order o28 ('m1', 'p2', 336, 348)
    (order_mine o28 m1)
    (order_port o28 p2)
    (= (load_after_time o28) 336)
    (= (unload_before_time o28) 348)
    (not-delivered o28)

    ;; consist c28
    (= (current_time c28) 0)
    (available c28)
    (empty c28)
    (at c28 y)

    (compat c28 o28)

    ;; order o29 ('m1', 'p2', 348, 360)
    (order_mine o29 m1)
    (order_port o29 p2)
    (= (load_after_time o29) 348)
    (= (unload_before_time o29) 360)
    (not-delivered o29)

    ;; consist c29
    (= (current_time c29) 0)
    (available c29)
    (empty c29)
    (at c29 y)

    (compat c29 o29)

    ;; order o30 ('m1', 'p2', 360, 372)
    (order_mine o30 m1)
    (order_port o30 p2)
    (= (load_after_time o30) 360)
    (= (unload_before_time o30) 372)
    (not-delivered o30)

    ;; consist c30
    (= (current_time c30) 0)
    (available c30)
    (empty c30)
    (at c30 y)

    (compat c30 o30)

    ;; order o31 ('m1', 'p2', 372, 384)
    (order_mine o31 m1)
    (order_port o31 p2)
    (= (load_after_time o31) 372)
    (= (unload_before_time o31) 384)
    (not-delivered o31)

    ;; consist c31
    (= (current_time c31) 0)
    (available c31)
    (empty c31)
    (at c31 y)

    (compat c31 o31)

    ;; order o32 ('m2', 'p1', 0, 12)
    (order_mine o32 m2)
    (order_port o32 p1)
    (= (load_after_time o32) 0)
    (= (unload_before_time o32) 12)
    (not-delivered o32)

    ;; consist c32
    (= (current_time c32) 0)
    (available c32)
    (empty c32)
    (at c32 y)

    (compat c32 o32)

    ;; order o33 ('m2', 'p1', 12, 24)
    (order_mine o33 m2)
    (order_port o33 p1)
    (= (load_after_time o33) 12)
    (= (unload_before_time o33) 24)
    (not-delivered o33)

    ;; consist c33
    (= (current_time c33) 0)
    (available c33)
    (empty c33)
    (at c33 y)

    (compat c33 o33)

    ;; order o34 ('m2', 'p1', 24, 36)
    (order_mine o34 m2)
    (order_port o34 p1)
    (= (load_after_time o34) 24)
    (= (unload_before_time o34) 36)
    (not-delivered o34)

    ;; consist c34
    (= (current_time c34) 0)
    (available c34)
    (empty c34)
    (at c34 y)

    (compat c34 o34)

    ;; order o35 ('m2', 'p1', 36, 48)
    (order_mine o35 m2)
    (order_port o35 p1)
    (= (load_after_time o35) 36)
    (= (unload_before_time o35) 48)
    (not-delivered o35)

    ;; consist c35
    (= (current_time c35) 0)
    (available c35)
    (empty c35)
    (at c35 y)

    (compat c35 o35)

    ;; order o36 ('m2', 'p1', 48, 60)
    (order_mine o36 m2)
    (order_port o36 p1)
    (= (load_after_time o36) 48)
    (= (unload_before_time o36) 60)
    (not-delivered o36)

    ;; consist c36
    (= (current_time c36) 0)
    (available c36)
    (empty c36)
    (at c36 y)

    (compat c36 o36)

    ;; order o37 ('m2', 'p1', 60, 72)
    (order_mine o37 m2)
    (order_port o37 p1)
    (= (load_after_time o37) 60)
    (= (unload_before_time o37) 72)
    (not-delivered o37)

    ;; consist c37
    (= (current_time c37) 0)
    (available c37)
    (empty c37)
    (at c37 y)

    (compat c37 o37)

    ;; order o38 ('m2', 'p1', 72, 84)
    (order_mine o38 m2)
    (order_port o38 p1)
    (= (load_after_time o38) 72)
    (= (unload_before_time o38) 84)
    (not-delivered o38)

    ;; consist c38
    (= (current_time c38) 0)
    (available c38)
    (empty c38)
    (at c38 y)

    (compat c38 o38)

    ;; order o39 ('m2', 'p1', 84, 96)
    (order_mine o39 m2)
    (order_port o39 p1)
    (= (load_after_time o39) 84)
    (= (unload_before_time o39) 96)
    (not-delivered o39)

    ;; consist c39
    (= (current_time c39) 0)
    (available c39)
    (empty c39)
    (at c39 y)

    (compat c39 o39)

    ;; order o40 ('m2', 'p1', 96, 108)
    (order_mine o40 m2)
    (order_port o40 p1)
    (= (load_after_time o40) 96)
    (= (unload_before_time o40) 108)
    (not-delivered o40)

    ;; consist c40
    (= (current_time c40) 0)
    (available c40)
    (empty c40)
    (at c40 y)

    (compat c40 o40)

    ;; order o41 ('m2', 'p1', 108, 120)
    (order_mine o41 m2)
    (order_port o41 p1)
    (= (load_after_time o41) 108)
    (= (unload_before_time o41) 120)
    (not-delivered o41)

    ;; consist c41
    (= (current_time c41) 0)
    (available c41)
    (empty c41)
    (at c41 y)

    (compat c41 o41)

    ;; order o42 ('m2', 'p1', 120, 132)
    (order_mine o42 m2)
    (order_port o42 p1)
    (= (load_after_time o42) 120)
    (= (unload_before_time o42) 132)
    (not-delivered o42)

    ;; consist c42
    (= (current_time c42) 0)
    (available c42)
    (empty c42)
    (at c42 y)

    (compat c42 o42)

    ;; order o43 ('m2', 'p1', 132, 144)
    (order_mine o43 m2)
    (order_port o43 p1)
    (= (load_after_time o43) 132)
    (= (unload_before_time o43) 144)
    (not-delivered o43)

    ;; consist c43
    (= (current_time c43) 0)
    (available c43)
    (empty c43)
    (at c43 y)

    (compat c43 o43)

    ;; order o44 ('m2', 'p1', 144, 156)
    (order_mine o44 m2)
    (order_port o44 p1)
    (= (load_after_time o44) 144)
    (= (unload_before_time o44) 156)
    (not-delivered o44)

    ;; consist c44
    (= (current_time c44) 0)
    (available c44)
    (empty c44)
    (at c44 y)

    (compat c44 o44)

    ;; order o45 ('m2', 'p1', 156, 168)
    (order_mine o45 m2)
    (order_port o45 p1)
    (= (load_after_time o45) 156)
    (= (unload_before_time o45) 168)
    (not-delivered o45)

    ;; consist c45
    (= (current_time c45) 0)
    (available c45)
    (empty c45)
    (at c45 y)

    (compat c45 o45)

    ;; order o46 ('m2', 'p1', 168, 180)
    (order_mine o46 m2)
    (order_port o46 p1)
    (= (load_after_time o46) 168)
    (= (unload_before_time o46) 180)
    (not-delivered o46)

    ;; consist c46
    (= (current_time c46) 0)
    (available c46)
    (empty c46)
    (at c46 y)

    (compat c46 o46)

    ;; order o47 ('m2', 'p1', 180, 192)
    (order_mine o47 m2)
    (order_port o47 p1)
    (= (load_after_time o47) 180)
    (= (unload_before_time o47) 192)
    (not-delivered o47)

    ;; consist c47
    (= (current_time c47) 0)
    (available c47)
    (empty c47)
    (at c47 y)

    (compat c47 o47)

    ;; order o48 ('m2', 'p1', 192, 204)
    (order_mine o48 m2)
    (order_port o48 p1)
    (= (load_after_time o48) 192)
    (= (unload_before_time o48) 204)
    (not-delivered o48)

    ;; consist c48
    (= (current_time c48) 0)
    (available c48)
    (empty c48)
    (at c48 y)

    (compat c48 o48)

    ;; order o49 ('m2', 'p1', 204, 216)
    (order_mine o49 m2)
    (order_port o49 p1)
    (= (load_after_time o49) 204)
    (= (unload_before_time o49) 216)
    (not-delivered o49)

    ;; consist c49
    (= (current_time c49) 0)
    (available c49)
    (empty c49)
    (at c49 y)

    (compat c49 o49)

    ;; order o50 ('m2', 'p1', 216, 228)
    (order_mine o50 m2)
    (order_port o50 p1)
    (= (load_after_time o50) 216)
    (= (unload_before_time o50) 228)
    (not-delivered o50)

    ;; consist c50
    (= (current_time c50) 0)
    (available c50)
    (empty c50)
    (at c50 y)

    (compat c50 o50)

    ;; order o51 ('m2', 'p1', 228, 240)
    (order_mine o51 m2)
    (order_port o51 p1)
    (= (load_after_time o51) 228)
    (= (unload_before_time o51) 240)
    (not-delivered o51)

    ;; consist c51
    (= (current_time c51) 0)
    (available c51)
    (empty c51)
    (at c51 y)

    (compat c51 o51)

    ;; order o52 ('m2', 'p1', 240, 252)
    (order_mine o52 m2)
    (order_port o52 p1)
    (= (load_after_time o52) 240)
    (= (unload_before_time o52) 252)
    (not-delivered o52)

    ;; consist c52
    (= (current_time c52) 0)
    (available c52)
    (empty c52)
    (at c52 y)

    (compat c52 o52)

    ;; order o53 ('m2', 'p1', 252, 264)
    (order_mine o53 m2)
    (order_port o53 p1)
    (= (load_after_time o53) 252)
    (= (unload_before_time o53) 264)
    (not-delivered o53)

    ;; consist c53
    (= (current_time c53) 0)
    (available c53)
    (empty c53)
    (at c53 y)

    (compat c53 o53)

    ;; order o54 ('m2', 'p1', 264, 276)
    (order_mine o54 m2)
    (order_port o54 p1)
    (= (load_after_time o54) 264)
    (= (unload_before_time o54) 276)
    (not-delivered o54)

    ;; consist c54
    (= (current_time c54) 0)
    (available c54)
    (empty c54)
    (at c54 y)

    (compat c54 o54)

    ;; order o55 ('m2', 'p1', 276, 288)
    (order_mine o55 m2)
    (order_port o55 p1)
    (= (load_after_time o55) 276)
    (= (unload_before_time o55) 288)
    (not-delivered o55)

    ;; consist c55
    (= (current_time c55) 0)
    (available c55)
    (empty c55)
    (at c55 y)

    (compat c55 o55)

    ;; order o56 ('m2', 'p1', 288, 300)
    (order_mine o56 m2)
    (order_port o56 p1)
    (= (load_after_time o56) 288)
    (= (unload_before_time o56) 300)
    (not-delivered o56)

    ;; consist c56
    (= (current_time c56) 0)
    (available c56)
    (empty c56)
    (at c56 y)

    (compat c56 o56)

    ;; order o57 ('m2', 'p1', 300, 312)
    (order_mine o57 m2)
    (order_port o57 p1)
    (= (load_after_time o57) 300)
    (= (unload_before_time o57) 312)
    (not-delivered o57)

    ;; consist c57
    (= (current_time c57) 0)
    (available c57)
    (empty c57)
    (at c57 y)

    (compat c57 o57)

    ;; order o58 ('m2', 'p1', 312, 324)
    (order_mine o58 m2)
    (order_port o58 p1)
    (= (load_after_time o58) 312)
    (= (unload_before_time o58) 324)
    (not-delivered o58)

    ;; consist c58
    (= (current_time c58) 0)
    (available c58)
    (empty c58)
    (at c58 y)

    (compat c58 o58)

    ;; order o59 ('m2', 'p1', 324, 336)
    (order_mine o59 m2)
    (order_port o59 p1)
    (= (load_after_time o59) 324)
    (= (unload_before_time o59) 336)
    (not-delivered o59)

    ;; consist c59
    (= (current_time c59) 0)
    (available c59)
    (empty c59)
    (at c59 y)

    (compat c59 o59)

    ;; order o60 ('m2', 'p1', 336, 348)
    (order_mine o60 m2)
    (order_port o60 p1)
    (= (load_after_time o60) 336)
    (= (unload_before_time o60) 348)
    (not-delivered o60)

    ;; consist c60
    (= (current_time c60) 0)
    (available c60)
    (empty c60)
    (at c60 y)

    (compat c60 o60)

    ;; order o61 ('m2', 'p1', 348, 360)
    (order_mine o61 m2)
    (order_port o61 p1)
    (= (load_after_time o61) 348)
    (= (unload_before_time o61) 360)
    (not-delivered o61)

    ;; consist c61
    (= (current_time c61) 0)
    (available c61)
    (empty c61)
    (at c61 y)

    (compat c61 o61)

    ;; order o62 ('m2', 'p1', 360, 372)
    (order_mine o62 m2)
    (order_port o62 p1)
    (= (load_after_time o62) 360)
    (= (unload_before_time o62) 372)
    (not-delivered o62)

    ;; consist c62
    (= (current_time c62) 0)
    (available c62)
    (empty c62)
    (at c62 y)

    (compat c62 o62)

    ;; order o63 ('m2', 'p1', 372, 384)
    (order_mine o63 m2)
    (order_port o63 p1)
    (= (load_after_time o63) 372)
    (= (unload_before_time o63) 384)
    (not-delivered o63)

    ;; consist c63
    (= (current_time c63) 0)
    (available c63)
    (empty c63)
    (at c63 y)

    (compat c63 o63)

)
(:goal
(and
  (delivered o0)
  (at c0 y)
  (delivered o1)
  (at c1 y)
  (delivered o2)
  (at c2 y)
  (delivered o3)
  (at c3 y)
  (delivered o4)
  (at c4 y)
  (delivered o5)
  (at c5 y)
  (delivered o6)
  (at c6 y)
  (delivered o7)
  (at c7 y)
  (delivered o8)
  (at c8 y)
  (delivered o9)
  (at c9 y)
  (delivered o10)
  (at c10 y)
  (delivered o11)
  (at c11 y)
  (delivered o12)
  (at c12 y)
  (delivered o13)
  (at c13 y)
  (delivered o14)
  (at c14 y)
  (delivered o15)
  (at c15 y)
  (delivered o16)
  (at c16 y)
  (delivered o17)
  (at c17 y)
  (delivered o18)
  (at c18 y)
  (delivered o19)
  (at c19 y)
  (delivered o20)
  (at c20 y)
  (delivered o21)
  (at c21 y)
  (delivered o22)
  (at c22 y)
  (delivered o23)
  (at c23 y)
  (delivered o24)
  (at c24 y)
  (delivered o25)
  (at c25 y)
  (delivered o26)
  (at c26 y)
  (delivered o27)
  (at c27 y)
  (delivered o28)
  (at c28 y)
  (delivered o29)
  (at c29 y)
  (delivered o30)
  (at c30 y)
  (delivered o31)
  (at c31 y)
  (delivered o32)
  (at c32 y)
  (delivered o33)
  (at c33 y)
  (delivered o34)
  (at c34 y)
  (delivered o35)
  (at c35 y)
  (delivered o36)
  (at c36 y)
  (delivered o37)
  (at c37 y)
  (delivered o38)
  (at c38 y)
  (delivered o39)
  (at c39 y)
  (delivered o40)
  (at c40 y)
  (delivered o41)
  (at c41 y)
  (delivered o42)
  (at c42 y)
  (delivered o43)
  (at c43 y)
  (delivered o44)
  (at c44 y)
  (delivered o45)
  (at c45 y)
  (delivered o46)
  (at c46 y)
  (delivered o47)
  (at c47 y)
  (delivered o48)
  (at c48 y)
  (delivered o49)
  (at c49 y)
  (delivered o50)
  (at c50 y)
  (delivered o51)
  (at c51 y)
  (delivered o52)
  (at c52 y)
  (delivered o53)
  (at c53 y)
  (delivered o54)
  (at c54 y)
  (delivered o55)
  (at c55 y)
  (delivered o56)
  (at c56 y)
  (delivered o57)
  (at c57 y)
  (delivered o58)
  (at c58 y)
  (delivered o59)
  (at c59 y)
  (delivered o60)
  (at c60 y)
  (delivered o61)
  (at c61 y)
  (delivered o62)
  (at c62 y)
  (delivered o63)
  (at c63 y)
))
(:metric minimize (total-time)))

