#include "landmark.hxx"

#include "gtest/gtest.h"

#define DIM 16
#define AREA (DIM*DIM)

#define ITERS 10000

using namespace aptk;
using namespace lml;
namespace lml {

bool lm_applies(const State& s, Landmark* lm);
void dump_lm(const STRIPS_Problem&, Landmark* lm, const State& s);

class LandmarksTest : public ::testing::Test {
public:
	STRIPS_Problem prob;
	LMDB lmdb;
	unsigned at[DIM][DIM];
	unsigned vis[DIM][DIM];
	State* s0;
	LandmarksTest(): prob(), lmdb(prob){
		std::vector<unsigned> goal;
		std::vector<unsigned> loc_mutex;
		for(int i = 0; i < DIM ; i++){
			for(int j = 0; j < DIM ; j++){
				at[i][j] = STRIPS_Problem::add_fluent(prob,
					"at-"
					+ std::to_string(i) + "-"
					+ std::to_string(j));
				vis[i][j] = STRIPS_Problem::add_fluent(
					prob,
					"visited-"
					+ std::to_string(i) + "-"
					+ std::to_string(j));
				goal.push_back(vis[i][j]);
				loc_mutex.push_back(at[i][j]);
			}
		}
		for(int i = 0; i < DIM ; i++){
			for(int j = 0; j < DIM ; j++){
				for(int di = -1 ; di <= 1; di+=2){
					if(i+di >= 0 && i+di <DIM)
						STRIPS_Problem::add_action(
							prob,
							"move-"
							+ std::to_string(i) + "-"
							+ std::to_string(j)
							+ "-to-"
							+ std::to_string(i+di) + "-"
							+ std::to_string(j),
							{at[i][j]},
							{at[i+di][j], vis[i+di][j]},
							{at[i][j]},
							{}
						);
				}
				for(int dj = -1 ; dj <= 1; dj+=2){
					if(j+dj >= 0 && j+dj <DIM)
						STRIPS_Problem::add_action(
							prob,
							"move-"
							+ std::to_string(i) + "-"
							+ std::to_string(j)
							+ "-to-"
							+ std::to_string(i) + "-"
							+ std::to_string(j+dj),
							{at[i][j]},
							{at[i][j+dj], vis[i][j+dj]},
							{at[i][j]},
							{}
						);
				}
			}
		}
		goal.push_back(at[0][0]);
		STRIPS_Problem::set_init(prob, {at[0][0], vis[0][0]});
		STRIPS_Problem::set_goal(prob, goal);
		prob.mutexes().add(loc_mutex);
		prob.make_action_tables(false);
		s0 = new State(prob);
		s0->set(prob.init());
		prob.compute_edeletes();
		lmdb.setup();
	}

	virtual ~LandmarksTest() {
	}

	void add_fact_lms(){
		for(int i = 0; i<DIM; i++)
			for(int j = 0; j<DIM; j++) {
				if(i == 0 && j == 0)
					continue;
				auto& adders = prob.actions_adding(vis[i][j]);
				std::vector<int> add_ops;
				for(auto act : adders)
					add_ops.push_back(act->index());
				auto lm = lmdb.add_lm(*s0, add_ops);
				ASSERT_TRUE(lm != NULL);
			}
	}

};

TEST_F(LandmarksTest, RC) {
	EXPECT_EQ(1, lmdb.rc(0, 1));
}

TEST_F(LandmarksTest, AddLM) {
	add_fact_lms();
	EXPECT_EQ((AREA - 1), lmdb.num_active());
}

TEST_F(LandmarksTest, Eval) {
	add_fact_lms();
	EXPECT_EQ((AREA - 1), lmdb.eval(*s0));
	EXPECT_EQ((AREA - 1), lmdb.num_active());
	EXPECT_EQ((AREA - 1), lmdb.size());

	// Ensure eval is idempotent
	for(int i =0 ; i < ITERS; i++)
		EXPECT_EQ((AREA - 1), lmdb.eval(*s0));
}

TEST_F(LandmarksTest, IncrEval) {
	add_fact_lms();
	EXPECT_EQ((AREA - 1), lmdb.eval(*s0));
	EXPECT_EQ((AREA - 1), lmdb.num_active());
	EXPECT_EQ((AREA - 1), lmdb.size());

	// Ensure incr_eval is idempotent
	for(int i = 0 ; i < ITERS; i++)
		EXPECT_EQ((AREA - 1), lmdb.incr_eval(*s0));
}

TEST_F(LandmarksTest, Discount) {
	EXPECT_EQ(1, lmdb.rc(0, 1));
	lmdb.discount_op(0, 1, 1);
	EXPECT_EQ(0, lmdb.rc(0, 1));
}

TEST_F(LandmarksTest, Undiscount) {
	lmdb.discount_op(0, 1, 1);
	EXPECT_EQ(0, lmdb.rc(0, 1));
	lmdb.undiscount_op(0, 1, 1);
	EXPECT_EQ(1, lmdb.rc(0, 1));
}


TEST_F(LandmarksTest, ApplicableActions) {
	add_fact_lms();
	std::vector<int> ops;
	lmdb.applicable_actions(*s0, ops);
	EXPECT_EQ(0, ops.size());

	// eval s0 to setup 0-rc actions
	lmdb.eval(*s0);
	for(int i = 0 ; i < ITERS; i++){
		ops.clear();
		lmdb.applicable_actions(*s0, ops);
		EXPECT_EQ(2, ops.size());
	}
}

TEST_F(LandmarksTest, Progress) {

	auto applicable_lm_set = [&](){
		std::set<Landmark*> all_lms;
		for(auto app_lm : lmdb.applicable_lms)
			all_lms.insert(app_lm);	
		return all_lms;
	};
	auto lm_set = [&](){
		auto lms = applicable_lm_set();
			for(auto watchlist : lmdb.disabled_lms) {
				for(auto disabled : watchlist){
					EXPECT_TRUE(lms.insert(disabled).second);
				}
			}
		return lms;
	};

	add_fact_lms();
	lmdb.eval(*s0);

	// op 0 shoud be 0,0 -> 1,0
	auto act = prob.actions()[0];

	ASSERT_TRUE(s0->entails(act->prec_vec()));
	State s1(*s0);
	s1.unset(act->del_vec());
	s1.set(act->add_vec());
	auto lm = lmdb.progress_lms(s1, act->index());
	EXPECT_EQ(AREA, lmdb.size());
	EXPECT_EQ((AREA - 1), lmdb.num_active());
	EXPECT_EQ((AREA - 1), lmdb.incr_eval(s1));
		
	ASSERT_TRUE(lm != NULL);
	EXPECT_TRUE(lm->applies(s1));
	EXPECT_EQ(1, lm->cost_assigned);
	EXPECT_EQ(0, lmdb.lm_cost(s1, lm));
	EXPECT_TRUE(applicable_lm_set().count(lm) == 1);
		
	EXPECT_EQ((AREA - 1), lmdb.eval(s1));
	EXPECT_TRUE(applicable_lm_set().count(lm) == 1);
	EXPECT_TRUE(lm_set().count(lm) == 1);
	
	EXPECT_EQ(AREA, lmdb.size());
	EXPECT_EQ((AREA - 1), lmdb.num_active());
	
	auto all_lms = applicable_lm_set();
	EXPECT_TRUE(applicable_lm_set().count(lm) == 1);
	EXPECT_EQ((AREA - 1), all_lms.size());
	
	all_lms = lm_set();
	EXPECT_EQ(AREA, all_lms.size());
	EXPECT_TRUE(all_lms.count(lm) == 1);
}

TEST_F(LandmarksTest, Backtrack) {
	add_fact_lms();
	auto act = prob.actions_adding(at[1][0])[0];
	State s1(prob);
	s1.set(act->add_vec()); s1.set(vis[0][0]);
	
	EXPECT_EQ((AREA - 1), lmdb.num_active());

	EXPECT_EQ(AREA-2, lmdb.eval(s1)); // setup watchlists for s1
	EXPECT_EQ(AREA-2, lmdb.num_active());

	lmdb.backtrack(*s0, act->index());

	EXPECT_EQ((AREA - 1), lmdb.num_active());
	EXPECT_EQ((AREA - 1), lmdb.incr_eval(*s0));
}


TEST_F(LandmarksTest, ProgressEvalBacktrack) {
	add_fact_lms();
	// op 0 shoud be 0,0 -> 1,0
	auto act = prob.actions()[0];
	ASSERT_TRUE(s0->entails(act->prec_vec()));
	
	State s1(*s0);
	s1.unset(act->del_vec());
	s1.set(act->add_vec());

	for(int i = 0; i < ITERS; i++){
		EXPECT_EQ((AREA - 1), lmdb.incr_eval(*s0));
		auto lm = lmdb.progress_lms(s1, act->index());
		EXPECT_EQ((AREA - 1), lmdb.num_active()) << "iteration " << i;
		EXPECT_EQ(AREA, lmdb.size()) << "iteration " << i;
		ASSERT_TRUE(lm != NULL) << "iteration " << i;

		EXPECT_EQ((AREA - 1), lmdb.incr_eval(s1)) << "iteration " << i;
		EXPECT_EQ(1, lm->cost_assigned + lmdb.lm_cost(s1, lm));
		EXPECT_EQ(1, lm->cost_assigned);

		lmdb.backtrack(*s0, act->index());
		EXPECT_EQ(1, lmdb.lm_cost(s1, lm)) << "iteration " << i;
		EXPECT_EQ((AREA - 1), lmdb.num_active()) << "iteration " << i;
		EXPECT_EQ(AREA, lmdb.size()) << "iteration " << i;
	}
}

TEST_F(LandmarksTest, Regress) {
	add_fact_lms();
	State s1(prob), s2(prob);
	s1.set(at[1][0]); s1.set(vis[1][0]); s1.set(vis[0][0]);
	s2.set(at[0][1]); s2.set(vis[0][1]); s2.set(vis[0][0]);

	State sg(prob);
	sg.set(prob.goal());

	std::vector<int> lm_ops;
	for(auto act : prob.actions_adding(at[0][0]))
		lm_ops.push_back(act->index());
	Landmark* lm = lmdb.add_lm(s1, lm_ops);
	ASSERT_TRUE(lm != NULL);
	lmdb.applicable_lms.reserve(AREA+1);
	for(int i = 0; i < ITERS; i++){
		Landmark* glm = lmdb.regress(
					     *s0,
					     { 
						     std::make_pair(0, lm),
						     std::make_pair(1, lm)
					     });
		ASSERT_TRUE(glm != NULL);
		EXPECT_TRUE(glm->applies(*s0));
		EXPECT_FALSE(glm->applies(sg));
		EXPECT_EQ(AREA, lmdb.incr_eval(s1));
		lmdb.applicable_lms.pop_back();
		lmdb.all_lms.pop_back();
	}
}


}
