"""
Generate pddl and dzn files for different order configs and graphs
"""

import pymas.test.bulkfreight as bfrs
import mako.template


def gen_model(graphfile, maxtime, freq, makofile, simple=False):
    tpl = mako.template.Template(filename=makofile)
    graph = bfrs.pypy_parse_dot(graphfile)
    mines = [n for n in graph.nodes() if n.startswith('m')]
    ports = [n for n in graph.nodes() if n.startswith('p')]
    if simple:
        orders = [(m, p, t0, t0+freq)
                  for m, p in zip(mines, ports)
                  for t0 in xrange(0, maxtime, freq)]
    else:
        orders = [(m, p, t0, t0+freq)
                  for m in mines
                  for p in ports
                  for t0 in xrange(0, maxtime, freq)]
    return tpl.render(graph=graph, orders=orders)


if __name__ == '__main__':
    import sys
    SIMPLE = len(sys.argv) > 4
    if SIMPLE:
        print >>sys.stderr, "running in simple mode"
    print gen_model(sys.argv[1],
                    int(sys.argv[2]),
                    int(sys.argv[3]),
                    "problem.mako",
                    SIMPLE)
