(define (problem bfrs_16)
(:domain bfrs)
(:objects
    p2 - location
    p1 - location
    d - location
    m1 - location
    m2 - location
    y - location
    x - location
    o0 - order
    o1 - order
    o2 - order
    o3 - order
    o4 - order
    o5 - order
    o6 - order
    o7 - order
    o8 - order
    o9 - order
    o10 - order
    o11 - order
    o12 - order
    o13 - order
    o14 - order
    o15 - order
    c0 - consist
    c1 - consist
    c2 - consist
    c3 - consist
    c4 - consist
    c5 - consist
    c6 - consist
    c7 - consist
    c8 - consist
    c9 - consist
    c10 - consist
    c11 - consist
    c12 - consist
    c13 - consist
    c14 - consist
    c15 - consist
)
(:init
  (is_yard y)
    (clear p2)
    (clear p1)
    (clear d)
    (clear m1)
    (clear m2)
    (clear y)
    (clear x)

    (conn p2 y)
    (conn y p2)
    (conn p1 y)
    (conn y p1)
    (conn d y)
    (conn y d)
    (conn d x)
    (conn x d)
    (conn d m1)
    (conn m1 d)
    (conn d m2)
    (conn m2 d)
    (conn y x)
    (conn x y)

    ;; order o0 ('m1', 'p2', 0, 12)
    (order_mine o0 m1)
    (order_port o0 p2)
    (not-delivered o0)

    ;; consist c0
    (available c0)
    (empty c0)
    (at c0 y)

    (compat c0 o0)

    ;; order o1 ('m1', 'p2', 12, 24)
    (order_mine o1 m1)
    (order_port o1 p2)
    (not-delivered o1)

    ;; consist c1
    (available c1)
    (empty c1)
    (at c1 y)

    (compat c1 o1)

    ;; order o2 ('m1', 'p2', 24, 36)
    (order_mine o2 m1)
    (order_port o2 p2)
    (not-delivered o2)

    ;; consist c2
    (available c2)
    (empty c2)
    (at c2 y)

    (compat c2 o2)

    ;; order o3 ('m1', 'p2', 36, 48)
    (order_mine o3 m1)
    (order_port o3 p2)
    (not-delivered o3)

    ;; consist c3
    (available c3)
    (empty c3)
    (at c3 y)

    (compat c3 o3)

    ;; order o4 ('m1', 'p1', 0, 12)
    (order_mine o4 m1)
    (order_port o4 p1)
    (not-delivered o4)

    ;; consist c4
    (available c4)
    (empty c4)
    (at c4 y)

    (compat c4 o4)

    ;; order o5 ('m1', 'p1', 12, 24)
    (order_mine o5 m1)
    (order_port o5 p1)
    (not-delivered o5)

    ;; consist c5
    (available c5)
    (empty c5)
    (at c5 y)

    (compat c5 o5)

    ;; order o6 ('m1', 'p1', 24, 36)
    (order_mine o6 m1)
    (order_port o6 p1)
    (not-delivered o6)

    ;; consist c6
    (available c6)
    (empty c6)
    (at c6 y)

    (compat c6 o6)

    ;; order o7 ('m1', 'p1', 36, 48)
    (order_mine o7 m1)
    (order_port o7 p1)
    (not-delivered o7)

    ;; consist c7
    (available c7)
    (empty c7)
    (at c7 y)

    (compat c7 o7)

    ;; order o8 ('m2', 'p2', 0, 12)
    (order_mine o8 m2)
    (order_port o8 p2)
    (not-delivered o8)

    ;; consist c8
    (available c8)
    (empty c8)
    (at c8 y)

    (compat c8 o8)

    ;; order o9 ('m2', 'p2', 12, 24)
    (order_mine o9 m2)
    (order_port o9 p2)
    (not-delivered o9)

    ;; consist c9
    (available c9)
    (empty c9)
    (at c9 y)

    (compat c9 o9)

    ;; order o10 ('m2', 'p2', 24, 36)
    (order_mine o10 m2)
    (order_port o10 p2)
    (not-delivered o10)

    ;; consist c10
    (available c10)
    (empty c10)
    (at c10 y)

    (compat c10 o10)

    ;; order o11 ('m2', 'p2', 36, 48)
    (order_mine o11 m2)
    (order_port o11 p2)
    (not-delivered o11)

    ;; consist c11
    (available c11)
    (empty c11)
    (at c11 y)

    (compat c11 o11)

    ;; order o12 ('m2', 'p1', 0, 12)
    (order_mine o12 m2)
    (order_port o12 p1)
    (not-delivered o12)

    ;; consist c12
    (available c12)
    (empty c12)
    (at c12 y)

    (compat c12 o12)

    ;; order o13 ('m2', 'p1', 12, 24)
    (order_mine o13 m2)
    (order_port o13 p1)
    (not-delivered o13)

    ;; consist c13
    (available c13)
    (empty c13)
    (at c13 y)

    (compat c13 o13)

    ;; order o14 ('m2', 'p1', 24, 36)
    (order_mine o14 m2)
    (order_port o14 p1)
    (not-delivered o14)

    ;; consist c14
    (available c14)
    (empty c14)
    (at c14 y)

    (compat c14 o14)

    ;; order o15 ('m2', 'p1', 36, 48)
    (order_mine o15 m2)
    (order_port o15 p1)
    (not-delivered o15)

    ;; consist c15
    (available c15)
    (empty c15)
    (at c15 y)

    (compat c15 o15)

)
(:goal
(and
  (delivered o0)
  (at c0 y)
  (delivered o1)
  (at c1 y)
  (delivered o2)
  (at c2 y)
  (delivered o3)
  (at c3 y)
  (delivered o4)
  (at c4 y)
  (delivered o5)
  (at c5 y)
  (delivered o6)
  (at c6 y)
  (delivered o7)
  (at c7 y)
  (delivered o8)
  (at c8 y)
  (delivered o9)
  (at c9 y)
  (delivered o10)
  (at c10 y)
  (delivered o11)
  (at c11 y)
  (delivered o12)
  (at c12 y)
  (delivered o13)
  (at c13 y)
  (delivered o14)
  (at c14 y)
  (delivered o15)
  (at c15 y)
))
(:metric minimize (total-time)))

