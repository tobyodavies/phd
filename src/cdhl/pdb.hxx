#include <climits>
#include <memory>
#include <unordered_map>

//#include <gtest/gtest_prod.h>

#include "types.hxx"
#include "strips_prob.hxx"
#include "strips_state.hxx"
#include "aptk/bit_set.hxx"

using aptk::STRIPS_Problem;
using aptk::State;
using aptk::Fluent_Vec;
using aptk::Bit_Set;

typedef int Action_Idx;

namespace cdhl {
const int BigM = 1000000000;



class LearningPDB {
public:
	LearningPDB(aptk::STRIPS_Problem& prob, bool trivial_expls=false, bool use_uip=false, bool strengthen=false);

	struct PathInfo {
		int min_h, max_h;
		std::pair<unsigned, unsigned> g;
	};

	struct LBCube {
		Action_Idx least_succ_op;
		unsigned lb;
		unsigned next_lb; // stores lb for nodes less than least_succ_op
		std::pair<unsigned, unsigned> min_g;
		aptk::Bit_Set state_ub;
		int num_vars;
		double activity;
		unsigned watching:29;
		bool dominated:1;
		bool locked:1;
		bool impl:1;
		int size(){ return state_ub.size(); }
	};

	struct CubeBuilder {
		LBCube e;

		CubeBuilder(LBCube e) : e(e){}
		CubeBuilder& state_ub(const Bit_Set& sub) {
			e.state_ub = sub;
			return *this;
		}
		CubeBuilder& lb(unsigned l) {
			e.lb = l;
			return *this;
		}
		CubeBuilder& lso(unsigned succ, unsigned next_lb=BigM) {
			e.least_succ_op = succ;
			e.next_lb = next_lb;
			return *this;
		}
		CubeBuilder& min_g(std::pair<unsigned, unsigned> g) {
			e.min_g = g;
			return *this;
		}
	};

	CubeBuilder builder();
	LBCube* addCube(const LBCube& e, LBCube* base=NULL);
	LBCube* addCube(const CubeBuilder& b, LBCube* base=NULL){
		return addCube(b.e, base);
	}

	void setup();

	typedef unsigned Expl;

	int incr_eval(const Bit_Set&, const PathInfo& bounds, Action_Idx);
	int incr_eval(const State& s, const PathInfo& bounds, Action_Idx op) { return incr_eval(s.fluent_set(), bounds, op); };
	int eval(const Bit_Set&, const PathInfo& bounds);
	int eval(const State& s, const PathInfo& bounds) { return eval(s.fluent_set(), bounds); };


	// best_action(s):
	// returns the cheapest & smallest op to escape all current critical Expls.
	// Or -1 if no such or exists.
	std::pair<int, Action_Idx> best_action(const State& s, const PathInfo& bounds){
		return best_action(s.fluent_set(), bounds);
	};
	std::pair<int, Action_Idx> best_action(const Bit_Set&, const PathInfo& bounds);

	// regress(State, Op)
	// Pre: Op is the cheapest op to escape all current critical Expls.
	// Post: creates a new Expl which prunes Op from being a solution to best_action
	//       until eval(s) >= Expl.lb + op.cost.
	// LBCube* regress(const Bit_Set& s, Action_Idx op);
	// LBCube* regress(const State& s, Action_Idx op) { return regress(s.fluent_set(), op); };

	// explain(s, lb)
	// Pre: no operator applicable in s escapes all critical plateaus;
	//      lb is a valid lower bound, provable in at most 1 step.
	// Post: creates a new Expl applicable in s which has an LB of at least `lb`
	// Returns: the lb actually proved for s, which may be more than `lb`.
	// LBCube* explain(const Bit_Set& s, int lb);
	// LBCube* explain(const State& s, int lb){ return explain(s.fluent_set(), lb); };


	// cycles(s, op)
	// Pre: applying op in s causes the search to cycle
	// Post: creates a new Expl applicable in s which explains why neither
	//       s nor its successor is a goal state.
	void cycles(const State& s, Action_Idx op);

	void gc(const std::vector<State*>& root, unsigned max_size, int cost_ub=BigM);


	friend class PDBTest_EvalGoal_Test;
	friend class PDBTest_Cubes_Test;
	friend class PDBTest_Regress_Test;
	friend class PDBTest_BestAction_Test;
	friend class PDBTest_BestActionNoEscape_Test;
	friend class PDBTest_UIP_Test;
	friend class PDBTest_UIP_STRENGTHEN_Test;

	struct OpOrder {
		bool operator()(Action_Idx, Action_Idx);
		STRIPS_Problem& prob;
	};
	struct CubeOrder {
		bool operator()(LBCube const*, LBCube const*);
		OpOrder& op_lt;
	};
	struct CubeSimilarityOrder {
		bool operator()(LBCube const*, LBCube const*);
		OpOrder& op_lt;
		LBCube* base;
		Action_Idx op;
		int max_lb;
	};

	OpOrder op_lt;
	CubeOrder cube_lt;

	CubeSimilarityOrder cube_similar(LBCube* base, Action_Idx after_op, int max_lb){
		return CubeSimilarityOrder({op_lt: op_lt, base:base, op: after_op, max_lb: max_lb});
	}

	LBCube* closeState(const Bit_Set& s, PathInfo bounds);

	unsigned size() const { return all_explanations.size(); };

	LBCube* best_expl(const Bit_Set& s, const PathInfo& bounds);
	LBCube* best_expl(const State& s, const PathInfo& bounds) { return best_expl(s.fluent_set(), bounds); };
	LBCube* incr_expl(const State& s, const PathInfo& bounds, LBCube* base, Action_Idx op) { return incr_expl(s.fluent_set(), bounds, base, op); };
	LBCube* incr_expl(const Bit_Set& s, const PathInfo& bounds, LBCube* base, Action_Idx);

	int compressed_vars();
	uint64_t compress_state(const Bit_Set& s);
	uint64_t compress_succ_state(const Bit_Set& s, Action_Idx, int delta_f=-1);
	uint64_t compress_delta_state(const Bit_Set& s, unsigned fluent);
	void uncompress_state(uint64_t compressed, Bit_Set& s);
	void explain_compressed_state(const Bit_Set& s, uint64_t compressed_lb, Bit_Set& expl);
	
protected:
	bool trivial_expls;
	bool use_uip;
	bool strengthen;
	
	STRIPS_Problem& prob;

	std::vector< int > fluent_var;

	std::vector< double > fluent_activity;

	std::vector< LBCube* > all_explanations;
	std::vector< LBCube* > expl_pool;
	std::vector< std::unique_ptr<LBCube[]> > expl_arenas;

	std::vector< std::vector<LBCube*> > watchers;
	std::unordered_multimap<unsigned, LBCube*> fixed_watchers;

	std::vector<Action_Idx> applicable_actions;

	std::vector<Action_Idx> ordered_actions;
	std::vector<unsigned> action_order_index;
	std::vector<unsigned> action_order_next;

	double activity_delta;

	Bit_Set max_state;

	struct VarInfo {
		int id;
		Bit_Set mask;
		Bit_Set inv_mask;
		double activity;
		unsigned begin_f, end_f;
		std::vector<unsigned> fluents;
		std::vector<unsigned>::iterator begin() { return fluents.begin();}
		std::vector<unsigned>::iterator end() { return fluents.end();}
		unsigned size() const { return end_f - begin_f; }
		template<typename CB> void map_true(const Bit_Set& s, CB cb){
			for(int f = s.min_elem(begin_f); f < end_f; f = s.min_elem(f+1))
				cb(f);
		}
		template<typename CB> void map_false(const Bit_Set& s, CB cb){
			for(int f = s.min_missing(mask, begin_f); f < end_f; f = s.min_missing(mask, f+1))
				cb(f);
		}
	};
	std::vector<VarInfo> vars;

	unsigned uip_lso;
	unsigned uip_lb;
	Bit_Set uip_state;
	std::vector<LBCube*> impl_graph;
	std::vector<LBCube*> lso_impl_stack;
	std::vector<LBCube*> lb_impl_stack;

	std::vector<unsigned> prop_queue;
	std::vector<unsigned> trail;
	std::vector<unsigned> var_heap;
	std::vector<int> decision_level;
	int current_level;
	void enqueue(unsigned f, LBCube* reason=NULL);



	int _compressed_vars;
	void reset_activity();
	bool dominates(const LBCube* e, const LBCube* dominated);
	void update_watcher(const Bit_Set&, std::vector<LBCube*>& wl, unsigned idx);

	template<class T>
	void map_watchers(const Bit_Set& s, unsigned f, T cb){
		auto& wl = watchers[f];
		// if(wl.size() > 1024 && wl.size() > (2.0*all_explanations.size()/watchers.size()))
		// 	for(int i = 0; i < wl.size(); i+=1024)
		// 		std::cerr << ".";
		for(int i = wl.size() - 1; i >= 0; i--){
			assert(i < wl.size());
			if(wl[i]->dominated || cb(wl[i])){
				update_watcher(s, wl, i);
			}
		}
	}

	template<class T>
	void map_watchers(const Bit_Set& s, T cb){
		void dump_cube(STRIPS_Problem&, const LBCube*);
		void dump_state(STRIPS_Problem&, const Bit_Set&);
		if(s.size() == vars.size()){
			auto f = fixed_watchers.equal_range(compress_state(s));
			auto dominated = f.second;
			for(auto i = f.first; i != f.second; ++i){
				if(!i->second->dominated){
					cb(i->second);
				} else {
					dominated = i;
				}
			}
			if(dominated != f.second)
				fixed_watchers.erase(dominated);
		}
		for(unsigned f = 0; f < prob.num_fluents(); f++){
			if(!s.isset(f))
				map_watchers(s, f, cb);
		}
	}

	template<class Filter>
	LBCube* best_expl(const Bit_Set& s, bool update, Filter f){
		LBCube *ret = NULL;
		map_watchers(s,
		     [&](LBCube* e){
			     if(!e->state_ub.contains(s)){
				     return update;
			     }
			     if(f(e) && (cube_lt(ret, e))){
				     if(ret != NULL) {
					     ret->dominated = dominates(e, ret);
					     if(ret->dominated)
						     e->activity += ret->activity;
				     }
				     ret = e;
			     } else if (!cube_lt(ret, e)) {
				     e->dominated = dominates(ret, e);
				     if(e->dominated)
					     ret->activity += e->activity;
			     }
			     return update;
		     });
		return ret;
	}


	bool regress_intersect(Bit_Set& s, Action_Idx op, const Bit_Set& s1);
	bool applies_after(const Bit_Set& s, const PathInfo& bounds, Action_Idx op, LBCube const * s1);
	void remove_expls(decltype(all_explanations)::iterator it0);

	LBCube* explain_lb(LBCube*);
	LBCube* explain_impossible_succ(const Bit_Set&, const PathInfo& bounds, LBCube*);
	LBCube* explain_unhelpful_succ(const Bit_Set&, const PathInfo& bounds, LBCube*);
	
	LBCube* compute_uip(const Bit_Set&, const PathInfo& bounds);
	LBCube* analyze_conflict(LBCube* base, const PathInfo& bounds);
	LBCube* analyze_final(LBCube* expl, const Bit_Set& assumps, const PathInfo& bounds);
	void backtrack(int to_level);
	void untrail();
	LBCube* propagate(const PathInfo& bounds);

	void resolve(LBCube* base, const PathInfo& bounds, const LBCube* e, unsigned var_idx);

	unsigned next_watcher(const Bit_Set& s, LBCube* e){
		return next_watcher(s, e, e->watching);
	}
	unsigned next_watcher(const Bit_Set& s, LBCube* e, unsigned watching){
		unsigned f = e->state_ub.min_missing(s, watching + 1);
		if(f == prob.num_fluents())
			return e->state_ub.min_missing(s);
		return f;
	}
	unsigned next_watcher_var(const Bit_Set& s, LBCube* e, unsigned watching){
		unsigned f = e->state_ub.min_missing(s, vars[fluent_var[watching]].end_f);
		if(f < prob.num_fluents())
			return f;
		
		f = e->state_ub.min_missing(s);
		if(fluent_var[f] == fluent_var[watching])
			return watching;
		return f;
	}

	std::pair<unsigned, unsigned> next_watchers(const Bit_Set& s, LBCube* e, unsigned watching){
		unsigned f0 = next_watcher(s, e, watching+1);
		unsigned f1 = f0==prob.num_fluents() ? f0 : next_watcher_var(s, e, f0);
		return std::make_pair(f0, f1);
	}

	bool var_fixed(int var_id);
};

void dump_state(STRIPS_Problem& prob, const Bit_Set& s);
void dump_cube(STRIPS_Problem& prob, const LearningPDB::LBCube* expl);

}
