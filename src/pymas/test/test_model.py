import pytest

import pymas
import pymas.utils
import pymas.model as m

## Domain 1

TestSit = m.situation('TestSit')


@m.action
def ts_lock(sit, res_id):
    """
    A simple mutex style resource
    """
    return 1e-10, {sit.shared(res_id): 1}


@m.action
def ts_unlock(sit, res_id):
    """
    A simple mutex style resource
    """
    return 1e-10, {sit.shared(res_id): -1}

## /Domain 1

## Domain 2

class TestSit2(m.situation('TestSit2')):
    @m.action
    def lock(sit, res_id):
        """
        A simple mutex style resource
        """
        return 1e-10, {sit.shared(res_id): 1}

    @m.action
    def unlock(sit, res_id):
        """
        A simple mutex style resource
        """
        return 1e-10, {sit.shared(res_id): -1}
## /Domain 1

@pytest.fixture(params=[TestSit, TestSit2])
def domain(request):
    cls = request.param
    sit0 = cls.s0()
    return (sit0,
            getattr(sit0, 'lock', ts_lock),
            getattr(sit0, 'unlock', ts_unlock))


def test_situation_construction(domain):
    sit, _lock, _unlock = domain
    assert sit.s0() == sit.__class__.s0()


def test_situation_legal(domain):
    sit0, lock, unlock = domain
    sit1 = sit0.do(lock(1))
    sit3 = sit1.do(lock(1))
    l1_res = sit0.shared(1)
    assert sit1.usage(l1_res) == 1
    assert sit3 is None or sit3.usage(l1_res) > l1_res.avail
    assert sit0.do(lock(1), lock(1)).usage(l1_res) > l1_res.avail
    assert sit0.do(lock(1), unlock(1), lock(1)).usage(l1_res) <= l1_res.avail


def test_shared_resources(domain):
    sit0, lock, unlock = domain
    sit1 = sit0.do(lock(1))
    assert set(lock(1).resources(sit0)) == set(lock(1).resources(sit1))


def test_action_equivalence(domain):
    _sit0, lock, unlock = domain
    for act in [lock, unlock]:
        assert act(1) == act(1)
        assert act(1) != act(2)
        assert hash(act(1)) == hash(act(1))


def test_situation_equivalence(domain):
    sit0, lock, unlock = domain
    assert sit0 == sit0.s0()
    assert hash(sit0.do(lock(1))) == hash(sit0.do(lock(1)))
    print sit0.do(lock(1)) is sit0.do(lock(1))
    assert sit0.do(lock(1)) == sit0.do(lock(1))
    assert sit0.do(lock(1)).do(lock(2)) == sit0.do(lock(1), lock(2))

def test_situation_non_equivalence(domain):
    sit0, lock, unlock = domain
    assert sit0.do(lock(1)).do(lock(2)) != sit0
    assert sit0.do(lock(1)).do(lock(2)) != sit0.do(lock(2))


def test_situation_dynamic_programming(domain):
    sit0, lock, unlock = domain
    seq = [lock(1), unlock(1), lock(2), lock(1), unlock(2)]
    sit_cache = {}

    sit_cache[sit0.do(*seq)] = 1

    sit = sit0
    for act in seq:
        sit = sit.do(act)
    assert sit in sit_cache


def test_bulkfreight():
    import bulkfreight
    sit = bulkfreight.BulkFreight.s0()
    visited = set()
    for _ in xrange(10):
        for nxt in sit.graph[sit.cur_section()]:
            if sit.do(sit.enter(nxt)) != None:
                sit = sit.do(sit.enter(nxt))
                visited.add(nxt)
    assert len(visited) > 1
