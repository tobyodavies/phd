#! /usr/bin/env python
import fd.grounding
import sys
import os
import time
import math
import contextlib

import atexit
import signal
import os

from libopset import OpSetPlanner
# MRJ: Profiler imports
#from prof import profiler_start, profiler_stop

import gurobipy as gpy


from state import PartState, Action, atom, model
from lm import JustificationGraph


GLOBAL_T0 = time.time()


def cur_runtime():
    return time.time() - GLOBAL_T0


total_seq_time = 0.0
total_seq_log_time = 0.0
total_seq_calls = 0

total_lps = 0


class DTG(object):
    def __init__(self, relaxation, mutexes):
        self.relaxation = relaxation
        self.mutexes = mutexes
        self.relevant = frozenset(a
                                  for m in mutexes
                                  for a in m)
        self.mutex_part_states = [PartState.all_from_mutex(self.task, m)
                                  for m in mutexes]
        self.edge_vars = {}
        self.action_cols = {}
        self.net_flow = {}
        goal = set(self.task.goal())
        self.goal_var = all(m & goal for m in mutexes)
        model.update()

    @property
    def actions(self):
        return self.relaxation.actions

    @property
    def task(self):
        return self.relaxation.task

    def action_col(self, action):
        if action.idx not in self.action_cols:
            constr = model.addConstr(-action.count_var <= 0)
            model.update()
            self.action_cols[action.idx] = gpy.Column([1], [constr])
        return self.action_cols[action.idx]

    def action_is_safe(self, action):
        prec = self.relevant_state(PartState(self.task, action.precs))
        return all(all(prec.entails(p) or prec.is_mutex_with(p)
                       for p in ps)
                   for ps in self.mutex_part_states)

    def edge_var(self, action, prec_state):
        prec_state = PartState(self.task,
                               prec_state.all_of & self.relevant,
                               prec_state.none_of & self.relevant)
        assert prec_state.consumed_by(action)

        if (action.idx, prec_state) not in self.edge_vars:
            if self.action_is_safe(action):
                var = action.count_var
            else:
                var = model.addVar(name="edge_%s_%s" % (action.idx, repr(prec_state)),
                                   column=self.action_col(action))
            self.edge_vars[action.idx, prec_state] = var
            model.update()
        return self.edge_vars[action.idx, prec_state]

    def relevant_state(self, ps):
        return PartState(self.relaxation.task,
                         ps.all_of & self.relevant,
                         ps.none_of & self.relevant)

    def constrained(self, ps):
        return self.relevant_state(ps) in self.net_flow

    def conserve_flow(self, ps):
        ps = self.relevant_state(ps)
        if ps in self.net_flow:
            return self.net_flow[ps]
        prods, cons = ps.producers(self.actions), ps.consumers(self.actions)
        xprods = set([(a, self.relevant_state(p))
                      for a, p0 in prods
                      for p in self.relevant_state(p0).specialize_product(self.mutex_part_states)
                      if p.maybe_legal and not p.entails(ps)])
        #print xprods - set(prods)
        min_d, max_d = ps.flow_delta_min_max(self.goal_var)
        net_flow = 0
        if prods or cons:
            net_flow = (
                gpy.quicksum([self.edge_var(a, p)
                              for a, p in xprods])
                -
                gpy.quicksum([self.edge_var(a, ps)
                              for a, _ in cons])
            )
            model.addRange(net_flow, min_d, max_d)
        self.net_flow[ps] = net_flow
        return net_flow

    def add_flow_constraints(self):
        total_flow = 0
        for ps in PartState.all_from_mutexes(self.task, self.mutexes):
            total_flow += self.conserve_flow(ps)
        if total_flow != 0:
            model.addConstr(total_flow == 0)

    def to_dot(self):
        print "digraph {"
        for node in self.node_generated:
            print hash(node), '[label="%s"]'%node, ";"
        targets = set()
        for (op, src) in self.edge_vars:
            act = self.relaxation.actions[op]
            trg = PartState(self.task,
                            src.all_of - frozenset(act.dels) | frozenset(act.adds),
                            src.none_of | frozenset(act.dels) - frozenset(act.adds))
            trg = self.relevant_state(trg)
            targets.add(trg)
            print hash(src), "->", hash(trg), '[label="%s"]'%self.task.action_signature(op), ";"
        for node in targets:
            print hash(node), '[label="%s"]'%node, ";"
        print "}"

    def debug(self):
        print "DTG", list(self.relevant)
        print "active edges:"
        for (a, p0), v in self.edge_vars.items():
            #if v.x:
                p1s = p0.consumers([self.relaxation.actions[a]])
                print '%'*10
                print p0
                print "--", self.task.action_signature(a), "-->"
                print [self.relevant_state(p) for _, p in p1s]
                print "v =", v, "(%x)" % id(v)
        print "END DTG"


class Relaxation(object):
    def __init__(self, task):
        self._lb = 0.0
        self.finish_formulate = None
        self.task = task
        self.actions = [Action(task, i)
                        for i in range(task.num_actions())]
        self.atoms = [atom(task, i)
                      for i in range(task.num_atoms())]
        self.total_actions = Action(task, -1, cost=0, name="TOTAL")
        self.mutexes = [set(task.get_mutex(i))
                        for i in range(task.num_mutexes())]

        model.update()
        model.addConstr(self.total_actions.count_var
                        ==
                        gpy.quicksum([a.count_var
                                      for a in self.actions]))
        model.update()
        model.setParam("OutputFlag", 0)
        model.setParam("LazyConstraints", 1)
        #model.setParam("Threads", 1)
        model.setParam("MIPGapAbs", 0.9)

        self.atom_mutexes = [set(i
                                 for i, m in enumerate(self.mutexes)
                                 if f in m)
                             for f in range(task.num_atoms())]
        self.marked_actions = set()

        self.best = (1e32, [])
        self.cut_queue = []
        self.lm_cut_queue = []
        self.dtgs = {}
        self.jgraph = JustificationGraph(self.actions, {}, self.task.init())
        self.unit_cost = max(a.cost for a in self.actions) == min(a.cost for a in self.actions)
        if self.unit_cost:
            for a in self.actions:
                a.set_unitcost()
            model.update()

    def set_lowbound(self, lb):
        if lb > self._lb:
            print "LB:", lb, "@ t =", cur_runtime()
            self._lb = lb

    def add_causal_lm_cuts(self):
        return PartState(self.task, self.task.goal()).add_lm(self.actions)

    def get_dtg(self, mutex_idxs):
        mutex_idxs = tuple(sorted(mutex_idxs))
        if mutex_idxs not in self.dtgs:
            dtg = DTG(self, [self.mutexes[i] for i in mutex_idxs])
            self.dtgs[mutex_idxs] = dtg
        return self.dtgs[mutex_idxs]

    def add_single_mutex_flows(self):
        # print self.mutexes
        for i, _ in enumerate(self.mutexes):
            self.get_dtg([i]).add_flow_constraints()

    def mergable(self, actions):
        actions = set(actions)
        ret = set((i, j)
                  for a in actions - self.marked_actions
                  for i in a.precs - a.prevs
                  for j in a.prevs
                  if i != j)
        self.marked_actions |= actions
        return ret

    def add_dynamic_cuts(self, *cut_generators, **kwargs):
        global total_lps
        iters = kwargs.get('iters', 10000)
        delrelax = kwargs.get('delrelax', False)
        self.total_actions.relax_count()
        for act in self.actions:
            act.relax_count()
        of = 1
        model.setParam("OutputFlag", 0)
        sol = {}
        oldsol = None
        i = 0
        while i < iters and (i == 0 or any(cutgen(sol) for cutgen in cut_generators)):
            i += 1
            model.optimize()
            total_lps += 1
            self.set_lowbound(model.getObjective().getValue())
            print ";dynamic cut", i, '/', iters

            if delrelax:
                sol = {a.idx: a.count_ge(1).x
                       for a in self.actions
                       if a.count_ge(1).x > 1e-6}
            else:
                sol = {a.idx: a.count_var.x
                       for a in self.actions
                       if a.count_ge(1).x > 1e-6}
            if sol == oldsol:
                break
            oldsol = sol
        for act in self.actions:
            act.unrelax_count()
        self.total_actions.unrelax_count()
        self.set_lowbound(model.getObjective().getValue())
        model.setParam("OutputFlag", 0)

    def add_dynamic_mutex_flows(self):
        for act in self.actions:
            act.relax_count()
        of = 1  # model.getParam("OutputFlag", 0)
        model.setParam("OutputFlag", 0)
        sol = {}
        while self.add_prevail_flows(sol):
            model.optimize()
            sol = {a
                   for a in self.actions
                   if a.count_ge(1).x > 1e-3}
        for act in self.actions:
            act.unrelax_count()
        model.setParam("OutputFlag", 0)

    def add_prevail_flows(self, sol):
        actions = [self.actions[op] for op in sol]
        count = 0
        for i, j in self.mergable(actions):
            for mi in self.atom_mutexes[i]:
                for mj in self.atom_mutexes[j]:
                    dtg = self.get_dtg([mi, mj])
                    ps = PartState(self.task, [i, j])
                    if not dtg.constrained(ps):
                        #print "merge", "x%d=%d" % (mi, i), "x%d=%d" % (mj, j)
                        dtg.conserve_flow(ps)
                        #dtg.add_flow_constraints()
                        #dtg.to_dot()
                        count += 1
        return count

    def add_opseq_cuts(self, sol):
        ops = {op: int(v + 0.9)
               for op, v in sol.items()}
        succ, plan, cut = sequence(ops)
        if not succ:
            self.add_cut(ops, cut)
        else:
            planobj = sum(self.actions[op].cost for op in plan)
            print "PLAN:", planobj
            if planobj < self.best[0]:
                self.best = (planobj, plan)
        return not succ

    def debug_atoms(self):
        for a in self.atoms:
            prod_idx = set([act.idx for act, _ in a.producers(self.actions)])
            missing_prods = set(self.task.actions_adding(min(a.all_of))) - prod_idx
            if missing_prods:
                print a, "Missing producers:", missing_prods
            req_idx = set([act.idx for act, _ in a.actions_requiring(self.actions)])
            extra_reqs = req_idx - set(self.task.actions_requiring(min(a.all_of)))
            if extra_reqs:
                print a, "Extra reqs:", extra_reqs

    def add_delete_relaxation(self, time_relaxed=False, linrelax=False):
        #self.debug_atoms()
        bintype = gpy.GRB.CONTINUOUS if linrelax else gpy.GRB.BINARY
        M = len(self.actions) + 1
        atom_vars = [(model.addVar(vtype=bintype, ub=1.0)
                      if i not in task.init()
                      else 1)
                     for i, _ in enumerate(self.atoms)]
        atom_achievers = [{op.idx: (model.addVar(vtype=bintype, ub=1.0)
                                    if i not in task.init()
                                    else 0)
                           for op, _ in a.producers(self.actions)}
                          for i, a in enumerate(self.atoms)]
        if not time_relaxed:
            atom_times = [(model.addVar()
                           if i not in task.init()
                           else 0)
                          for i in range(len(self.atoms))]
            action_times = [model.addVar()
                            for _ in self.actions]
        model.update()

        # Constraint 1 from Imai & Fukunaka 2014
        model.addConstr(gpy.quicksum(atom_vars[g] for g in task.goal()) == len(task.goal()))
        for i, v in enumerate(atom_vars):
            atom = self.atoms[i]
            for act, _ in atom.actions_requiring(self.actions):
                op = act.idx
                act_occurs = self.actions[op].count_ge(1)
                # Constraint 2
                model.addConstr(v >= act_occurs)
                if not time_relaxed:
                    # Constraint 5
                    model.addConstr(atom_times[i] <= action_times[op])
            if i not in task.init():
                # Constraint 4
                model.addConstr(gpy.quicksum(atom_achievers[i].values())
                                >=
                                v)
            for op, achv_var in atom_achievers[i].items():
                    act_occurs = self.actions[op].count_ge(1)
                    # Constraint 3
                    model.addConstr(achv_var <= act_occurs)
                    if not time_relaxed:
                        # Constraint 6
                        model.addConstr(action_times[op] + 1
                                        <= atom_times[i] + M * (1- atom_achievers[i][op]))

    def get_cut_safe(self, opset, cut):
        acts = [(self.actions[i], opset.get(i, 0))
                for i in cut]
        return (self.total_actions.count_ge_safe(task.num_layers() + 1)
                + gpy.quicksum([a.count_ge_safe(n + 1)
                                for a, n in acts]))

    def get_cut(self, opset, cut):
        acts = [(self.actions[i], opset.get(i, 0))
                for i in cut]
        if self.task.has_relaxation():
            total = 0
        else:
            total = self.total_actions.count_ge(task.num_layers() + 1)
        assert acts
        return (total
                + gpy.quicksum([a.count_ge(n + 1)
                                for a, n in acts])
                >= 1)

    def add_cut(self, opset, cut):
        con = model.addConstr(self.get_cut(opset, cut))
        #self.add_prevail_flows(opset)
        model.update()
        return model.getRow(con)

    def solve_relaxed(self):
        for ops, cut in self.cut_queue:
            self.add_cut(ops, cut)
        self.cut_queue = []
        rm = model.relax()
        rm.optimize()
        return {op: int(v.x + .9)
                for op, v in [(a.idx, rm.getVarByName("act_%d" % a.idx)) for a in self.actions]
                if v.x >= .1}

    def lm_cuts(self, sol, randomize=False):
        newcut = False
        #jgraph = JustificationGraph(self.actions, sol, self.task.init())
        for cut in self.jgraph.get_cuts(self.task.goal(), sol, randomize):
            #model.addConstr(gpy.quicksum([self.actions[op].count_var for op in cut]) >= 1)
            model.addConstr(gpy.quicksum([self.actions[op].count_ge(1) for op in cut]) >= 1)
            if sum(sol.get(op, 0) for op in cut) < 1.0 - 1e-3:
                newcut = True
        return newcut

    def add_lm_cut(self):
        self.add_causal_lm_cuts()
        cuts = set()
        sol = {}
        newcut = True
        jgraph = JustificationGraph(self.actions, sol, self.task.init())
        for act in self.actions:
            act.relax_count()
        #model.setParam("OutputFlag", 0)
        while newcut:
            newcut = False
            for cut in jgraph.get_cuts(self.task.goal(), sol):
                if cut not in cuts:
                    cuts.add(cut)
                    constr = gpy.quicksum([self.actions[op].count_ge(1) for op in cut]) >= 1
                    model.addConstr(constr)
                    if sum(sol.get(op, 0) for op in cut) < 1.0:
                        newcut = True
            model.update()
            model.optimize()
            sol = {a.idx: a.count_ge(1).x
                   for a in self.actions}
        for act in self.actions:
            act.unrelax_count()
        model.setParam("OutputFlag", 0)

    def solve(self, callback=None):
        global total_lps
        print "PLAN:", self.best
        runtime_restart = 3000.0

        if not callback:
            def defaultcb(model, where):
                global total_lps
                try:
                    runtime = 0
                    if where != gpy.GRB.callback.POLLING:
                        runtime = model.cbGet(gpy.GRB.callback.RUNTIME)
                    get_sol = None
                    lb = 0.0
                    ub = 1e32
                    if where == gpy.GRB.callback.MIPNODE:
                        lb = max([lb, model.cbGet(gpy.GRB.callback.MIPNODE_OBJBND)])
                        ub = min([ub, model.cbGet(gpy.GRB.callback.MIPNODE_OBJBST)])
                        get_sol = model.cbGetNodeRel
                        if self.best[0] < 1e9:
                            planobj, plan = self.best
                            ops_in_plan = {op: 1.0 * plan.count(op)
                                           for op in plan}
                            #print "Soln:", planobj

                            #model.cbLazy(gpy.quicksum(self.actions[op].count_ge(int(v)) for op, v in ops_in_plan.items()) == sum(ops_in_plan.values()))
                            model.cbSetSolution([act.count_var
                                                 for act in self.actions],
                                                [ops_in_plan.get(act.idx, 0)
                                                 for act in self.actions])

                    if where == gpy.GRB.callback.MIPSOL:
                        lb = max([lb, model.cbGet(gpy.GRB.callback.MIPSOL_OBJBND)])
                        ub = min([ub, model.cbGet(gpy.GRB.callback.MIPSOL_OBJBST)])
                        get_sol = model.cbGetSolution
                    if not get_sol:
                        return
                    total_lps += 1
                    self.set_lowbound(lb)
                    sol = get_sol([a.count_var
                                   for a in self.actions])
                    ops = {op: int(sol[op] + 0.9)
                           for op in range(task.num_actions())
                           if sol[op] > 0.9}
                    # jgraph = self.jgraph  # JustificationGraph(self.actions, {}, self.task.init())
                    # for cut in jgraph.get_cuts(self.task.goal(), ops):
                    #     constr = gpy.quicksum([self.actions[op].count_ge(1) for op in cut]) >= 1
                    #     model.cbLazy(constr)
                    #     self.lm_cut_queue.append(constr)
                    ops_obj = sum(self.actions[op].cost * count
                                  for op, count in ops.items())
                    if sum(ops.values()) < 1.2 * sum(sol) and ops_obj < 1.2 * lb:
                        succ, plan, cut = sequence(ops)
                        #print succ, plan, cut
                        if not succ:
                            self.cut_queue.append((ops, cut))
                            expr = self.get_cut_safe(ops, cut)
                            # vars = [(expr.getVar(i), expr.getCoeff(i))
                            #         for i in range(expr.size())]
                            # vals = get_sol([v for v, _ in vars])
                            # value = sum(val * const
                            #             for (val, (_, const)) in zip(vals, vars))
                            #print "VAL:", value
                            model.cbLazy(expr >= 1)
                        else:
                            planobj = sum(self.actions[op].cost for op in plan)
                            if planobj < self.best[0]:
                                print "PLAN:", planobj
                                self.best = (planobj, plan)
                    if runtime > runtime_restart:
                        model.terminate()

                except Exception as e:
                    import traceback as tb
                    print "EXCEPTION:", e
                    tb.print_exc()
                    raise

            callback = defaultcb
        with timing("optimization"):
            status = None
            while status not in [gpy.GRB.Status.OPTIMAL]:
                if self.best[0] < self._lb + 0.99:
                    break
                #print "SUBOPTIMAL", status
                with timing("Delayed cuts"):
                    for i, (ops, cut) in enumerate(self.cut_queue):
                        self.add_cut(ops, cut)
                    for constr in self.lm_cut_queue:
                        model.addConstr(constr)
                self.cut_queue = []
                #print self.best
                model.optimize(callback)
                status = model.getAttr("Status")
                self.set_lowbound(model.getAttr("ObjBound"))
                model.addConstr(model.getObjective() >= self._lb)
                runtime_restart *= 2

        print "*"*70
        print "*"*70
        # for d in self.dtgs.values():
        #     d.debug()
        # model.computeIIS()
        # model.write("model.ilp")
        obj = model.getObjective().getValue()
        print "Obj:", model.getObjective().getValue()
        print "*"*70
        print "*"*70
        relevant_actions = self.relevant_actions()
        opset = {a.idx: c
                 for a, c in relevant_actions.items()}
        if self.best[0] - .9 <= obj:
            print "SOLVED!"
            return {op: self.best[1].count(op) for op in self.best[1]}
        return opset

    def relevant_actions(self):
        return {self.actions[op]: self.actions[op].count
                for op in range(task.num_actions())
                if self.actions[op].count}

    def forceexit(self):
        print "LB:", self._lb
        print "UB:", self.best[0]
        print "TotalLps:", total_lps
        if total_seq_calls:
            print "AvgSeq:", total_seq_time / total_seq_calls
            print "CumSeq:", total_seq_time
            print "NumSeq:", total_seq_calls
            print "SeqGeo:", 10**(total_seq_log_time / total_seq_calls)
        if self.finish_formulate:
            nf_time = time.time() - self.finish_formulate
            print "NonFormulateTime:", time.time() - self.finish_formulate
            if total_seq_time > 0.0:
                print "PercentSeq:", 100.0 * total_seq_time / time.time()
                print "PercentNFSeq:", 100.0 * total_seq_time / nf_time
        import sys
        sys.stdout.flush()
        time.sleep(1)
        os.kill(0, 9)


def sequence(ops, n_layers=0):
    global total_seq_time
    global total_seq_log_time
    global total_seq_calls
    task.set_ops(ops)
    #print "Solve for", sum(1 for v in ops.values() if v > 0), "distinct ops, ", sum(ops.values()), "total ops"
    t0 = time.time()
    try:
        print ",".join(["startseq", str(t0 - GLOBAL_T0), str(len(ops)), str(sum(ops.values())), str(task.num_layers())])
        success = task.solve(n_layers)
    finally:
        dt = time.time() - t0
        total_seq_time += dt
        total_seq_log_time += math.log10(dt)
        total_seq_calls += 1
        print ",".join(["seqtime", str(time.time() - t0), str(len(ops)), str(sum(ops.values())), str(task.num_layers())])
    # if total_seq_calls % 10 == 0:
    #     print total_seq_time, '/', total_seq_calls, '=', total_seq_time / max(1, total_seq_calls), "| layers =", task.num_layers()
    #     print "SeqGeoMean =", 10**(total_seq_log_time / total_seq_calls)
    # print success, [task.action_signature(i) for i in task.get_plan()], ' + '.join(["[Y_{%s} \ge %d]" % (task.action_signature(i), ops.get(i, 0) + 1) for i in task.get_cut()])
    return success, task.get_plan(), task.get_cut()


@contextlib.contextmanager
def timing(label):
    t0 = time.time()
    print "start", label
    yield
    print "end", label
    print label, "took", time.time() - t0, "seconds"


def search(objtype="OPT"):
    t0 = time.time()
    relaxation = Relaxation(task)

    #atexit.register(relaxation.atexit)
    signal.signal(signal.SIGTERM, lambda *a: relaxation.forceexit())
    signal.signal(signal.SIGINT, lambda *a: relaxation.forceexit())
    #signal.signal(signal.SIGINT, lambda *a: sys.exit(1))
    if objtype=="LAMA":
        model.setObjective(
            gpy.quicksum((1 + a.cost) * a.count_var
                         for a in relaxation.actions))
    elif objtype=="UNIT":
        model.setObjective(
            gpy.quicksum(a.count_var
                         for a in relaxation.actions))
    else:
        model.setObjective(
            gpy.quicksum(a.cost * a.count_var
                         for a in relaxation.actions))

    # import IPython; IPython.embed()

    with timing("add causal lm cuts"):
        relaxation.add_causal_lm_cuts()
    with timing("formulate single mutex flows"):
        relaxation.add_single_mutex_flows()
    with timing("formulate pairwise mutex flows"):
        relaxation.add_prevail_flows(range(relaxation.task.num_actions()))
    with timing("add lm cut"):
        relaxation.add_dynamic_cuts(relaxation.lm_cuts,
                                    lambda(sol): relaxation.lm_cuts(sol, randomize=True),
                                    # relaxation.add_prevail_flows,
                                    # relaxation.add_opseq_cuts,
                                    delrelax=True)
    # with timing("formulate single mutex flows"):
    #     relaxation.add_single_mutex_flows()

    # with timing("add dynamic merge flows"):
    #     relaxation.add_dynamic_cuts(relaxation.lm_cuts,
    #                                 relaxation.add_prevail_flows,
    #                                 #relaxation.add_opseq_cuts
    #                                 )
    # with timing("formulate h+"):
    #     relaxation.add_delete_relaxation(linrelax=True)
    relaxation.finish_formulate = time.time()
    #relaxation.atexit()
    with timing("add linear opseq cuts"):
        relaxation.add_dynamic_cuts(relaxation.add_opseq_cuts,
                                    iters=1000)

    success = False
    while not success:
        with timing("choose optimal opset"):
            ops = relaxation.solve()
        print {task.action_signature(op): n for op, n in ops.items()}
        print "Total time:", time.time() - t0
        with timing("sequence opset"):
            success, plan, cut = sequence(ops)
        print "*"*5, "Cut len =", len(cut)
        if not success:
            relaxation.add_cut(ops, cut)
    plan_opset = {op: plan.count(op)
                  for op in plan}
    print "Unscheduled:", {task.action_signature(op): ops[op] - plan_opset.get(op, 0)
                           for op in ops
                           if ops[op] - plan_opset.get(op, 0)}
    return plan


task = OpSetPlanner()


def main(domain_file, problem_file, plan_file):

    fd.grounding.default(domain_file, problem_file, task)

    #MRJ: Uncomment to check what actions are being loaded
    #for i in range( 0, task.num_actions() ) :
    #	task.print_action( i )

    # MRJ: Setting planner parameters is as easy as setting the values
    # of Python object attributes

    # MRJ: log filename set
    task.log_filename = 'opset.log'

    # MRJ: plan file
    task.plan_filename = plan_file

    # MRJ: Comment line below to deactivate profiling
    #profiler_start( 'siw.prof' )

    # MRJ: We call the setup method in SIW_Planner
    task.setup()
    with timing("Add relaxed prefix"):
        task.add_relaxation()
    # actions = [(set(task.precs(op)), set(task.adds(op)), set(task.dels(op)))
    #            for op in range(task.num_actions())]
    # mutexes = [set(task.get_mutex(i))
    #            for i in range(task.num_mutexes())]
    # mutex_cover = set(i for m in mutexes for i in m) == set(range(task.num_atoms()))
    # #assert mutex_cover
    # atom_mutexes = [set(i
    #                     for (i, m) in enumerate(mutexes)
    #                     if f in m)
    #                 for f in range(task.num_atoms())]
    # import IPython
    # IPython.embed()
    # t0 = time.time()
    # search("LAMA")
    # print time.time() - t0, "LAMA opt time"
    t0 = time.time()
    total_cost = 0
    with open(plan_file, "w") as f:
        for i, action in enumerate(search()):
            total_cost += task.cost(action)
            print i + 1, "-", task.action_signature(action), "(cost=%d)"%task.cost(action)
            f.write(task.action_signature(action) + "\n")
    print "; total_cost = ", total_cost
    print time.time() - t0, "solve time"
    print "total_seq_time", total_seq_time
    print "avg_seq_time", total_seq_time / total_seq_calls
    #MRJ: Comment lines below to deactivate profile
    #profiler_stop()

    #rv = os.system( 'google-pprof --pdf libsiw.so siw.prof > siw.pdf' )
    #if rv != 0 :
    #	print >> sys.stderr, "An error occurred while translating google-perftools profiling information into valgrind format"


def debug() :
	main( "/home/bowman/Sandboxes/Fast-Downward/benchmarks/miconic-simpleadl/domain.pddl",
		"/home/bowman/Sandboxes/Fast-Downward/benchmarks/miconic-simpleadl/s3-0.pddl" )

if __name__ == "__main__":
	main( sys.argv[1], sys.argv[2], sys.argv[3] )

