import math
import networkx as nx
import itertools as it
import time

import pymas
import pymas.test.bulkfreight as bfrs

bfrs.BulkFreight.orders = ${orders}
bfrs.BulkFreight.graph = nx.from_edgelist(${graph.edges()})

start_t = time.time()
init = list(bfrs.solve(nfrags=${len(orders)}/2+1))
selector = pymas.FragSelector(init, bfrs.solve)
print "#frags:", len(selector.fragments)
initial_cost, selection = selector.heuristic_select()
print "Delivered:", len(selection)
print "Time:", time.time() - start_t
exes = [{sit.time(): act
         for (act, sit) in frag.sit.history}
        for frag in selection]
jexe = [(t, [exes[i].get(t) for i in xrange(len(selection))])
        for t in xrange(max(max(ex) for ex in exes))]
print "\n".join(str(e) for e in jexe)
