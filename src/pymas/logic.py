"""
Provides tools for unifying python primative types (sequences, dicts, numbers)
"""

from contextlib import contextmanager
from collections import namedtuple
from decorator import decorator

from utils import LinkedList


class Var(object):
    pass


def deref(thing, bindings=()):
    lookup = dict(bindings)

    def deref_seq(things):
        cls = things.__class__
        return cls(_deref(thing)
                   for thing in things)

    def deref_dict(things):
        cls = things.__class__
        return cls((_deref(k), deref(v))
                   for (k, v) in things.iteritems)

    def _deref(thing):
        while thing in lookup:
            thing = lookup[thing]
        if hasattr(thing, 'iteritems'):
            return deref_dict(thing)
        if hasattr(thing, '__iter__'):
            return deref_seq(thing)
        return thing

    return _deref(thing)


def ground(thing):
    if isinstance(thing, Var):
        return False
    if hasattr(thing, 'iteritems'):
        return all(ground(k) and ground(v) for (k, v) in thing.iteritems())
    if hasattr(thing, '__iter__'):
        return all(ground(subthing) for subthing in thing)
    return True


def _unify(left, right, bindings=None):
    bindings = bindings or LinkedList.nil()
    if left == right:
        yield bindings
    elif isinstance(right, Var):
        for bind in unify_var(right, left, bindings):
            yield bind
    elif isinstance(left, Var):
        for bind in unify_var(left, right, bindings):
            yield bind
    elif hasattr(left, 'iteritems'):
        for bind in unify_dict(left, right, bindings):
            yield bind
    elif hasattr(left, '__iter__'):
        for bind in unify_seq(left, right, bindings):
            yield bind


def unify_var(var, right, bindings):
    lookup = dict(bindings)
    if var in lookup:
        for bind in _unify(lookup[var], right, bindings):
            yield bind
    else:
        yield bindings.cons((var, right))


def unify_seq(left, right, bindings):
    if len(left) == len(right):
        if (not hasattr(left, '__getitem__') and
                not hasattr(right, '__getitem__')):
            for bind in unify_dict({el: True
                                    for el in left},
                                    {el: True
                                     for el in right},
                                     bindings):
                yield bind
        elif hasattr(left, '__iter__') and hasattr(right, '__iter__'):
            for bind in _unify(left[0], right[0], bindings):
                for bind2 in _unify(left[1:], right[1:], bind):
                    yield bind2


def unify_dict(left, right, bindings):
    lookup = dict(bindings)
    if left == right:
        yield bindings
    if len(left) == len(right):
        for (k, v) in left.iteritems():
            for (k2, v2) in right.iteritems():
                for bind in _unify((k, v), (k2, v2), bindings):
                    for bind2 in unify_dict({k_: left[k_]
                                             for k_ in left if k_ != k},
                                             {k_: right[k_]
                                              for k_ in right if k_ != k2},
                                              bind):
                        yield bind2


def unify(left, right, bindings=None):
    for bind in _unify(left, right, bindings):
        lookup = dict(bind)
        yield {var: deref(val, lookup)
               for var, val in bind}



def unifiable(left, right, bindings=()):
    for _ in unify(left, right, bindings):
        return True
    return False


def term(func):
    tpl = namedtuple(func.func_name, func.func_code.co_varnames)

    class Term(tpl):
        def sub(self, bindings):
            return Term(deref(self, bindings))

        def eval(self, bindings=()):
            groundterm = self
            if bindings:
                groundterm = self.sub(bindings)
            return func(*groundterm)

    def ctor(_f, *args):
        return Term(*args)
    return decorator(ctor, func)
